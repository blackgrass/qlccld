﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Business
{
    public class DAL
    {
        private static string _connString;
        public static string ConnString
        {
            get
            {

                if (string.IsNullOrEmpty(_connString) && ConfigurationManager.ConnectionStrings.Count > 0)
                {
                    for (int i = 0; i < ConfigurationManager.ConnectionStrings.Count; i++)
                    {
                        //todo: change the customDefaultConnectionstring name to what you would like your datalayer to use when you don't provide a Connectionstring in your sqlCommand
                        if (ConfigurationManager.ConnectionStrings[i].Name.Contains("CCLD"))
                        {
                            _connString = ConfigurationManager.ConnectionStrings[i].ConnectionString;
                        }
                    }
                }
                return _connString;
            }
            set { _connString = value; }
        }

        /// <summary>Returns true if your stored procedure runs and returns no errors.</summary>
        ///   <returns>The stored proc should return 1 to indicate success and 0 to indicate failure
        ///   </returns>
        /// 
        public static bool RunActionCmdReturnBool(SqlCommand cmd)
        {
            bool result = false;

            SqlParameter returnParam = new SqlParameter("@RETURN_VALUE", SqlDbType.Int);
            returnParam.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(returnParam);


            if (cmd.Connection == null)
                cmd.Connection = new SqlConnection(ConnString);
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();

            using (SqlDataReader r = cmd.ExecuteReader(CommandBehavior.CloseConnection))
            {

                if (returnParam.Value == DBNull.Value)
                    result = false;
                else if (returnParam.Value.ToString() == "1")
                    result = true;
                else if (returnParam.Value != DBNull.Value)
                    bool.TryParse(returnParam.Value.ToString(), out result);
                r.Close();
            }

            cmd.Dispose();
            return result;
        }

        ///// <summary>
        ///// assumes that one of the parameterDirections is output or inputoutput.  If there are more than one, it returns the first one.  If none, it returns 0.
        ///// </summary>
        ///// <param name="cmd"></param>
        //public static int RunCmdReturn_int(SqlCommand cmd)
        //{
        //    int result = 0;
        //    SqlParameter returnParam = GetOutputParameter(cmd.Parameters);

        //    if (cmd.Connection == null)
        //        cmd.Connection = new SqlConnection(ConnString);
        //    if (cmd.Connection.State == ConnectionState.Closed)
        //        cmd.Connection.Open();

        //    int i = cmd.ExecuteNonQuery();
        //    if (returnParam != null)
        //        result = (returnParam.Value == DBNull.Value) ? 0 : (int)returnParam.Value;


        //    cmd.Dispose();

        //    return result;
        //}


        private static SqlParameter GetOutputParameter(SqlParameterCollection sqlParameters)
        {
            SqlParameter returnParam = null;
            SqlParameter inoutParam = null;
            foreach (SqlParameter p in sqlParameters)
            {
                if (p.Direction == ParameterDirection.Output)
                    returnParam = p;
                if (p.Direction == ParameterDirection.InputOutput)
                    inoutParam = p;
            }
            //if the return parameter is not defined but there is an inputoutput parameter present, 
            if (returnParam == null && inoutParam != null)
                returnParam = inoutParam;

            return returnParam;
        }


        public static void RunActionCmd(SqlCommand cmd)
        {
            if (cmd.Connection == null)
                cmd.Connection = new SqlConnection(ConnString);
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();
            cmd.ExecuteNonQuery();

            cmd.Dispose();
        }

        public static DataSet RunCMDGetDataSet(SqlCommand cmd)
        {
            DataSet result = new DataSet();
            if (cmd.Connection == null)
                cmd.Connection = new SqlConnection(ConnString);
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(result);
            da = null;

            cmd.Dispose();
            return result;
        }


        public static string RunCMDGetString(SqlCommand cmd)
        {
            if (cmd.Connection == null)
                cmd.Connection = new SqlConnection(ConnString);
            if (cmd.Connection.State == ConnectionState.Closed)
                cmd.Connection.Open();
            if (cmd.ExecuteScalar() == null)
                return "";
            string result = cmd.ExecuteScalar().ToString();

            cmd.Dispose();
            return result;
        }


        /// <summary>
        /// hàm chạy store procedure
        /// </summary>
        /// pProcedureName : tên store
        /// pParaIn: danh sách các tham số truyền vào cho store     

        public static DataTable ExecProcedure(string pProcedureName, List<DataParameter> pParaIn)
        {
            try
            {

                DataTable dtb = new DataTable();

                SqlCommand cmd = new SqlCommand();
                if (cmd.Connection == null)
                    cmd.Connection = new SqlConnection(ConnString);
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure; // đặt kiểu chạy command là store
                cmd.CommandText = pProcedureName; // truyền tên store vào               

                SqlParameter sqlParam = null;

                //Truyền tham số ở trong danh sách cho store 
                foreach (DataParameter item in pParaIn)
                {
                    sqlParam = new SqlParameter();
                    sqlParam.ParameterName = item.Param_Name; // tên tham số
                    sqlParam.SqlDbType = (System.Data.SqlDbType)GetDBType(item.Param_DataType); // kiểu của tham số (là string, int, float, bool, ...)

                    sqlParam.Value = item.Param_Value == null ? DBNull.Value : // gán giá trị cho tham số
                        (item.Param_Value.ToString().Trim().Length == 0 ? DBNull.Value : item.Param_Value);
                    cmd.Parameters.Add(sqlParam);
                }

                //Chạy store
                SqlDataReader reader = cmd.ExecuteReader();
                dtb.Load(reader); // trả về DataTable

                cmd.Dispose();

                return dtb;
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.Message));
            }

        }

        public static DataSet ExecProcReturnDs(string pProcedureName, List<DataParameter> pParaIn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                if (cmd.Connection == null)
                    cmd.Connection = new SqlConnection(ConnString);
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure; // đặt kiểu chạy command là store
                cmd.CommandText = pProcedureName; // truyền tên store vào               

                SqlParameter sqlParam = null;

                //Truyền tham số ở trong danh sách cho store 
                foreach (DataParameter item in pParaIn)
                {
                    sqlParam = new SqlParameter();
                    sqlParam.ParameterName = item.Param_Name; // tên tham số
                    sqlParam.SqlDbType = (System.Data.SqlDbType)GetDBType(item.Param_DataType); // kiểu của tham số (là string, int, float, bool, ...)

                    sqlParam.Value = item.Param_Value == null ? DBNull.Value : // gán giá trị cho tham số
                        item.Param_Value.ToString().Trim().Length == 0 ? DBNull.Value : item.Param_Value;

                    cmd.Parameters.Add(sqlParam);
                }

                //Chạy store
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.Message));
            }

        }

        public static bool ExecFunction(string pProcedureName, List<DataParameter> pParaIn)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                if (cmd.Connection == null)
                    cmd.Connection = new SqlConnection(ConnString);
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();

                cmd.Parameters.Clear();
                cmd.CommandType = CommandType.StoredProcedure; // đặt kiểu chạy command là store
                cmd.CommandText = pProcedureName; // truyền tên store vào               

                SqlParameter sqlParam = null;

                //Truyền tham số ở trong danh sách cho store 
                foreach (DataParameter item in pParaIn)
                {
                    sqlParam = new SqlParameter();
                    sqlParam.ParameterName = item.Param_Name; // tên tham số
                    sqlParam.SqlDbType = (System.Data.SqlDbType)GetDBType(item.Param_DataType); // kiểu của tham số (là string, int, float, bool, ...)

                    sqlParam.Value = item.Param_Value == null ? DBNull.Value : // gán giá trị cho tham số
                        item.Param_Value.ToString().Trim().Length == 0 ? DBNull.Value : item.Param_Value;

                    cmd.Parameters.Add(sqlParam);
                }

                //Chạy store
                cmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private static object GetDBType(DataType dataType)
        {
            try
            {
                switch (dataType)
                {
                    case DataType.NUMBER:
                        return System.Data.SqlDbType.Int;
                    case DataType.FLOAT:
                        return System.Data.SqlDbType.Float;
                    case DataType.DOUBLE:
                        return System.Data.SqlDbType.Float;
                    case DataType.CHAR:
                        return System.Data.SqlDbType.Char;
                    case DataType.STRING:
                        return System.Data.SqlDbType.NVarChar;
                    case DataType.TEXT:
                        return System.Data.SqlDbType.Text;
                    case DataType.NTEXT:
                        return System.Data.SqlDbType.NText;
                    case DataType.BYTE:
                        return System.Data.SqlDbType.Bit;
                    case DataType.DATE:
                        return System.Data.SqlDbType.DateTime;
                    case DataType.IMAGE:
                        return System.Data.SqlDbType.Image;
                    case DataType.BINARY:
                        return System.Data.SqlDbType.VarBinary;
                    //case DataType.NULL:
                    //   return System.Data.SqlDbType.Variant;
                    default:
                        return System.Data.SqlDbType.NVarChar;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
