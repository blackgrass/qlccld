﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Business
{
    /// <summary>
    /// Tham số
    /// </summary>
    public class DataParameter
    {
        /// <summary>
        /// teen tham so
        /// </summary>
        private string _Param_Name;

        /// <summary>
        /// kieu du lieu
        /// </summary>
        private DataType _Param_DataType;

        /// <summary>
        /// gia tri
        /// </summary>
        private object _Param_Value;

        private bool _OutPutParam;

        /// <summary>
        /// kich thuoc gia tri param
        /// </summary>
        private int _Size;

        private int _paramNumber;

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="value"></param>
        public DataParameter(string paramName, object value)
        {
            _Param_Name = paramName;
            _Param_Value = value ?? DBNull.Value;

            if (value == null)
                _Param_DataType = DataType.STRING;
            else
            {
                switch (value.GetType().FullName)
                {
                    case "System.Int16":
                        _Param_DataType = DataType.NUMBER;
                        break;
                    case "System.Int32":
                        _Param_DataType = DataType.NUMBER;
                        break;
                    case "System.Int64":
                        _Param_DataType = DataType.DOUBLE;
                        break;
                    case "System.String":
                        _Param_DataType = DataType.STRING;
                        break;
                    case "System.Double":
                        _Param_DataType = DataType.DOUBLE;
                        break;
                    case "System.Boolean":
                        _Param_DataType = DataType.NUMBER;
                        break;
                    case "System.Byte[]":
                        _Param_DataType = DataType.IMAGE;
                        break;
                    case "System.Char":
                        _Param_DataType = DataType.CHAR;
                        break;
                    case "System.DateTime":
                        _Param_DataType = DataType.DATE;
                        break;
                    case "System.Object":
                        _Param_DataType = DataType.STRING;
                        break;
                    default:
                        _Param_DataType = DataType.STRING;
                        break;
                }
            }
        }

        /// <summary>
        /// contructer
        /// </summary>
        /// <param name="paramName"></param>
        /// <param name="value"></param>
        public DataParameter(string paramName, DataType value)
        {
            _Param_Name = paramName;
            _Param_DataType = value;
        }

        public DataParameter()
        {
        }

        /// <summary>
        /// Tên tham số
        /// </summary>
        public string Param_Name
        {
            get
            {
                return _Param_Name;
            }
            set
            {
                _Param_Name = value;
            }
        }

        /// <summary>
        /// Kiểm dữ liệu
        /// </summary>
        public DataType Param_DataType
        {
            get
            {
                return _Param_DataType;
            }
            set
            {
                _Param_DataType = value;
            }
        }

        /// <summary>
        /// Giá trị của tham số
        /// </summary>
        public object Param_Value
        {
            get
            {
                return _Param_Value;
            }
            set
            {
                _Param_Value = value;
            }
        }

        /// <summary>
        /// get/set size
        /// </summary>
        public int Size
        {
            get
            {
                return _Size;
            }
            set
            {
                _Size = value;
            }
        }

        /// <summary>
        /// Out put
        /// </summary>
        public bool OutPut
        {
            get
            {
                return _OutPutParam;
            }
            set
            {
                _OutPutParam = value;
            }
        }

        /// <summary>
        /// Số thứ tự của param trong store
        /// </summary>
        public int ParamNumber
        {
            get
            {
                return _paramNumber;
            }
            set
            {
                _paramNumber = value;
            }
        }
    }

    /// <summary>
    /// Kiểu dữ liệu
    /// </summary>
    public enum DataType
    {
        /// <summary>
        /// Kiểu dữ liệu dạng số int
        /// </summary>
        NUMBER = 0,

        /// <summary>
        /// Kiểu dữ liệu Float
        /// </summary>
        FLOAT = 1,

        /// <summary>
        /// Kiểu dữ liệu double
        /// </summary>
        DOUBLE = 2,

        /// <summary>
        /// Kiểu dữ liệu dạng byte
        /// </summary>
        BYTE = 3,

        /// <summary>
        /// Kiểu dữ liệu dang chuỗi
        /// </summary>
        STRING = 4,

        /// <summary>
        /// Kiểu dữ liệu dạng text
        /// </summary>
        TEXT = 5,

        /// <summary>
        /// Kiểu dữ liệu dạng ký tự
        /// </summary>
        CHAR = 6,

        /// <summary>
        /// Kiểu dữ liệu ngày
        /// </summary>
        DATE = 7,

        /// <summary>
        /// Kiểu dữ liệu N text
        /// </summary>
        NTEXT = 8,

        IMAGE = 9,

        BINARY = 10,

        NULL = 11
    }
}
