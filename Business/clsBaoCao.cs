﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace Business
{
    public class clsBaoCao
    {
        public DataSet XuatKho(string sSoPhieu)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoPhieu", sSoPhieu));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_BC_PhieuXuatKho", lstPara);
            return ds;
        }

        public DataSet NhapKho(string sSoPhieu)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoPhieu", sSoPhieu));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_BC_PhieuNhapKho", lstPara);
            return ds;
        }

        public DataSet BanGiao(string sSoBB)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoBienBan", sSoBB));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_BC_BienBanBanGiao", lstPara);
            return ds;
        }

        public DataSet DieuChuyenCungDVSD(string sSoGiaoDich)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_BC_DieuChuyenCungDVSD", lstPara);
            return ds;
        }

        public DataSet DieuChuyenKhacDVSD(string sSoGiaoDich)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_BC_DieuChuyenKhacDVSD", lstPara);
            return ds;
        }

        public DataSet ThanhLy(string sSoBB, int iHienThi) //0: theo CCLĐ; 1: theo lô
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoBienBan", sSoBB));
            lstPara.Add(new DataParameter("HienThi", iHienThi));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_BC_ThanhLyCCLD", lstPara);
            return ds;
        }

        public DataSet KiemKe(string sSoBB, int iHienThi, string sMaKho) //0: kiểm kê CCLĐ đang sử dụng; 1: kiểm kê CCLĐ trong kho 
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoBienBan", sSoBB));

            DataSet ds = new DataSet();
            if (iHienThi == 0)
                ds = DAL.ExecProcReturnDs("sp_BC_KiemKe_M1", lstPara);
            else
            {
                lstPara.Add(new DataParameter("MaKho", sMaKho));
                ds = DAL.ExecProcReturnDs("sp_BC_KiemKe_M2", lstPara);
            }
            return ds;
        }

        public DataSet SoKho(string sMaLoCCLD, string sTuNgay, string sDenNgay, string sMaKho)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("pMaLoCCLD", sMaLoCCLD));
            lstPara.Add(new DataParameter("pTuNgay", sTuNgay));
            lstPara.Add(new DataParameter("pDenNgay", sDenNgay));
            lstPara.Add(new DataParameter("pMaKho", sMaKho));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_BC_SoKho", lstPara);
            return ds;
        }
    }
}
