﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Business
{
    public class clsCCLD
    {
        public string LayIdKho(string sMaKho)
        {
            if (!string.IsNullOrEmpty(sMaKho))
                return DAL.RunCMDGetString(new SqlCommand("SELECT ID_KHO FROM DM_KHO WHERE MA_KHO = '" + sMaKho + "'"));
            return sMaKho;
        }

        #region "Tăng CCLĐ"

        public DataTable TimKiemLoCCLD(string sSoGDTu, string sSoGDDen, string sNgayChungTuTu, string sNgayChungTuDen,
                                            string sNgayTangTu, string sNgayTangDen, string sMaLo, bool bChoDuyet, bool bDuyet,
                                            bool bTuChoiDuyet, bool bHuyDuyet)
        {
            DataTable dtb = new DataTable();

            // tạo một danh sách các tham số truyền vào cho store
            // Class DataParameter lấy trên mạng, có nhiều thuộc tính nhưng chỉ quan tâm đến 2 thuộc tính chính là _Param_Name (tên) và _Param_Value (giá trị)
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoGDTu", sSoGDTu)); // thêm parameter vào danh sách, với tên trong "..." là tên tham số trong store
            lstPara.Add(new DataParameter("SoGDDen", sSoGDDen));

            if (!string.IsNullOrEmpty(sNgayChungTuTu))
                lstPara.Add(new DataParameter("NgayChungTuTu", DateTime.Parse(sNgayChungTuTu).ToString("yyyyMMdd")));

            if (!string.IsNullOrEmpty(sNgayChungTuDen))
                lstPara.Add(new DataParameter("NgayChungTuDen", DateTime.Parse(sNgayChungTuDen).ToString("yyyyMMdd")));

            if (!string.IsNullOrEmpty(sNgayTangTu))
                lstPara.Add(new DataParameter("NgayTangTu", DateTime.Parse(sNgayTangTu).ToString("yyyyMMdd")));

            if (!string.IsNullOrEmpty(sNgayTangDen))
                lstPara.Add(new DataParameter("NgayTangDen", DateTime.Parse(sNgayTangDen).ToString("yyyyMMdd")));

            lstPara.Add(new DataParameter("MaLo", sMaLo));

            if (bChoDuyet)
                lstPara.Add(new DataParameter("ChoDuyet", "1"));
            else
                lstPara.Add(new DataParameter("ChoDuyet", "0"));

            if (bDuyet)
                lstPara.Add(new DataParameter("Duyet", "1"));
            else
                lstPara.Add(new DataParameter("Duyet", "0"));

            if (bTuChoiDuyet)
                lstPara.Add(new DataParameter("TuChoiDuyet", "1"));
            else
                lstPara.Add(new DataParameter("TuChoiDuyet", "0"));

            if (bHuyDuyet)
                lstPara.Add(new DataParameter("HuyDuyet", "1"));
            else
                lstPara.Add(new DataParameter("HuyDuyet", "0"));

            dtb = DAL.ExecProcedure("sp_CC_TANG_LO_TimKiem", lstPara);
            return dtb;
        }

        public DataSet GetDataGiaoDich(string sSoGiaoDich)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_CC_TANG_LO_select", lstPara);
            return ds;
        }

        public DataTable GetThongTinLo(string sMaLo)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("MaLoCCLD", sMaLo));

            DataTable dt = new DataTable();
            dt = DAL.ExecProcedure("sp_CC_TANG_CHI_TIET_LO", lstPara);
            return dt;
        }

        public bool DeleteGiaoDich(string sSoGiaoDich)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE CC_TANG_LO WHERE ID_BD = (SELECT GD.ID_BD FROM CC_TANG_LO_GD GD WHERE GD.SO_GIAO_DICH = '" + sSoGiaoDich + "');";
                cmd.CommandText += " DELETE CC_TANG_LO_GD WHERE SO_GIAO_DICH = '" + sSoGiaoDich + "';";
                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public bool UpdateTrangThai(string sSoGiaoDich, string sTrangThai)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                //cmd.CommandText = "UPDATE CC_TANG_LO SET TRANG_THAI = '" + sTrangThai + "' WHERE SO_GIAO_DICH = '" + sSoGiaoDich + "';";
                cmd.CommandText += " UPDATE CC_TANG_LO_GD SET TRANG_THAI = '" + sTrangThai + "' WHERE SO_GIAO_DICH = '" + sSoGiaoDich + "'";
                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public bool DuyetCCLD(string sSoGiaoDich, string sTrangThai, string sMaLoCCLD, string sMaCCLD)
        {
            bool bResult = false;

            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
            lstPara.Add(new DataParameter("TrangThai", sTrangThai)); //0: chờ duyệt; 1: đã duyệt; 2: từ chối duyệt; 3: hủy duyệt
            lstPara.Add(new DataParameter("MaLoCCLD", sMaLoCCLD));
            lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));

            bResult = DAL.ExecFunction("sp_CC_TANG_Insert", lstPara);
            return bResult;
        }

        public bool HuyDuyetCCLD(string sMaLoCCLD)
        {
            bool bResult = false;

            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("MaLoCCLD", sMaLoCCLD));

            bResult = DAL.ExecFunction("sp_CC_TANG_Delete", lstPara);
            return bResult;
        }

        public bool DelLoCCLD(string sMaLoCCLD)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE CC_TANG_LO WHERE MA_LO_CCLD =  '" + sMaLoCCLD + "';";
                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public bool InsertGiaoDich(string sSoChungTu, string sNgayChungTu, string sNgayTang, string sDienGiai, string sSoGD)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoChungTu", sSoChungTu));
                lstPara.Add(new DataParameter("NgayChungTu", sNgayChungTu));
                lstPara.Add(new DataParameter("NgayTang", sNgayTang));
                lstPara.Add(new DataParameter("DienGiai", sDienGiai));
                lstPara.Add(new DataParameter("SoGiaoDich", sSoGD));

                bResult = DAL.ExecFunction("sp_CC_TANG_LO_GD_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool InsertLoCCLD(string sSoGD, string sNgayTang, string sTenLo, string sSoLuong,
                                string sDonGia, string sDonGiaDGL, string sTongTien, string sMaNguonGoc, string sMaNhomCCLD,
                                string sMaTinhTrang, string sSoThangBH, string sNgayBH, string sMaDVSD)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoGiaoDich", sSoGD));
                //lstPara.Add(new DataParameter("SoChungTu", sSoCT));
                //lstPara.Add(new DataParameter("NgayChungTu", sNgayCT));
                lstPara.Add(new DataParameter("NgayTang", sNgayTang));
                lstPara.Add(new DataParameter("TenLoCCLD", sTenLo));
                lstPara.Add(new DataParameter("SoLuong", sSoLuong));
                lstPara.Add(new DataParameter("DonGia", sDonGia));
                lstPara.Add(new DataParameter("DonGiaDGL", sDonGiaDGL));
                lstPara.Add(new DataParameter("TongTien", sTongTien));
                lstPara.Add(new DataParameter("MaNguonGoc", sMaNguonGoc));
                lstPara.Add(new DataParameter("MaNhomCCLD", sMaNhomCCLD));
                lstPara.Add(new DataParameter("MaTinhTrang", sMaTinhTrang));
                lstPara.Add(new DataParameter("SoThangBH", sSoThangBH));
                lstPara.Add(new DataParameter("NgayBH", sNgayBH));
                //lstPara.Add(new DataParameter("DienGiai", sDienGiai));
                lstPara.Add(new DataParameter("MaDVSD", sMaDVSD));

                bResult = DAL.ExecFunction("sp_CC_TANG_LO_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetSoGiaoDich(string sNamTaiChinh)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT TOP 1 SO_GD FROM HT_SO_GD WHERE NAM_TC = " + sNamTaiChinh + " ORDER BY CONVERT(NUMERIC,SO_GD) DESC";

            string sSoGD = DAL.RunCMDGetString(cmd);

            if (string.IsNullOrEmpty(sSoGD))
                sSoGD = "0";
            long lSoGD = long.Parse(sSoGD) + 1;
            return lSoGD.ToString().PadLeft(6, '0');
        }

        public bool UpdateLoCCLD(string sTenLo, string sSoLuong, string sDonGia, string sDonGiaDGL, string sTongTien, string sMaNguonGoc,
                                string sMaTinhTrang, string sSoThangBH, string sNgayBH, string sMaLoCCLD)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                //lstPara.Add(new DataParameter("SoChungTu", sSoCT));
                //lstPara.Add(new DataParameter("NgayChungTu", sNgayCT));
                //lstPara.Add(new DataParameter("NgayTang", sNgayTang));
                lstPara.Add(new DataParameter("TenLoCCLD", sTenLo));
                lstPara.Add(new DataParameter("SoLuong", sSoLuong));
                lstPara.Add(new DataParameter("DonGia", sDonGia));
                lstPara.Add(new DataParameter("DonGiaDGL", sDonGiaDGL));
                lstPara.Add(new DataParameter("TongTien", sTongTien));
                lstPara.Add(new DataParameter("MaNguonGoc", sMaNguonGoc));
                lstPara.Add(new DataParameter("MaTinhTrang", sMaTinhTrang));
                lstPara.Add(new DataParameter("SoThangBH", sSoThangBH));
                lstPara.Add(new DataParameter("NgayBH", sNgayBH));
                //lstPara.Add(new DataParameter("DienGiai", sDienGiai));
                lstPara.Add(new DataParameter("MaLoCCLD", sMaLoCCLD));

                bResult = DAL.ExecFunction("sp_CC_TANG_LO_Update", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateGiaoDich(string sSoChungTu, string sNgayChungTu, string sNgayTang, string sDienGiai, string sSoGD)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoChungTu", sSoChungTu));
                lstPara.Add(new DataParameter("NgayChungTu", sNgayChungTu));
                lstPara.Add(new DataParameter("NgayTang", sNgayTang));
                lstPara.Add(new DataParameter("DienGiai", sDienGiai));
                lstPara.Add(new DataParameter("SoGiaoDich", sSoGD));

                bResult = DAL.ExecFunction("sp_CC_TANG_LO_GD_Update", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #endregion

        #region "Nhập kho"
        public DataTable TimKiemNhapXuat(string sSoPhieuTu, string sSoPhieuDen, string sThoiGianTu, string sThoiGianDen,
                                        string sMaKho, string sMaCCLD, bool bChoDuyet, bool bDuyet, bool bTuChoiDuyet, bool bHuyDuyet, string sLoai)
        {
            DataTable dtb = new DataTable();

            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoPhieuTu", sSoPhieuTu)); // thêm parameter vào danh sách
            lstPara.Add(new DataParameter("SoPhieuDen", sSoPhieuDen));
            lstPara.Add(new DataParameter("Loai", sLoai)); //1: nhập kho, 0: xuất kho

            if (!string.IsNullOrEmpty(sThoiGianTu))
                lstPara.Add(new DataParameter("ThoiGianTu", DateTime.Parse(sThoiGianTu).ToString("yyyyMMdd")));

            if (!string.IsNullOrEmpty(sThoiGianDen))
                lstPara.Add(new DataParameter("ThoiGianDen", DateTime.Parse(sThoiGianDen).ToString("yyyyMMdd")));

            lstPara.Add(new DataParameter("MaKho", sMaKho));
            lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));

            if (bChoDuyet)
                lstPara.Add(new DataParameter("ChoDuyet", "1"));
            else
                lstPara.Add(new DataParameter("ChoDuyet", "0"));

            if (bDuyet)
                lstPara.Add(new DataParameter("Duyet", "1"));
            else
                lstPara.Add(new DataParameter("Duyet", "0"));

            if (bTuChoiDuyet)
                lstPara.Add(new DataParameter("TuChoiDuyet", "1"));
            else
                lstPara.Add(new DataParameter("TuChoiDuyet", "0"));

            if (bHuyDuyet)
                lstPara.Add(new DataParameter("HuyDuyet", "1"));
            else
                lstPara.Add(new DataParameter("HuyDuyet", "0"));

            dtb = DAL.ExecProcedure("sp_CC_NHAP_XUAT_TimKiem", lstPara);
            return dtb;
        }

        public DataTable TimKiemCCLD(bool bCCLDChuaBanGiao, bool bCCLDDangDung, string sMaDVSD, string sMaNhomCCLD, string sMaLoCCLD, string sMaCCLDDaChon)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT	CC.MA_CCLD, CC.TEN_CCLD, NH.TEN_NHOM_CCLD, ISNULL(CC.DON_GIA,CC.DON_GIA_DGL) AS DON_GIA, NH.DON_VI_TINH, 1 AS SL_THUC_TE, ISNULL(CC.DON_GIA,CC.DON_GIA_DGL) AS TONG_TIEN, DVSD.TEN_DVSDTS,  CAST(0 AS BIT) AS is_check
                                FROM	CC_TANG CC INNER JOIN DM_NHOM_CCLD NH ON CC.MA_NHOM_CCLD = NH.MA_NHOM_CCLD
		                                INNER JOIN CC_TANG_LO TL ON CC.ID_LO_CCLD = TL.ID_LO_CCLD 
                                        LEFT JOIN DM_DVSDTS DVSD ON CC.MA_DVSDTS = DVSD.MA_DVSDTS ";
            cmd.CommandText += " WHERE	CC.TT_NVU_HIEN_TAI = 1 AND CC.ID_KHO IS NULL " +
                                "       AND CC.NVU_HIEN_TAI != 7";
                               //"        AND CC.ID_CCLD NOT IN ( SELECT TOP 1 LS.ID_CCLD " +
                               //"                                FROM CC_LICH_SU LS " +
                               //"                                WHERE LS.ID_CCLD = CC.ID_CCLD AND LS.LOAI_BIEN_DONG = 7 AND LS.TRANG_THAI = 1 " +
                               //"                                ORDER BY LS.ID_LICH_SU DESC) ";
            if (!string.IsNullOrEmpty(sMaDVSD))
                cmd.CommandText += " AND CC.MA_DVSDTS =  N'%" + sMaDVSD.Replace("'", "''") + "%' ";
            if (!string.IsNullOrEmpty(sMaNhomCCLD))
                cmd.CommandText += " AND CC.MA_NHOM_CCLD =  N'%" + sMaNhomCCLD.Replace("'", "''") + "%'";
            if (!string.IsNullOrEmpty(sMaLoCCLD))
                cmd.CommandText += " AND TL.MA_LO_CCLD =  N'%" + sMaLoCCLD.Replace("'", "''") + "%'";
            if (!bCCLDDangDung)
                cmd.CommandText += " AND CC.MA_DVSDTS IS NULL ";
            if (!bCCLDChuaBanGiao)
                cmd.CommandText += " AND CC.MA_DVSDTS IS NOT NULL ";
            if (!string.IsNullOrEmpty(sMaCCLDDaChon))
                cmd.CommandText += " AND CC.MA_CCLD NOT IN (" + sMaCCLDDaChon + ")";
            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public DataSet GetPhieuNhapXuat(string sSoPhieu, string sLoai)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoPhieu", sSoPhieu));
            lstPara.Add(new DataParameter("Loai", sLoai));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_CC_NHAP_XUAT_Select", lstPara);
            return ds;
        }

        public string GetSoPhieu(string sLoai)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT TOP 1 SO_PHIEU FROM CC_NHAP_XUAT WHERE LOAI = " + sLoai + " ORDER BY CONVERT(NUMERIC,SO_PHIEU) DESC";

            string sSoPhieu = DAL.RunCMDGetString(cmd);

            if (string.IsNullOrEmpty(sSoPhieu))
                sSoPhieu = "0";
            long lSoPhieu = long.Parse(sSoPhieu) + 1;
            return lSoPhieu.ToString().PadLeft(4, '0');
        }

        public bool DelCCLD(string sMaCCLD)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE CC_NHAP_XUAT_CT WHERE ID_CCLD = (SELECT CC.ID_CCLD FROM CC_TANG CC WHERE CC.MA_CCLD ='" + sMaCCLD + "')";
                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public bool InsertNhapXuat(string sNgayNhap, string sSoPhieu, string sSoChungTu, string sNgayChungTu, string sTenNguoiGiao, string sDiaChiNguoiGiao,
                                    string sCanCu, string sSoQD, string sNguoiQD, string sNgayQD, string sMaKho, string sDienGiai, string sGhiChu,
                                    string sThuTruong, string sKeToan, string sNguoiGiao, string sThuKho, string sTruongPhongKeToan, string sLoai, decimal lTongTien)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("NgayNhap", sNgayNhap));
                lstPara.Add(new DataParameter("SoPhieu", sSoPhieu));
                lstPara.Add(new DataParameter("SoChungTu", sSoChungTu));
                lstPara.Add(new DataParameter("NgayChungTu", sNgayChungTu));
                lstPara.Add(new DataParameter("TenNguoiGiao", sTenNguoiGiao));
                lstPara.Add(new DataParameter("DiaChiNguoiGiao", sDiaChiNguoiGiao));
                lstPara.Add(new DataParameter("CanCu", sCanCu));
                lstPara.Add(new DataParameter("SoQD", sSoQD));
                lstPara.Add(new DataParameter("NguoiQD", sNguoiQD));
                lstPara.Add(new DataParameter("NgayQD", sNgayQD));
                lstPara.Add(new DataParameter("MaKho", sMaKho));
                lstPara.Add(new DataParameter("DienGiai", sDienGiai));
                lstPara.Add(new DataParameter("GhiChu", sGhiChu));
                lstPara.Add(new DataParameter("ThuTruong", sThuTruong));
                lstPara.Add(new DataParameter("KeToan", sKeToan));
                lstPara.Add(new DataParameter("NguoiGiao", sNguoiGiao));
                lstPara.Add(new DataParameter("ThuKho", sThuKho));
                lstPara.Add(new DataParameter("TruongPhongKeToan", sTruongPhongKeToan));
                lstPara.Add(new DataParameter("Loai", sLoai)); //1: nhập kho; 0: xuất kho
                lstPara.Add(new DataParameter("TongTien", lTongTien));
                bResult = DAL.ExecFunction("sp_CC_NHAP_XUAT_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool UpdateNhapXuat(string sNgayNhap, string sSoPhieu, string sSoChungTu, string sNgayChungTu, string sTenNguoiGiao, string sDiaChiNguoiGiao,
                                    string sCanCu, string sSoQD, string sNguoiQD, string sNgayQD, string sMaKho, string sDienGiai, string sGhiChu,
                                    string sThuTruong, string sKeToan, string sNguoiGiao, string sThuKho, string sTruongPhongKeToan, string sLoai, decimal lTongTien)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("NgayNhap", sNgayNhap));
                lstPara.Add(new DataParameter("SoPhieu", sSoPhieu));
                lstPara.Add(new DataParameter("SoChungTu", sSoChungTu));
                lstPara.Add(new DataParameter("NgayChungTu", sNgayChungTu));
                lstPara.Add(new DataParameter("TenNguoiGiao", sTenNguoiGiao));
                lstPara.Add(new DataParameter("DiaChiNguoiGiao", sDiaChiNguoiGiao));
                lstPara.Add(new DataParameter("CanCu", sCanCu));
                lstPara.Add(new DataParameter("SoQD", sSoQD));
                lstPara.Add(new DataParameter("NguoiQD", sNguoiQD));
                lstPara.Add(new DataParameter("NgayQD", sNgayQD));
                lstPara.Add(new DataParameter("MaKho", sMaKho));
                lstPara.Add(new DataParameter("DienGiai", sDienGiai));
                lstPara.Add(new DataParameter("GhiChu", sGhiChu));
                lstPara.Add(new DataParameter("ThuTruong", sThuTruong));
                lstPara.Add(new DataParameter("KeToan", sKeToan));
                lstPara.Add(new DataParameter("NguoiGiao", sNguoiGiao));
                lstPara.Add(new DataParameter("ThuKho", sThuKho));
                lstPara.Add(new DataParameter("TruongPhongKeToan", sTruongPhongKeToan));
                lstPara.Add(new DataParameter("Loai", sLoai)); //1: nhập kho; 0: xuất kho
                lstPara.Add(new DataParameter("TongTien", lTongTien));

                bResult = DAL.ExecFunction("sp_CC_NHAP_XUAT_Update", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable CheckMaCCLD(string sMaCCLD, string sLoai)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT CT.ID_NHAP_XUAT_CT FROM CC_NHAP_XUAT_CT CT INNER JOIN CC_TANG CC ON CT.ID_CCLD = CC.ID_CCLD " +
                              " INNER JOIN CC_NHAP_XUAT NX ON NX.ID_BD = CT.ID_BD WHERE CC.ID_KHO IS NOT NULL AND CC.MA_CCLD = '" + sMaCCLD + "' AND NX.LOAI =" + sLoai;

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public bool InsertNhapXuatCT(string sSoPhieu, string sMaCCLD, string sSLThucTe, string sMaKho, string sMaLyDoNhap, decimal TongTien, string sLoai)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoPhieu", sSoPhieu));
                lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));
                lstPara.Add(new DataParameter("MaKho", sMaKho));
                lstPara.Add(new DataParameter("SLThucTe", sSLThucTe));
                lstPara.Add(new DataParameter("MaLyDoNhap", sMaLyDoNhap));
                lstPara.Add(new DataParameter("TongTien", TongTien));
                lstPara.Add(new DataParameter("Loai", sLoai));

                bResult = DAL.ExecFunction("sp_CC_NHAP_XUAT_CT_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public bool UpdateNhapXuatCT(string sMaLyDoNhap, string sSoPhieu, string sMaCCLD, decimal TongTien, string sLoai)
        //{
        //    try
        //    {
        //        bool bResult = false;

        //        List<DataParameter> lstPara = new List<DataParameter>();

        //        //lstPara.Add(new DataParameter("NgayNhapXuat", sNgayNhapXuat));
        //        lstPara.Add(new DataParameter("MaLyDoNhap", sMaLyDoNhap));
        //        lstPara.Add(new DataParameter("SoPhieu", sSoPhieu));
        //        lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));
        //        lstPara.Add(new DataParameter("TongTien", TongTien));
        //        lstPara.Add(new DataParameter("Loai", sLoai));

        //        bResult = DAL.ExecFunction("sp_CC_NHAP_XUAT_CT_Update", lstPara);
        //        return bResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool DeleteNhapXuat(string sSoPhieu, string sLoai)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE CC_NHAP_XUAT WHERE SO_PHIEU = @SoPhieu AND LOAI = @Loai";

                cmd.Parameters.Add(new SqlParameter("@SoPhieu", sSoPhieu));
                cmd.Parameters.Add(new SqlParameter("@Loai", sLoai));

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }
            return true;
        }

        public bool DelNhapXuatCT(string sSoPhieu, string sLoai, string sMaCCLD)
        { //del từng cclđ trong phiếu nhập kho
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoPhieu", sSoPhieu));
                lstPara.Add(new DataParameter("Loai", sLoai)); //1: nhập kho; 0: xuất kho
                lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));

                bResult = DAL.ExecFunction("sp_CC_NHAP_XUAT_Delete", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public bool DuyetNhapXuat(string sSoPhieu, string sTrangThai, string sLoai)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoPhieu", sSoPhieu));
                lstPara.Add(new DataParameter("Loai", sLoai)); //1: nhập kho; 0: xuất kho
                lstPara.Add(new DataParameter("TrangThai", sTrangThai)); //0: chờ duyệt, 1:duyệt, 2: từ chối duyệt, 3: hủy duyệt

                bResult = DAL.ExecFunction("sp_CC_NHAP_XUAT_DUYET", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        #endregion

        #region Xuất kho
        public DataTable LayCCLDTrongKho(string sMaNhomCCLD, string sMaLoCCLD, string sMaCCLDDaChon, string sMaKho)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT	CC.MA_CCLD, CC.TEN_CCLD, NH.TEN_NHOM_CCLD, ISNULL(CC.DON_GIA,CC.DON_GIA_DGL) AS DON_GIA, NH.DON_VI_TINH,
		                                CC.SO_THANG_BH, CONVERT(NVARCHAR(10),CAST (CC.NGAY_TANG AS DATETIME),103) AS NGAY_TANG,
		                                 1 AS SL_THUC_TE, ISNULL(CC.DON_GIA,CC.DON_GIA_DGL) AS TONG_TIEN, CAST(0 AS BIT) AS is_check
                                FROM	CC_TANG CC INNER JOIN DM_NHOM_CCLD NH ON CC.MA_NHOM_CCLD = NH.MA_NHOM_CCLD
                                        INNER JOIN CC_TANG_LO TL ON CC.ID_LO_CCLD = TL.ID_LO_CCLD 
                                        LEFT JOIN DM_KHO KH ON CC.ID_KHO = KH.ID_KHO";
            cmd.CommandText += " WHERE	CC.TT_NVU_HIEN_TAI = 1 AND CC.NVU_HIEN_TAI != 7 AND KH.MA_KHO = " + sMaKho;

            if (!string.IsNullOrEmpty(sMaNhomCCLD))
                cmd.CommandText += " AND CC.MA_NHOM_CCLD =  N'%" + sMaNhomCCLD.Replace("'", "''") + "%'";
            if (!string.IsNullOrEmpty(sMaLoCCLD))
                cmd.CommandText += " AND TL.MA_LO_CCLD =  N'%" + sMaLoCCLD.Replace("'", "''") + "%'";
            if (!string.IsNullOrEmpty(sMaCCLDDaChon))
                cmd.CommandText += " AND CC.MA_CCLD NOT IN (" + sMaCCLDDaChon + ")";
            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }
        #endregion

        #region "Bàn giao"

        public string GetIdDaiDienBanGiao() // hàm lấy ra ID_BAN_GIAO_HD lớn nhất, sau đó + 1
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT TOP 1 ID_BAN_GIAO_HD FROM CC_BAN_GIAO_HD ORDER BY ID_BAN_GIAO_HD DESC";

            string ID = DAL.RunCMDGetString(cmd);

            if (string.IsNullOrEmpty(ID))
                ID = "0";
            int result = int.Parse(ID) + 1;

            return result.ToString();
        }

        public DataTable SearchBanGiao(string sSoBBTu, string sSoBBDen, string sThoiGianTu, string sThoiGianDen,
                                       string sMaLoCCLD, string sMaDVSDTS, string sMaCCLD, bool bChoDuyet, bool bDuyet,
                                            bool bTuChoiDuyet, bool bHuyDuyet)
        {
            DataTable dtb = new DataTable();

            // tạo một danh sách các tham số truyền vào cho store
            // Class DataParameter lấy trên mạng, có nhiều thuộc tính nhưng chỉ quan tâm đến 2 thuộc tính chính là _Param_Name (tên) và _Param_Value (giá trị)
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoBBTu", sSoBBTu));
            lstPara.Add(new DataParameter("SoBBDen", sSoBBDen));

            if (!string.IsNullOrEmpty(sThoiGianTu))
                lstPara.Add(new DataParameter("ThoiGianTu", DateTime.Parse(sThoiGianTu).ToString("yyyyMMdd")));

            if (!string.IsNullOrEmpty(sThoiGianDen))
                lstPara.Add(new DataParameter("ThoiGianDen", DateTime.Parse(sThoiGianDen).ToString("yyyyMMdd")));

            lstPara.Add(new DataParameter("MaLoCCLD", sMaLoCCLD));
            lstPara.Add(new DataParameter("MaDVSDTS", sMaDVSDTS));
            lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));
            if (bChoDuyet)
                lstPara.Add(new DataParameter("ChoDuyet", "1"));
            else
                lstPara.Add(new DataParameter("ChoDuyet", "0"));

            if (bDuyet)
                lstPara.Add(new DataParameter("Duyet", "1"));
            else
                lstPara.Add(new DataParameter("Duyet", "0"));

            if (bTuChoiDuyet)
                lstPara.Add(new DataParameter("TuChoiDuyet", "1"));
            else
                lstPara.Add(new DataParameter("TuChoiDuyet", "0"));

            if (bHuyDuyet)
                lstPara.Add(new DataParameter("HuyDuyet", "1"));
            else
                lstPara.Add(new DataParameter("HuyDuyet", "0"));

            dtb = DAL.ExecProcedure("sp_CC_BAN_GIAO_TimKiem", lstPara);
            return dtb;
        }

        public DataSet GetBBBanGiao(string sSoBB)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoBB", sSoBB));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_CC_BAN_GIAO_Select", lstPara);
            return ds;
        }

        public string GetSoBBBanGiao()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT TOP 1 SO_BIEN_BAN FROM CC_BAN_GIAO ORDER BY CONVERT(NUMERIC,SO_BIEN_BAN) DESC";

            string sSoPhieu = DAL.RunCMDGetString(cmd);

            if (string.IsNullOrEmpty(sSoPhieu))
                sSoPhieu = "0";
            long lSoPhieu = long.Parse(sSoPhieu) + 1;
            return lSoPhieu.ToString().PadLeft(4, '0');
        }

        public bool DelCCLDBanGiao(string sMaCCLD)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE CC_BAN_GIAO_CC WHERE ID_CCLD = (SELECT CC.ID_CCLD FROM CC_TANG CC WHERE CC.MA_CCLD = '" + sMaCCLD + "')";
                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public bool DelDaiDienBanGiao(string sIDBanGiaoHD)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE CC_BAN_GIAO_HD WHERE ID_BAN_GIAO_HD = " + sIDBanGiaoHD;
                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public bool InsertBanGiao(string sNgayBB, string sSoBB, string sMaDVSDTS, string sMaDTSDTS, string sDaiDienBenGiao, string sDaiDienBenNhan,
                                string sDiaDiem)
        {
            try
            {

                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("NgayBB", sNgayBB));
                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("MaDVSDTS", sMaDVSDTS));
                lstPara.Add(new DataParameter("MaDTSDTS", sMaDTSDTS));
                lstPara.Add(new DataParameter("DaiDienBenGiao", sDaiDienBenGiao));
                lstPara.Add(new DataParameter("DaiDienBenNhan", sDaiDienBenNhan));
                lstPara.Add(new DataParameter("DiaDiem", sDiaDiem));

                bResult = DAL.ExecFunction("sp_CC_BAN_GIAO_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool UpdateBanGiao(string sNgayBB, string sSoBB, string sMaDVSDTS, string sMaDTSDTS, string sDaiDienBenGiao, string sDaiDienBenNhan,
                                string sDiaDiem)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("NgayBB", sNgayBB));
                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("MaDVSDTS", sMaDVSDTS));
                lstPara.Add(new DataParameter("MaDTSDTS", sMaDTSDTS));
                lstPara.Add(new DataParameter("DaiDienBenGiao", sDaiDienBenGiao));
                lstPara.Add(new DataParameter("DaiDienBenNhan", sDaiDienBenNhan));
                lstPara.Add(new DataParameter("DiaDiem", sDiaDiem));

                bResult = DAL.ExecFunction("sp_CC_BAN_GIAO_Update", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable CheckMaCCLDBanGiao(string sMaCCLD, string sSoBB)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = " SELECT	CT.ID_BAN_GIAO_CC FROM	CC_BAN_GIAO_CC CT INNER JOIN CC_TANG CC ON CT.ID_CCLD = CC.ID_CCLD " +
                              "INNER JOIN CC_BAN_GIAO BG ON BG.ID_BD = CT.ID_BD WHERE CC.MA_CCLD = '" + sMaCCLD + "' AND BG.SO_BIEN_BAN = '" + sSoBB + "'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public bool InsertBanGiaoCC(string sSoBB, string sMaCCLD, string sMaTinhTrang, string sGhiChu, string sMaDuAn)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));
                //lstPara.Add(new DataParameter("NgayBanGiao", sNgayBanGiao));
                lstPara.Add(new DataParameter("MaTinhTrang", sMaTinhTrang));
                lstPara.Add(new DataParameter("GhiChu", sGhiChu));
                lstPara.Add(new DataParameter("MaDuAn", sMaDuAn));

                bResult = DAL.ExecFunction("sp_CC_BAN_GIAO_CC_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public bool UpdateBanGiaoCC(string sSoBB, string sMaCCLD, string sNgayBanGiao, string sMaTinhTrang, string sGhiChu, string sMaDuAn)
        //{
        //    try
        //    {
        //        bool bResult = false;

        //        List<DataParameter> lstPara = new List<DataParameter>();

        //        lstPara.Add(new DataParameter("SoBB", sSoBB));
        //        lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));
        //        lstPara.Add(new DataParameter("NgayBanGiao", sNgayBanGiao));
        //        lstPara.Add(new DataParameter("MaTinhTrang", sMaTinhTrang));
        //        lstPara.Add(new DataParameter("GhiChu", sGhiChu));
        //        lstPara.Add(new DataParameter("MaDuAn", sMaDuAn));

        //        bResult = DAL.ExecFunction("sp_CC_BAN_GIAO_CC_Update", lstPara);
        //        return bResult;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool DuyetBanGiao(string sSoBB, string sTrangThai)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("TrangThai", sTrangThai)); //0: chờ duyệt, 1:duyệt, 2: từ chối duyệt, 3: hủy duyệt

                bResult = DAL.ExecFunction("sp_CC_BAN_GIAO_DUYET", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public bool InsertBanGiaoHD(string sSoBB, string sHoTen, string sChucVu, string sDaiDien)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("HoTen", sHoTen));
                lstPara.Add(new DataParameter("ChucVu", sChucVu));
                if (sDaiDien == "Bên nhận")
                    lstPara.Add(new DataParameter("DaiDien", "1")); //bên nhận
                else lstPara.Add(new DataParameter("DaiDien", "0")); //bên giao

                bResult = DAL.ExecFunction("sp_CC_BAN_GIAO_HD_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateBanGiaoHD(string sIDBanGiaoHD, string sHoTen, string sChucVu, string sDaiDien)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                if (sDaiDien == "Bên nhận")
                    sDaiDien = "1";
                else sDaiDien = "0";

                cmd.CommandText = "UPDATE CC_BAN_GIAO_HD SET HO_VA_TEN = @HoTen, CHUC_VU = @ChucVu, DAI_DIEN = @DaiDien WHERE ID_BAN_GIAO_HD = " + sIDBanGiaoHD;

                cmd.Parameters.Add(new SqlParameter("@HoTen", sHoTen));
                cmd.Parameters.Add(new SqlParameter("@ChucVu", sChucVu));
                cmd.Parameters.Add(new SqlParameter("@DaiDien", sDaiDien));

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }
            return true;
        }

        public bool DelBanGiaoCC(string sSoBB, string sMaCCLD)
        { //del từng cclđ trong phiếu nhập kho
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));

                bResult = DAL.ExecFunction("sp_CC_BAN_GIAO_CC_Delete", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public bool DeleteBanGiao(string sSoBB)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE CC_BAN_GIAO WHERE SO_BIEN_BAN = @SoBB ; ";
                cmd.CommandText +=
                    "DELETE CC_BAN_GIAO_HD WHERE ID_BD = (SELECT BG.ID_BD FROM CC_BAN_GIAO BG WHERE BG.SO_BIEN_BAN = @SoBB)";

                cmd.Parameters.Add(new SqlParameter("@SoBB", sSoBB));

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }
            return true;
        }

        #endregion

        #region "Điều chuyển cùng ĐVSD"

        public string GetSoGiaoDichDieuChuyen()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT TOP 1 SO_GIAO_DICH FROM CC_DC_NOI_BO ORDER BY CONVERT(NUMERIC,SO_GIAO_DICH) DESC";

            string sSoGiaoDich = DAL.RunCMDGetString(cmd);

            if (string.IsNullOrEmpty(sSoGiaoDich))
                sSoGiaoDich = "0";
            long lSoGiaoDich = long.Parse(sSoGiaoDich) + 1;
            return lSoGiaoDich.ToString().PadLeft(6, '0');
        }

        public DataTable TimKiemDieuChuyenCungDVSD(string sSoGiaoDichTu, string sSoGiaoDichDen, string sThoiGianTu, string sThoiGianDen,
                                        string sMaDVSD, string sMaDTSDDieuChuyen, string sMaDTSDNhan, string sMaCCLD, bool bChoDuyet, bool bDuyet, bool bTuChoiDuyet, bool bHuyDuyet, string sLoai)
        {
            DataTable dtb = new DataTable();

            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoGiaoDichTu", sSoGiaoDichTu)); // thêm parameter vào danh sách
            lstPara.Add(new DataParameter("SoGiaoDichDen", sSoGiaoDichDen));
            lstPara.Add(new DataParameter("Loai", sLoai)); // 0: cùng ĐVSD, 1: khác ĐVSD

            if (!string.IsNullOrEmpty(sThoiGianTu))
                lstPara.Add(new DataParameter("ThoiGianTu", DateTime.Parse(sThoiGianTu).ToString("yyyyMMdd")));

            if (!string.IsNullOrEmpty(sThoiGianDen))
                lstPara.Add(new DataParameter("ThoiGianDen", DateTime.Parse(sThoiGianDen).ToString("yyyyMMdd")));

            lstPara.Add(new DataParameter("MaDVSD", sMaDVSD));
            lstPara.Add(new DataParameter("MaDTSDDieuChuyen", sMaDTSDDieuChuyen));
            lstPara.Add(new DataParameter("MaDTSDNhan", sMaDTSDNhan));
            lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));

            if (bChoDuyet)
                lstPara.Add(new DataParameter("ChoDuyet", "1"));
            else
                lstPara.Add(new DataParameter("ChoDuyet", "0"));

            if (bDuyet)
                lstPara.Add(new DataParameter("Duyet", "1"));
            else
                lstPara.Add(new DataParameter("Duyet", "0"));

            if (bTuChoiDuyet)
                lstPara.Add(new DataParameter("TuChoiDuyet", "1"));
            else
                lstPara.Add(new DataParameter("TuChoiDuyet", "0"));

            if (bHuyDuyet)
                lstPara.Add(new DataParameter("HuyDuyet", "1"));
            else
                lstPara.Add(new DataParameter("HuyDuyet", "0"));

            dtb = DAL.ExecProcedure("sp_CC_DC_NOI_BO_TimKiem", lstPara);
            return dtb;
        }

        public DataTable LayCCLDDieuChuyenCungDVSD(string sMaNhomCCLD, string sMaLoCCLD, string sMaCCLDDaChon, string sMaDVSD, string sMaDTSD)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT	CC.MA_CCLD, CC.TEN_CCLD, NH.TEN_NHOM_CCLD, ISNULL(CC.DON_GIA,CC.DON_GIA_DGL) AS DON_GIA, NH.DON_VI_TINH, CC.MA_DTSDTS, CC.MA_DU_AN, DT.TEN_DTSDTS, DA.TEN_DU_AN,
		                                CC.SO_THANG_BH, CONVERT(NVARCHAR(10),CAST (CC.NGAY_TANG AS DATETIME),103) AS NGAY_TANG,
		                                CAST(0 AS BIT) AS is_check
                                FROM	CC_TANG CC INNER JOIN DM_NHOM_CCLD NH ON CC.MA_NHOM_CCLD = NH.MA_NHOM_CCLD
                                        INNER JOIN CC_TANG_LO TL ON CC.ID_LO_CCLD = TL.ID_LO_CCLD
                                        LEFT JOIN DM_DTSDTS DT ON CC.MA_DTSDTS = DT.MA_DTSDTS
                                        LEFT JOIN DM_DU_AN DA ON CC.MA_DU_AN = DA.MA_DU_AN 
                                        INNER JOIN DM_DVSDTS DV ON CC.MA_DVSDTS = DV.MA_DVSDTS";
            cmd.CommandText += " WHERE	CC.TT_NVU_HIEN_TAI = 1 AND CC.NVU_HIEN_TAI != 7 AND DV.MA_DVSDTS = '" + sMaDVSD + "' AND CC.ID_KHO IS NULL";

            if (!string.IsNullOrEmpty(sMaNhomCCLD))
                cmd.CommandText += " AND CC.MA_NHOM_CCLD =  N'%" + sMaNhomCCLD.Replace("'", "''") + "%'";
            if (!string.IsNullOrEmpty(sMaLoCCLD))
                cmd.CommandText += " AND TL.MA_LO_CCLD =  N'%" + sMaLoCCLD.Replace("'", "''") + "%'";
            if (!string.IsNullOrEmpty(sMaCCLDDaChon))
                cmd.CommandText += " AND CC.MA_CCLD NOT IN (" + sMaCCLDDaChon + ")";
            if (!string.IsNullOrEmpty(sMaDTSD))
                cmd.CommandText += " AND CC.MA_DTSDTS = '" + sMaDTSD + "'";
            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }


        public bool InsertDieuChuyenCungDVSD(string sNgayDieuChuyen, string sSoGiaoDich, string sMaDVSD, string sDienGiai, string sLoai)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("NgayDieuChuyen", sNgayDieuChuyen));
                lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
                lstPara.Add(new DataParameter("MaDVSDDi", sMaDVSD));
                lstPara.Add(new DataParameter("MaDVSDDen", sMaDVSD));
                lstPara.Add(new DataParameter("DienGiai", sDienGiai));
                lstPara.Add(new DataParameter("Loai", sLoai)); //0: cùng ĐVSD; 1: khác ĐVSD

                bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool InsertDieuChuyenCungDVSDCT(string sSoGiaoDich, string sMaCCLD, string sMaDVSD, string sDTSD_DieuChuyenDi, string sDTSD_DieuChuyenDen, string sDuAn_DieuChuyenDi, string sDuAn_DieuChuyenDen, string sLoai)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
                lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));
                lstPara.Add(new DataParameter("DVSD_DieuChuyenDi", sMaDVSD));
                lstPara.Add(new DataParameter("DVSD_DieuChuyenDen", sMaDVSD));
                lstPara.Add(new DataParameter("DTSD_DieuChuyenDi", sDTSD_DieuChuyenDi));
                lstPara.Add(new DataParameter("DTSD_DieuChuyenDen", sDTSD_DieuChuyenDen));
                lstPara.Add(new DataParameter("DuAn_DieuChuyenDi", sDuAn_DieuChuyenDi));
                lstPara.Add(new DataParameter("DuAn_DieuChuyenDen", sDuAn_DieuChuyenDen));
                lstPara.Add(new DataParameter("Loai", sLoai));
                lstPara.Add(new DataParameter("LoaiBienDong", "5")); // 5: điều chuyển cùng ĐVSD

                bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_CT_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateDieuChuyenCungDVSD(string sNgayDieuChuyen, string sSoGiaoDich, string sMaDVSD, string sDienGiai, string sLoai, string sLoaiBienDong)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("NgayDieuChuyen", sNgayDieuChuyen));
                lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
                lstPara.Add(new DataParameter("MaDVSDDi", sMaDVSD));
                lstPara.Add(new DataParameter("MaDVSDDen", sMaDVSD));
                lstPara.Add(new DataParameter("DienGiai", sDienGiai));
                lstPara.Add(new DataParameter("Loai", sLoai)); //0: cùng ĐVSD; 1: khác ĐVSD
                lstPara.Add(new DataParameter("LoaiBienDong", sLoaiBienDong)); //5: cùng ĐVSD; 6: khác ĐVSD

                bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_Update", lstPara);

                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataSet GetGiaoDichDieuChuyen(string sSoGiaoDich, string sLoai)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
            lstPara.Add(new DataParameter("Loai", sLoai));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_CC_DC_NOI_BO_Select", lstPara);
            return ds;
        }

        public bool DeleteDieuChuyen(string sSoGiaoDich, string sLoai, string sLoaiBienDong)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
                lstPara.Add(new DataParameter("Loai", sLoai)); //0: cùng DVSD, 1: khác DVSD
                lstPara.Add(new DataParameter("LoaiBienDong", sLoaiBienDong));

                bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_Delete", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }
            return true;
        }

        public bool DeleteDieuChuyenCT(string sSoGiaoDich, string MaCCLD, string sLoai, string sLoaiBienDong)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
                lstPara.Add(new DataParameter("MaCCLD", MaCCLD));
                lstPara.Add(new DataParameter("Loai", sLoai));
                lstPara.Add(new DataParameter("LoaiBienDong", sLoaiBienDong));

                bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_CT_Delete", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }
            return true;
        }

        public bool DuyetDieuChuyen(string sSoGiaoDich, string sTrangThai, string sLoai, string sLoaiBienDong)
        {
            bool bResult = false;

            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
            lstPara.Add(new DataParameter("TrangThai", sTrangThai)); //0: chờ duyệt; 1: đã duyệt; 2: từ chối duyệt; 3: hủy duyệt          
            lstPara.Add(new DataParameter("Loai", sLoai));
            lstPara.Add(new DataParameter("LoaiBienDong", sLoaiBienDong)); // 5: điều chuyển cùng ĐVSD, 6: khác ĐVSD

            bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_DUYET", lstPara);
            if (bResult)
                bResult = DuyetCongCuDieuChuyen(sSoGiaoDich, sTrangThai, sLoai);
            return bResult;
        }

        public bool DuyetCongCuDieuChuyen(string sSoGiaoDich, string sTrangThai, string sLoai)
        {
            bool bResult = false;

            DataTable dtb = GetGiaoDichDieuChuyen(sSoGiaoDich, sLoai).Tables[1];

            foreach (DataRow row in dtb.Rows)
            {
                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("ID_CCLD", row["ID_CCLD"].ToString()));
                lstPara.Add(new DataParameter("TrangThai", sTrangThai)); //0: chờ duyệt; 1: đã duyệt; 2: từ chối duyệt; 3: hủy duyệt          
                lstPara.Add(new DataParameter("Ma_DVSDTS_Di", row["MA_DVSDTS_DI"].ToString()));
                lstPara.Add(new DataParameter("Ma_DVSDTS_Den", row["MA_DVSDTS_DEN"].ToString()));
                lstPara.Add(new DataParameter("Ma_DTSDTS_Di", row["DTSD_DIEU_CHUYEN_DI"].ToString()));
                lstPara.Add(new DataParameter("Ma_DTSDTS_Den", row["DTSD_DIEU_CHUYEN_DEN"].ToString()));
                lstPara.Add(new DataParameter("Id_Kho_Di", row["ID_KHO_DI"].ToString()));
                lstPara.Add(new DataParameter("Id_Kho_Den", row["ID_KHO_DEN"].ToString()));

                bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_CT_DUYET", lstPara);
            }
            return bResult;
        }


        #endregion

        #region "Điều chuyển khác ĐVSD"
        public DataTable TimKiemDieuChuyenKhacDVSD(string sSoQuyetDinh, string sSoGiaoDichTu, string sSoGiaoDichDen, string sThoiGianTu, string sThoiGianDen,
                                        string sMaDVSDDieuChuyen, string sMaDVSDNhan, string sMaKhoNhan, string sMaCCLD, bool bChoDuyet, bool bDuyet, bool bTuChoiDuyet, bool bHuyDuyet, string sLoai)
        {
            DataTable dtb = new DataTable();

            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoQuyetDinh", sSoQuyetDinh));
            lstPara.Add(new DataParameter("SoGiaoDichTu", sSoGiaoDichTu)); // thêm parameter vào danh sách
            lstPara.Add(new DataParameter("SoGiaoDichDen", sSoGiaoDichDen));
            lstPara.Add(new DataParameter("Loai", sLoai)); // 0: cùng ĐVSD, 1: khác ĐVSD

            if (!string.IsNullOrEmpty(sThoiGianTu))
                lstPara.Add(new DataParameter("ThoiGianTu", DateTime.Parse(sThoiGianTu).ToString("yyyyMMdd")));

            if (!string.IsNullOrEmpty(sThoiGianDen))
                lstPara.Add(new DataParameter("ThoiGianDen", DateTime.Parse(sThoiGianDen).ToString("yyyyMMdd")));

            lstPara.Add(new DataParameter("MaDVSD", sMaDVSDDieuChuyen));
            lstPara.Add(new DataParameter("MaDVSDNhan", sMaDVSDNhan));
            if (!string.IsNullOrEmpty(sMaKhoNhan))
            {
                int id_kho = int.Parse(LayIdKho(sMaKhoNhan));
                lstPara.Add(new DataParameter("IdKhoNhan", id_kho));
            }
            lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));

            if (bChoDuyet)
                lstPara.Add(new DataParameter("ChoDuyet", "1"));
            else
                lstPara.Add(new DataParameter("ChoDuyet", "0"));

            if (bDuyet)
                lstPara.Add(new DataParameter("Duyet", "1"));
            else
                lstPara.Add(new DataParameter("Duyet", "0"));

            if (bTuChoiDuyet)
                lstPara.Add(new DataParameter("TuChoiDuyet", "1"));
            else
                lstPara.Add(new DataParameter("TuChoiDuyet", "0"));

            if (bHuyDuyet)
                lstPara.Add(new DataParameter("HuyDuyet", "1"));
            else
                lstPara.Add(new DataParameter("HuyDuyet", "0"));

            dtb = DAL.ExecProcedure("sp_CC_DC_NOI_BO_TimKiem", lstPara);
            return dtb;
        }

        public DataTable LayCCLDDieuChuyenKhacDVSD(string sMaNhomCCLD, string sMaLoCCLD, string sMaCCLDDaChon, string sMaDVSD, string sMaKho)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT	CC.MA_CCLD, CC.TEN_CCLD, NH.TEN_NHOM_CCLD, ISNULL(CC.DON_GIA,CC.DON_GIA_DGL) AS DON_GIA, NH.DON_VI_TINH, ISNULL(CC.MA_DVSDTS, KH.MA_KHO) AS MA_DON_VI, ISNULL(DV.TEN_DVSDTS, KH.TEN_KHO) AS TEN_DON_VI,
		                                CC.SO_THANG_BH, CONVERT(NVARCHAR(10),CAST (CC.NGAY_TANG AS DATETIME),103) AS NGAY_TANG,
		                                CAST(0 AS BIT) AS is_check
                                FROM	CC_TANG CC INNER JOIN DM_NHOM_CCLD NH ON CC.MA_NHOM_CCLD = NH.MA_NHOM_CCLD
                                        INNER JOIN CC_TANG_LO TL ON CC.ID_LO_CCLD = TL.ID_LO_CCLD                                       
                                        LEFT JOIN DM_KHO KH ON CC.ID_KHO = KH.ID_KHO 
                                        LEFT JOIN DM_DVSDTS DV ON CC.MA_DVSDTS = DV.MA_DVSDTS";
            cmd.CommandText += " WHERE	CC.TT_NVU_HIEN_TAI = 1 AND CC.NVU_HIEN_TAI != 7 AND (CC.MA_DVSDTS IS NOT NULL OR CC.ID_KHO IS NOT NULL) ";

            if (!string.IsNullOrEmpty(sMaNhomCCLD))
                cmd.CommandText += " AND CC.MA_NHOM_CCLD =  N'%" + sMaNhomCCLD.Replace("'", "''") + "%'";
            if (!string.IsNullOrEmpty(sMaLoCCLD))
                cmd.CommandText += " AND TL.MA_LO_CCLD =  N'%" + sMaLoCCLD.Replace("'", "''") + "%'";
            if (!string.IsNullOrEmpty(sMaCCLDDaChon))
                cmd.CommandText += " AND CC.MA_CCLD NOT IN (" + sMaCCLDDaChon + ")";
            if (!string.IsNullOrEmpty(sMaDVSD))
                cmd.CommandText += " AND CC.MA_DVSDTS = '" + sMaDVSD + "'";
            if (!string.IsNullOrEmpty(sMaKho))
            {
                string id_kho = LayIdKho(sMaKho);
                cmd.CommandText += " AND CC.ID_KHO = " + id_kho;
            }
            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }


        public bool InsertDieuChuyenKhacDVSD(string sSoQuyetDinh, string sNgayQuyetDinh, string sNguoiQuyetDinh, string sNgayDieuChuyen, string sSoGiaoDich, string sMaDVSD, string sMaKho, string sDienGiai, string sLoai)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoQuyetDinh", sSoQuyetDinh));
                lstPara.Add(new DataParameter("NgayQuyetDinh", sNgayQuyetDinh));
                lstPara.Add(new DataParameter("NguoiQuyetDinh", sNguoiQuyetDinh));
                lstPara.Add(new DataParameter("NgayDieuChuyen", sNgayDieuChuyen));
                lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
                lstPara.Add(new DataParameter("MaDVSDDen", sMaDVSD));
                if (!string.IsNullOrEmpty(sMaKho))
                {
                    string id_kho = LayIdKho(sMaKho);
                    lstPara.Add(new DataParameter("IdKhoDen", id_kho));
                }
                lstPara.Add(new DataParameter("DienGiai", sDienGiai));
                lstPara.Add(new DataParameter("Loai", sLoai)); //0: cùng ĐVSD; 1: khác ĐVSD

                bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool InsertDieuChuyenKhacDVSDCT(string sSoGiaoDich, string sMaCCLD, string sMaDVSD_DieuChuyenDi, string sMaDVSD_DieuChuyenDen, string sDTSD_DieuChuyenDi, string sDTSD_DieuChuyenDen, string sIdKho_DieuChuyenDi, string sMaKho_DieuChuyenDen, string sDuAn_DieuChuyenDi, string sDuAn_DieuChuyenDen, string sLoai)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
                lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));
                lstPara.Add(new DataParameter("DVSD_DieuChuyenDi", sMaDVSD_DieuChuyenDi));
                lstPara.Add(new DataParameter("DVSD_DieuChuyenDen", sMaDVSD_DieuChuyenDen));
                lstPara.Add(new DataParameter("DTSD_DieuChuyenDi", sDTSD_DieuChuyenDi));
                lstPara.Add(new DataParameter("DTSD_DieuChuyenDen", sDTSD_DieuChuyenDen));

                lstPara.Add(new DataParameter("IdKho_DieuChuyenDi", sIdKho_DieuChuyenDi));

                if (!string.IsNullOrEmpty(sMaKho_DieuChuyenDen))
                {
                    string id_kho = LayIdKho(sMaKho_DieuChuyenDen);
                    lstPara.Add(new DataParameter("IdKho_DieuChuyenDen", id_kho));
                }
                lstPara.Add(new DataParameter("DuAn_DieuChuyenDi", sDuAn_DieuChuyenDi));
                lstPara.Add(new DataParameter("DuAn_DieuChuyenDen", sDuAn_DieuChuyenDen));
                lstPara.Add(new DataParameter("Loai", sLoai));
                lstPara.Add(new DataParameter("LoaiBienDong", "6")); // 5: điều chuyển khác ĐVSD

                bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_CT_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateDieuChuyenKhacDVSD(string sSoQuyetDinh, string sNgayQuyetDinh, string sNguoiQuyetDinh, string sNgayDieuChuyen, string sSoGiaoDich, string sMaDVSD, string sMaKho, string sDienGiai, string sLoai, string sLoaiBienDong)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoQuyetDinh", sSoQuyetDinh));
                lstPara.Add(new DataParameter("NgayQuyetDinh", sNgayQuyetDinh));
                lstPara.Add(new DataParameter("NguoiQuyetDinh", sNguoiQuyetDinh));
                lstPara.Add(new DataParameter("NgayDieuChuyen", sNgayDieuChuyen));
                lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
                lstPara.Add(new DataParameter("MaDVSDDen", sMaDVSD));
                if (!string.IsNullOrEmpty(sMaKho))
                {
                    string id_kho = LayIdKho(sMaKho);
                    lstPara.Add(new DataParameter("IdKhoDen", id_kho));
                }
                lstPara.Add(new DataParameter("DienGiai", sDienGiai));
                lstPara.Add(new DataParameter("Loai", sLoai)); //0: cùng ĐVSD; 1: khác ĐVSD
                lstPara.Add(new DataParameter("LoaiBienDong", sLoaiBienDong)); //5: cùng ĐVSD; 6: khác ĐVSD

                bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_Update", lstPara);

                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        //        public DataSet GetGiaoDichDieuChuyen(string sSoGiaoDich, string sLoai)
        //        {
        //            List<DataParameter> lstPara = new List<DataParameter>();

        //            lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
        //            lstPara.Add(new DataParameter("Loai", sLoai));

        //            DataSet ds = new DataSet();
        //            ds = DAL.ExecProcReturnDs("sp_CC_DC_NOI_BO_Select", lstPara);
        //            return ds;
        //        }

        //        public bool DeleteDieuChuyen(string sSoGiaoDich, string sLoai)
        //        {
        //            try
        //            {
        //                bool bResult = false;

        //                List<DataParameter> lstPara = new List<DataParameter>();

        //                lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
        //                lstPara.Add(new DataParameter("Loai", sLoai)); //0: cùng DVSD, 1: khác DVSD
        //                lstPara.Add(new DataParameter("LoaiBienDong", "5"));

        //                bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_Delete", lstPara);
        //                return bResult;
        //            }
        //            catch (Exception ex)
        //            {
        //                throw ex;
        //                return false;
        //            }
        //            return true;
        //        }



        //        public bool DuyetDieuChuyen(string sSoGiaoDich, string sTrangThai, string sLoai, string sLoaiBienDong)
        //        {
        //            bool bResult = false;

        //            List<DataParameter> lstPara = new List<DataParameter>();

        //            lstPara.Add(new DataParameter("SoGiaoDich", sSoGiaoDich));
        //            lstPara.Add(new DataParameter("TrangThai", sTrangThai)); //0: chờ duyệt; 1: đã duyệt; 2: từ chối duyệt; 3: hủy duyệt          
        //            lstPara.Add(new DataParameter("Loai", sLoai));
        //            lstPara.Add(new DataParameter("LoaiBienDong", sLoaiBienDong)); // 5: điều chuyển cùng ĐVSD, 6: khác ĐVSD

        //            bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_DUYET", lstPara);
        //            if (bResult)
        //                bResult = DuyetCongCuDieuChuyenCungDVSD(sSoGiaoDich, sTrangThai, sLoai);
        //            return bResult;
        //        }

        //        public bool DuyetCongCuDieuChuyenCungDVSD(string sSoGiaoDich, string sTrangThai, string sLoai)
        //        {
        //            bool bResult = false;

        //            DataTable dtb = GetGiaoDichDieuChuyen(sSoGiaoDich, sLoai).Tables[1];

        //            foreach (DataRow row in dtb.Rows)
        //            {
        //                List<DataParameter> lstPara = new List<DataParameter>();

        //                lstPara.Add(new DataParameter("ID_CCLD", row["ID_CCLD"].ToString()));
        //                lstPara.Add(new DataParameter("TrangThai", sTrangThai)); //0: chờ duyệt; 1: đã duyệt; 2: từ chối duyệt; 3: hủy duyệt          
        //                lstPara.Add(new DataParameter("Ma_DVSDTS_Di", row["MA_DVSDTS_DI"].ToString()));
        //                lstPara.Add(new DataParameter("Ma_DVSDTS_Den", row["MA_DVSDTS_DEN"].ToString()));
        //                lstPara.Add(new DataParameter("Ma_DTSDTS_Di", row["DTSD_DIEU_CHUYEN_DI"].ToString()));
        //                lstPara.Add(new DataParameter("Ma_DTSDTS_Den", row["DTSD_DIEU_CHUYEN_DEN"].ToString()));

        //                bResult = DAL.ExecFunction("sp_CC_DC_NOI_BO_CT_DUYET", lstPara);
        //            }
        //            return bResult;
        //        }


        #endregion

        #region "Thanh lý"

        public string GetIdThanhVienThanhLy()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT TOP 1 ID_THANH_LY_HD FROM CC_THANH_LY_HD ORDER BY ID_THANH_LY_HD DESC";

            string ID = DAL.RunCMDGetString(cmd);

            if (string.IsNullOrEmpty(ID))
                ID = "0";
            int result = int.Parse(ID) + 1;

            return result.ToString();
        }

        public DataTable TimKiemThanhLy(string sSoBBTu, string sSoBBDen, string sThoiGianTu, string sThoiGianDen,
                                        string sMaCCLD, bool bChoDuyet, bool bDuyet, bool bTuChoiDuyet, bool bHuyDuyet)
        {
            DataTable dtb = new DataTable();

            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoBBTu", sSoBBTu)); // thêm parameter vào danh sách
            lstPara.Add(new DataParameter("SoBBDen", sSoBBDen));

            if (!string.IsNullOrEmpty(sThoiGianTu))
                lstPara.Add(new DataParameter("ThoiGianTu", DateTime.Parse(sThoiGianTu).ToString("yyyyMMdd")));

            if (!string.IsNullOrEmpty(sThoiGianDen))
                lstPara.Add(new DataParameter("ThoiGianDen", DateTime.Parse(sThoiGianDen).ToString("yyyyMMdd")));

            lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));

            if (bChoDuyet)
                lstPara.Add(new DataParameter("ChoDuyet", "1"));
            else
                lstPara.Add(new DataParameter("ChoDuyet", "0"));

            if (bDuyet)
                lstPara.Add(new DataParameter("Duyet", "1"));
            else
                lstPara.Add(new DataParameter("Duyet", "0"));

            if (bTuChoiDuyet)
                lstPara.Add(new DataParameter("TuChoiDuyet", "1"));
            else
                lstPara.Add(new DataParameter("TuChoiDuyet", "0"));

            if (bHuyDuyet)
                lstPara.Add(new DataParameter("HuyDuyet", "1"));
            else
                lstPara.Add(new DataParameter("HuyDuyet", "0"));

            dtb = DAL.ExecProcedure("sp_CC_THANH_LY_TimKiem", lstPara);
            return dtb;
        }

        public DataTable LayCCLDThanhLy(string sMaNhomCCLD, string sMaLoCCLD, string sMaCCLDDaChon)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT	CC.MA_CCLD, CC.TEN_CCLD, NH.TEN_NHOM_CCLD, ISNULL(CC.DON_GIA,CC.DON_GIA_DGL) AS DON_GIA, NH.DON_VI_TINH,
		                                CC.SO_THANG_BH, CONVERT(NVARCHAR(10),CAST (CC.NGAY_TANG AS DATETIME),103) AS NGAY_TANG, CC.MA_TINH_TRANG, 
		                                TT.TEN_TINH_TRANG, 1 AS SL_THUC_TE, CAST(0 AS BIT) AS is_check
                                FROM	CC_TANG CC INNER JOIN DM_NHOM_CCLD NH ON CC.MA_NHOM_CCLD = NH.MA_NHOM_CCLD
		                                INNER JOIN CC_TANG_LO TL ON CC.ID_LO_CCLD = TL.ID_LO_CCLD 
		                                LEFT JOIN DM_TINH_TRANG TT ON CC.MA_TINH_TRANG = TT.MA_TINH_TRANG ";
            cmd.CommandText += " WHERE	CC.TT_NVU_HIEN_TAI = 1 AND CC.NVU_HIEN_TAI != 7 AND CC.ID_KHO IS NULL ";

            if (!string.IsNullOrEmpty(sMaNhomCCLD))
                cmd.CommandText += " AND CC.MA_NHOM_CCLD =  N'%" + sMaNhomCCLD.Replace("'", "''") + "%'";
            if (!string.IsNullOrEmpty(sMaLoCCLD))
                cmd.CommandText += " AND TL.MA_LO_CCLD =  N'%" + sMaLoCCLD.Replace("'", "''") + "%'";
            if (!string.IsNullOrEmpty(sMaCCLDDaChon))
                cmd.CommandText += " AND CC.MA_CCLD NOT IN (" + sMaCCLDDaChon + ")";
            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public bool DelThanhLyCC(string sSoBB, string sMaCCLD)
        { //del từng cclđ trong biên bản thanh lý
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));

                bResult = DAL.ExecFunction("sp_CC_THANH_LY_CC_Delete", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public bool DeleteThanhLy(string sSoBB)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                
                cmd.CommandText = "DELETE CC_THANH_LY_HD WHERE CC_THANH_LY_HD.ID_BD = (SELECT TL.ID_BD FROM CC_THANH_LY TL WHERE TL.SO_BIEN_BAN = @SoBB);";
                
                cmd.CommandText += "DELETE CC_THANH_LY WHERE SO_BIEN_BAN = @SoBB ; ";
                cmd.Parameters.Add(new SqlParameter("@SoBB", sSoBB));

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
               // throw ex;
                return false;
            }
            return true;
        }

        public DataSet GetBBThanhLy(string sSoBB)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoBB", sSoBB));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_CC_THANH_LY_Select", lstPara);
            return ds;
        }

        public bool DuyetThanhLy(string sSoBB, string sTrangThai)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("TrangThai", sTrangThai)); //0: chờ duyệt, 1:duyệt, 2: từ chối duyệt, 3: hủy duyệt

                bResult = DAL.ExecFunction("sp_CC_THANH_LY_DUYET", lstPara);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
         
        }

        public string GetSoBBThanhLy()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT TOP 1 SO_BIEN_BAN FROM CC_THANH_LY ORDER BY CONVERT(NUMERIC,SO_BIEN_BAN) DESC";

            string sSoPhieu = DAL.RunCMDGetString(cmd);

            if (string.IsNullOrEmpty(sSoPhieu))
                sSoPhieu = "0";
            long lSoPhieu = long.Parse(sSoPhieu) + 1;
            return lSoPhieu.ToString().PadLeft(4, '0');
        }

        public bool DelCCLDThanhLy(string sMaCCLD)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE CC_THANH_LY_CC WHERE ID_CCLD = (SELECT CC.ID_CCLD FROM CC_TANG CC WHERE CC.MA_CCLD = '" + sMaCCLD + "')";
                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public bool DelDaiDienThanhLy(string sIDBanGiaoHD)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE CC_THANH_LY_HD WHERE ID_THANH_LY_HD = " + sIDBanGiaoHD;
                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public bool InsertThanhLy(string sNgayThanhLy, string sSoBB, string sSoQD, string sNgayQD, string sNguoiQD,
                                string sSoChungTu, string sNgayChungTu, string sSoTienChi, string sSoTienThu,
                                string sThuTruongDV, string sTruongPhongKeToan, string sChuTich, string sKetLuan)
        {
            try
            {

                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("NgayThanhLy", sNgayThanhLy));
                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("SoQD", sSoQD));
                lstPara.Add(new DataParameter("NgayQD", sNgayQD));
                lstPara.Add(new DataParameter("NguoiQD", sNguoiQD));
                lstPara.Add(new DataParameter("SoChungTu", sSoChungTu));
                lstPara.Add(new DataParameter("NgayChungTu", sNgayChungTu));
                lstPara.Add(new DataParameter("SoTienChi", sSoTienChi));
                lstPara.Add(new DataParameter("SoTienThu", sSoTienThu));
                lstPara.Add(new DataParameter("ThuTruongDV", sThuTruongDV));
                lstPara.Add(new DataParameter("TruongPhongKeToan", sTruongPhongKeToan));
                lstPara.Add(new DataParameter("ChuTich", sChuTich));
                lstPara.Add(new DataParameter("KetLuan", sKetLuan));

                bResult = DAL.ExecFunction("sp_CC_THANH_LY_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool UpdateThanhLy(string sNgayThanhLy, string sSoBB, string sSoQD, string sNgayQD, string sNguoiQD,
                                string sSoChungTu, string sNgayChungTu, string sSoTienChi, string sSoTienThu,
                                string sThuTruongDV, string sTruongPhongKeToan, string sChuTich, string sKetLuan)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("NgayThanhLy", sNgayThanhLy));
                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("SoQD", sSoQD));
                lstPara.Add(new DataParameter("NgayQD", sNgayQD));
                lstPara.Add(new DataParameter("NguoiQD", sNguoiQD));
                lstPara.Add(new DataParameter("SoChungTu", sSoChungTu));
                lstPara.Add(new DataParameter("NgayChungTu", sNgayChungTu));
                lstPara.Add(new DataParameter("SoTienChi", sSoTienChi));
                lstPara.Add(new DataParameter("SoTienThu", sSoTienThu));
                lstPara.Add(new DataParameter("ThuTruongDV", sThuTruongDV));
                lstPara.Add(new DataParameter("TruongPhongKeToan", sTruongPhongKeToan));
                lstPara.Add(new DataParameter("ChuTich", sChuTich));
                lstPara.Add(new DataParameter("KetLuan", sKetLuan));

                bResult = DAL.ExecFunction("sp_CC_THANH_LY_Update", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable CheckMaCCLDThanhLy(string sMaCCLD, string sSoBB)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = " SELECT	CT.ID_THANH_LY_CC FROM	CC_THANH_LY_CC CT INNER JOIN CC_TANG CC ON CT.ID_CCLD = CC.ID_CCLD " +
                              " INNER JOIN CC_THANH_LY TL ON TL.ID_BD = CT.ID_BD WHERE CC.MA_CCLD = '" + sMaCCLD + "' AND TL.SO_BIEN_BAN = '" + sSoBB + "'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public bool InsertThanhLyCC(string sSoBB, string sMaCCLD, string sMaTinhTrang, string sGhiChu)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));
                lstPara.Add(new DataParameter("MaTinhTrang", sMaTinhTrang));
                lstPara.Add(new DataParameter("GhiChu", sGhiChu));

                bResult = DAL.ExecFunction("sp_CC_THANH_LY_CC_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateThanhLyCC(string sSoBB, string sMaCCLD, string sMaTinhTrang, string sGhiChu)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));
                lstPara.Add(new DataParameter("MaTinhTrang", sMaTinhTrang));
                lstPara.Add(new DataParameter("GhiChu", sGhiChu));

                bResult = DAL.ExecFunction("sp_CC_THANH_LY_CC_Update", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertThanhLyHD(string sSoBB, string sHoTen, string sChucVu, string sViTri)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBB", sSoBB));
                lstPara.Add(new DataParameter("HoTen", sHoTen));
                lstPara.Add(new DataParameter("ChucVu", sChucVu));
                if (sViTri == "Ủy viên")
                    lstPara.Add(new DataParameter("ViTri", "1")); //Ủy viên
                else lstPara.Add(new DataParameter("ViTri", "0")); //chủ tịch hội đồng

                bResult = DAL.ExecFunction("sp_CC_THANH_LY_HD_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateThanhLyHD(string sIDThanhLyHD, string sHoTen, string sChucVu, string sViTri)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                if (sViTri == "Ủy viên")
                    sViTri = "1";
                else sViTri = "0";
                cmd.CommandText = "UPDATE CC_THANH_LY_HD SET HO_TEN = @HoTen, CHUC_VU = @ChucVu, VI_TRI = @ViTri WHERE ID_THANH_LY_HD = " + sIDThanhLyHD;

                cmd.Parameters.Add(new SqlParameter("@HoTen", sHoTen));
                cmd.Parameters.Add(new SqlParameter("@ChucVu", sChucVu));
                cmd.Parameters.Add(new SqlParameter("@ViTri", sViTri));

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }
            return true;
        }

        #endregion

        #region Kiểm kê

        public string GetIdThanhVienKiemKe()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT TOP 1 ID_KIEM_KE_HD FROM CC_KIEM_KE_HD ORDER BY ID_KIEM_KE_HD DESC";

            string ID = DAL.RunCMDGetString(cmd);

            if (string.IsNullOrEmpty(ID))
                ID = "0";
            int result = int.Parse(ID) + 1;

            return result.ToString();
        }

        public string GetIdCCLDThua()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT TOP 1 ID_KIEM_KE_THUA FROM CC_KIEM_KE_THUA ORDER BY ID_KIEM_KE_THUA DESC";

            string ID = DAL.RunCMDGetString(cmd);

            if (string.IsNullOrEmpty(ID))
                ID = "0";
            int result = int.Parse(ID) + 1;

            return result.ToString();
        }

        public DataTable TimKiemKiemKe(string sSoBBTu, string sSoBBDen, string sThoiGianTu, string sThoiGianDen,
                                        string sMaDVSD, string sMaKho, bool bChoDuyet, bool bDuyet, bool bTuChoiDuyet, bool bHuyDuyet)
        {
            DataTable dtb = new DataTable();

            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoBBTu", sSoBBTu)); // thêm parameter vào danh sách
            lstPara.Add(new DataParameter("SoBBDen", sSoBBDen));

            if (!string.IsNullOrEmpty(sThoiGianTu))
                lstPara.Add(new DataParameter("ThoiGianTu", DateTime.Parse(sThoiGianTu).ToString("yyyyMMdd")));

            if (!string.IsNullOrEmpty(sThoiGianDen))
                lstPara.Add(new DataParameter("ThoiGianDen", DateTime.Parse(sThoiGianDen).ToString("yyyyMMdd")));

            lstPara.Add(new DataParameter("MaDVSD", sMaDVSD));
            if (!string.IsNullOrEmpty(sMaKho))
            {
                int id_kho = int.Parse(LayIdKho(sMaKho));
                lstPara.Add(new DataParameter("IdKho", id_kho));
            }

            if (bChoDuyet)
                lstPara.Add(new DataParameter("ChoDuyet", "1"));
            else
                lstPara.Add(new DataParameter("ChoDuyet", "0"));

            if (bDuyet)
                lstPara.Add(new DataParameter("Duyet", "1"));
            else
                lstPara.Add(new DataParameter("Duyet", "0"));

            if (bTuChoiDuyet)
                lstPara.Add(new DataParameter("TuChoiDuyet", "1"));
            else
                lstPara.Add(new DataParameter("TuChoiDuyet", "0"));

            if (bHuyDuyet)
                lstPara.Add(new DataParameter("HuyDuyet", "1"));
            else
                lstPara.Add(new DataParameter("HuyDuyet", "0"));

            dtb = DAL.ExecProcedure("sp_CC_KIEM_KE_TimKiem", lstPara);
            return dtb;
        }

        public DataTable LayCCLDKiemKe(string sMaNhomCCLD, string sMaLoCCLD, string sMaCCLDDaChon, string sMaDonViDaChon, string mode)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT	CC.MA_CCLD, CC.TEN_CCLD, NH.TEN_NHOM_CCLD, CC.DON_GIA, CC.DON_GIA_DGL, NH.DON_VI_TINH,
		                                CC.SO_THANG_BH, CONVERT(NVARCHAR(10),CAST (CC.NGAY_TANG AS DATETIME),103) AS NGAY_TANG,";
            if (mode == "ĐVSD")
                cmd.CommandText += " DV.TEN_DVSDTS AS DON_VI ";
            else
                cmd.CommandText += " KH.TEN_KHO AS DON_VI ";
            cmd.CommandText += @"FROM CC_TANG CC INNER JOIN DM_NHOM_CCLD NH ON CC.MA_NHOM_CCLD = NH.MA_NHOM_CCLD
                                        INNER JOIN CC_TANG_LO TL ON CC.ID_LO_CCLD = TL.ID_LO_CCLD
                                        LEFT JOIN DM_DVSDTS DV ON CC.MA_DVSDTS = DV.MA_DVSDTS 
                                        LEFT JOIN DM_KHO KH ON CC.ID_KHO = KH.ID_KHO";
            cmd.CommandText += " WHERE	CC.TT_NVU_HIEN_TAI = 1 AND CC.NVU_HIEN_TAI != 7 ";

            if (!string.IsNullOrEmpty(sMaNhomCCLD))
                cmd.CommandText += " AND CC.MA_NHOM_CCLD =  N'%" + sMaNhomCCLD.Replace("'", "''") + "%'";
            if (!string.IsNullOrEmpty(sMaLoCCLD))
                cmd.CommandText += " AND TL.MA_LO_CCLD =  N'%" + sMaLoCCLD.Replace("'", "''") + "%'";
            if (!string.IsNullOrEmpty(sMaCCLDDaChon))
                cmd.CommandText += " AND CC.MA_CCLD NOT IN (" + sMaCCLDDaChon + ")";
            if (mode == "ĐVSD")
                cmd.CommandText += " AND CC.MA_DVSDTS IN (" + sMaDonViDaChon + ")";
            else
                cmd.CommandText += " AND CC.ID_KHO IN (" + sMaDonViDaChon + ")";
            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public string GetSoBienBanKiemKe()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "SELECT TOP 1 SO_BIEN_BAN FROM CC_KIEM_KE ORDER BY CONVERT(NUMERIC,SO_BIEN_BAN) DESC";

            string sSoBienBan = DAL.RunCMDGetString(cmd);

            if (string.IsNullOrEmpty(sSoBienBan))
                sSoBienBan = "0";
            long lSoBienBan = long.Parse(sSoBienBan) + 1;
            return lSoBienBan.ToString().PadLeft(6, '0');
        }

        public DataSet GetBienBanKiemKe(string sSoBienBan)
        {
            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoBienBan", sSoBienBan));

            DataSet ds = new DataSet();
            ds = DAL.ExecProcReturnDs("sp_CC_KIEM_KE_Select", lstPara);
            return ds;
        }

        public bool InsertKiemKe(string sNgayKiemKe, string sSoBienBan, string sLoai,
                                string sThuTruongDV, string sTruongPhongHanhChinh, string sTruongPhongKeToan, string sKiemSoatVien, string sChuTichHoiDong, string sThuKho)
        {
            try
            {

                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("NgayKiemKe", sNgayKiemKe));
                lstPara.Add(new DataParameter("SoBienBan", sSoBienBan));
                lstPara.Add(new DataParameter("Loai", sLoai));
                lstPara.Add(new DataParameter("ThuTruongDV", sThuTruongDV));
                lstPara.Add(new DataParameter("TruongPhongHanhChinh", sTruongPhongHanhChinh));
                lstPara.Add(new DataParameter("TruongPhongKeToan", sTruongPhongKeToan));
                lstPara.Add(new DataParameter("KiemSoatVien", sKiemSoatVien));
                lstPara.Add(new DataParameter("ChuTichHoiDong", sChuTichHoiDong));
                lstPara.Add(new DataParameter("ThuKho", sThuKho));

                bResult = DAL.ExecFunction("sp_CC_KIEM_KE_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool InsertKiemKeHD(string sSoBienBan, string sHoTen, string sChucVu, string sViTri)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBienBan", sSoBienBan));
                lstPara.Add(new DataParameter("HoTen", sHoTen));
                lstPara.Add(new DataParameter("ChucVu", sChucVu));
                if (sViTri == "Ủy viên")
                    lstPara.Add(new DataParameter("ViTri", "1")); //Ủy viên
                else lstPara.Add(new DataParameter("ViTri", "0")); //trưởng ban

                bResult = DAL.ExecFunction("sp_CC_KIEM_KE_HD_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertKiemKePV(string sSoBienBan, string sMaDonVi, string sLoai)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBienBan", sSoBienBan));
                if (sLoai == "0") // kiểm kê ĐVSD
                    lstPara.Add(new DataParameter("MaDVSDTS", sMaDonVi));
                else // kiểm kê kho
                    lstPara.Add(new DataParameter("IdKho", LayIdKho(sMaDonVi)));

                bResult = DAL.ExecFunction("sp_CC_KIEM_KE_PV_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertKiemKeCL(string sSoBienBan, string sMaCCLD, string sMaTinhTrang, string sSoLuongSS, string sSoLuongKK, string sThanhTienSS, string sThanhTienKK)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBienBan", sSoBienBan));
                lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));
                lstPara.Add(new DataParameter("MaTinhTrang", sMaTinhTrang));
                lstPara.Add(new DataParameter("SoLuongSS", sSoLuongSS));
                lstPara.Add(new DataParameter("SoLuongKK", sSoLuongKK));
                lstPara.Add(new DataParameter("ThanhTienSS", sThanhTienSS));
                lstPara.Add(new DataParameter("ThanhTienKK", sThanhTienKK));

                bResult = DAL.ExecFunction("sp_CC_KIEM_KE_CL_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertKiemKeThua(string sSoBienBan, string sTenCCLD, string sMaTinhTrang, string sSoLuongSS, string sSoLuongKK, string sDonGia, string sDonGiaDGL, string sMaDonVi, string sMaNhomCCLD, string sLoai)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBienBan", sSoBienBan));
                lstPara.Add(new DataParameter("TenCCLD", sTenCCLD));
                lstPara.Add(new DataParameter("MaTinhTrang", sMaTinhTrang));
                lstPara.Add(new DataParameter("SoLuongSS", sSoLuongSS));
                lstPara.Add(new DataParameter("SoLuongKK", sSoLuongKK));
                lstPara.Add(new DataParameter("DonGia", sDonGia));
                lstPara.Add(new DataParameter("DonGiaDGL", sDonGiaDGL));
                if (sLoai == "0") // kiểm kê ĐVSD
                    lstPara.Add(new DataParameter("MaDVSDTS", sMaDonVi));
                else // kiểm kê kho
                    lstPara.Add(new DataParameter("IdKho", LayIdKho(sMaDonVi)));
                lstPara.Add(new DataParameter("MaNhomCCLD", sMaNhomCCLD));

                bResult = DAL.ExecFunction("sp_CC_KIEM_KE_THUA_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertKiemKeDu(string sSoBienBan, string sIdCCLD, string sMaTinhTrang, string sMaDVSDTS, string sIdKho)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBienBan", sSoBienBan));
                lstPara.Add(new DataParameter("IdCCLD", sIdCCLD));
                lstPara.Add(new DataParameter("MaTinhTrang", sMaTinhTrang));
                lstPara.Add(new DataParameter("MaDVSDTS", sMaDVSDTS));
                lstPara.Add(new DataParameter("IdKho", sIdKho));

                bResult = DAL.ExecFunction("sp_CC_KIEM_KE_DU_Insert", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateKiemKe(string sNgayKiemKe, string sSoBienBan, string sLoai,
                                string sThuTruongDV, string sTruongPhongHanhChinh, string sTruongPhongKeToan, string sKiemSoatVien, string sChuTichHoiDong, string sThuKho)
        {
            try
            {

                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("NgayKiemKe", sNgayKiemKe));
                lstPara.Add(new DataParameter("SoBienBan", sSoBienBan));
                lstPara.Add(new DataParameter("Loai", sLoai));
                lstPara.Add(new DataParameter("ThuTruongDV", sThuTruongDV));
                lstPara.Add(new DataParameter("TruongPhongHanhChinh", sTruongPhongHanhChinh));
                lstPara.Add(new DataParameter("TruongPhongKeToan", sTruongPhongKeToan));
                lstPara.Add(new DataParameter("KiemSoatVien", sKiemSoatVien));
                lstPara.Add(new DataParameter("ChuTichHoiDong", sChuTichHoiDong));
                lstPara.Add(new DataParameter("ThuKho", sThuKho));

                bResult = DAL.ExecFunction("sp_CC_KIEM_KE_Update", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool DeleteKiemKe(string sSoBienBan)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBienBan", sSoBienBan));

                bResult = DAL.ExecFunction("sp_CC_KIEM_KE_Delete", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }
            return true;
        }

        public bool DeleteKiemKeCT(string sSoBienBan, string MaCCLD)
        {
            try
            {
                bool bResult = false;

                List<DataParameter> lstPara = new List<DataParameter>();

                lstPara.Add(new DataParameter("SoBienBan", sSoBienBan));
                lstPara.Add(new DataParameter("MaCCLD", MaCCLD));

                bResult = DAL.ExecFunction("sp_CC_KIEM_KE_CT_Delete", lstPara);
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
                return false;
            }
            return true;
        }

        public bool DuyetKiemKe(string sSoBienBan, string sTrangThai)
        {
            bool bResult = false;

            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("SoBienBan", sSoBienBan));
            lstPara.Add(new DataParameter("TrangThai", sTrangThai)); //0: chờ duyệt; 1: đã duyệt; 2: từ chối duyệt; 3: hủy duyệt          

            bResult = DAL.ExecFunction("sp_CC_KIEM_KE_DUYET", lstPara);

            return bResult;
        }

        public DataTable SearchKhoKiemKe(string sMa, string sTen, string sSoBB)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT	KH.MA_KHO, KH.TEN_KHO, KH.DIA_CHI
                                FROM	CC_KIEM_KE_PV PV INNER JOIN CC_KIEM_KE KK ON PV.ID_BD = KK.ID_BD
		                                LEFT JOIN DM_KHO KH ON PV.ID_KHO = KH.ID_KHO";
            cmd.CommandText += " WHERE KK.SO_BIEN_BAN = '" + sSoBB + "' AND PV.ID_KHO IS NOT NULL ";
            if (!string.IsNullOrEmpty(sMa))
                cmd.CommandText += " AND KH.MA_KHO LIKE N'%" + sMa.Replace("'", "''") + "%' ";
            if (!string.IsNullOrEmpty(sTen))
                cmd.CommandText += " AND KH.TEN_KHO LIKE N'%" + sTen.Replace("'", "''") + "%'";
            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        #endregion

        #region Tra cứu thông tin CCLĐ

        public DataTable TimKiemCCLD(string sMaDVSD, string sMaKho, string sMaDTSD, string sMaDuAn,
                                            string sMaNhomCCLD, string sMaTinhTrang, string sMaLo, string sTenLo, string sDonGiaTu, string sDonGiaDen, bool bChoDuyet, bool bDuyet,
                                            bool bTuChoiDuyet, bool bHuyDuyet)
        {
            DataTable dtb = new DataTable();

            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("MaDVSD", sMaDVSD));
            lstPara.Add(new DataParameter("MaKho", sMaKho));
            lstPara.Add(new DataParameter("MaDTSD", sMaDTSD));
            lstPara.Add(new DataParameter("MaDuAn", sMaDuAn));
            lstPara.Add(new DataParameter("MaNhomCCLD", sMaNhomCCLD));
            lstPara.Add(new DataParameter("MaTinhTrang", sMaTinhTrang));
            lstPara.Add(new DataParameter("MaLo", sMaLo));
            lstPara.Add(new DataParameter("TenLo", sTenLo));
            if (sDonGiaTu != "0")
                lstPara.Add(new DataParameter("DonGiaTu", sDonGiaTu.Replace(",","")));
            if (sDonGiaDen != "0")
                lstPara.Add(new DataParameter("DonGiaDen", sDonGiaDen.Replace(",", "")));

            if (bChoDuyet)
                lstPara.Add(new DataParameter("ChoDuyet", "1"));
            else
                lstPara.Add(new DataParameter("ChoDuyet", "0"));

            if (bDuyet)
                lstPara.Add(new DataParameter("Duyet", "1"));
            else
                lstPara.Add(new DataParameter("Duyet", "0"));

            if (bTuChoiDuyet)
                lstPara.Add(new DataParameter("TuChoiDuyet", "1"));
            else
                lstPara.Add(new DataParameter("TuChoiDuyet", "0"));

            if (bHuyDuyet)
                lstPara.Add(new DataParameter("HuyDuyet", "1"));
            else
                lstPara.Add(new DataParameter("HuyDuyet", "0"));

            dtb = DAL.ExecProcedure("sp_CC_TimKiem", lstPara);
            return dtb;
        }

        public DataTable TimKiemLichSuCCLD(string sMaCCLD)
        {
            DataTable dtb = new DataTable();

            List<DataParameter> lstPara = new List<DataParameter>();

            lstPara.Add(new DataParameter("MaCCLD", sMaCCLD));
            dtb = DAL.ExecProcedure("sp_CC_LICH_SU_TimKiem", lstPara);
            return dtb;
        }

        #endregion
    }
}
