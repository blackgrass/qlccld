﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Business
{
   public class clsCCLDKiemKe
    {
        #region "Kiểm kê"

        public DataTable TimKiemKiemKe(string soBBTu, string soBBDen, string thoiGianTu, string thoiGianDen,
                                        string maDVSD, string maKho, bool choDuyet, bool duyet, bool tuChoiDuyet, bool huyDuyet)
        {
            DataTable dataTable = new DataTable();
            List<DataParameter> lstPara = new List<DataParameter>();
            lstPara.Add(new DataParameter("SoBBTu", soBBTu));
            lstPara.Add(new DataParameter("SoBBDen", soBBDen));

            if (!string.IsNullOrEmpty(thoiGianTu))
                lstPara.Add(new DataParameter("ThoiGianTu", DateTime.Parse(thoiGianTu).ToString("yyyyMMdd")));

            if (!string.IsNullOrEmpty(thoiGianDen))
                lstPara.Add(new DataParameter("ThoiGianDen", DateTime.Parse(thoiGianDen).ToString("yyyyMMdd")));

            lstPara.Add(new DataParameter("MaDVSD", maDVSD));
            lstPara.Add(new DataParameter("MaKho", maKho));

            lstPara.Add(choDuyet ? new DataParameter("ChoDuyet", "1") : new DataParameter("ChoDuyet", "0"));
            lstPara.Add(duyet ? new DataParameter("Duyet", "1") : new DataParameter("Duyet", "0"));
            lstPara.Add(tuChoiDuyet ? new DataParameter("TuChoiDuyet", "1") : new DataParameter("TuChoiDuyet", "0"));
            lstPara.Add(huyDuyet ? new DataParameter("HuyDuyet", "1") : new DataParameter("HuyDuyet", "0"));

            dataTable = DAL.ExecProcedure("sp_CC_KIEM_KE_TimKiem", lstPara);
            return dataTable;
        }

        #endregion
    }
}
