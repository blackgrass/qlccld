﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Business
{
    public class clsDanhMuc
    {
        #region Các hàm dùng chung

        // Lấy thông tin từ bảng nào, trường nào và mã gì
        public DataTable LayThongTinTuMa(string strMa, string TenBang, string TenTruong)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select * from " + TenBang + " WHERE " + TenTruong + " = '" + strMa + "'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        #endregion

        #region Nhóm CCLĐ

        public DataTable LayDanhSachNhomCCLD(string strTimKiem)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select A.MA_NHOM_CCLD AS MA, A.TEN_NHOM_CCLD AS TEN, A.DON_VI_TINH AS DVT, B.TEN_NHOM_CCLD AS TENCAPTREN
                                from DM_NHOM_CCLD A LEFT JOIN DM_NHOM_CCLD B ON A.MA_NHOM_CHA = B.MA_NHOM_CCLD";

            if (!string.IsNullOrEmpty(strTimKiem))
                cmd.CommandText += " WHERE A.MA_NHOM_CCLD LIKE N'%" + strTimKiem + "%' OR A.TEN_NHOM_CCLD LIKE N'%" + strTimKiem + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public DataTable LayDanhSachNhomCCLDCapTren()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select * from DM_NHOM_CCLD ORDER BY TEN_NHOM_CCLD";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public void ThemNhomCCLD(string strMaNhom, string strTenNhom, string strDonViTinh, string strMaNhomCha)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO DM_NHOM_CCLD VALUES (@MaNhom, @TenNhom, @DonViTinh, @MaNhomCha)";

            cmd.Parameters.Add(new SqlParameter("@MaNhom", strMaNhom));
            cmd.Parameters.Add(new SqlParameter("@TenNhom", strTenNhom));
            cmd.Parameters.Add(new SqlParameter("@DonViTinh", strDonViTinh));
            cmd.Parameters.Add(new SqlParameter("@MaNhomCha", strMaNhomCha));

            DAL.RunActionCmd(cmd);
        }

        public void SuaNhomCCLD(string strMaNhom, string strTenNhom, string strDonViTinh, string strMaNhomCha)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE DM_NHOM_CCLD SET TEN_NHOM_CCLD = @TenNhom, DON_VI_TINH = @DonViTinh, MA_NHOM_CHA = @MaNhomCha WHERE MA_NHOM_CCLD = @MaNhom";

            cmd.Parameters.Add(new SqlParameter("@MaNhom", strMaNhom));
            cmd.Parameters.Add(new SqlParameter("@TenNhom", strTenNhom));
            cmd.Parameters.Add(new SqlParameter("@DonViTinh", strDonViTinh));
            cmd.Parameters.Add(new SqlParameter("@MaNhomCha", strMaNhomCha));

            DAL.RunActionCmd(cmd);
        }

        public bool XoaNhomCCLD(string strMaNhom)
        {
            // kiểm tra có con hay không
            SqlCommand check = new SqlCommand();
            check.CommandText = @"select * from DM_NHOM_CCLD WHERE MA_NHOM_CHA = '" + strMaNhom + "'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(check).Tables[0];

            if (dtb.Rows.Count != 0)
                return false;

            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE DM_NHOM_CCLD WHERE MA_NHOM_CCLD = '" + strMaNhom + "'";

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public DataTable SearchNhomCCLD(string sMa, string sTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT MA_NHOM_CCLD AS MA, TEN_NHOM_CCLD AS TEN, DON_VI_TINH, MA_NHOM_CHA FROM DM_NHOM_CCLD ";
            cmd.CommandText += " WHERE MA_NHOM_CHA IS NOT NULL ";
            if (!string.IsNullOrEmpty(sMa))
                cmd.CommandText += " AND MA_NHOM_CCLD LIKE N'%" + sMa.Replace("'", "''") + "%' ";
            if (!string.IsNullOrEmpty(sTen))
                cmd.CommandText += " AND TEN_NHOM_CCLD LIKE N'%" + sTen.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public DataTable SearchLoCCLD(string sMa, string sTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT DISTINCT	TL.MA_LO_CCLD AS MA, TL.TEN_LO_CCLD AS TEN, ISNULL(TL.DON_GIA,TL.DON_GIA_DGL) AS DON_GIA, CONVERT(NVARCHAR(10),CAST(GD.NGAY_TANG AS DATETIME),103) AS NGAY_TANG";
            cmd.CommandText += " FROM	CC_TANG CC INNER JOIN CC_TANG_LO TL ON CC.ID_LO_CCLD = TL.ID_LO_CCLD INNER JOIN CC_TANG_LO_GD GD ON GD.ID_BD = TL.ID_BD";
            cmd.CommandText += " WHERE	CC.TT_NVU_HIEN_TAI = 1 AND CC.ID_KHO IS NULL " +
                               " AND CC.ID_CCLD NOT IN ( SELECT TOP 1 LS.ID_CCLD " +
                               "                        FROM CC_LICH_SU LS " +
                               "                        WHERE LS.ID_CCLD = CC.ID_CCLD AND LS.LOAI_BIEN_DONG = 7 AND LS.TRANG_THAI = 1 " +
                               "                        ORDER BY LS.ID_LICH_SU DESC)";//LOAI_BIEN_DONG = 7 -> thanh lý
            if (!string.IsNullOrEmpty(sMa))
                cmd.CommandText += " AND TL.MA_LO_CCLD LIKE  N'%" + sMa.Replace("'", "''") + "%' ";
            if (!string.IsNullOrEmpty(sTen))
                cmd.CommandText += " AND TL.TEN_LO_CCLD LIKE  N'%" + sTen.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public DataTable SearchCCLD(string sMaNhomCCLD, string sMaLo, string sNghiepVu,string sMaCCLDDaChon)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT	CC.MA_CCLD, CC.TEN_CCLD, ISNULL(CC.DON_GIA,CC.DON_GIA_DGL) AS DON_GIA, CC.SO_THANG_BH, CC.NGAY_TANG, NH.TEN_NHOM_CCLD,
                                        CC.MA_TINH_TRANG, TT.TEN_TINH_TRANG, CAST(0 AS BIT) AS ChkChon
                                FROM	CC_TANG CC INNER JOIN DM_NHOM_CCLD NH ON CC.MA_NHOM_CCLD = NH.MA_NHOM_CCLD
                                        LEFT JOIN DM_TINH_TRANG TT ON CC.MA_TINH_TRANG = TT.MA_TINH_TRANG
                                        INNER JOIN CC_TANG_LO TL ON CC.ID_LO_CCLD = TL.ID_LO_CCLD
                                WHERE	CC.TT_NVU_HIEN_TAI = 1 " +
                                "       AND CC.NVU_HIEN_TAI != 7";
//                                        AND CC.ID_CCLD NOT IN ( SELECT LS.ID_CCLD 
//                                                                FROM CC_LICH_SU LS 
//                                                                WHERE LS.ID_CCLD = CC.ID_CCLD AND LS.LOAI_BIEN_DONG = 7 AND LS.TRANG_THAI = 1 ) ";
            if (sNghiepVu == "2") //bàn giao
                cmd.CommandText += " AND CC.ID_KHO IS NULL";
            if (sNghiepVu == "2" && !string.IsNullOrEmpty(sMaCCLDDaChon)) //bàn giao
                cmd.CommandText += " AND CC.MA_CCLD NOT IN ("+ sMaCCLDDaChon +")";
            if (!string.IsNullOrEmpty(sMaLo))
                cmd.CommandText += " AND TL.MA_LO_CCLD LIKE N'%" + sMaLo.Replace("'", "''") + "%' ";
            if (!string.IsNullOrEmpty(sMaNhomCCLD))
                cmd.CommandText += " AND CC.MA_NHOM_CCLD LIKE  N'%" + sMaNhomCCLD.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        #endregion

        #region Nguồn gốc

        public DataTable LayDanhSachNguonGoc(string strTimKiem)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_NGUON_GOC AS MA, TEN_NGUON_GOC AS TEN FROM DM_NGUON_GOC";

            if (!string.IsNullOrEmpty(strTimKiem))
                cmd.CommandText += " WHERE MA_NGUON_GOC LIKE N'%" + strTimKiem.Replace("'", "''") + "%' OR TEN_NGUON_GOC LIKE N'%" + strTimKiem.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public void ThemNguonGoc(string strMa, string strTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO DM_NGUON_GOC VALUES (@Ma, @Ten)";

            cmd.Parameters.Add(new SqlParameter("@Ma", strMa));
            cmd.Parameters.Add(new SqlParameter("@Ten", strTen));
            DAL.RunActionCmd(cmd);
        }

        public void SuaNguonGoc(string strMa, string strTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE DM_NGUON_GOC SET TEN_NGUON_GOC = @Ten WHERE MA_NGUON_GOC = @Ma";

            cmd.Parameters.Add(new SqlParameter("@Ma", strMa));
            cmd.Parameters.Add(new SqlParameter("@Ten", strTen));

            DAL.RunActionCmd(cmd);
        }

        public bool XoaNguonGoc(string strMa)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE DM_NGUON_GOC WHERE MA_NGUON_GOC = '" + strMa + "'";

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public DataTable SearchNguonGoc(string sMa, string sTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_NGUON_GOC AS MA, TEN_NGUON_GOC AS TEN FROM DM_NGUON_GOC";
            cmd.CommandText += " WHERE 1=1 ";
            if (!string.IsNullOrEmpty(sMa))
                cmd.CommandText += " AND MA_NGUON_GOC LIKE N'%" + sMa.Replace("'","''") + "%' ";
            if (!string.IsNullOrEmpty(sTen))
                cmd.CommandText += " AND TEN_NGUON_GOC LIKE N'%" + sTen.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        #endregion

        #region Quốc gia

        public DataTable LayDanhSachQuocGia(string strTimKiem)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_QUOC_GIA AS MA, TEN_QUOC_GIA AS TEN FROM DM_QUOC_GIA";

            if (!string.IsNullOrEmpty(strTimKiem))
                cmd.CommandText += " WHERE MA_QUOC_GIA LIKE N'%" + strTimKiem + "%' OR TEN_QUOC_GIA LIKE N'%" + strTimKiem + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public void ThemQuocGia(string strMaQuocGia, string strTenQuocGia)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO DM_QUOC_GIA VALUES (@MaQuocGia, @TenQuocGia)";

            cmd.Parameters.Add(new SqlParameter("@MaQuocGia", strMaQuocGia));
            cmd.Parameters.Add(new SqlParameter("@TenQuocGia", strTenQuocGia));

            DAL.RunActionCmd(cmd);
        }

        public void SuaQuocGia(string strMaQuocGia, string strTenQuocGia)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE DM_QUOC_GIA SET TEN_QUOC_GIA = @TenQuocGia WHERE MA_QUOC_GIA = @MaQuocGia";

            cmd.Parameters.Add(new SqlParameter("@MaQuocGia", strMaQuocGia));
            cmd.Parameters.Add(new SqlParameter("@TenQuocGia", strTenQuocGia));

            DAL.RunActionCmd(cmd);
        }

        public bool XoaQuocGia(string strMaQuocGia)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE DM_QUOC_GIA WHERE MA_QUOC_GIA = N'" + strMaQuocGia + "'";

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        #endregion

        #region ĐVSD

        public DataTable LayDanhSachDVSD(string strTimKiem)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_DVSDTS AS MA, TEN_DVSDTS AS TEN, DIA_CHI, DIEN_THOAI, FAX FROM DM_DVSDTS";

            if (!string.IsNullOrEmpty(strTimKiem))
                cmd.CommandText += " WHERE MA_DVSDTS LIKE N'%" + strTimKiem.Replace("'", "''") + "%' OR TEN_DVSDTS LIKE N'%" + strTimKiem.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public void ThemDVSD(string strMaDVSD, string strTenDVSD, string strDiaChi,string strDienThoai, string strFax)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO DM_DVSDTS VALUES (@MaDVSD, @TenDVSD,@DiaChi, @DienThoai, @FAX)";

            cmd.Parameters.Add(new SqlParameter("@MaDVSD", strMaDVSD));
            cmd.Parameters.Add(new SqlParameter("@TenDVSD", strTenDVSD));
            cmd.Parameters.Add(new SqlParameter("@DiaChi", strDiaChi));
            cmd.Parameters.Add(new SqlParameter("@DienThoai", strDienThoai));
            cmd.Parameters.Add(new SqlParameter("@FAX", strFax));

            DAL.RunActionCmd(cmd);
        }

        public void SuaDVSD(string strMaDVSD, string strTenDVSD,string strDiaChi,string strDienThoai, string strFax)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE DM_DVSDTS " +
                              "SET TEN_DVSDTS = @TenDVSDTS, DIA_CHI = @DiaChi, DIEN_THOAI = @DienThoai, FAX = @Fax " +
                              "WHERE MA_DVSDTS = @MaDVSDTS";

            cmd.Parameters.Add(new SqlParameter("@MaDVSDTS", strMaDVSD));
            cmd.Parameters.Add(new SqlParameter("@TenDVSDTS", strTenDVSD));
            cmd.Parameters.Add(new SqlParameter("@DiaChi", strDiaChi));
            cmd.Parameters.Add(new SqlParameter("@DienThoai", strDienThoai));
            cmd.Parameters.Add(new SqlParameter("@Fax", strFax));

            DAL.RunActionCmd(cmd);
        }

        public bool XoaDVSD(string strMaDVSD)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE DM_DVSDTS WHERE MA_DVSDTS = '" + strMaDVSD + "'";

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        //public DataTable SearchDVSD(string sMa, string sTen)
        //{
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.CommandText = @"select MA_DVSDTS AS MA, TEN_DVSDTS AS TEN, DIA_CHI, DIEN_THOAI, FAX FROM DM_DVSDTS";
        //    cmd.CommandText += " WHERE 1=1 ";
        //    if (!string.IsNullOrEmpty(sMa))
        //        cmd.CommandText += " AND MA_DVSDTS LIKE N'%" + sMa.Replace("'", "''") + "%' ";
        //    if (!string.IsNullOrEmpty(sTen))
        //        cmd.CommandText += " AND TEN_DVSDTS LIKE N'%" + sTen.Replace("'", "''") + "%'";

        //    DataTable dtb = new DataTable();
        //    dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
        //    return dtb;
        //}

        public DataTable SearchDVSD(string sMa, string sTen, string sMaDaChon = "")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_DVSDTS AS MA, TEN_DVSDTS AS TEN, DIA_CHI, DIEN_THOAI, FAX, CAST(0 AS BIT) AS is_check FROM DM_DVSDTS";
            cmd.CommandText += " WHERE 1=1 ";
            if (!string.IsNullOrEmpty(sMa))
                cmd.CommandText += " AND MA_DVSDTS LIKE N'%" + sMa.Replace("'", "''") + "%' ";
            if (!string.IsNullOrEmpty(sTen))
                cmd.CommandText += " AND TEN_DVSDTS LIKE N'%" + sTen.Replace("'", "''") + "%'";
            if (sMaDaChon != "")
                cmd.CommandText += " AND MA_DVSDTS NOT IN (" + sMaDaChon + ")";
            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        #endregion

        #region Tình trạng

        public DataTable LayDanhSachTinhTrang(string strTimKiem)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_TINH_TRANG AS MA, TEN_TINH_TRANG AS TEN FROM DM_TINH_TRANG";

            if (!string.IsNullOrEmpty(strTimKiem))
                cmd.CommandText += " WHERE MA_TINH_TRANG LIKE N'%" + strTimKiem.Replace("'", "''") + "%' OR TEN_TINH_TRANG LIKE N'%" + strTimKiem.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public void ThemTinhTrang(string strMa, string strTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO DM_TINH_TRANG VALUES (@Ma, @Ten)";

            cmd.Parameters.Add(new SqlParameter("@Ma", strMa));
            cmd.Parameters.Add(new SqlParameter("@Ten", strTen));

            DAL.RunActionCmd(cmd);
        }

        public void SuaTinhTrang(string strMa, string strTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE DM_TINH_TRANG SET TEN_TINH_TRANG = @Ten WHERE MA_TINH_TRANG = @Ma";

            cmd.Parameters.Add(new SqlParameter("@Ma", strMa));
            cmd.Parameters.Add(new SqlParameter("@Ten", strTen));

            DAL.RunActionCmd(cmd);
        }

        public bool XoaTinhTrang(string strMa)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE DM_TINH_TRANG WHERE MA_TINH_TRANG = '" + strMa + "'";

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public DataTable SearchTinhTrang(string sMa, string sTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_TINH_TRANG AS MA, TEN_TINH_TRANG AS TEN FROM DM_TINH_TRANG";
            cmd.CommandText += " WHERE 1=1 ";
            if (!string.IsNullOrEmpty(sMa))
                cmd.CommandText += " AND MA_TINH_TRANG LIKE N'%" + sMa.Replace("'", "''") + "%' ";
            if (!string.IsNullOrEmpty(sTen))
                cmd.CommandText += " AND TEN_TINH_TRANG LIKE N'%" + sTen.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        #endregion

        #region Dự án

        public DataTable LayDanhSachDuAn(string strTimKiem)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_DU_AN AS MA, TEN_DU_AN AS TEN, SO_QUYET_DINH,CONVERT(NVARCHAR(10),CAST(NGAY_QUYET_DINH AS DATETIME),103) AS NGAY_QUYET_DINH FROM DM_DU_AN";

            if (!string.IsNullOrEmpty(strTimKiem))
                cmd.CommandText += " WHERE MA_DU_AN LIKE N'%" + strTimKiem.Replace("'", "''") + "%' OR TEN_DU_AN LIKE N'%" + strTimKiem.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public void ThemDuAn(string strMa, string strTen,string strSoQD, string strNgayQD)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO DM_DU_AN VALUES (@Ma, @Ten,@SoQuyetDinh,@NgayQuyetDinh)";

            cmd.Parameters.Add(new SqlParameter("@Ma", strMa));
            cmd.Parameters.Add(new SqlParameter("@Ten", strTen));
            cmd.Parameters.Add(new SqlParameter("@SoQuyetDinh", strSoQD));
            cmd.Parameters.Add(new SqlParameter("@NgayQuyetDinh", strNgayQD));

            DAL.RunActionCmd(cmd);
        }

        public void SuaDuAn(string strMa, string strTen,string strSoQD, string strNgayQD)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE DM_DU_AN " +
                              "SET TEN_DU_AN = @Ten, SO_QUYET_DINH = @SoQuyetDinh, NGAY_QUYET_DINH = @NgayQuyetDinh " +
                              "WHERE MA_DU_AN = @Ma";

            cmd.Parameters.Add(new SqlParameter("@Ma", strMa));
            cmd.Parameters.Add(new SqlParameter("@Ten", strTen));
            cmd.Parameters.Add(new SqlParameter("@SoQuyetDinh", strSoQD));
            cmd.Parameters.Add(new SqlParameter("@NgayQuyetDinh", strNgayQD));

            DAL.RunActionCmd(cmd);
        }

        public bool XoaDuAn(string strMa)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE DM_DU_AN WHERE MA_DU_AN = '" + strMa + "'";

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public DataTable SearchDuAn(string sMa, string sTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_DU_AN AS MA, TEN_DU_AN AS TEN, SO_QUYET_DINH, NGAY_QUYET_DINH FROM DM_DU_AN";
            cmd.CommandText += " WHERE 1=1 ";
            if (!string.IsNullOrEmpty(sMa))
                cmd.CommandText += " AND MA_DU_AN LIKE N'%" + sMa.Replace("'", "''") + "%' ";
            if (!string.IsNullOrEmpty(sTen))
                cmd.CommandText += " AND TEN_DU_AN LIKE N'%" + sTen.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        #endregion

        #region Lý do nhập kho

        public DataTable LayDanhSachLyDoNhapKho(string strTimKiem)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_LY_DO_NHAP AS MA, TEN_LY_DO_NHAP AS TEN FROM DM_LY_DO_NHAP_KHO";

            if (!string.IsNullOrEmpty(strTimKiem))
                cmd.CommandText += " WHERE MA_LY_DO_NHAP LIKE N'%" + strTimKiem.Replace("'", "''") + "%' OR TEN_LY_DO_NHAP LIKE N'%" + strTimKiem.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public void ThemLyDoNhapKho(string strMa, string strTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO DM_LY_DO_NHAP_KHO VALUES (@Ma, @Ten)";

            cmd.Parameters.Add(new SqlParameter("@Ma", strMa));
            cmd.Parameters.Add(new SqlParameter("@Ten", strTen));

            DAL.RunActionCmd(cmd);
        }

        public void SuaLyDoNhapKho(string strMa, string strTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE DM_LY_DO_NHAP_KHO SET TEN_LY_DO_NHAP = @Ten WHERE MA_LY_DO_NHAP = @Ma";

            cmd.Parameters.Add(new SqlParameter("@Ma", strMa));
            cmd.Parameters.Add(new SqlParameter("@Ten", strTen));

            DAL.RunActionCmd(cmd);
        }

        public bool XoaLyDoNhapKho(string strMa)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE DM_LY_DO_NHAP_KHO WHERE MA_LY_DO_NHAP = '" + strMa + "'";

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public DataTable SearchLyDoNhapKho(string sMa, string sTen)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_LY_DO_NHAP AS MA, TEN_LY_DO_NHAP AS TEN FROM DM_LY_DO_NHAP_KHO";
            cmd.CommandText += " WHERE 1=1 ";
            if (!string.IsNullOrEmpty(sMa))
                cmd.CommandText += " AND MA_LY_DO_NHAP LIKE N'%" + sMa.Replace("'", "''") + "%' ";
            if (!string.IsNullOrEmpty(sTen))
                cmd.CommandText += " AND TEN_LY_DO_NHAP LIKE N'%" + sTen.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        #endregion

        #region Kho

        public DataTable LayDanhSachKho(string strTimKiem)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_KHO AS MA, TEN_KHO AS TEN, DIA_CHI FROM DM_KHO";

            if (!string.IsNullOrEmpty(strTimKiem))
                cmd.CommandText += " WHERE MA_KHO LIKE N'%" + strTimKiem.Replace("'", "''") + "%' OR TEN_KHO LIKE N'%" + strTimKiem.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public void ThemKho(string strMa, string strTen, string sDiaChi)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO DM_KHO (MA_KHO,TEN_KHO,DIA_CHI) VALUES (@Ma, @Ten,@DiaChi)";

            cmd.Parameters.Add(new SqlParameter("@Ma", strMa));
            cmd.Parameters.Add(new SqlParameter("@Ten", strTen));
            cmd.Parameters.Add(new SqlParameter("@DiaChi", sDiaChi));

            DAL.RunActionCmd(cmd);
        }

        public void SuaKho(string strMa, string strTen, string sDiaChi)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE DM_KHO SET TEN_KHO = @Ten, DIA_CHI = @DiaChi WHERE MA_KHO = @Ma";

            cmd.Parameters.Add(new SqlParameter("@Ma", strMa));
            cmd.Parameters.Add(new SqlParameter("@Ten", strTen));
            cmd.Parameters.Add(new SqlParameter("@DiaChi", sDiaChi));

            DAL.RunActionCmd(cmd);
        }

        public bool XoaKho(string strMa)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE DM_KHO WHERE MA_KHO = '" + strMa + "'";

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        //public DataTable SearchKho(string sMa, string sTen)
        //{
        //    SqlCommand cmd = new SqlCommand();
        //    cmd.CommandText = @"select MA_KHO AS MA, TEN_KHO AS TEN, DIA_CHI FROM DM_KHO";
        //    cmd.CommandText += " WHERE 1=1 ";
        //    if (!string.IsNullOrEmpty(sMa))
        //        cmd.CommandText += " AND MA_KHO LIKE N'%" + sMa.Replace("'", "''") + "%' ";
        //    if (!string.IsNullOrEmpty(sTen))
        //        cmd.CommandText += " AND TEN_KHO LIKE N'%" + sTen.Replace("'", "''") + "%'";

        //    DataTable dtb = new DataTable();
        //    dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
        //    return dtb;
        //}

        public DataTable SearchKho(string sMa, string sTen, string sMaDaChon = "")
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"select MA_KHO AS MA, TEN_KHO AS TEN, DIA_CHI, CAST(0 AS BIT) AS is_check FROM DM_KHO";
            cmd.CommandText += " WHERE 1=1 ";
            if (!string.IsNullOrEmpty(sMa))
                cmd.CommandText += " AND MA_KHO LIKE N'%" + sMa.Replace("'", "''") + "%' ";
            if (!string.IsNullOrEmpty(sTen))
                cmd.CommandText += " AND TEN_KHO LIKE N'%" + sTen.Replace("'", "''") + "%'";
            if (sMaDaChon != "")
                cmd.CommandText += " AND MA_KHO NOT IN (" + sMaDaChon + ")";
            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        #endregion

        #region ĐTSD

        public DataTable LayDanhSachDTSD(string strTimKiem)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT DT.MA_DTSDTS AS MA, DT.TEN_DTSDTS AS TEN, DT.DIA_CHI, DT.DIEN_THOAI, DT.FAX, DV.TEN_DVSDTS
                                FROM DM_DTSDTS DT INNER JOIN DM_DVSDTS DV ON DT.MA_DVSDTS = DV.MA_DVSDTS ";

            if (!string.IsNullOrEmpty(strTimKiem))
                cmd.CommandText += " WHERE DT.MA_DTSDTS LIKE N'%" + strTimKiem.Replace("'", "''") + "%' OR DT.TEN_DTSDTS LIKE N'%" + strTimKiem.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        public void ThemDTSD(string strMaDTSD, string strTenDTSD, string strDiaChi, string strDienThoai, string strFax, string sMaDVSD)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "INSERT INTO DM_DTSDTS(MA_DTSDTS,TEN_DTSDTS,DIA_CHI,DIEN_THOAI,FAX,MA_DVSDTS) VALUES (@MaDTSD, @TenDTSD,@DiaChi, @DienThoai, @FAX, @MaDVSD)";

            cmd.Parameters.Add(new SqlParameter("@MaDTSD", strMaDTSD));
            cmd.Parameters.Add(new SqlParameter("@TenDTSD", strTenDTSD));
            cmd.Parameters.Add(new SqlParameter("@DiaChi", strDiaChi));
            cmd.Parameters.Add(new SqlParameter("@DienThoai", strDienThoai));
            cmd.Parameters.Add(new SqlParameter("@FAX", strFax));
            cmd.Parameters.Add(new SqlParameter("@MaDVSD", sMaDVSD));

            DAL.RunActionCmd(cmd);
        }

        public void SuaDTSD(string strMaDTSD, string strTenDTSD, string strDiaChi, string strDienThoai, string strFax, string sMaDVSD)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "UPDATE DM_DTSDTS " +
                              "SET TEN_DTSDTS = @TenDTSDTS, DIA_CHI = @DiaChi, DIEN_THOAI = @DienThoai, FAX = @Fax, MA_DVSDTS = @MaDVSDTS " +
                              "WHERE MA_DTSDTS = @MaDTSDTS";

            cmd.Parameters.Add(new SqlParameter("@MaDTSDTS", strMaDTSD));
            cmd.Parameters.Add(new SqlParameter("@TenDTSDTS", strTenDTSD));
            cmd.Parameters.Add(new SqlParameter("@DiaChi", strDiaChi));
            cmd.Parameters.Add(new SqlParameter("@DienThoai", strDienThoai));
            cmd.Parameters.Add(new SqlParameter("@Fax", strFax));
            cmd.Parameters.Add(new SqlParameter("@MaDVSDTS", sMaDVSD));

            DAL.RunActionCmd(cmd);
        }

        public bool XoaDTSD(string strMaDTSD)
        {
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "DELETE DM_DTSDTS WHERE MA_DTSDTS = '" + strMaDTSD + "'";

                DAL.RunActionCmd(cmd);
            }
            catch (Exception ex)
            {
                // nếu có ràng buộc foreign key sẽ không xóa được
                return false;
            }
            return true;
        }

        public DataTable SearchDTSD(string sMa, string sTen, string sMaDVSD)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = @"SELECT DT.MA_DTSDTS AS MA, DT.TEN_DTSDTS AS TEN, DT.DIA_CHI, DT.DIEN_THOAI, DT.FAX, DV.TEN_DVSDTS
                                FROM DM_DTSDTS DT INNER JOIN DM_DVSDTS DV ON DT.MA_DVSDTS = DV.MA_DVSDTS";
            cmd.CommandText += " WHERE 1=1 ";
            if (!string.IsNullOrEmpty(sMaDVSD))
                cmd.CommandText += " AND DT.MA_DVSDTS = '" + sMaDVSD + "' ";
            if (!string.IsNullOrEmpty(sMa))
                cmd.CommandText += " AND MA_DTSDTS LIKE N'%" + sMa.Replace("'", "''") + "%' ";
            if (!string.IsNullOrEmpty(sTen))
                cmd.CommandText += " AND TEN_DTSDTS LIKE N'%" + sTen.Replace("'", "''") + "%'";

            DataTable dtb = new DataTable();
            dtb = DAL.RunCMDGetDataSet(cmd).Tables[0];
            return dtb;
        }

        #endregion
    }
}
