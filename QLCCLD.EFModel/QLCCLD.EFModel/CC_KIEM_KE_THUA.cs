//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QLCCLD.EFModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class CC_KIEM_KE_THUA
    {
        public decimal ID_KIEM_KE_THUA { get; set; }
        public Nullable<decimal> ID_BD { get; set; }
        public string TEN_CONG_CU { get; set; }
        public string THOI_GIAN_KIEM_KE { get; set; }
        public Nullable<decimal> SO_LUONG_SS { get; set; }
        public Nullable<decimal> SO_LUONG_KK { get; set; }
        public Nullable<decimal> DON_GIA { get; set; }
        public Nullable<decimal> DON_GIA_DANH_GIA_LAI { get; set; }
        public string MA_TINH_TRANG { get; set; }
        public Nullable<decimal> ID_KHO { get; set; }
        public string MA_DVSDTS { get; set; }
        public string MA_NHOM_CCLD { get; set; }
    
        public virtual CC_KIEM_KE CC_KIEM_KE { get; set; }
        public virtual DM_TINH_TRANG DM_TINH_TRANG { get; set; }
        public virtual DM_KHO DM_KHO { get; set; }
        public virtual DM_NHOM_CCLD DM_NHOM_CCLD { get; set; }
        public virtual DM_DVSDTS DM_DVSDTS { get; set; }
    }
}
