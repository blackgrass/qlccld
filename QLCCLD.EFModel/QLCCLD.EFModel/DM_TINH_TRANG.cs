//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QLCCLD.EFModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class DM_TINH_TRANG
    {
        public DM_TINH_TRANG()
        {
            this.CC_BAN_GIAO_CC = new HashSet<CC_BAN_GIAO_CC>();
            this.CC_KIEM_KE_CL = new HashSet<CC_KIEM_KE_CL>();
            this.CC_KIEM_KE_DU = new HashSet<CC_KIEM_KE_DU>();
            this.CC_KIEM_KE_THUA = new HashSet<CC_KIEM_KE_THUA>();
            this.CC_TANG = new HashSet<CC_TANG>();
            this.CC_TANG_LO = new HashSet<CC_TANG_LO>();
            this.CC_THANH_LY_CC = new HashSet<CC_THANH_LY_CC>();
        }
    
        public string MA_TINH_TRANG { get; set; }
        public string TEN_TINH_TRANG { get; set; }
    
        public virtual ICollection<CC_BAN_GIAO_CC> CC_BAN_GIAO_CC { get; set; }
        public virtual ICollection<CC_KIEM_KE_CL> CC_KIEM_KE_CL { get; set; }
        public virtual ICollection<CC_KIEM_KE_DU> CC_KIEM_KE_DU { get; set; }
        public virtual ICollection<CC_KIEM_KE_THUA> CC_KIEM_KE_THUA { get; set; }
        public virtual ICollection<CC_TANG> CC_TANG { get; set; }
        public virtual ICollection<CC_TANG_LO> CC_TANG_LO { get; set; }
        public virtual ICollection<CC_THANH_LY_CC> CC_THANH_LY_CC { get; set; }
    }
}
