//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QLCCLD.EFModel
{
    using System;
    
    public partial class sp_CC_NHAP_XUAT_Select_Result
    {
        public string SO_CHUNG_TU { get; set; }
        public string NGAY_CHUNG_TU { get; set; }
        public Nullable<decimal> TRANG_THAI { get; set; }
        public string SO_PHIEU { get; set; }
        public string NGAY_NHAP_XUAT { get; set; }
        public string NGUOI_GIAO_NHAN { get; set; }
        public string DIA_CHI_GIAO_NHAN { get; set; }
        public string SO_QUYET_DINH { get; set; }
        public string CAN_CU { get; set; }
        public string NGUOI_QUYET_DINH { get; set; }
        public string NGAY_QUYET_DINH { get; set; }
        public string MA_KHO { get; set; }
        public string DIA_CHI { get; set; }
        public string TEN_KHO { get; set; }
        public string DIEN_GIAI { get; set; }
        public string GHI_CHU { get; set; }
        public string THU_TRUONG_DON_VI { get; set; }
        public string KE_TOAN { get; set; }
        public string NGUOI_GIAO { get; set; }
        public string TRUONG_PHONG_KE_TOAN { get; set; }
        public string THU_KHO { get; set; }
    }
}
