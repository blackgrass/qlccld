//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QLCCLD.EFModel
{
    using System;
    
    public partial class sp_CC_TANG_CHI_TIET_LO_Result
    {
        public string MA_NGUON_GOC { get; set; }
        public string TEN_NGUON_GOC { get; set; }
        public string MA_DVSDTS { get; set; }
        public string TEN_DVSDTS { get; set; }
        public string MA_NHOM_CCLD { get; set; }
        public string TEN_NHOM_CCLD { get; set; }
        public string MA_LO_CCLD { get; set; }
        public string DON_VI_TINH { get; set; }
        public string TEN_LO_CCLD { get; set; }
        public Nullable<decimal> DON_GIA { get; set; }
        public Nullable<decimal> DON_GIA_DGL { get; set; }
        public Nullable<decimal> SO_LUONG { get; set; }
        public Nullable<decimal> TONG_TIEN { get; set; }
        public string MA_TINH_TRANG { get; set; }
        public string TEN_TINH_TRANG { get; set; }
        public Nullable<decimal> SO_THANG_BH { get; set; }
        public string NGAY_BH { get; set; }
    }
}
