﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using DevExpress.XtraLayout.Utils;
using QLCCLD.CCLD.BaoCao;
using QLCCLD.DanhMuc.DTSD;
using QLCCLD.DanhMuc.DVSD;
using QLCCLD.DanhMuc.NhomCCLD;
using EM.UI;

namespace QLCCLD.CCLD.BanGiao
{
    public partial class frmBanGiao : DialogBase
    {
        #region "Khai báo biến và tham số"

        public string _sTrangThai = "";
        public string _sSoBB = "";
        clsCCLD cc = new clsCCLD();
        DataTable dtChonCCLD = new DataTable();
        DataTable dtDaiDien = new DataTable();
        DataSet ds = new DataSet(); //chứa dữ liệu 1 giao dịch đã chọn
        List<string> lstDelCCLD = new List<string>();
        List<string> lstDelDaiDien = new List<string>();

        public frmBanGiao()
        {
            InitializeComponent();
        }
        #endregion

        #region "Các sự kiện"

        private void frmBanGiao_Load(object sender, EventArgs e)
        {
            CreateTable();
            if (_sTrangThai == "Search") //Tìm kiếm
            {
                Text = "Sửa biên bản bàn giao";
                LoadForm();
                VisiableControl(true);
            }
            else
            {
                Text = "Thêm biên bản bàn giao";
                ResetForm(); //Thêm mới
                VisiableControl(false);
            }
        }

        private void btnThemCCLD_Click(object sender, EventArgs e)
        {
            string sMaCCLDDaChon = "";
            for (int i = 0; i < gvCCLD.RowCount; i++)
            {
                if (i > 0)
                    sMaCCLDDaChon += ",'" + gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
                else sMaCCLDDaChon += "'" + gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
            }

            frmSearchCCLD frm = new frmSearchCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sNghiepVu = "2"; //bàn giao
            //frm._sEvent = "1"; //thêm mới lô
            frm.dtChonCCLD = dtChonCCLD.Copy();
            frm._sMaCCLDDaChon = sMaCCLDDaChon;
            frm.ShowDialog();
            dtChonCCLD = frm.dtChonCCLD.Copy();

            gcCCLD.DataSource = dtChonCCLD;
            gvCCLD.BestFitColumns();
        }

        private void btnThemDaiDien_Click(object sender, EventArgs e)
        {
            frmDaiDien frm = new frmDaiDien();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sEvent = "Insert";
            frm.dtDaiDien = dtDaiDien.Copy();
            frm.ShowDialog();
            dtDaiDien = frm.dtDaiDien.Copy();
            gcDaiDien.DataSource = dtDaiDien;
            gvDaiDien.BestFitColumns();
        }

        private void btnDVSD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.DVSD.frmDVSDTK frm = new frmDVSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();

            btnDVSD.Text = frm._sMa;
            txtTenDVSD.Text = frm._sTen;
            btnDTSD.Text = "";
            txtTenDTSD.Text = "";
        }

        private void btnDTSD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.DTSD.frmDTSDTK frm = new frmDTSDTK();
            frm._sMaDVSD = btnDVSD.Text;
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnDTSD.Text = frm._sMa;
            txtTenDTSD.Text = frm._sTen;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
            {
                DialogResult = DialogResult.None;
                return;
            }

            if (string.IsNullOrEmpty(txtSoBienBan.Text)) //thêm mới
            {
                if (gvCCLD.RowCount < 1 || gvDaiDien.RowCount < 1)
                {
                    XtraMessageBox.Show("Lỗi: Chưa chọn CCLĐ hoặc chưa nhập thông tin đại diện");
                }
                else
                {
                    if (SaveData())
                        lblMessage.Text = "Ghi dữ liệu thành công!";
                    else lblMessage.Text = "Không ghi được dữ liệu!";
                    _sTrangThai = "Search";
                    LoadForm();
                    VisiableControl(true);
                }
            }
            else //sửa
            {
                if (SaveData())
                    lblMessage.Text = "Ghi dữ liệu thành công!";
                else lblMessage.Text = "Không ghi được dữ liệu!";
                _sTrangThai = "Search";
                LoadForm();
                VisiableControl(true);
            }
        }

        private void btnXoaCCLD_Click(object sender, EventArgs e)
        {
            if (gvCCLD.RowCount != 0)
            {
                List<string> lstDelCCLDOnDtChonCCLD = new List<string>();
                if (XtraMessageBox.Show("Xóa những CCLĐ này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // hiển thị câu hỏi xác nhận xóa
                {
                    foreach (int i in gvCCLD.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string sMa = gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString(); // GetRowCellValue để lấy ra giá trị của cột MA ở dòng thứ i
                        if (!string.IsNullOrEmpty(sMa))
                            lstDelCCLD.Add(sMa); //lấy MA_CCLD của các bản ghi cần xóa trong cơ sở dữ liệu
                        lstDelCCLDOnDtChonCCLD.Add(sMa);//lấy MA_CCLD của các bản ghi cần xóa trong datatable dtChonCCLD
                    }
                    for (int i = 0; i < lstDelCCLDOnDtChonCCLD.Count; i++) //xóa CCLĐ tại dtChonCCLD
                    {
                        DataRow[] dr = dtChonCCLD.Select("MA_CCLD='" + lstDelCCLDOnDtChonCCLD[i] + "'");
                        dtChonCCLD.Rows.Remove(dr[0]);
                    }
                    gcCCLD.DataSource = dtChonCCLD;
                    gvCCLD.BestFitColumns();
                    gvCCLD.SelectRow(0);
                }
            }
        }

        private void btnXoaDaiDien_Click(object sender, EventArgs e)
        {
            if (gvDaiDien.RowCount != 0)
            {

                if (XtraMessageBox.Show("Xóa những đại diện này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // hiển thị câu hỏi xác nhận xóa
                {
                    foreach (int i in gvDaiDien.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string sIDBanGiaoHD = gvDaiDien.GetRowCellValue(i, "ID_BAN_GIAO_HD").ToString();
                        if (!string.IsNullOrEmpty(sIDBanGiaoHD))
                            lstDelDaiDien.Add(sIDBanGiaoHD); //lấy ID_BAN_GIAO_HD của các bản ghi cần xóa trong cơ sở dữ liệu                       
                    }
                    for (int i = 0; i < lstDelDaiDien.Count; i++)
                    {
                        DataRow[] dr = dtDaiDien.Select("ID_BAN_GIAO_HD='" + lstDelDaiDien[i] + "'");
                        dtDaiDien.Rows.Remove(dr[0]);
                    }

                    gcDaiDien.DataSource = dtDaiDien;
                    gvDaiDien.SelectRow(0);

                }
                lstDelDaiDien.Clear();
            }
        }

        private void LinkCCLD_Click(object sender, EventArgs e)
        {
            frmChiTietCCLD frm = new frmChiTietCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;

            frm._sMaCCLD = gvCCLD.GetFocusedRowCellValue("MA_CCLD").ToString();
            frm._sTenCCLD = gvCCLD.GetFocusedRowCellValue("TEN_CCLD").ToString();
            frm._sDonGia = gvCCLD.GetFocusedRowCellValue("DON_GIA").ToString();
            frm._sMaDuAn = gvCCLD.GetFocusedRowCellValue("MA_DU_AN").ToString();
            frm._sTenDuAn = gvCCLD.GetFocusedRowCellValue("TEN_DU_AN").ToString();
            frm._sMaTinhTrang = gvCCLD.GetFocusedRowCellValue("MA_TINH_TRANG").ToString();
            frm._sTenTinhTrang = gvCCLD.GetFocusedRowCellValue("TEN_TINH_TRANG").ToString();
            frm._sGhiChu = gvCCLD.GetFocusedRowCellValue("GHI_CHU").ToString();

            if (_sTrangThai == "Search") //xem chi tiết CCLD
            {
                frm._sEvent = "View"; //xem 
            }
            else //sửa 
            {
                frm._sEvent = "Edit"; //sửa 
            }
            frm.ShowDialog();

            int iIndex = gvCCLD.GetSelectedRows()[0];
            dtChonCCLD.Rows[iIndex]["MA_TINH_TRANG"] = frm._sMaTinhTrang;
            dtChonCCLD.Rows[iIndex]["TEN_TINH_TRANG"] = frm._sTenTinhTrang;
            dtChonCCLD.Rows[iIndex]["MA_DU_AN"] = frm._sMaDuAn;
            dtChonCCLD.Rows[iIndex]["TEN_DU_AN"] = frm._sTenDuAn;
            dtChonCCLD.Rows[iIndex]["GHI_CHU"] = frm._sGhiChu;

            gcCCLD.DataSource = dtChonCCLD;
            gvCCLD.BestFitColumns();
        }

        private void LinkHoTen_Click(object sender, EventArgs e)
        {
            frmDaiDien frm = new frmDaiDien();
            frm.StartPosition = FormStartPosition.CenterParent;

            frm._sHoTen = gvDaiDien.GetFocusedRowCellValue("HO_VA_TEN").ToString();
            frm._sChucVu = gvDaiDien.GetFocusedRowCellValue("CHUC_VU").ToString();
            frm._sDaiDien = gvDaiDien.GetFocusedRowCellValue("DAI_DIEN").ToString();


            if (_sTrangThai == "Search") //xem chi tiết CCLD
            {
                frm._sEvent = "View"; //xem 
            }
            else //sửa 
            {
                frm._sEvent = "Edit"; //sửa 
            }
            frm.ShowDialog();
            int iIndex = gvDaiDien.GetSelectedRows()[0];
            dtDaiDien.Rows[iIndex]["HO_VA_TEN"] = frm._sHoTen;
            dtDaiDien.Rows[iIndex]["CHUC_VU"] = frm._sChucVu;
            dtDaiDien.Rows[iIndex]["DAI_DIEN"] = frm._sDaiDien;

            gcDaiDien.DataSource = dtDaiDien;
            gvDaiDien.BestFitColumns();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _sTrangThai = "Update";
            VisiableControl(false);
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            if (_sTrangThai == "Insert")
            {
                LoadForm();
                this.Close();
            }
            else
            {
                _sTrangThai = "Search";
                LoadForm();
                VisiableControl(true);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult Result = XtraMessageBox.Show(this, "Xóa biên bản bàn giao " + txtSoBienBan.Text + "?", "Xóa biên bản bàn giao",
               MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Result == DialogResult.Yes)
            {
                for (int i = 0; i < gvCCLD.RowCount; i++)
                {
                    cc.DelBanGiaoCC(txtSoBienBan.Text, gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString());
                }

                cc.DeleteBanGiao(txtSoBienBan.Text);
                this.Close();
            }
        }

        private void btnDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetBanGiao(txtSoBienBan.Text, "1")) //trạng thái = 1: đã duyệt
                lblMessage.Text = "Bạn đã duyệt thành công";
            else
                lblMessage.Text = "Bạn đã duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnTuChoiDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetBanGiao(txtSoBienBan.Text, "2")) //trang thái = 2: từ chối duyệt
                lblMessage.Text = "Bạn đã từ chối duyệt thành công";
            else
                lblMessage.Text = "Bạn đã từ chối duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnHuyDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetBanGiao(txtSoBienBan.Text, "3")) //trạng thái = 3: hủy duyệt
                lblMessage.Text = "Bạn đã hủy duyệt thành công";
            else
                lblMessage.Text = "Bạn đã hủy duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            BaoCao.frmReport frm = new frmReport();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sNghiepVu = "2"; //bàn giao
            frm._sThamSo = txtSoBienBan.Text;
            frm.ShowDialog();
        }

        #endregion

        #region Các hàm

        private void VisiableControl(bool bHide)
        {
            if (_sTrangThai == "Insert") //thêm mới
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemDaiDien.Visibility = LayoutVisibility.Always;
                lcXoaDaiDien.Visibility = LayoutVisibility.Always;
                lcThemCCLD.Visibility = LayoutVisibility.Always;
                lcXoaCCLD.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Never;
            }
            else if (_sTrangThai == "Search") //tìm kiếm
            {
                lcGhi.Visibility = LayoutVisibility.Never;
                lcHuyBo.Visibility = LayoutVisibility.Never;
                lcThemDaiDien.Visibility = LayoutVisibility.Never;
                lcXoaDaiDien.Visibility = LayoutVisibility.Never;
                lcThemCCLD.Visibility = LayoutVisibility.Never;
                lcXoaCCLD.Visibility = LayoutVisibility.Never;

                lcSua.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcXoa.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcHuyDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Always;
            }
            else //sửa thông tin
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemDaiDien.Visibility = LayoutVisibility.Always;
                lcXoaDaiDien.Visibility = LayoutVisibility.Always;
                lcThemCCLD.Visibility = LayoutVisibility.Always;
                lcXoaCCLD.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Never;
            }
            datNgayBanGiao.Properties.ReadOnly = bHide;
            btnDVSD.Properties.Enabled = !bHide;
            btnDTSD.Properties.Enabled = !bHide;
            txtDiaDiem.Properties.ReadOnly = bHide;
        }

        private void LoadForm()
        {
            //load thông tin chung
            ds = cc.GetBBBanGiao(_sSoBB);
            if (ds.Tables[0].Rows.Count < 1)
                return;
            switch (ds.Tables[0].Rows[0]["TRANG_THAI"].ToString())
            {
                case "0":
                    txtTrangThai.Text = "Chờ duyệt";
                    break;
                case "1":
                    txtTrangThai.Text = "Đã duyệt";
                    break;
                case "2":
                    txtTrangThai.Text = "Từ chối duyệt";
                    break;
                case "3":
                    txtTrangThai.Text = "Hủy duyệt";
                    break;
            }
            txtSoBienBan.Text = _sSoBB;
            txtDiaDiem.Text = ds.Tables[0].Rows[0]["DIA_DIEM"].ToString();
            datNgayBanGiao.Text = ds.Tables[0].Rows[0]["NGAY_BIEN_BAN"].ToString();
            btnDVSD.Text = ds.Tables[0].Rows[0]["MA_DVSDTS"].ToString();
            txtTenDVSD.Text = ds.Tables[0].Rows[0]["TEN_DVSDTS"].ToString();
            btnDTSD.Text = ds.Tables[0].Rows[0]["MA_DTSDTS"].ToString();
            txtTenDTSD.Text = ds.Tables[0].Rows[0]["TEN_DTSDTS"].ToString();
            txtDaiDienGiao.Text = ds.Tables[0].Rows[0]["DAI_DIEN_BEN_GIAO"].ToString();
            txtDaiDienNhan.Text = ds.Tables[0].Rows[0]["DAI_DIEN_BEN_NHAN"].ToString();

            //load grid thông tin lô cclđ
            gcCCLD.DataSource = ds.Tables[1];
            gvCCLD.BestFitColumns(); //điều chỉnh độ rộng của cột tự động

            gcDaiDien.DataSource = ds.Tables[2];
            gvCCLD.BestFitColumns();

            dtChonCCLD = ds.Tables[1].Copy();
            dtDaiDien = ds.Tables[2].Copy();
        }

        private bool SaveData()
        {
            bool bResult = true;
            try
            {

                //if (lstDelCCLD.Count > 0) //xóa CCLĐ
                //{
                //    for (int i = 0; i < lstDelCCLD.Count; i++)
                //    {
                //        bResult = cc.DelCCLDBanGiao(lstDelCCLD[i]);
                //    }
                //}

                //if (lstDelDaiDien.Count > 0) //xóa đại diện
                //{
                //    for (int i = 0; i < lstDelDaiDien.Count; i++)
                //    {
                //        bResult = cc.DelDaiDienBanGiao(lstDelDaiDien[i]);
                //    }
                //}

                if (_sTrangThai == "Insert") //thêm mới bàn giao
                {
                    string sSoBB = cc.GetSoBBBanGiao();
                    bResult = cc.InsertBanGiao(datNgayBanGiao.Text, sSoBB, btnDVSD.Text, btnDTSD.Text,
                                               txtDaiDienGiao.Text, txtDaiDienNhan.Text,
                                               txtDiaDiem.Text);
                    _sSoBB = sSoBB;
                }
                else //sửa thông tin bàn giao
                {
                    bResult = cc.UpdateBanGiao(datNgayBanGiao.Text, txtSoBienBan.Text, btnDVSD.Text, btnDTSD.Text,
                                               txtDaiDienGiao.Text, txtDaiDienNhan.Text,
                                               txtDiaDiem.Text);
                    _sSoBB = txtSoBienBan.Text;
                }
                //thêm CCLĐ
                foreach (DataRow dr in dtChonCCLD.Rows)
                {
                    bResult = cc.InsertBanGiaoCC(_sSoBB, dr["MA_CCLD"].ToString(), dr["MA_TINH_TRANG"].ToString(),
                                                        dr["GHI_CHU"].ToString(), dr["MA_DU_AN"].ToString());
                    //if (cc.CheckMaCCLDBanGiao(dr["MA_CCLD"].ToString(), txtSoBienBan.Text).Rows.Count == 0) //thêm mới
                    //{
                    //    if (string.IsNullOrEmpty(txtSoBienBan.Text)) //thêm mới bàn giao và chi tiết bàn giao
                    //        bResult = cc.InsertBanGiaoCC(sSoBB, dr["MA_CCLD"].ToString(), datNgayBanGiao.Text, dr["MA_TINH_TRANG"].ToString(),
                    //                                    dr["GHI_CHU"].ToString(), dr["MA_DU_AN"].ToString());
                    //    else
                    //    { //thêm CCLĐ tại số biên bản bàn giao đã có
                    //        bResult = cc.InsertBanGiaoCC(txtSoBienBan.Text, dr["MA_CCLD"].ToString(), datNgayBanGiao.Text, dr["MA_TINH_TRANG"].ToString(),
                    //                                    dr["GHI_CHU"].ToString(), dr["MA_DU_AN"].ToString());
                    //    }
                    //}
                    //else //sửa
                    //{
                    //    bResult = cc.UpdateBanGiaoCC(txtSoBienBan.Text, dr["MA_CCLD"].ToString(), datNgayBanGiao.Text, dr["MA_TINH_TRANG"].ToString(),
                    //                                    dr["GHI_CHU"].ToString(), dr["MA_DU_AN"].ToString());
                    //}
                }

                //thêm Đại diện
                foreach (DataRow dr in dtDaiDien.Rows)
                {
                    bResult = cc.InsertBanGiaoHD(_sSoBB, dr["HO_VA_TEN"].ToString(), dr["CHUC_VU"].ToString(), dr["DAI_DIEN"].ToString());
                    //if (string.IsNullOrEmpty(dr["ID_BAN_GIAO_HD"].ToString())) //thêm mới
                    //{
                    //    if (string.IsNullOrEmpty(txtSoBienBan.Text)) //thêm mới bàn giao và đại diện
                    //        bResult = cc.InsertBanGiaoHD(sSoBB, dr["HO_VA_TEN"].ToString(), dr["CHUC_VU"].ToString(), dr["DAI_DIEN"].ToString());
                    //    else
                    //    { //thêm đại diện tại số biên bản bàn giao đã có
                    //        bResult = cc.InsertBanGiaoHD(txtSoBienBan.Text, dr["HO_VA_TEN"].ToString(), dr["CHUC_VU"].ToString(), dr["DAI_DIEN"].ToString());
                    //    }
                    //}
                    //else //sửa
                    //{
                    //    bResult = cc.UpdateBanGiaoHD(dr["ID_BAN_GIAO_HD]"].ToString(), dr["HO_VA_TEN"].ToString(), dr["CHUC_VU"].ToString(), dr["DAI_DIEN"].ToString());
                    //}

                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return bResult;
        }

        private void ResetForm()
        {
            txtTrangThai.ResetText();
            datNgayBanGiao.ResetText();
            btnDVSD.ResetText();
            btnDTSD.ResetText();
            txtDiaDiem.ResetText();
            txtDaiDienGiao.ResetText();
            txtDaiDienNhan.ResetText();
        }

        private void CreateTable()
        {
            //tạo bảng CCLĐ bàn giao
            dtChonCCLD.Columns.Add("MA_CCLD");
            dtChonCCLD.Columns.Add("TEN_CCLD");
            //dtChonCCLD.Columns.Add("DON_VI_TINH");
            dtChonCCLD.Columns.Add("DON_GIA");
            dtChonCCLD.Columns.Add("TONG_TIEN");
            dtChonCCLD.Columns.Add("MA_DU_AN");
            dtChonCCLD.Columns.Add("TEN_DU_AN");
            dtChonCCLD.Columns.Add("SO_LUONG");
            dtChonCCLD.Columns.Add("MA_TINH_TRANG");
            dtChonCCLD.Columns.Add("TEN_TINH_TRANG");
            dtChonCCLD.Columns.Add("GHI_CHU");

            //tạo bảng đại diện
            dtDaiDien.Columns.Add("ID_BAN_GIAO_HD");
            dtDaiDien.Columns.Add("HO_VA_TEN");
            dtDaiDien.Columns.Add("CHUC_VU");
            dtDaiDien.Columns.Add("DAI_DIEN");
        }

        private bool DuyetBanGiao(string sSoBB, string sTrangThai)
        {
            bool bResult = true;
            try
            {
                bResult = cc.DuyetBanGiao(sSoBB, sTrangThai);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bResult;
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            btnDVSD.ValidateEmptyStringRule(); // check bắt buộc nhập
            datNgayBanGiao.ValidateEmptyStringRule();
        }

        #endregion





    }
}