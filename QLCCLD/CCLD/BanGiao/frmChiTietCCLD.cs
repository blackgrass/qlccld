﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using QLCCLD.DanhMuc.DuAn;
using QLCCLD.DanhMuc.TinhTrang;

namespace QLCCLD.CCLD.BanGiao
{
    public partial class frmChiTietCCLD : DevExpress.XtraEditors.XtraForm
    {
        public string _sMaCCLD = "";
        public string _sTenCCLD = "";
        public string _sDonGia = "";
        public string _sTongTien = "";
        public string _sMaTinhTrang = "";
        public string _sTenTinhTrang = "";
        public string _sMaDuAn = "";
        public string _sTenDuAn = "";
        public string _sGhiChu = "";
        public string _sEvent = "";

        public frmChiTietCCLD()
        {
            InitializeComponent();
        }

        private void frmChiTietCCLD_Load(object sender, EventArgs e)
        {
            txtMaCCLD.Text = _sMaCCLD;
            txtTenCCLD.Text = _sTenCCLD;
            txtDonGia.Text = _sDonGia;
            txtSoLuong.Text = "1";
            txtThanhTien.Text = _sTongTien;
            btnMaDuAn.Text = _sMaDuAn;
            txtTenDuAn.Text = _sTenDuAn;
            btnMaTinhTrang.Text = _sMaTinhTrang;
            txtTenTinhTrang.Text = _sTenTinhTrang;
            txtGhiChu.Text = _sGhiChu;
            if (_sEvent == "View")
                HideControl(false);
            else HideControl(true); //edit
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            _sMaTinhTrang = btnMaTinhTrang.Text;
            _sTenTinhTrang = txtTenTinhTrang.Text;
            _sMaDuAn = btnMaDuAn.Text;
            _sTenDuAn = txtTenDuAn.Text;
            _sGhiChu = txtGhiChu.Text;
            this.Close();
        }

        private void HideControl(bool bHide)
        {
            btnLuu.Enabled = bHide;
            btnMaDuAn.Enabled = bHide;
            btnMaTinhTrang.Enabled = bHide;
            txtGhiChu.Properties.ReadOnly = !bHide;
        }

        private void btnMaTinhTrang_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.TinhTrang.frmTinhTrangTK frm = new frmTinhTrangTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaTinhTrang.Text = frm._sMa;
            txtTenTinhTrang.Text = frm._sTen;
        }

        private void btnMaDuAn_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.DuAn.frmDuAnTK frm = new frmDuAnTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDuAn.Text = frm._sMa;
            txtTenDuAn.Text = frm._sTen;
        }
    }
}