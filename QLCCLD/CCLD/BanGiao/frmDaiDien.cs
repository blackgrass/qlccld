﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using EM.UI;

namespace QLCCLD.CCLD.BanGiao
{
    public partial class frmDaiDien : DialogBase
    {
        public string _sEvent = "";
        public string _sHoTen = "";
        public string _sChucVu = "";
        public string _sDaiDien = "";
        public int _sID = 0;
        public DataTable dtDaiDien = new DataTable();
        clsCCLD cc = new clsCCLD();

        public frmDaiDien()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmDaiDien_Load(object sender, EventArgs e)
        {
            if (_sEvent == "View")
            {
                LoadForm();
                HideControl(true);
                btnLuuVaThem.Enabled = false;
            }
            else if (_sEvent == "Insert")
            {
                ResetForm();
                HideControl(false);
                btnLuuVaThem.Enabled = true;
            }
            else //Edit
            {
                LoadForm();
                HideControl(false);
                btnLuuVaThem.Enabled = false;
            }
        }

        private void LoadForm()
        {
            txtHoTen.Text = _sHoTen;
            txtChucVu.Text = _sChucVu;
            chkBenGiao.Checked = _sDaiDien =="Bên giao" ? true : false;
            chkBenNhan.Checked = _sDaiDien == "Bên giao" ? false : true;
        }

        private void HideControl(bool bHide)
        {
            txtHoTen.Properties.ReadOnly = bHide;
            txtChucVu.Properties.ReadOnly = bHide;
            chkBenGiao.Properties.ReadOnly = bHide;
            chkBenNhan.Properties.ReadOnly = bHide;
            btnLuu.Enabled = !bHide;
            
        }

        private void ResetForm()
        {
            txtHoTen.ResetText();
            txtChucVu.ResetText();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (HasErrors)
                return;
            SaveData();
            this.Close();
        }

        private void btnLuuVaThem_Click(object sender, EventArgs e)
        {
            if (HasErrors)
                return;
            SaveData();
            ResetForm();
        }

        private void SaveData()
        {
            if (_sEvent == "Insert")
            {
                string id_bangiao = "";
                if (_sID == 0)               
                    id_bangiao = cc.GetIdDaiDienBanGiao();                                  
                else
                    id_bangiao = (_sID + 1).ToString();
                _sID = int.Parse(id_bangiao);
                DataRow dr = dtDaiDien.NewRow();
                dr["ID_BAN_GIAO_HD"] = id_bangiao;
                dr["HO_VA_TEN"] = txtHoTen.Text;
                dr["CHUC_VU"] = txtChucVu.Text;
                dr["DAI_DIEN"] = chkBenGiao.Checked ? "Bên giao" : "Bên nhận";
                dtDaiDien.Rows.Add(dr);
            }
            else //Edit
            {
                _sHoTen = txtHoTen.Text;
                _sChucVu = txtChucVu.Text;
                _sDaiDien = chkBenGiao.Checked ? "Bên giao" : "Bên nhận";
            }
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            txtHoTen.ValidateEmptyStringRule(); // check bắt buộc nhập
            txtChucVu.ValidateEmptyStringRule();
        }
    }
}