﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using QLCCLD.DanhMuc.DVSD;
using QLCCLD.DanhMuc.NhomCCLD;

namespace QLCCLD.CCLD.BanGiao
{
    public partial class ucBanGiao : DevExpress.XtraEditors.XtraUserControl
    {
        clsCCLD cc = new clsCCLD();

        public ucBanGiao()
        {
            InitializeComponent();
        }

        private void ucBanGiao_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void LoadDanhSach()
        {
            gcBanGiao.DataSource = cc.SearchBanGiao(txtSoBBTu.Text,txtSoBBDen.Text,datThoiGianTu.Text,datThoiGianDen.Text,
                                                    btnMaLoCCLD.Text,btnMaDVSD.Text,btnMaCCLD.Text,chkChoDuyet.Checked,chkDaDuyet.Checked,
                                                    chkTuChoiDuyet.Checked,chkHuyDuyet.Checked);
            gvBanGiao.BestFitColumns();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmBanGiao f = new frmBanGiao();
            f.StartPosition = FormStartPosition.CenterParent;
            f._sTrangThai = "Insert";
            f.ShowDialog();
            LoadDanhSach();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void gvBanGiao_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void LinkSoBienBan_Click(object sender, EventArgs e)
        {
            frmBanGiao frm = new frmBanGiao();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sTrangThai = "Search";
            frm._sSoBB = gvBanGiao.GetFocusedRowCellValue("SO_BIEN_BAN").ToString();
            frm.ShowDialog();
            LoadDanhSach();
        }

        private void btnMaLoCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmLoCCLD frm = new frmLoCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaLoCCLD.Text = frm._sMa;
            txtTenLoCCLD.Text = frm._sTen;
        }

        private void btnMaDVSD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.DVSD.frmDVSDTK frm = new frmDVSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDVSD.Text = frm._sMa;
            txtTenDVSD.Text = frm._sTen;
        }

        private void btnMaCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmNhomCCLDTK frm = new frmNhomCCLDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaCCLD.Text = frm._sMa;
            txtTenCCLD.Text = frm._sTen;
        }


    }
}
