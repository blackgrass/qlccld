﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EM.UI;

namespace QLCCLD.CCLD.BaoCao
{
    public partial class frmBaoCaoSoKho : DialogBase
    {
        public frmBaoCaoSoKho()
        {
            InitializeComponent();
        }

        private void btnMaKho_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.Kho.frmKhoTK frm = new DanhMuc.Kho.frmKhoTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaKho.Text = frm._sMa;
            txtTenKho.Text = frm._sTen;           
        }

        //private void btnMaNhomCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        //{
        //    QLCCLD.DanhMuc.NhomCCLD.frmNhomCCLDTK frm = new QLCCLD.DanhMuc.NhomCCLD.frmNhomCCLDTK();
        //    frm.StartPosition = FormStartPosition.CenterParent;
        //    frm.ShowDialog();
        //    btnMaNhomCCLD.Text = frm._sMa;
        //    txtTenNhomCCLD.Text = frm._sTen;            
        //}

        private void btnMaLoCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmLoCCLD frm = new DanhMuc.NhomCCLD.frmLoCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaLoCCLD.Text = frm._sMa;
            txtTenLoCCLD.Text = frm._sTen;
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            if (HasErrors)
            {
                DialogResult = DialogResult.None;
                return;
            }

            frmReport frm = new frmReport();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sNghiepVu = "9"; //sổ kho
            frm._sMaLoCCLD = btnMaLoCCLD.Text;
            frm._sTuNgay = dtTuNgay.Text;
            frm._sDenNgay = dtDenNgay.Text;
            frm._sMaKho = btnMaKho.Text;
            frm.ShowDialog();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            dtTuNgay.ValidateEmptyStringRule(); // check bắt buộc nhập
            dtDenNgay.ValidateEmptyStringRule();
            btnMaKho.ValidateEmptyStringRule();
            btnMaLoCCLD.ValidateEmptyStringRule();
        }
    }
}