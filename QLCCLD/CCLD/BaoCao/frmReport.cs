﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.CCLD.BaoCao
{
    public partial class frmReport : DevExpress.XtraEditors.XtraForm
    {
        public string _sNghiepVu = "";
        public string _sThamSo = "";
        public int _iHienThi = 0;
        public string _sMaKho = "";
        public string _sMaLoCCLD = "";
        public string _sTuNgay = "";
        public string _sDenNgay = "";
        private string _sTenFileRpt = "";
        public frmReport()
        {
            InitializeComponent();
        }
        clsBaoCao bc = new clsBaoCao();
        public void ReportLoad()
        {
            ReportDocument rd = new ReportDocument();
            DataSet ds = new DataSet();
            switch (_sNghiepVu)
            {
                case "2":
                    ds = bc.BanGiao(_sThamSo);
                    ds.Tables[0].TableName = "TBL1";
                    ds.Tables[1].TableName = "TBL2";
                    ds.Tables[2].TableName = "TBL3";
                    _sTenFileRpt = "\\BienBanBanGiao.rpt";
                    this.Text = "Biên bản bàn giao";
                    break;
                case "3":
                    ds = bc.NhapKho(_sThamSo);
                    ds.Tables[0].TableName = "TBL1";
                    _sTenFileRpt = "\\NhapKho.rpt";
                    this.Text = "Phiếu nhập kho";
                    break;
                case "4":
                    ds = bc.XuatKho(_sThamSo);
                    ds.Tables[0].TableName = "TBL1";
                    _sTenFileRpt = "\\XuatKho.rpt";
                    this.Text = "Phiếu xuất kho";
                    break;
                case "5":
                    ds = bc.DieuChuyenCungDVSD(_sThamSo);
                    ds.Tables[0].TableName = "TBL1";
                    _sTenFileRpt = "\\DieuChuyenCungDVSD.rpt";
                    this.Text = "Biên bản điều chuyển CCLĐ";
                    break;
                case "6":
                    ds = bc.DieuChuyenKhacDVSD(_sThamSo);
                    ds.Tables[0].TableName = "TBL1";
                    _sTenFileRpt = "\\DieuChuyenKhacDVSD.rpt";
                    this.Text = "Biên bản điều chuyển CCLĐ";
                    break;
                case "7":
                    ds = bc.ThanhLy(_sThamSo,_iHienThi); //0: theo CCLĐ, 1: theo lô
                    ds.Tables[0].TableName = "TBL1";
                    ds.Tables[1].TableName = "TBL2";
                    ds.Tables[2].TableName = "TBL3";
                    _sTenFileRpt = "\\ThanhLy.rpt";
                    this.Text = "Biên bản thanh lý";
                    break;
                case "8":
                    ds = bc.KiemKe(_sThamSo,_iHienThi,_sMaKho); //0: kiểm kê CCLĐ đang sử dụng; 1: kiểm kê CCLĐ trong kho
                    ds.Tables[0].TableName = "TBL1";
                    ds.Tables[1].TableName = "TBL2";
                    
                    if (_iHienThi == 0)

                        _sTenFileRpt = "\\KiemKe_M1.rpt";
                    else
                    {
                        ds.Tables[2].TableName = "TBL3";
                        _sTenFileRpt = "\\KiemKe_M2.rpt";
                    }
                    this.Text = "Biên bản kiểm kê";
                    break;
                case "9": //sổ kho
                    ds = bc.SoKho(_sMaLoCCLD, _sTuNgay, _sDenNgay, _sMaKho);
                    ds.Tables[0].TableName = "TBL1";
                    ds.Tables[1].TableName = "TBL2";
                    _sTenFileRpt = "\\SoKho.rpt";
                    this.Text = "Sổ kho (Thẻ kho)";
                    break;

            }

            rd.Load(Application.StartupPath + _sTenFileRpt);
            rd.SetDataSource(ds);
            crvXem.ReportSource = rd;
            crvXem.Refresh();
        }

        private void frmReport_Load(object sender, EventArgs e)
        {
            ReportLoad();
        }
    }
}