﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace QLCCLD.CCLD.BaoCao
{
    public partial class ucBaoCao : DevExpress.XtraEditors.XtraUserControl
    {
        public DataTable dtb = new DataTable();
        public ucBaoCao()
        {
            InitializeComponent();
        }

        private void ucBaoCao_Load(object sender, EventArgs e)
        {
            dtb.Columns.Add("STT", typeof(string));
            dtb.Columns.Add("TenBaoCao", typeof(string));
            dtb.Columns.Add("MaBaoCao", typeof(string));

            dtb.Rows.Add("1", "Sổ kho Công cụ lao động", "1");
            dtb.Rows.Add("2", "Kiểm kê Công cụ lao động", "2");

            gcBaoCao.DataSource = dtb;
        }

        private void linkXem_Click(object sender, EventArgs e)
        {
            string ma = gvBaoCao.GetFocusedRowCellValue("MaBaoCao").ToString();

            switch (ma)
            {
                case "1":
                    frmBaoCaoSoKho f1 = new frmBaoCaoSoKho();
                    f1.StartPosition = FormStartPosition.CenterParent;
                    f1.ShowDialog();
                    break;
                case "2":
                    frmBaoCaoKiemKe f2 = new frmBaoCaoKiemKe();
                    f2.StartPosition = FormStartPosition.CenterParent;
                    f2.ShowDialog();
                    break;
            }
        }

        private void linkXem_CustomDisplayText(object sender, DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs e)
        {
            e.DisplayText = "";
        }

        
    }
}
