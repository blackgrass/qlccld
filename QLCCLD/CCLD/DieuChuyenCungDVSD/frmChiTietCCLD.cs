﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EM.UI;

namespace QLCCLD.CCLD.DieuChuyenCungDVSD
{
    public partial class frmChiTietCCLD : DialogBase
    {
        public string _sMaDVSD = "";
        public string _sMaCCLD = "";
        public string _sTenCCLD = "";
        public string _sDonGia = "";
        public string _sNgayTang = "";
        public string _sMaDTSD_DieuChuyenDi = "";
        public string _sTenDTSD_DieuChuyenDi = "";
        public string _sMaDTSD_NhanDieuChuyen = "";
        public string _sTenDTSD_NhanDieuChuyen = "";
        public string _sMaDuAn_NhanDieuChuyen = "";
        public string _sTenDuAn_NhanDieuChuyen = "";
        public string _sEvent = ""; //0: xem; 1: sửa

        public frmChiTietCCLD()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmChiTietCCLD_Load(object sender, EventArgs e)
        {
            txtMaCCLD.Text = _sMaCCLD;
            txtTenCCLD.Text = _sTenCCLD;
            txtDonGia.Text = _sDonGia;
            txtNgayTang.Text = _sNgayTang;
            txtMaDTSDDieuChuyenDi.Text = _sMaDTSD_DieuChuyenDi;
            txtTenDTSDDieuChuyenDi.Text = _sTenDTSD_DieuChuyenDi;
            btnMaDTSDNhanDieuChuyen.Text = _sMaDTSD_NhanDieuChuyen;
            txtTenDTSDNhanDieuChuyen.Text = _sTenDTSD_NhanDieuChuyen;
            btnMaDuAn.Text = _sMaDuAn_NhanDieuChuyen;
            txtTenDuAn.Text = _sTenDuAn_NhanDieuChuyen;
            btnMaDTSDNhanDieuChuyen.Enabled = _sEvent == "0" ? false : true;
            btnMaDuAn.Enabled = _sEvent == "0" ? false : true;
            btnLuu.Enabled = _sEvent == "0" ? false : true;
        }

        private void btnMaDTSDNhanDieuChuyen_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.DTSD.frmDTSDTK frm = new QLCCLD.DanhMuc.DTSD.frmDTSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sMaDVSD = _sMaDVSD;
            frm.ShowDialog();
            btnMaDTSDNhanDieuChuyen.Text = frm._sMa;
            txtTenDTSDNhanDieuChuyen.Text = frm._sTen;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if(HasErrors)
                return;
            _sMaDTSD_NhanDieuChuyen = btnMaDTSDNhanDieuChuyen.Text;
            _sTenDTSD_NhanDieuChuyen = txtTenDTSDNhanDieuChuyen.Text;
            _sMaDuAn_NhanDieuChuyen = btnMaDuAn.Text;
            _sTenDuAn_NhanDieuChuyen = txtTenDuAn.Text;
            this.Close();
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            btnMaDTSDNhanDieuChuyen.ValidateCustom(CheckMa); // check bắt buộc nhập        
        }

        private string CheckMa(Control c) // hàm validate mã ko được để trống hoặc đã tồn tại
        {
            ButtonEdit ma = (ButtonEdit)c;
            if (string.IsNullOrEmpty(ma.Text) && string.IsNullOrEmpty(btnMaDuAn.Text))
                return "Phải nhập ĐTSD hoặc Dự án.";

            return string.Empty;
        }

        private void btnMaDuAn_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.DuAn.frmDuAnTK frm = new QLCCLD.DanhMuc.DuAn.frmDuAnTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDuAn.Text = frm._sMa;
            txtTenDuAn.Text = frm._sTen;
        }
    }
}