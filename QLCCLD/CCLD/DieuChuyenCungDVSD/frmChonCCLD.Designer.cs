﻿namespace QLCCLD.CCLD.DieuChuyenCungDVSD
{
    partial class frmChonCCLD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtTenDTSD = new DevExpress.XtraEditors.TextEdit();
            this.btnMaDTSD = new DevExpress.XtraEditors.ButtonEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtTenDuAnNhan = new DevExpress.XtraEditors.TextEdit();
            this.btnMaDuAnNhan = new DevExpress.XtraEditors.ButtonEdit();
            this.txtTenDTSDNhan = new DevExpress.XtraEditors.TextEdit();
            this.btnMaDTSDNhan = new DevExpress.XtraEditors.ButtonEdit();
            this.txtTenDVSD = new DevExpress.XtraEditors.TextEdit();
            this.txtMaDVSD = new DevExpress.XtraEditors.TextEdit();
            this.txtTenLoCCLD = new DevExpress.XtraEditors.TextEdit();
            this.txtTenNhomCCLD = new DevExpress.XtraEditors.TextEdit();
            this.btnThoat = new DevExpress.XtraEditors.SimpleButton();
            this.btnChon = new DevExpress.XtraEditors.SimpleButton();
            this.gcCCLD = new DevExpress.XtraGrid.GridControl();
            this.gvCCLD = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnTimKiem = new DevExpress.XtraEditors.SimpleButton();
            this.btnMaLoCCLD = new DevExpress.XtraEditors.ButtonEdit();
            this.btnMaNhomCCLD = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDTSD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDTSD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDuAnNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDuAnNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDTSDNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDTSDNhan.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDVSD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDVSD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenLoCCLD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNhomCCLD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcCCLD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCCLD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaLoCCLD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaNhomCCLD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.labelControl2);
            this.layoutControl1.Controls.Add(this.txtTenDTSD);
            this.layoutControl1.Controls.Add(this.btnMaDTSD);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.txtTenDuAnNhan);
            this.layoutControl1.Controls.Add(this.btnMaDuAnNhan);
            this.layoutControl1.Controls.Add(this.txtTenDTSDNhan);
            this.layoutControl1.Controls.Add(this.btnMaDTSDNhan);
            this.layoutControl1.Controls.Add(this.txtTenDVSD);
            this.layoutControl1.Controls.Add(this.txtMaDVSD);
            this.layoutControl1.Controls.Add(this.txtTenLoCCLD);
            this.layoutControl1.Controls.Add(this.txtTenNhomCCLD);
            this.layoutControl1.Controls.Add(this.btnThoat);
            this.layoutControl1.Controls.Add(this.btnChon);
            this.layoutControl1.Controls.Add(this.gcCCLD);
            this.layoutControl1.Controls.Add(this.btnTimKiem);
            this.layoutControl1.Controls.Add(this.btnMaLoCCLD);
            this.layoutControl1.Controls.Add(this.btnMaNhomCCLD);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(771, 526);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(12, 115);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(86, 13);
            this.labelControl2.StyleController = this.layoutControl1;
            this.labelControl2.TabIndex = 21;
            this.labelControl2.Text = "Thông tin tìm kiếm";
            // 
            // txtTenDTSD
            // 
            this.txtTenDTSD.Location = new System.Drawing.Point(475, 132);
            this.txtTenDTSD.Name = "txtTenDTSD";
            this.txtTenDTSD.Properties.ReadOnly = true;
            this.txtTenDTSD.Size = new System.Drawing.Size(284, 20);
            this.txtTenDTSD.StyleController = this.layoutControl1;
            this.txtTenDTSD.TabIndex = 20;
            // 
            // btnMaDTSD
            // 
            this.btnMaDTSD.Location = new System.Drawing.Point(105, 132);
            this.btnMaDTSD.Name = "btnMaDTSD";
            this.btnMaDTSD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnMaDTSD.Properties.ReadOnly = true;
            this.btnMaDTSD.Size = new System.Drawing.Size(283, 20);
            this.btnMaDTSD.StyleController = this.layoutControl1;
            this.btnMaDTSD.TabIndex = 19;
            this.btnMaDTSD.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMaDTSD_ButtonClick);
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 12);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(77, 13);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 18;
            this.labelControl1.Text = "Thông tin chung";
            // 
            // txtTenDuAnNhan
            // 
            this.txtTenDuAnNhan.Location = new System.Drawing.Point(475, 77);
            this.txtTenDuAnNhan.Name = "txtTenDuAnNhan";
            this.txtTenDuAnNhan.Properties.ReadOnly = true;
            this.txtTenDuAnNhan.Size = new System.Drawing.Size(284, 20);
            this.txtTenDuAnNhan.StyleController = this.layoutControl1;
            this.txtTenDuAnNhan.TabIndex = 17;
            // 
            // btnMaDuAnNhan
            // 
            this.btnMaDuAnNhan.Location = new System.Drawing.Point(105, 77);
            this.btnMaDuAnNhan.Name = "btnMaDuAnNhan";
            this.btnMaDuAnNhan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnMaDuAnNhan.Properties.ReadOnly = true;
            this.btnMaDuAnNhan.Size = new System.Drawing.Size(283, 20);
            this.btnMaDuAnNhan.StyleController = this.layoutControl1;
            this.btnMaDuAnNhan.TabIndex = 16;
            this.btnMaDuAnNhan.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMaDuAnNhan_ButtonClick);
            // 
            // txtTenDTSDNhan
            // 
            this.txtTenDTSDNhan.Location = new System.Drawing.Point(475, 53);
            this.txtTenDTSDNhan.Name = "txtTenDTSDNhan";
            this.txtTenDTSDNhan.Properties.ReadOnly = true;
            this.txtTenDTSDNhan.Size = new System.Drawing.Size(284, 20);
            this.txtTenDTSDNhan.StyleController = this.layoutControl1;
            this.txtTenDTSDNhan.TabIndex = 15;
            // 
            // btnMaDTSDNhan
            // 
            this.btnMaDTSDNhan.Location = new System.Drawing.Point(105, 53);
            this.btnMaDTSDNhan.Name = "btnMaDTSDNhan";
            this.btnMaDTSDNhan.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnMaDTSDNhan.Properties.ReadOnly = true;
            this.btnMaDTSDNhan.Size = new System.Drawing.Size(283, 20);
            this.btnMaDTSDNhan.StyleController = this.layoutControl1;
            this.btnMaDTSDNhan.TabIndex = 14;
            this.btnMaDTSDNhan.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMaDTSDNhan_ButtonClick);
            // 
            // txtTenDVSD
            // 
            this.txtTenDVSD.Location = new System.Drawing.Point(475, 29);
            this.txtTenDVSD.Name = "txtTenDVSD";
            this.txtTenDVSD.Properties.ReadOnly = true;
            this.txtTenDVSD.Size = new System.Drawing.Size(284, 20);
            this.txtTenDVSD.StyleController = this.layoutControl1;
            this.txtTenDVSD.TabIndex = 13;
            // 
            // txtMaDVSD
            // 
            this.txtMaDVSD.Location = new System.Drawing.Point(105, 29);
            this.txtMaDVSD.Name = "txtMaDVSD";
            this.txtMaDVSD.Properties.ReadOnly = true;
            this.txtMaDVSD.Size = new System.Drawing.Size(283, 20);
            this.txtMaDVSD.StyleController = this.layoutControl1;
            this.txtMaDVSD.TabIndex = 12;
            // 
            // txtTenLoCCLD
            // 
            this.txtTenLoCCLD.Location = new System.Drawing.Point(475, 180);
            this.txtTenLoCCLD.Name = "txtTenLoCCLD";
            this.txtTenLoCCLD.Properties.ReadOnly = true;
            this.txtTenLoCCLD.Size = new System.Drawing.Size(284, 20);
            this.txtTenLoCCLD.StyleController = this.layoutControl1;
            this.txtTenLoCCLD.TabIndex = 11;
            // 
            // txtTenNhomCCLD
            // 
            this.txtTenNhomCCLD.Location = new System.Drawing.Point(475, 156);
            this.txtTenNhomCCLD.Name = "txtTenNhomCCLD";
            this.txtTenNhomCCLD.Properties.ReadOnly = true;
            this.txtTenNhomCCLD.Size = new System.Drawing.Size(284, 20);
            this.txtTenNhomCCLD.StyleController = this.layoutControl1;
            this.txtTenNhomCCLD.TabIndex = 10;
            // 
            // btnThoat
            // 
            this.btnThoat.Image = global::QLCCLD.Properties.Resources.cancel_16x16;
            this.btnThoat.Location = new System.Drawing.Point(683, 492);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(76, 22);
            this.btnThoat.StyleController = this.layoutControl1;
            this.btnThoat.TabIndex = 9;
            this.btnThoat.Text = "Đóng";
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnChon
            // 
            this.btnChon.Image = global::QLCCLD.Properties.Resources.apply_16x16;
            this.btnChon.Location = new System.Drawing.Point(603, 492);
            this.btnChon.Name = "btnChon";
            this.btnChon.Size = new System.Drawing.Size(76, 22);
            this.btnChon.StyleController = this.layoutControl1;
            this.btnChon.TabIndex = 8;
            this.btnChon.Text = "Chọn";
            this.btnChon.Click += new System.EventHandler(this.btnChon_Click);
            // 
            // gcCCLD
            // 
            this.gcCCLD.Location = new System.Drawing.Point(12, 246);
            this.gcCCLD.MainView = this.gvCCLD;
            this.gcCCLD.Name = "gcCCLD";
            this.gcCCLD.Size = new System.Drawing.Size(747, 242);
            this.gcCCLD.TabIndex = 7;
            this.gcCCLD.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvCCLD});
            // 
            // gvCCLD
            // 
            this.gvCCLD.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1,
            this.STT,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6,
            this.gridColumn7,
            this.gridColumn8});
            this.gvCCLD.GridControl = this.gcCCLD;
            this.gvCCLD.Name = "gvCCLD";
            this.gvCCLD.OptionsView.ShowGroupPanel = false;
            this.gvCCLD.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gvCCLD_CustomColumnDisplayText);
            this.gvCCLD.Click += new System.EventHandler(this.gvCCLD_Click);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Chọn";
            this.gridColumn1.FieldName = "is_check";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // STT
            // 
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.Visible = true;
            this.STT.VisibleIndex = 1;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Mã CCLĐ";
            this.gridColumn3.FieldName = "MA_CCLD";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Tên CCLĐ";
            this.gridColumn4.FieldName = "TEN_CCLD";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Đơn giá/Đơn giá ĐGL";
            this.gridColumn5.FieldName = "DON_GIA";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 4;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Thời hạn bảo hành";
            this.gridColumn6.FieldName = "SO_THANG_BH";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 5;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Ngày tăng";
            this.gridColumn7.FieldName = "NGAY_TANG";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 6;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Nhóm CCLĐ";
            this.gridColumn8.FieldName = "TEN_NHOM_CCLD";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 7;
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Image = global::QLCCLD.Properties.Resources.zoom_16x16;
            this.btnTimKiem.Location = new System.Drawing.Point(683, 204);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(76, 22);
            this.btnTimKiem.StyleController = this.layoutControl1;
            this.btnTimKiem.TabIndex = 6;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // btnMaLoCCLD
            // 
            this.btnMaLoCCLD.Location = new System.Drawing.Point(105, 180);
            this.btnMaLoCCLD.Name = "btnMaLoCCLD";
            this.btnMaLoCCLD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnMaLoCCLD.Properties.ReadOnly = true;
            this.btnMaLoCCLD.Size = new System.Drawing.Size(283, 20);
            this.btnMaLoCCLD.StyleController = this.layoutControl1;
            this.btnMaLoCCLD.TabIndex = 5;
            this.btnMaLoCCLD.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMaLoCCLD_ButtonClick);
            // 
            // btnMaNhomCCLD
            // 
            this.btnMaNhomCCLD.Location = new System.Drawing.Point(105, 156);
            this.btnMaNhomCCLD.Name = "btnMaNhomCCLD";
            this.btnMaNhomCCLD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnMaNhomCCLD.Properties.ReadOnly = true;
            this.btnMaNhomCCLD.Size = new System.Drawing.Size(283, 20);
            this.btnMaNhomCCLD.StyleController = this.layoutControl1;
            this.btnMaNhomCCLD.TabIndex = 4;
            this.btnMaNhomCCLD.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMaNhomCCLD_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem8,
            this.emptySpaceItem1,
            this.emptySpaceItem2,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.emptySpaceItem3,
            this.emptySpaceItem4,
            this.emptySpaceItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(771, 526);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnMaNhomCCLD;
            this.layoutControlItem1.CustomizationFormText = "Mã nhóm CCLĐ";
            this.layoutControlItem1.Location = new System.Drawing.Point(10, 144);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(370, 24);
            this.layoutControlItem1.Text = "Mã nhóm CCLĐ";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnMaLoCCLD;
            this.layoutControlItem2.CustomizationFormText = "Mã lô CCLĐ";
            this.layoutControlItem2.Location = new System.Drawing.Point(10, 168);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(370, 24);
            this.layoutControlItem2.Text = "Mã lô CCLĐ";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnTimKiem;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(671, 192);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gcCCLD;
            this.layoutControlItem4.CustomizationFormText = "Danh sách CCLĐ";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 218);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(751, 262);
            this.layoutControlItem4.Text = "Danh sách CCLĐ";
            this.layoutControlItem4.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.btnChon;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(591, 480);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.btnThoat;
            this.layoutControlItem6.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItem6.Location = new System.Drawing.Point(671, 480);
            this.layoutControlItem6.MaxSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem6.MinSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem6.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem6.Text = "layoutControlItem6";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextToControlDistance = 0;
            this.layoutControlItem6.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtTenNhomCCLD;
            this.layoutControlItem7.CustomizationFormText = "Tên nhóm CCLĐ";
            this.layoutControlItem7.Location = new System.Drawing.Point(380, 144);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(371, 24);
            this.layoutControlItem7.Text = "Tên nhóm CCLĐ";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.txtTenLoCCLD;
            this.layoutControlItem8.CustomizationFormText = "Tên Lô CCLĐ";
            this.layoutControlItem8.Location = new System.Drawing.Point(380, 168);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(371, 24);
            this.layoutControlItem8.Text = "Tên Lô CCLĐ";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(80, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 192);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(671, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 480);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(591, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtMaDVSD;
            this.layoutControlItem9.CustomizationFormText = "Mã ĐVSD";
            this.layoutControlItem9.Location = new System.Drawing.Point(10, 17);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(370, 24);
            this.layoutControlItem9.Text = "Mã ĐVSD";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtTenDVSD;
            this.layoutControlItem10.CustomizationFormText = "Tên ĐVSD";
            this.layoutControlItem10.Location = new System.Drawing.Point(380, 17);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(371, 24);
            this.layoutControlItem10.Text = "Tên ĐVSD";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btnMaDTSDNhan;
            this.layoutControlItem11.CustomizationFormText = "Mã ĐTSD nhận";
            this.layoutControlItem11.Location = new System.Drawing.Point(10, 41);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(370, 24);
            this.layoutControlItem11.Text = "Mã ĐTSD nhận";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.txtTenDTSDNhan;
            this.layoutControlItem12.CustomizationFormText = "Tên ĐTSD nhận";
            this.layoutControlItem12.Location = new System.Drawing.Point(380, 41);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(371, 24);
            this.layoutControlItem12.Text = "Tên ĐTSD nhận";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.btnMaDuAnNhan;
            this.layoutControlItem13.CustomizationFormText = "Mã dự án nhận";
            this.layoutControlItem13.Location = new System.Drawing.Point(10, 65);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(370, 24);
            this.layoutControlItem13.Text = "Mã dự án nhận";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtTenDuAnNhan;
            this.layoutControlItem14.CustomizationFormText = "Tên dự án nhận";
            this.layoutControlItem14.Location = new System.Drawing.Point(380, 65);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(371, 24);
            this.layoutControlItem14.Text = "Tên dự án nhận";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.labelControl1;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(751, 17);
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.btnMaDTSD;
            this.layoutControlItem16.CustomizationFormText = "Mã ĐTSD";
            this.layoutControlItem16.Location = new System.Drawing.Point(10, 120);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(370, 24);
            this.layoutControlItem16.Text = "Mã ĐTSD";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.txtTenDTSD;
            this.layoutControlItem17.CustomizationFormText = "Tên ĐTSD";
            this.layoutControlItem17.Location = new System.Drawing.Point(380, 120);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(371, 24);
            this.layoutControlItem17.Text = "Tên ĐTSD";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(80, 13);
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.labelControl2;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 103);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(751, 17);
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 17);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(10, 72);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.CustomizationFormText = "emptySpaceItem4";
            this.emptySpaceItem4.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(10, 72);
            this.emptySpaceItem4.Text = "emptySpaceItem4";
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 89);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(751, 14);
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // frmChonCCLD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(771, 526);
            this.Controls.Add(this.layoutControl1);
            this.Name = "frmChonCCLD";
            this.Text = "Chọn CCLĐ điều chuyển";
            this.Load += new System.EventHandler(this.frmChonCCLD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDTSD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDTSD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDuAnNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDuAnNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDTSDNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDTSDNhan.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDVSD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDVSD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenLoCCLD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenNhomCCLD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcCCLD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvCCLD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaLoCCLD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaNhomCCLD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txtTenLoCCLD;
        private DevExpress.XtraEditors.TextEdit txtTenNhomCCLD;
        private DevExpress.XtraEditors.SimpleButton btnThoat;
        private DevExpress.XtraEditors.SimpleButton btnChon;
        private DevExpress.XtraGrid.GridControl gcCCLD;
        private DevExpress.XtraGrid.Views.Grid.GridView gvCCLD;
        private DevExpress.XtraEditors.SimpleButton btnTimKiem;
        private DevExpress.XtraEditors.ButtonEdit btnMaLoCCLD;
        private DevExpress.XtraEditors.ButtonEdit btnMaNhomCCLD;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraEditors.TextEdit txtMaDVSD;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraEditors.TextEdit txtTenDuAnNhan;
        private DevExpress.XtraEditors.ButtonEdit btnMaDuAnNhan;
        private DevExpress.XtraEditors.TextEdit txtTenDTSDNhan;
        private DevExpress.XtraEditors.ButtonEdit btnMaDTSDNhan;
        private DevExpress.XtraEditors.TextEdit txtTenDVSD;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.TextEdit txtTenDTSD;
        private DevExpress.XtraEditors.ButtonEdit btnMaDTSD;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
    }
}