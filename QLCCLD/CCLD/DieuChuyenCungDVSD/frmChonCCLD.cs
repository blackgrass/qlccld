﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using QLCCLD.DanhMuc.NhomCCLD;

namespace QLCCLD.CCLD.DieuChuyenCungDVSD
{
    public partial class frmChonCCLD : DevExpress.XtraEditors.XtraForm
    {
        clsCCLD cc = new clsCCLD();
        public string _sMaCCLDDaChon = "";
        public string _sMaDVSD = "";
        public string _sTenDVSD = "";
        public DataTable dtChonCCLD = new DataTable();

        public frmChonCCLD()
        {
            InitializeComponent();
        }

        private void frmChonCCLD_Load(object sender, EventArgs e)
        {
            txtMaDVSD.Text = _sMaDVSD;
            txtTenDVSD.Text = _sTenDVSD;
            LoadDanhSach();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadDanhSach()
        {
            gcCCLD.DataSource = cc.LayCCLDDieuChuyenCungDVSD(btnMaNhomCCLD.Text, btnMaLoCCLD.Text, _sMaCCLDDaChon, _sMaDVSD, btnMaDTSD.Text);
            gvCCLD.BestFitColumns();
        }

        private void gvCCLD_Click(object sender, EventArgs e)
        {
            //check all 
            if (gvCCLD.PressedColumn == gvCCLD.Columns["is_check"])
            {
                int count = 0;
                for (int i = 0; i < gvCCLD.RowCount; i++)
                {
                    if ((bool)gvCCLD.GetRowCellValue(i, gvCCLD.Columns["is_check"]) == true)
                        count++;
                }

                if (count == gvCCLD.RowCount)
                {
                    for (int j = 0; j < gvCCLD.RowCount; j++)
                        gvCCLD.SetRowCellValue(j, "is_check", false);
                }
                else
                {
                    for (int i = 0; i < gvCCLD.RowCount; i++)
                        gvCCLD.SetRowCellValue(i, "is_check", true);
                }
            }
        }

        private void gvCCLD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gvCCLD.RowCount; i++)
            {
                if ((bool)gvCCLD.GetRowCellValue(i, "is_check") == true)
                {
                    DataRow dr = dtChonCCLD.NewRow();
                    dr["MA_CCLD"] = gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString();
                    dr["TEN_CCLD"] = gvCCLD.GetRowCellValue(i, "TEN_CCLD").ToString();
                    dr["DON_GIA"] = gvCCLD.GetRowCellValue(i, "DON_GIA").ToString();
                    dr["NGAY_TANG"] = gvCCLD.GetRowCellValue(i, "NGAY_TANG").ToString();
                    dr["DTSD_DIEU_CHUYEN_DI"] = gvCCLD.GetRowCellValue(i, "MA_DTSDTS").ToString();
                    dr["TEN_DTSD_DIEU_CHUYEN_DI"] = gvCCLD.GetRowCellValue(i, "TEN_DTSDTS").ToString();
                    dr["DU_AN_DIEU_CHUYEN_DI"] = gvCCLD.GetRowCellValue(i, "MA_DU_AN").ToString();
                    dr["TEN_DU_AN_DIEU_CHUYEN_DI"] = gvCCLD.GetRowCellValue(i, "TEN_DU_AN").ToString();
                    dr["DTSD_DIEU_CHUYEN_DEN"] = btnMaDTSDNhan.Text;
                    dr["TEN_DTSD_DIEU_CHUYEN_DEN"] = txtTenDTSDNhan.Text;
                    dr["DU_AN_DIEU_CHUYEN_DEN"] = btnMaDuAnNhan.Text;
                    dr["TEN_DU_AN_DIEU_CHUYEN_DEN"] = txtTenDuAnNhan.Text;

                    dtChonCCLD.Rows.Add(dr);
                }
            }
            this.Close();
        }

        private void btnMaNhomCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmNhomCCLDTK frm = new frmNhomCCLDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaNhomCCLD.Text = frm._sMa;
            txtTenNhomCCLD.Text = frm._sTen;
        }

        private void btnMaLoCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmLoCCLD frm = new frmLoCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaLoCCLD.Text = frm._sMa;
            txtTenLoCCLD.Text = frm._sTen;
        }

        private void btnMaDTSD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.DTSD.frmDTSDTK frm = new QLCCLD.DanhMuc.DTSD.frmDTSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sMaDVSD = _sMaDVSD;
            frm.ShowDialog();
            btnMaDTSD.Text = frm._sMa;
            txtTenDTSD.Text = frm._sTen;
        }

        private void btnMaDTSDNhan_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.DTSD.frmDTSDTK frm = new QLCCLD.DanhMuc.DTSD.frmDTSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sMaDVSD = _sMaDVSD;
            frm.ShowDialog();
            btnMaDTSDNhan.Text = frm._sMa;
            txtTenDTSDNhan.Text = frm._sTen;
        }

        private void btnMaDuAnNhan_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.DuAn.frmDuAnTK frm = new QLCCLD.DanhMuc.DuAn.frmDuAnTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDuAnNhan.Text = frm._sMa;
            txtTenDuAnNhan.Text = frm._sTen;
        }
    }
}