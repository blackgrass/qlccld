﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using DevExpress.XtraLayout.Utils;
using EM.UI;
using QLCCLD.CCLD.BaoCao;

namespace QLCCLD.CCLD.DieuChuyenCungDVSD
{
    public partial class frmDieuChuyenCungDVSD : DialogBase
    {
        #region Khai báo biến và tham số
        public string _sTrangThai = "";
        public string _sSoGiaoDich = "";
        clsCCLD cc = new clsCCLD();
        DataTable dtChonCCLD = new DataTable();
        DataSet ds = new DataSet(); //chứa dữ liệu 1 số giao dịch đã chọn
        List<string> lstDelCCLD = new List<string>();

        public frmDieuChuyenCungDVSD()
        {
            InitializeComponent();
        }

        #endregion

        #region Các sự kiện
        private void frmDieuChuyenCungDVSD_Load(object sender, EventArgs e)
        {
            CreateTable();
            if (_sTrangThai == "Search") //Tìm kiếm
            {
                LoadForm();
                VisiableControl(true);
            }
            else
            {
                ResetForm(); //Thêm mới
                VisiableControl(false);
            }
        }

        private void btnThemCCLD_Click(object sender, EventArgs e)
        {
            if (HasErrors)
                return;
            string sMaCCLDDaChon = "";
            for (int i = 0; i < gvCCLD.RowCount; i++)
            {
                if (i > 0)
                    sMaCCLDDaChon += ",'" + gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
                else sMaCCLDDaChon += "'" + gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
            }

            frmChonCCLD frm = new frmChonCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;

            frm.dtChonCCLD = dtChonCCLD.Copy();
            frm._sMaCCLDDaChon = sMaCCLDDaChon;
            frm._sMaDVSD = btnMaDVSD.Text;
            frm._sTenDVSD = txtTenDVSD.Text;
            frm.ShowDialog();
            dtChonCCLD = frm.dtChonCCLD.Copy();

            gcCCLD.DataSource = dtChonCCLD;
            gvCCLD.BestFitColumns();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
            {
                DialogResult = DialogResult.None;
                return;
            }

            if (gvCCLD.RowCount < 1)
            {
                XtraMessageBox.Show("Lỗi: Chưa chọn CCLĐ");
                return;
            }

            if (SaveData())
                lblMessage.Text = "Ghi dữ liệu thành công!";
            else lblMessage.Text = "Không ghi được dữ liệu!";
            _sTrangThai = "Search";
            LoadForm();
            VisiableControl(true);

        }      

        private void btnXoaCCLD_Click(object sender, EventArgs e)
        {
            if (gvCCLD.RowCount != 0)
            {
                if (XtraMessageBox.Show("Xóa những CCLĐ này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (int i in gvCCLD.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string sMa = gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString(); // GetRowCellValue để lấy ra giá trị của cột MA ở dòng thứ i
                        if (!string.IsNullOrEmpty(sMa))
                            lstDelCCLD.Add(sMa); //lấy MA_CCLD của các bản ghi cần xóa trong cơ sở dữ liệu

                        dtChonCCLD.Rows.Remove(dtChonCCLD.Rows[i]);
                    }
                    gcCCLD.DataSource = dtChonCCLD;
                }
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _sTrangThai = "Update";
            VisiableControl(false);
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            if (_sTrangThai == "Insert")
            {
                LoadForm();
                this.Close();
            }
            else
            {
                _sTrangThai = "Search";
                LoadForm();
                VisiableControl(true);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult Result = XtraMessageBox.Show(this, "Xóa giao dịch điều chuyển " + txtSoGiaoDich.Text + "?", "Xóa giao dịch điều chuyển",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Result == DialogResult.Yes)
            {
                // xóa CCLD 
                foreach (DataRow dr in dtChonCCLD.Rows)
                {
                    cc.DeleteDieuChuyenCT(txtSoGiaoDich.Text, dr["MA_CCLD"].ToString(), "0", "5");
                }
                cc.DeleteDieuChuyen(txtSoGiaoDich.Text, "0", "5");
                this.Close();
            }
        }

        private void btnDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetDieuChuyen(txtSoGiaoDich.Text, "1", "0")) //trạng thái = 1: đã duyệt; loại = 0: cùng ĐVSD
                lblMessage.Text = "Bạn đã duyệt thành công";
            else
                lblMessage.Text = "Bạn đã duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnTuChoiDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetDieuChuyen(txtSoGiaoDich.Text, "2", "0")) //trang thái = 2: từ chối duyệt; loại = 0: cùng ĐVSD
                lblMessage.Text = "Bạn đã từ chối duyệt thành công";
            else
                lblMessage.Text = "Bạn đã từ chối duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnHuyDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetDieuChuyen(txtSoGiaoDich.Text, "3", "0")) //trạng thái = 3: hủy duyệt; loại = 0: cùng ĐVSD
                lblMessage.Text = "Bạn đã hủy duyệt thành công";
            else
                lblMessage.Text = "Bạn đã hủy duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnMaDVSD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.DVSD.frmDVSDTK frm = new QLCCLD.DanhMuc.DVSD.frmDVSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDVSD.Text = frm._sMa;
            txtTenDVSD.Text = frm._sTen;
        }

        private void LinkMaCCLD_Click(object sender, EventArgs e)
        {
            frmChiTietCCLD frm = new frmChiTietCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;

            frm._sMaDVSD = btnMaDVSD.Text;
            frm._sMaCCLD = gvCCLD.GetFocusedRowCellValue("MA_CCLD").ToString();
            frm._sTenCCLD = gvCCLD.GetFocusedRowCellValue("TEN_CCLD").ToString();
            frm._sDonGia = gvCCLD.GetFocusedRowCellValue("DON_GIA").ToString();
            frm._sNgayTang = gvCCLD.GetFocusedRowCellValue("NGAY_TANG").ToString();
            frm._sMaDTSD_DieuChuyenDi = gvCCLD.GetFocusedRowCellValue("DTSD_DIEU_CHUYEN_DI").ToString();
            frm._sTenDTSD_DieuChuyenDi = gvCCLD.GetFocusedRowCellValue("TEN_DTSD_DIEU_CHUYEN_DI").ToString();
            frm._sMaDTSD_NhanDieuChuyen = gvCCLD.GetFocusedRowCellValue("DTSD_DIEU_CHUYEN_DEN").ToString();
            frm._sTenDTSD_NhanDieuChuyen = gvCCLD.GetFocusedRowCellValue("TEN_DTSD_DIEU_CHUYEN_DEN").ToString();
            frm._sMaDuAn_NhanDieuChuyen = gvCCLD.GetFocusedRowCellValue("DU_AN_DIEU_CHUYEN_DEN").ToString();
            frm._sTenDuAn_NhanDieuChuyen = gvCCLD.GetFocusedRowCellValue("TEN_DU_AN_DIEU_CHUYEN_DEN").ToString();

            if (_sTrangThai == "Search") //xem chi tiết CCLD
            {
                frm._sEvent = "0"; //xem 
            }
            else //sửa 
            {
                frm._sEvent = "1"; //sửa 
            }
            frm.ShowDialog();
            int iIndex = gvCCLD.GetSelectedRows()[0];
            dtChonCCLD.Rows[iIndex]["DTSD_DIEU_CHUYEN_DEN"] = frm._sMaDTSD_NhanDieuChuyen;
            dtChonCCLD.Rows[iIndex]["TEN_DTSD_DIEU_CHUYEN_DEN"] = frm._sTenDTSD_NhanDieuChuyen;
            dtChonCCLD.Rows[iIndex]["DU_AN_DIEU_CHUYEN_DEN"] = frm._sMaDuAn_NhanDieuChuyen;
            dtChonCCLD.Rows[iIndex]["TEN_DU_AN_DIEU_CHUYEN_DEN"] = frm._sTenDuAn_NhanDieuChuyen;

            gcCCLD.DataSource = dtChonCCLD;
            gvCCLD.BestFitColumns();
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            BaoCao.frmReport frm = new frmReport();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sNghiepVu = "5"; //điều chuyển cùng ĐVSD
            frm._sThamSo = txtSoGiaoDich.Text;
            frm.ShowDialog();
        }

        #endregion

        #region Các hàm

        private void VisiableControl(bool bHide)
        {
            if (_sTrangThai == "Insert") //thêm mới
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemLo.Visibility = LayoutVisibility.Always;
                lcXoaLo.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Never;
            }
            else if (_sTrangThai == "Search") //tìm kiếm
            {
                lcGhi.Visibility = LayoutVisibility.Never;
                lcHuyBo.Visibility = LayoutVisibility.Never;
                lcThemLo.Visibility = LayoutVisibility.Never;
                lcXoaLo.Visibility = LayoutVisibility.Never;

                lcSua.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcXoa.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcHuyDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Always;
            }
            else //sửa thông tin
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemLo.Visibility = LayoutVisibility.Always;
                lcXoaLo.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Never;
            }

            btnMaDVSD.Properties.Enabled = !bHide;
            txtDienGiai.Properties.ReadOnly = bHide;
            datNgayDieuChuyen.Properties.ReadOnly = bHide;

        }

        private void LoadForm()
        {
            //load thông tin chung
            ds = cc.GetGiaoDichDieuChuyen(_sSoGiaoDich, "0"); //
            if (ds.Tables[0].Rows.Count < 1)
                return;
            switch (ds.Tables[0].Rows[0]["TRANG_THAI"].ToString())
            {
                case "0":
                    txtTrangThai.Text = "Chờ duyệt";
                    break;
                case "1":
                    txtTrangThai.Text = "Đã duyệt";
                    break;
                case "2":
                    txtTrangThai.Text = "Từ chối duyệt";
                    break;
                case "3":
                    txtTrangThai.Text = "Hủy duyệt";
                    break;
            }
            txtSoGiaoDich.Text = _sSoGiaoDich;
            datNgayDieuChuyen.Text = ds.Tables[0].Rows[0]["NGAY_DIEU_CHUYEN"].ToString();
            txtDienGiai.Text = ds.Tables[0].Rows[0]["DIEN_GIAI"].ToString();
            btnMaDVSD.Text = ds.Tables[0].Rows[0]["MA_DVSDTS_DI"].ToString();
            txtTenDVSD.Text = ds.Tables[0].Rows[0]["TEN_DVSDTS_DI"].ToString();
            //load grid thông tin lô cclđ
            gcCCLD.DataSource = ds.Tables[1];
            gvCCLD.BestFitColumns(); //điều chỉnh độ rộng của cột tự động
            dtChonCCLD = ds.Tables[1].Copy();
        }

        private bool SaveData()
        {
            bool bResult = true;
            try
            {
                if (_sTrangThai == "Insert") //thêm mới 
                {
                    string sSoGiaoDich = cc.GetSoGiaoDichDieuChuyen();
                    bResult = cc.InsertDieuChuyenCungDVSD(datNgayDieuChuyen.Text, sSoGiaoDich, btnMaDVSD.Text, txtDienGiai.Text, "0");
                    _sSoGiaoDich = sSoGiaoDich;
                }
                else //sửa thông tin
                {
                    bResult = cc.UpdateDieuChuyenCungDVSD(datNgayDieuChuyen.Text, txtSoGiaoDich.Text, btnMaDVSD.Text, txtDienGiai.Text, "0", "5");
                    _sSoGiaoDich = txtSoGiaoDich.Text;
                }
                //thêm CCLĐ
                foreach (DataRow dr in dtChonCCLD.Rows)
                {
                    bResult = cc.InsertDieuChuyenCungDVSDCT(_sSoGiaoDich, dr["MA_CCLD"].ToString(), btnMaDVSD.Text, dr["DTSD_DIEU_CHUYEN_DI"].ToString(), dr["DTSD_DIEU_CHUYEN_DEN"].ToString(), dr["DU_AN_DIEU_CHUYEN_DI"].ToString(), dr["DU_AN_DIEU_CHUYEN_DEN"].ToString(), "0");
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return bResult;
        }

        private void ResetForm()
        {
            txtTrangThai.ResetText();
            txtSoGiaoDich.ResetText();
            btnMaDVSD.ResetText();
            txtTenDVSD.ResetText();
            txtDienGiai.ResetText();
            datNgayDieuChuyen.ResetText();
        }

        private void CreateTable()
        {
            dtChonCCLD.Columns.Add("MA_CCLD");
            dtChonCCLD.Columns.Add("TEN_CCLD");
            dtChonCCLD.Columns.Add("DON_GIA");
            dtChonCCLD.Columns.Add("NGAY_TANG");
            dtChonCCLD.Columns.Add("DTSD_DIEU_CHUYEN_DI");
            dtChonCCLD.Columns.Add("TEN_DTSD_DIEU_CHUYEN_DI");
            dtChonCCLD.Columns.Add("DU_AN_DIEU_CHUYEN_DI");
            dtChonCCLD.Columns.Add("TEN_DU_AN_DIEU_CHUYEN_DI");
            dtChonCCLD.Columns.Add("DTSD_DIEU_CHUYEN_DEN");
            dtChonCCLD.Columns.Add("TEN_DTSD_DIEU_CHUYEN_DEN");
            dtChonCCLD.Columns.Add("DU_AN_DIEU_CHUYEN_DEN");
            dtChonCCLD.Columns.Add("TEN_DU_AN_DIEU_CHUYEN_DEN");
        }

        private bool DuyetDieuChuyen(string sSoGiaoDich, string sTrangThai, string sLoai)
        {
            bool bResult = true;
            try
            {
                bResult = cc.DuyetDieuChuyen(sSoGiaoDich, sTrangThai, sLoai, "5");
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bResult;
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            datNgayDieuChuyen.ValidateEmptyStringRule(); // check bắt buộc nhập
            btnMaDVSD.ValidateEmptyStringRule();
        }

        #endregion
    }
}