﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.CCLD.DieuChuyenCungDVSD
{
    public partial class ucDieuChuyenCungDVSD : DevExpress.XtraEditors.XtraUserControl
    {
        #region "khai báo biến và tham số"
        clsCCLD cc = new clsCCLD();

        public ucDieuChuyenCungDVSD()
        {
            InitializeComponent();
        }
        #endregion

        #region các sự kiện
        private void ucDieuChuyenCungDVSD_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void gvDieuChuyen_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void LinkSoGiaoDich_Click(object sender, EventArgs e)
        {
            frmDieuChuyenCungDVSD frm = new frmDieuChuyenCungDVSD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sTrangThai = "Search";
            frm._sSoGiaoDich = gvDieuChuyen.GetFocusedRowCellValue("SO_GIAO_DICH").ToString();
            frm.ShowDialog();
            LoadDanhSach();
        }

        private void btnMaDVSD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.DVSD.frmDVSDTK frm = new QLCCLD.DanhMuc.DVSD.frmDVSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDVSD.Text = frm._sMa;
            txtTenDVSD.Text = frm._sTen;
        }

        private void btnMaDTSDDieuChuyenDi_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.DTSD.frmDTSDTK frm = new QLCCLD.DanhMuc.DTSD.frmDTSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDTSDDieuChuyenDi.Text = frm._sMa;
            txtTenDTSDDieuChuyenDi.Text = frm._sTen;
        }

        private void btnMaDTSDNhanDieuChuyen_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.DTSD.frmDTSDTK frm = new QLCCLD.DanhMuc.DTSD.frmDTSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDTSDNhanDieuChuyen.Text = frm._sMa;
            txtTenDTSDNhanDieuChuyen.Text = frm._sTen;
        }

        private void btnMaCCLD_ButtonClick(object sender, EventArgs e)
        {
            DanhMuc.NhomCCLD.frmSearchCCLD frm = new DanhMuc.NhomCCLD.frmSearchCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaCCLD.Text = frm._sMaCCLD;
            txtTenCCLD.Text = frm._sTenCCLD;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmDieuChuyenCungDVSD f = new frmDieuChuyenCungDVSD();
            f.StartPosition = FormStartPosition.CenterParent;
            f._sTrangThai = "Insert";
            f.ShowDialog();
            LoadDanhSach();
        }
        #endregion

        #region các hàm
        private void LoadDanhSach()
        {
            gcDieuChuyen.DataSource = cc.TimKiemDieuChuyenCungDVSD(txtSoGiaoDichTu.Text, txtSoGiaoDichDen.Text, datThoiGianTu.Text, datThoiGianDen.Text,
                                                        btnMaDVSD.Text, btnMaDTSDDieuChuyenDi.Text, btnMaDTSDNhanDieuChuyen.Text, btnMaCCLD.Text, chkChoDuyet.Checked, chkDaDuyet.Checked, chkTuChoiDuyet.Checked,
                                                        chkHuyDuyet.Checked, "0"); //0: điều chuyển cùng ĐVSD
            gvDieuChuyen.BestFitColumns(); //tự động điều chỉnh độ rộng của cột
        }

        #endregion       
       
    }
}
