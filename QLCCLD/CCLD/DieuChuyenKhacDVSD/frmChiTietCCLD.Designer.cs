﻿namespace QLCCLD.CCLD.DieuChuyenKhacDVSD
{
    partial class frmChiTietCCLD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnLuu = new DevExpress.XtraEditors.SimpleButton();
            this.txtNgayTang = new DevExpress.XtraEditors.TextEdit();
            this.txtTenDuAn = new DevExpress.XtraEditors.TextEdit();
            this.btnMaDuAn = new DevExpress.XtraEditors.ButtonEdit();
            this.txtTenDonViDieuChuyenDi = new DevExpress.XtraEditors.TextEdit();
            this.txtMaDonViDieuChuyenDi = new DevExpress.XtraEditors.TextEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtDonGia = new DevExpress.XtraEditors.TextEdit();
            this.txtTenCCLD = new DevExpress.XtraEditors.TextEdit();
            this.txtMaCCLD = new DevExpress.XtraEditors.TextEdit();
            this.txtTenDTSDNhanDieuChuyen = new DevExpress.XtraEditors.TextEdit();
            this.btnMaDTSDNhanDieuChuyen = new DevExpress.XtraEditors.ButtonEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayTang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDuAn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDuAn.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDonViDieuChuyenDi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDonViDieuChuyenDi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonGia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenCCLD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaCCLD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDTSDNhanDieuChuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDTSDNhanDieuChuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnDong;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(712, 120);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 26);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(41, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(85, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // btnDong
            // 
            this.btnDong.Image = global::QLCCLD.Properties.Resources.cancel_16x16;
            this.btnDong.Location = new System.Drawing.Point(724, 132);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(81, 22);
            this.btnDong.StyleController = this.layoutControl1;
            this.btnDong.TabIndex = 11;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.labelControl2);
            this.layoutControl1.Controls.Add(this.btnLuu);
            this.layoutControl1.Controls.Add(this.txtNgayTang);
            this.layoutControl1.Controls.Add(this.txtTenDuAn);
            this.layoutControl1.Controls.Add(this.btnMaDuAn);
            this.layoutControl1.Controls.Add(this.txtTenDonViDieuChuyenDi);
            this.layoutControl1.Controls.Add(this.txtMaDonViDieuChuyenDi);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.btnDong);
            this.layoutControl1.Controls.Add(this.txtDonGia);
            this.layoutControl1.Controls.Add(this.txtTenCCLD);
            this.layoutControl1.Controls.Add(this.txtMaCCLD);
            this.layoutControl1.Controls.Add(this.txtTenDTSDNhanDieuChuyen);
            this.layoutControl1.Controls.Add(this.btnMaDTSDNhanDieuChuyen);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(817, 166);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(12, 108);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(9, 13);
            this.labelControl2.StyleController = this.layoutControl1;
            this.labelControl2.TabIndex = 19;
            this.labelControl2.Text = "* ";
            // 
            // btnLuu
            // 
            this.btnLuu.Image = global::QLCCLD.Properties.Resources.save_16x16;
            this.btnLuu.Location = new System.Drawing.Point(644, 132);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(76, 22);
            this.btnLuu.StyleController = this.layoutControl1;
            this.btnLuu.TabIndex = 18;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // txtNgayTang
            // 
            this.txtNgayTang.Location = new System.Drawing.Point(563, 60);
            this.txtNgayTang.Name = "txtNgayTang";
            this.txtNgayTang.Properties.ReadOnly = true;
            this.txtNgayTang.Size = new System.Drawing.Size(242, 20);
            this.txtNgayTang.StyleController = this.layoutControl1;
            this.txtNgayTang.TabIndex = 17;
            // 
            // txtTenDuAn
            // 
            this.txtTenDuAn.Location = new System.Drawing.Point(563, 108);
            this.txtTenDuAn.Name = "txtTenDuAn";
            this.txtTenDuAn.Properties.ReadOnly = true;
            this.txtTenDuAn.Size = new System.Drawing.Size(242, 20);
            this.txtTenDuAn.StyleController = this.layoutControl1;
            this.txtTenDuAn.TabIndex = 16;
            // 
            // btnMaDuAn
            // 
            this.btnMaDuAn.Location = new System.Drawing.Point(170, 108);
            this.btnMaDuAn.Name = "btnMaDuAn";
            this.btnMaDuAn.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnMaDuAn.Properties.ReadOnly = true;
            this.btnMaDuAn.Size = new System.Drawing.Size(244, 20);
            this.btnMaDuAn.StyleController = this.layoutControl1;
            this.btnMaDuAn.TabIndex = 15;
            this.btnMaDuAn.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMaDuAn_ButtonClick);
            // 
            // txtTenDonViDieuChuyenDi
            // 
            this.txtTenDonViDieuChuyenDi.Location = new System.Drawing.Point(563, 36);
            this.txtTenDonViDieuChuyenDi.Name = "txtTenDonViDieuChuyenDi";
            this.txtTenDonViDieuChuyenDi.Properties.ReadOnly = true;
            this.txtTenDonViDieuChuyenDi.Size = new System.Drawing.Size(242, 20);
            this.txtTenDonViDieuChuyenDi.StyleController = this.layoutControl1;
            this.txtTenDonViDieuChuyenDi.TabIndex = 14;
            // 
            // txtMaDonViDieuChuyenDi
            // 
            this.txtMaDonViDieuChuyenDi.Location = new System.Drawing.Point(170, 36);
            this.txtMaDonViDieuChuyenDi.Name = "txtMaDonViDieuChuyenDi";
            this.txtMaDonViDieuChuyenDi.Properties.ReadOnly = true;
            this.txtMaDonViDieuChuyenDi.Size = new System.Drawing.Size(244, 20);
            this.txtMaDonViDieuChuyenDi.StyleController = this.layoutControl1;
            this.txtMaDonViDieuChuyenDi.TabIndex = 13;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Black;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(12, 84);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(9, 13);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 12;
            this.labelControl1.Text = "* ";
            // 
            // txtDonGia
            // 
            this.txtDonGia.Location = new System.Drawing.Point(170, 60);
            this.txtDonGia.Name = "txtDonGia";
            this.txtDonGia.Properties.ReadOnly = true;
            this.txtDonGia.Size = new System.Drawing.Size(244, 20);
            this.txtDonGia.StyleController = this.layoutControl1;
            this.txtDonGia.TabIndex = 8;
            // 
            // txtTenCCLD
            // 
            this.txtTenCCLD.Location = new System.Drawing.Point(563, 12);
            this.txtTenCCLD.Name = "txtTenCCLD";
            this.txtTenCCLD.Properties.ReadOnly = true;
            this.txtTenCCLD.Size = new System.Drawing.Size(242, 20);
            this.txtTenCCLD.StyleController = this.layoutControl1;
            this.txtTenCCLD.TabIndex = 7;
            // 
            // txtMaCCLD
            // 
            this.txtMaCCLD.Location = new System.Drawing.Point(170, 12);
            this.txtMaCCLD.Name = "txtMaCCLD";
            this.txtMaCCLD.Properties.ReadOnly = true;
            this.txtMaCCLD.Size = new System.Drawing.Size(244, 20);
            this.txtMaCCLD.StyleController = this.layoutControl1;
            this.txtMaCCLD.TabIndex = 6;
            // 
            // txtTenDTSDNhanDieuChuyen
            // 
            this.txtTenDTSDNhanDieuChuyen.Location = new System.Drawing.Point(563, 84);
            this.txtTenDTSDNhanDieuChuyen.Name = "txtTenDTSDNhanDieuChuyen";
            this.txtTenDTSDNhanDieuChuyen.Properties.ReadOnly = true;
            this.txtTenDTSDNhanDieuChuyen.Size = new System.Drawing.Size(242, 20);
            this.txtTenDTSDNhanDieuChuyen.StyleController = this.layoutControl1;
            this.txtTenDTSDNhanDieuChuyen.TabIndex = 5;
            // 
            // btnMaDTSDNhanDieuChuyen
            // 
            this.btnMaDTSDNhanDieuChuyen.Location = new System.Drawing.Point(170, 84);
            this.btnMaDTSDNhanDieuChuyen.Name = "btnMaDTSDNhanDieuChuyen";
            this.btnMaDTSDNhanDieuChuyen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnMaDTSDNhanDieuChuyen.Properties.ReadOnly = true;
            this.btnMaDTSDNhanDieuChuyen.Size = new System.Drawing.Size(244, 20);
            this.btnMaDTSDNhanDieuChuyen.StyleController = this.layoutControl1;
            this.btnMaDTSDNhanDieuChuyen.TabIndex = 4;
            this.btnMaDTSDNhanDieuChuyen.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMaDTSDNhanDieuChuyen_ButtonClick);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.emptySpaceItem1,
            this.emptySpaceItem3,
            this.layoutControlItem12,
            this.layoutControlItem13,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.emptySpaceItem2,
            this.layoutControlItem9,
            this.layoutControlItem17,
            this.layoutControlItem7,
            this.layoutControlItem5,
            this.layoutControlItem14,
            this.emptySpaceItem5});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(817, 166);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtMaCCLD;
            this.layoutControlItem3.CustomizationFormText = "Mã CCLĐ";
            this.layoutControlItem3.Location = new System.Drawing.Point(13, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem3.Text = "Mã CCLĐ";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtTenCCLD;
            this.layoutControlItem4.CustomizationFormText = "Tên CCLĐ";
            this.layoutControlItem4.Location = new System.Drawing.Point(406, 0);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(391, 24);
            this.layoutControlItem4.Text = "Tên CCLĐ";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(142, 13);
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 120);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(632, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem3.MaxSize = new System.Drawing.Size(13, 0);
            this.emptySpaceItem3.MinSize = new System.Drawing.Size(13, 10);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(13, 24);
            this.emptySpaceItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.btnMaDuAn;
            this.layoutControlItem12.CustomizationFormText = "Mã dự án";
            this.layoutControlItem12.Location = new System.Drawing.Point(13, 96);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem12.Text = "Mã dự án nhận điều chuyển";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.txtTenDuAn;
            this.layoutControlItem13.CustomizationFormText = "Tên dự án";
            this.layoutControlItem13.Location = new System.Drawing.Point(406, 96);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(391, 24);
            this.layoutControlItem13.Text = "Tên dự án nhận điều chuyển";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.txtMaDonViDieuChuyenDi;
            this.layoutControlItem10.CustomizationFormText = "Số lượng";
            this.layoutControlItem10.Location = new System.Drawing.Point(13, 24);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem10.Text = "Mã ĐVSD/Kho điều chuyển đi";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtTenDonViDieuChuyenDi;
            this.layoutControlItem11.CustomizationFormText = "Thành tiền";
            this.layoutControlItem11.Location = new System.Drawing.Point(406, 24);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(391, 24);
            this.layoutControlItem11.Text = "Tên ĐVSD/Kho điều chuyển đi";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.btnMaDTSDNhanDieuChuyen;
            this.layoutControlItem1.CustomizationFormText = "Mã lý do nhập kho";
            this.layoutControlItem1.Location = new System.Drawing.Point(13, 72);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem1.Text = "Mã ĐTSD nhận điều chuyển";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.txtTenDTSDNhanDieuChuyen;
            this.layoutControlItem2.CustomizationFormText = "Tên lý do nhập kho";
            this.layoutControlItem2.Location = new System.Drawing.Point(406, 72);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(391, 24);
            this.layoutControlItem2.Text = "Tên ĐTSD nhận điều chuyển";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(142, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 24);
            this.emptySpaceItem2.MaxSize = new System.Drawing.Size(13, 0);
            this.emptySpaceItem2.MinSize = new System.Drawing.Size(13, 10);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(13, 24);
            this.emptySpaceItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.labelControl1;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 72);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(13, 17);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(13, 17);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(13, 24);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.labelControl2;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(13, 24);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnLuu;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(632, 120);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtDonGia;
            this.layoutControlItem5.CustomizationFormText = "Đơn giá/Đơn giá ĐGL";
            this.layoutControlItem5.Location = new System.Drawing.Point(13, 48);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(393, 24);
            this.layoutControlItem5.Text = "Đơn giá/Đơn giá ĐGL";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(142, 13);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.txtNgayTang;
            this.layoutControlItem14.CustomizationFormText = "Ghi chú";
            this.layoutControlItem14.Location = new System.Drawing.Point(406, 48);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(391, 24);
            this.layoutControlItem14.Text = "Ngày tăng";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(142, 13);
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.CustomizationFormText = "emptySpaceItem5";
            this.emptySpaceItem5.Location = new System.Drawing.Point(0, 48);
            this.emptySpaceItem5.MinSize = new System.Drawing.Size(10, 10);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(13, 24);
            this.emptySpaceItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.emptySpaceItem5.Text = "emptySpaceItem5";
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.labelControl1;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(13, 17);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(13, 17);
            this.layoutControlItem15.Name = "layoutControlItem9";
            this.layoutControlItem15.Size = new System.Drawing.Size(13, 24);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItem9";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.labelControl1;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 48);
            this.layoutControlItem16.MaxSize = new System.Drawing.Size(13, 17);
            this.layoutControlItem16.MinSize = new System.Drawing.Size(13, 17);
            this.layoutControlItem16.Name = "layoutControlItem9";
            this.layoutControlItem16.Size = new System.Drawing.Size(13, 24);
            this.layoutControlItem16.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem16.Text = "layoutControlItem9";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // frmChiTietCCLD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(817, 166);
            this.Controls.Add(this.layoutControl1);
            this.Name = "frmChiTietCCLD";
            this.Text = "Chi tiết CCLĐ điều chuyển";
            this.Load += new System.EventHandler(this.frmChiTietCCLD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtNgayTang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDuAn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDuAn.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDonViDieuChuyenDi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaDonViDieuChuyenDi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDonGia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenCCLD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMaCCLD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDTSDNhanDieuChuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDTSDNhanDieuChuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.SimpleButton btnLuu;
        private DevExpress.XtraEditors.TextEdit txtNgayTang;
        private DevExpress.XtraEditors.TextEdit txtTenDuAn;
        private DevExpress.XtraEditors.ButtonEdit btnMaDuAn;
        private DevExpress.XtraEditors.TextEdit txtTenDonViDieuChuyenDi;
        private DevExpress.XtraEditors.TextEdit txtMaDonViDieuChuyenDi;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtDonGia;
        private DevExpress.XtraEditors.TextEdit txtTenCCLD;
        private DevExpress.XtraEditors.TextEdit txtMaCCLD;
        private DevExpress.XtraEditors.TextEdit txtTenDTSDNhanDieuChuyen;
        private DevExpress.XtraEditors.ButtonEdit btnMaDTSDNhanDieuChuyen;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
    }
}