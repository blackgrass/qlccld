﻿namespace QLCCLD.CCLD.DieuChuyenKhacDVSD
{
    partial class ucDieuChuyenKhacDVSD
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.txtSoGiaoDichTu = new DevExpress.XtraEditors.TextEdit();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.txtSoQuyetDinh = new DevExpress.XtraEditors.TextEdit();
            this.txtTenKhoNhanDieuChuyen = new DevExpress.XtraEditors.TextEdit();
            this.txtTenDVSDNhanDieuChuyen = new DevExpress.XtraEditors.TextEdit();
            this.btnMaKhoNhanDieuChuyen = new DevExpress.XtraEditors.ButtonEdit();
            this.btnMaDVSDNhanDieuChuyen = new DevExpress.XtraEditors.ButtonEdit();
            this.txtTenCCLD = new DevExpress.XtraEditors.TextEdit();
            this.btnMaCCLD = new DevExpress.XtraEditors.ButtonEdit();
            this.txtTenDVSDDieuChuyenDi = new DevExpress.XtraEditors.TextEdit();
            this.btnMaDVSDDieuChuyenDi = new DevExpress.XtraEditors.ButtonEdit();
            this.chkDaDuyet = new DevExpress.XtraEditors.CheckEdit();
            this.chkHuyDuyet = new DevExpress.XtraEditors.CheckEdit();
            this.chkTuChoiDuyet = new DevExpress.XtraEditors.CheckEdit();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.chkChoDuyet = new DevExpress.XtraEditors.CheckEdit();
            this.datThoiGianDen = new DevExpress.XtraEditors.DateEdit();
            this.datThoiGianTu = new DevExpress.XtraEditors.DateEdit();
            this.txtSoGiaoDichDen = new DevExpress.XtraEditors.TextEdit();
            this.btnTimKiem = new DevExpress.XtraEditors.SimpleButton();
            this.btnThem = new DevExpress.XtraEditors.SimpleButton();
            this.gcDieuChuyen = new DevExpress.XtraGrid.GridControl();
            this.gvDieuChuyen = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.STT = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LinkSoGiaoDich = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGiaoDichTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoQuyetDinh.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhoNhanDieuChuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDVSDNhanDieuChuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaKhoNhanDieuChuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDVSDNhanDieuChuyen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenCCLD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaCCLD.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDVSDDieuChuyenDi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDVSDDieuChuyenDi.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDaDuyet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHuyDuyet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTuChoiDuyet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChoDuyet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datThoiGianDen.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datThoiGianDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datThoiGianTu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datThoiGianTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGiaoDichDen.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDieuChuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDieuChuyen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkSoGiaoDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.txtSoGiaoDichTu;
            this.layoutControlItem4.CustomizationFormText = "Số giao dịch";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 73);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(231, 24);
            this.layoutControlItem4.Text = "Số giao dịch";
            this.layoutControlItem4.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem4.TextSize = new System.Drawing.Size(105, 13);
            this.layoutControlItem4.TextToControlDistance = 5;
            // 
            // txtSoGiaoDichTu
            // 
            this.txtSoGiaoDichTu.Location = new System.Drawing.Point(122, 85);
            this.txtSoGiaoDichTu.Name = "txtSoGiaoDichTu";
            this.txtSoGiaoDichTu.Properties.MaxLength = 10;
            this.txtSoGiaoDichTu.Size = new System.Drawing.Size(117, 20);
            this.txtSoGiaoDichTu.StyleController = this.layoutControl1;
            this.txtSoGiaoDichTu.TabIndex = 7;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.txtSoQuyetDinh);
            this.layoutControl1.Controls.Add(this.txtTenKhoNhanDieuChuyen);
            this.layoutControl1.Controls.Add(this.txtTenDVSDNhanDieuChuyen);
            this.layoutControl1.Controls.Add(this.btnMaKhoNhanDieuChuyen);
            this.layoutControl1.Controls.Add(this.btnMaDVSDNhanDieuChuyen);
            this.layoutControl1.Controls.Add(this.txtTenCCLD);
            this.layoutControl1.Controls.Add(this.btnMaCCLD);
            this.layoutControl1.Controls.Add(this.txtTenDVSDDieuChuyenDi);
            this.layoutControl1.Controls.Add(this.btnMaDVSDDieuChuyenDi);
            this.layoutControl1.Controls.Add(this.chkDaDuyet);
            this.layoutControl1.Controls.Add(this.chkHuyDuyet);
            this.layoutControl1.Controls.Add(this.chkTuChoiDuyet);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.chkChoDuyet);
            this.layoutControl1.Controls.Add(this.datThoiGianDen);
            this.layoutControl1.Controls.Add(this.datThoiGianTu);
            this.layoutControl1.Controls.Add(this.txtSoGiaoDichDen);
            this.layoutControl1.Controls.Add(this.btnTimKiem);
            this.layoutControl1.Controls.Add(this.btnThem);
            this.layoutControl1.Controls.Add(this.txtSoGiaoDichTu);
            this.layoutControl1.Controls.Add(this.gcDieuChuyen);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(784, 536);
            this.layoutControl1.TabIndex = 2;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // txtSoQuyetDinh
            // 
            this.txtSoQuyetDinh.Location = new System.Drawing.Point(122, 61);
            this.txtSoQuyetDinh.Name = "txtSoQuyetDinh";
            this.txtSoQuyetDinh.Size = new System.Drawing.Size(650, 20);
            this.txtSoQuyetDinh.StyleController = this.layoutControl1;
            this.txtSoQuyetDinh.TabIndex = 28;
            // 
            // txtTenKhoNhanDieuChuyen
            // 
            this.txtTenKhoNhanDieuChuyen.Location = new System.Drawing.Point(506, 157);
            this.txtTenKhoNhanDieuChuyen.Name = "txtTenKhoNhanDieuChuyen";
            this.txtTenKhoNhanDieuChuyen.Properties.ReadOnly = true;
            this.txtTenKhoNhanDieuChuyen.Size = new System.Drawing.Size(266, 20);
            this.txtTenKhoNhanDieuChuyen.StyleController = this.layoutControl1;
            this.txtTenKhoNhanDieuChuyen.TabIndex = 27;
            // 
            // txtTenDVSDNhanDieuChuyen
            // 
            this.txtTenDVSDNhanDieuChuyen.Location = new System.Drawing.Point(506, 133);
            this.txtTenDVSDNhanDieuChuyen.Name = "txtTenDVSDNhanDieuChuyen";
            this.txtTenDVSDNhanDieuChuyen.Properties.ReadOnly = true;
            this.txtTenDVSDNhanDieuChuyen.Size = new System.Drawing.Size(266, 20);
            this.txtTenDVSDNhanDieuChuyen.StyleController = this.layoutControl1;
            this.txtTenDVSDNhanDieuChuyen.TabIndex = 25;
            // 
            // btnMaKhoNhanDieuChuyen
            // 
            this.btnMaKhoNhanDieuChuyen.Location = new System.Drawing.Point(122, 157);
            this.btnMaKhoNhanDieuChuyen.Name = "btnMaKhoNhanDieuChuyen";
            this.btnMaKhoNhanDieuChuyen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnMaKhoNhanDieuChuyen.Properties.ReadOnly = true;
            this.btnMaKhoNhanDieuChuyen.Size = new System.Drawing.Size(267, 20);
            this.btnMaKhoNhanDieuChuyen.StyleController = this.layoutControl1;
            this.btnMaKhoNhanDieuChuyen.TabIndex = 26;
            this.btnMaKhoNhanDieuChuyen.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMaKho_ButtonClick);
            // 
            // btnMaDVSDNhanDieuChuyen
            // 
            this.btnMaDVSDNhanDieuChuyen.Location = new System.Drawing.Point(122, 133);
            this.btnMaDVSDNhanDieuChuyen.Name = "btnMaDVSDNhanDieuChuyen";
            this.btnMaDVSDNhanDieuChuyen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnMaDVSDNhanDieuChuyen.Properties.ReadOnly = true;
            this.btnMaDVSDNhanDieuChuyen.Size = new System.Drawing.Size(267, 20);
            this.btnMaDVSDNhanDieuChuyen.StyleController = this.layoutControl1;
            this.btnMaDVSDNhanDieuChuyen.TabIndex = 24;
            this.btnMaDVSDNhanDieuChuyen.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMaDVSDNhanDieuChuyen_ButtonClick);
            // 
            // txtTenCCLD
            // 
            this.txtTenCCLD.Location = new System.Drawing.Point(506, 181);
            this.txtTenCCLD.Name = "txtTenCCLD";
            this.txtTenCCLD.Properties.ReadOnly = true;
            this.txtTenCCLD.Size = new System.Drawing.Size(266, 20);
            this.txtTenCCLD.StyleController = this.layoutControl1;
            this.txtTenCCLD.TabIndex = 23;
            // 
            // btnMaCCLD
            // 
            this.btnMaCCLD.Location = new System.Drawing.Point(122, 181);
            this.btnMaCCLD.Name = "btnMaCCLD";
            this.btnMaCCLD.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnMaCCLD.Properties.ReadOnly = true;
            this.btnMaCCLD.Size = new System.Drawing.Size(267, 20);
            this.btnMaCCLD.StyleController = this.layoutControl1;
            this.btnMaCCLD.TabIndex = 22;
            this.btnMaCCLD.Click += new System.EventHandler(this.btnMaCCLD_ButtonClick);
            // 
            // txtTenDVSDDieuChuyenDi
            // 
            this.txtTenDVSDDieuChuyenDi.Location = new System.Drawing.Point(506, 109);
            this.txtTenDVSDDieuChuyenDi.Name = "txtTenDVSDDieuChuyenDi";
            this.txtTenDVSDDieuChuyenDi.Properties.ReadOnly = true;
            this.txtTenDVSDDieuChuyenDi.Size = new System.Drawing.Size(266, 20);
            this.txtTenDVSDDieuChuyenDi.StyleController = this.layoutControl1;
            this.txtTenDVSDDieuChuyenDi.TabIndex = 21;
            // 
            // btnMaDVSDDieuChuyenDi
            // 
            this.btnMaDVSDDieuChuyenDi.Location = new System.Drawing.Point(122, 109);
            this.btnMaDVSDDieuChuyenDi.Name = "btnMaDVSDDieuChuyenDi";
            this.btnMaDVSDDieuChuyenDi.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.btnMaDVSDDieuChuyenDi.Properties.ReadOnly = true;
            this.btnMaDVSDDieuChuyenDi.Size = new System.Drawing.Size(267, 20);
            this.btnMaDVSDDieuChuyenDi.StyleController = this.layoutControl1;
            this.btnMaDVSDDieuChuyenDi.TabIndex = 20;
            this.btnMaDVSDDieuChuyenDi.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMaDVSDDieuChuyenDi_ButtonClick);
            // 
            // chkDaDuyet
            // 
            this.chkDaDuyet.EditValue = true;
            this.chkDaDuyet.Location = new System.Drawing.Point(198, 38);
            this.chkDaDuyet.Name = "chkDaDuyet";
            this.chkDaDuyet.Properties.Caption = "Đã duyệt";
            this.chkDaDuyet.Size = new System.Drawing.Size(68, 19);
            this.chkDaDuyet.StyleController = this.layoutControl1;
            this.chkDaDuyet.TabIndex = 19;
            // 
            // chkHuyDuyet
            // 
            this.chkHuyDuyet.EditValue = true;
            this.chkHuyDuyet.Location = new System.Drawing.Point(270, 38);
            this.chkHuyDuyet.Name = "chkHuyDuyet";
            this.chkHuyDuyet.Properties.Caption = "Hủy duyệt";
            this.chkHuyDuyet.Size = new System.Drawing.Size(73, 19);
            this.chkHuyDuyet.StyleController = this.layoutControl1;
            this.chkHuyDuyet.TabIndex = 18;
            // 
            // chkTuChoiDuyet
            // 
            this.chkTuChoiDuyet.EditValue = true;
            this.chkTuChoiDuyet.Location = new System.Drawing.Point(347, 38);
            this.chkTuChoiDuyet.Name = "chkTuChoiDuyet";
            this.chkTuChoiDuyet.Properties.Caption = "Từ chối duyệt";
            this.chkTuChoiDuyet.Size = new System.Drawing.Size(425, 19);
            this.chkTuChoiDuyet.StyleController = this.layoutControl1;
            this.chkTuChoiDuyet.TabIndex = 17;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(12, 38);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(49, 13);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "Trạng thái";
            // 
            // chkChoDuyet
            // 
            this.chkChoDuyet.EditValue = true;
            this.chkChoDuyet.Location = new System.Drawing.Point(121, 38);
            this.chkChoDuyet.Name = "chkChoDuyet";
            this.chkChoDuyet.Properties.Caption = "Chờ duyệt";
            this.chkChoDuyet.Size = new System.Drawing.Size(73, 19);
            this.chkChoDuyet.StyleController = this.layoutControl1;
            this.chkChoDuyet.TabIndex = 15;
            // 
            // datThoiGianDen
            // 
            this.datThoiGianDen.EditValue = null;
            this.datThoiGianDen.Location = new System.Drawing.Point(654, 85);
            this.datThoiGianDen.Name = "datThoiGianDen";
            this.datThoiGianDen.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.datThoiGianDen.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.datThoiGianDen.Size = new System.Drawing.Size(118, 20);
            this.datThoiGianDen.StyleController = this.layoutControl1;
            this.datThoiGianDen.TabIndex = 10;
            // 
            // datThoiGianTu
            // 
            this.datThoiGianTu.EditValue = null;
            this.datThoiGianTu.Location = new System.Drawing.Point(506, 85);
            this.datThoiGianTu.Name = "datThoiGianTu";
            this.datThoiGianTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.datThoiGianTu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.datThoiGianTu.Size = new System.Drawing.Size(129, 20);
            this.datThoiGianTu.StyleController = this.layoutControl1;
            this.datThoiGianTu.TabIndex = 9;
            // 
            // txtSoGiaoDichDen
            // 
            this.txtSoGiaoDichDen.Location = new System.Drawing.Point(258, 85);
            this.txtSoGiaoDichDen.Name = "txtSoGiaoDichDen";
            this.txtSoGiaoDichDen.Properties.MaxLength = 10;
            this.txtSoGiaoDichDen.Size = new System.Drawing.Size(131, 20);
            this.txtSoGiaoDichDen.StyleController = this.layoutControl1;
            this.txtSoGiaoDichDen.TabIndex = 8;
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Image = global::QLCCLD.Properties.Resources.zoom_16x16;
            this.btnTimKiem.Location = new System.Drawing.Point(92, 12);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(76, 22);
            this.btnTimKiem.StyleController = this.layoutControl1;
            this.btnTimKiem.TabIndex = 6;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // btnThem
            // 
            this.btnThem.Image = global::QLCCLD.Properties.Resources.add_16x16;
            this.btnThem.Location = new System.Drawing.Point(12, 12);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(76, 22);
            this.btnThem.StyleController = this.layoutControl1;
            this.btnThem.TabIndex = 5;
            this.btnThem.Text = "Thêm mới";
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // gcDieuChuyen
            // 
            this.gcDieuChuyen.Location = new System.Drawing.Point(12, 221);
            this.gcDieuChuyen.MainView = this.gvDieuChuyen;
            this.gcDieuChuyen.Name = "gcDieuChuyen";
            this.gcDieuChuyen.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LinkSoGiaoDich});
            this.gcDieuChuyen.Size = new System.Drawing.Size(760, 303);
            this.gcDieuChuyen.TabIndex = 4;
            this.gcDieuChuyen.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvDieuChuyen});
            // 
            // gvDieuChuyen
            // 
            this.gvDieuChuyen.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.STT,
            this.gridColumn1,
            this.gridColumn2,
            this.gridColumn4,
            this.gridColumn3,
            this.gridColumn5});
            this.gvDieuChuyen.GridControl = this.gcDieuChuyen;
            this.gvDieuChuyen.Name = "gvDieuChuyen";
            this.gvDieuChuyen.OptionsView.ShowGroupPanel = false;
            this.gvDieuChuyen.CustomColumnDisplayText += new DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventHandler(this.gvDieuChuyen_CustomColumnDisplayText);
            // 
            // STT
            // 
            this.STT.Caption = "STT";
            this.STT.Name = "STT";
            this.STT.Visible = true;
            this.STT.VisibleIndex = 0;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Số giao dịch";
            this.gridColumn1.ColumnEdit = this.LinkSoGiaoDich;
            this.gridColumn1.FieldName = "SO_GIAO_DICH";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 1;
            // 
            // LinkSoGiaoDich
            // 
            this.LinkSoGiaoDich.AutoHeight = false;
            this.LinkSoGiaoDich.Name = "LinkSoGiaoDich";
            this.LinkSoGiaoDich.Click += new System.EventHandler(this.LinkSoGiaoDich_Click);
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Ngày điều chuyển";
            this.gridColumn2.FieldName = "NGAY_DIEU_CHUYEN";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 2;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Số quyết định";
            this.gridColumn4.FieldName = "SO_QUYET_DINH";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "ĐVSD/Kho nhận";
            this.gridColumn3.FieldName = "TEN_DVSDTS_NHAN";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 4;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Trạng thái";
            this.gridColumn5.FieldName = "TRANG_THAI";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 5;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.emptySpaceItem1,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem6,
            this.layoutControlItem7,
            this.layoutControlItem15,
            this.layoutControlItem14,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.emptySpaceItem2,
            this.layoutControlItem1,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11,
            this.layoutControlItem19,
            this.layoutControlItem20,
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(784, 536);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.btnThem;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.MaxSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem2.MinSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem2.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.btnTimKiem;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(80, 0);
            this.layoutControlItem3.MaxSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem3.MinSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem3.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(160, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(604, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtSoGiaoDichDen;
            this.layoutControlItem5.CustomizationFormText = " - ";
            this.layoutControlItem5.Location = new System.Drawing.Point(231, 73);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(150, 24);
            this.layoutControlItem5.Text = " - ";
            this.layoutControlItem5.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem5.TextSize = new System.Drawing.Size(10, 13);
            this.layoutControlItem5.TextToControlDistance = 5;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.datThoiGianTu;
            this.layoutControlItem6.CustomizationFormText = "Ngày chứng từ";
            this.layoutControlItem6.Location = new System.Drawing.Point(381, 73);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(246, 24);
            this.layoutControlItem6.Text = "Ngày điều chuyển đi";
            this.layoutControlItem6.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(108, 13);
            this.layoutControlItem6.TextToControlDistance = 5;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.datThoiGianDen;
            this.layoutControlItem7.CustomizationFormText = " - ";
            this.layoutControlItem7.Location = new System.Drawing.Point(627, 73);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(137, 24);
            this.layoutControlItem7.Text = " - ";
            this.layoutControlItem7.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem7.TextSize = new System.Drawing.Size(10, 13);
            this.layoutControlItem7.TextToControlDistance = 5;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.labelControl1;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItem15";
            this.layoutControlItem15.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(53, 23);
            this.layoutControlItem15.Text = "layoutControlItem15";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.chkChoDuyet;
            this.layoutControlItem14.CustomizationFormText = "layoutControlItem14";
            this.layoutControlItem14.Location = new System.Drawing.Point(109, 26);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(77, 23);
            this.layoutControlItem14.Text = "layoutControlItem14";
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextToControlDistance = 0;
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.chkTuChoiDuyet;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(335, 26);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(429, 23);
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.chkHuyDuyet;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(258, 26);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(77, 23);
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.chkDaDuyet;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(186, 26);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(72, 23);
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(53, 26);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(56, 23);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gcDieuChuyen;
            this.layoutControlItem1.CustomizationFormText = "Danh sách giao dịch tăng CCLĐ";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 193);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(764, 323);
            this.layoutControlItem1.Text = "Danh sách giao dịch điều chuyển";
            this.layoutControlItem1.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem1.TextSize = new System.Drawing.Size(156, 13);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnMaDVSDDieuChuyenDi;
            this.layoutControlItem8.CustomizationFormText = "Mã kho";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 97);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(191, 24);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(381, 24);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "Mã ĐVSD Đ.chuyển đi";
            this.layoutControlItem8.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem8.TextSize = new System.Drawing.Size(105, 13);
            this.layoutControlItem8.TextToControlDistance = 5;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.txtTenDVSDDieuChuyenDi;
            this.layoutControlItem9.CustomizationFormText = "Tên kho";
            this.layoutControlItem9.Location = new System.Drawing.Point(381, 97);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(383, 24);
            this.layoutControlItem9.Text = "Tên ĐVSD Đ.chuyển đi";
            this.layoutControlItem9.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(108, 20);
            this.layoutControlItem9.TextToControlDistance = 5;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.btnMaCCLD;
            this.layoutControlItem10.CustomizationFormText = "Mã CCLĐ";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 169);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(381, 24);
            this.layoutControlItem10.Text = "Mã CCLĐ";
            this.layoutControlItem10.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem10.TextSize = new System.Drawing.Size(105, 20);
            this.layoutControlItem10.TextToControlDistance = 5;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.txtTenCCLD;
            this.layoutControlItem11.CustomizationFormText = "Tên CCLĐ";
            this.layoutControlItem11.Location = new System.Drawing.Point(381, 169);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(383, 24);
            this.layoutControlItem11.Text = "Tên CCLĐ";
            this.layoutControlItem11.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem11.TextSize = new System.Drawing.Size(108, 20);
            this.layoutControlItem11.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.btnMaDVSDNhanDieuChuyen;
            this.layoutControlItem19.CustomizationFormText = "Mã ĐTSD điều chuyển đi";
            this.layoutControlItem19.Location = new System.Drawing.Point(0, 121);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(0, 24);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(191, 24);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(381, 24);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "Mã ĐVSD nhận";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(105, 20);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.txtTenDVSDNhanDieuChuyen;
            this.layoutControlItem20.CustomizationFormText = "Tên ĐTSD điều chuyển đi";
            this.layoutControlItem20.Location = new System.Drawing.Point(381, 121);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(383, 24);
            this.layoutControlItem20.Text = "Tên ĐVSD nhận";
            this.layoutControlItem20.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem20.TextSize = new System.Drawing.Size(108, 20);
            this.layoutControlItem20.TextToControlDistance = 5;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.btnMaKhoNhanDieuChuyen;
            this.layoutControlItem21.CustomizationFormText = "layoutControlItem21";
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 145);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(381, 24);
            this.layoutControlItem21.Text = "Mã kho nhận";
            this.layoutControlItem21.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem21.TextSize = new System.Drawing.Size(105, 13);
            this.layoutControlItem21.TextToControlDistance = 5;
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.txtTenKhoNhanDieuChuyen;
            this.layoutControlItem22.CustomizationFormText = "layoutControlItem22";
            this.layoutControlItem22.Location = new System.Drawing.Point(381, 145);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(383, 24);
            this.layoutControlItem22.Text = "Tên kho nhận";
            this.layoutControlItem22.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(108, 13);
            this.layoutControlItem22.TextToControlDistance = 5;
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.txtSoQuyetDinh;
            this.layoutControlItem23.CustomizationFormText = "Số quyết định";
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 49);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(764, 24);
            this.layoutControlItem23.Text = "Số quyết định";
            this.layoutControlItem23.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.CustomSize;
            this.layoutControlItem23.TextSize = new System.Drawing.Size(105, 20);
            this.layoutControlItem23.TextToControlDistance = 5;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.btnTimKiem;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem12.Location = new System.Drawing.Point(100, 0);
            this.layoutControlItem12.MaxSize = new System.Drawing.Size(100, 26);
            this.layoutControlItem12.MinSize = new System.Drawing.Size(34, 26);
            this.layoutControlItem12.Name = "layoutControlItem3";
            this.layoutControlItem12.Size = new System.Drawing.Size(100, 26);
            this.layoutControlItem12.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem12.Text = "layoutControlItem3";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.btnTimKiem;
            this.layoutControlItem13.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem13.Location = new System.Drawing.Point(100, 0);
            this.layoutControlItem13.MaxSize = new System.Drawing.Size(100, 26);
            this.layoutControlItem13.MinSize = new System.Drawing.Size(34, 26);
            this.layoutControlItem13.Name = "layoutControlItem3";
            this.layoutControlItem13.Size = new System.Drawing.Size(100, 26);
            this.layoutControlItem13.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem13.Text = "layoutControlItem3";
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextToControlDistance = 0;
            this.layoutControlItem13.TextVisible = false;
            // 
            // ucDieuChuyenKhacDVSD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.layoutControl1);
            this.Name = "ucDieuChuyenKhacDVSD";
            this.Size = new System.Drawing.Size(784, 536);
            this.Load += new System.EventHandler(this.ucDieuChuyenKhacDVSD_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGiaoDichTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSoQuyetDinh.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenKhoNhanDieuChuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDVSDNhanDieuChuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaKhoNhanDieuChuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDVSDNhanDieuChuyen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenCCLD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaCCLD.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTenDVSDDieuChuyenDi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaDVSDDieuChuyenDi.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDaDuyet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHuyDuyet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTuChoiDuyet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChoDuyet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datThoiGianDen.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datThoiGianDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datThoiGianTu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datThoiGianTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGiaoDichDen.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcDieuChuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvDieuChuyen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkSoGiaoDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.TextEdit txtSoGiaoDichTu;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraEditors.TextEdit txtTenCCLD;
        private DevExpress.XtraEditors.ButtonEdit btnMaCCLD;
        private DevExpress.XtraEditors.TextEdit txtTenDVSDDieuChuyenDi;
        private DevExpress.XtraEditors.ButtonEdit btnMaDVSDDieuChuyenDi;
        private DevExpress.XtraEditors.CheckEdit chkDaDuyet;
        private DevExpress.XtraEditors.CheckEdit chkHuyDuyet;
        private DevExpress.XtraEditors.CheckEdit chkTuChoiDuyet;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.CheckEdit chkChoDuyet;
        private DevExpress.XtraEditors.DateEdit datThoiGianDen;
        private DevExpress.XtraEditors.DateEdit datThoiGianTu;
        private DevExpress.XtraEditors.TextEdit txtSoGiaoDichDen;
        private DevExpress.XtraEditors.SimpleButton btnTimKiem;
        private DevExpress.XtraEditors.SimpleButton btnThem;
        private DevExpress.XtraGrid.GridControl gcDieuChuyen;
        private DevExpress.XtraGrid.Views.Grid.GridView gvDieuChuyen;
        private DevExpress.XtraGrid.Columns.GridColumn STT;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit LinkSoGiaoDich;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.TextEdit txtTenKhoNhanDieuChuyen;
        private DevExpress.XtraEditors.TextEdit txtTenDVSDNhanDieuChuyen;
        private DevExpress.XtraEditors.ButtonEdit btnMaKhoNhanDieuChuyen;
        private DevExpress.XtraEditors.ButtonEdit btnMaDVSDNhanDieuChuyen;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.TextEdit txtSoQuyetDinh;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
    }
}
