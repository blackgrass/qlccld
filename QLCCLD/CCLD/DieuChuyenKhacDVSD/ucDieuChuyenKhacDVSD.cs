﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.CCLD.DieuChuyenKhacDVSD
{
    public partial class ucDieuChuyenKhacDVSD : DevExpress.XtraEditors.XtraUserControl
    {
        #region "khai báo biến và tham số"
        clsCCLD cc = new clsCCLD();

        public ucDieuChuyenKhacDVSD()
        {
            InitializeComponent();
        }
        #endregion

        #region các sự kiện
        private void ucDieuChuyenKhacDVSD_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void gvDieuChuyen_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void LinkSoGiaoDich_Click(object sender, EventArgs e)
        {
            frmDieuChuyenKhacDVSD frm = new frmDieuChuyenKhacDVSD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sTrangThai = "Search";
            frm._sSoGiaoDich = gvDieuChuyen.GetFocusedRowCellValue("SO_GIAO_DICH").ToString();
            frm.ShowDialog();
            LoadDanhSach();
        }

        private void btnMaDVSDDieuChuyenDi_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.DVSD.frmDVSDTK frm = new QLCCLD.DanhMuc.DVSD.frmDVSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDVSDDieuChuyenDi.Text = frm._sMa;
            txtTenDVSDDieuChuyenDi.Text = frm._sTen;
        }

        private void btnMaDVSDNhanDieuChuyen_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.DVSD.frmDVSDTK frm = new QLCCLD.DanhMuc.DVSD.frmDVSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDVSDNhanDieuChuyen.Text = frm._sMa;
            txtTenDVSDNhanDieuChuyen.Text = frm._sTen;
        }

        private void btnMaKho_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.Kho.frmKhoTK frm = new DanhMuc.Kho.frmKhoTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaKhoNhanDieuChuyen.Text = frm._sMa;
            txtTenKhoNhanDieuChuyen.Text = frm._sTen;
        }

        private void btnMaCCLD_ButtonClick(object sender, EventArgs e)
        {
            DanhMuc.NhomCCLD.frmSearchCCLD frm = new DanhMuc.NhomCCLD.frmSearchCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaCCLD.Text = frm._sMaCCLD;
            txtTenCCLD.Text = frm._sTenCCLD;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmDieuChuyenKhacDVSD f = new frmDieuChuyenKhacDVSD();
            f.StartPosition = FormStartPosition.CenterParent;
            f._sTrangThai = "Insert";
            f.ShowDialog();
            LoadDanhSach();
        }
        #endregion

        #region các hàm
        private void LoadDanhSach()
        {
            gcDieuChuyen.DataSource = cc.TimKiemDieuChuyenKhacDVSD(txtSoQuyetDinh.Text, txtSoGiaoDichTu.Text, txtSoGiaoDichDen.Text, datThoiGianTu.Text, datThoiGianDen.Text,
                                                        btnMaDVSDDieuChuyenDi.Text, btnMaDVSDNhanDieuChuyen.Text, btnMaKhoNhanDieuChuyen.Text, btnMaCCLD.Text, chkChoDuyet.Checked, chkDaDuyet.Checked, chkTuChoiDuyet.Checked,
                                                        chkHuyDuyet.Checked, "1"); //0: điều chuyển khác ĐVSD
            gvDieuChuyen.BestFitColumns(); //tự động điều chỉnh độ rộng của cột
        }

        #endregion

        
    }
}
