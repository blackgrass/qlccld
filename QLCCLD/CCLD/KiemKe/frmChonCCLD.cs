﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using QLCCLD.DanhMuc.NhomCCLD;

namespace QLCCLD.CCLD.KiemKe
{
    public partial class frmChonCCLD : DevExpress.XtraEditors.XtraForm
    {
        clsCCLD cc = new clsCCLD();
        public string _sMaCCLDDaChon = "";
        public string mode = "";
        public string _sMaDonViDaChon = "";
        public string MaCCLD = "";
        public string TenCCLD = "";
        public string NhomCCLD = "";
        public string DonVi = "";
        public string DonGia = "";
        public string DonGiaDGL = "";
        
        public frmChonCCLD()
        {
            InitializeComponent();
        }

        private void frmChonCCLD_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadDanhSach()
        {
            gcCCLD.DataSource = cc.LayCCLDKiemKe(btnMaNhomCCLD.Text, btnMaLoCCLD.Text, _sMaCCLDDaChon, _sMaDonViDaChon, mode);
            gvCCLD.BestFitColumns();
        }



        private void gvCCLD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            MaCCLD = gvCCLD.GetFocusedRowCellValue("MA_CCLD").ToString();
            TenCCLD = gvCCLD.GetFocusedRowCellValue("TEN_CCLD").ToString();
            NhomCCLD = gvCCLD.GetFocusedRowCellValue("TEN_NHOM_CCLD").ToString();
            DonVi = gvCCLD.GetFocusedRowCellValue("DON_VI").ToString();
            DonGia = gvCCLD.GetFocusedRowCellValue("DON_GIA").ToString();
            DonGiaDGL = gvCCLD.GetFocusedRowCellValue("DON_GIA_DGL").ToString(); 

            this.Close();
        }

        private void btnMaNhomCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmNhomCCLDTK frm = new frmNhomCCLDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaNhomCCLD.Text = frm._sMa;
            txtTenNhomCCLD.Text = frm._sTen;
        }

        private void btnMaLoCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmLoCCLD frm = new frmLoCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaLoCCLD.Text = frm._sMa;
            txtTenLoCCLD.Text = frm._sTen;
        }
    }
}