﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EM.UI;
using Business;
using System.Data.SqlClient;

namespace QLCCLD.CCLD.KiemKe
{
    public partial class frmChonCCLDChenhLech : DialogBase
    {
        public string _sMaCCLDDaChon = "";
        public string mode = "";
        public string _sMaDonViDaChon = "";
        public string MaCCLD = "";
        public string TenCCLD = "";
        public string NhomCCLD = "";
        public string DonVi = "";
        public string MaTinhTrang = "";
        public string TenTinhTrang = "";
        public string SoLuongKK = "";
        public string ThanhTienKK = "";
        public string SoLuongThieu = "";
        public string ThanhTienThieu = "";
        public DataTable dtChonCCLD = new DataTable();
        
        public string _sEvent = ""; //0: xem; 1: sửa

        public frmChonCCLDChenhLech()
        {
            InitializeComponent();
        }

        private void btnMaCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmChonCCLD frm = new frmChonCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.mode = mode;
            frm._sMaCCLDDaChon = _sMaCCLDDaChon;
            frm._sMaDonViDaChon = _sMaDonViDaChon;
            
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                MaCCLD = btnMaCCLD.Text = frm.MaCCLD;
                TenCCLD = txtTenCCLD.Text = frm.TenCCLD;
                NhomCCLD = frm.NhomCCLD;
                DonVi = frm.DonVi;
                txtDonGia.Text = txtThanhTien.Text = frm.DonGia;
                txtDonGiaDGL.Text = frm.DonGiaDGL;
                txtSoLuongSoSach.Text = "1";
                spinSoLuongKiemKe.Text = "1";
                txtThanhTienThieu.Text = "0";
            }
        }       

        private void btnMaTinhTrang_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.TinhTrang.frmTinhTrangTK frm = new QLCCLD.DanhMuc.TinhTrang.frmTinhTrangTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaTinhTrang.Text = frm._sMa;
            txtTenTinhTrang.Text = frm._sTen;
        }

        private void spinSoLuongThieu_EditValueChanged(object sender, EventArgs e)
        {
            txtThanhTienThieu.Text = (decimal.Parse(spinSoLuongThieu.Text) * decimal.Parse(txtDonGia.Text)).ToString();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (_sEvent == "1") // sửa
            {
                MaTinhTrang = btnMaTinhTrang.Text;
                TenTinhTrang = txtTenTinhTrang.Text;
                SoLuongKK = spinSoLuongKiemKe.Text;
                SoLuongThieu = spinSoLuongThieu.Text;
                ThanhTienThieu = txtThanhTienThieu.Text;
            }
            else
            {
                DataRow dr = dtChonCCLD.NewRow();
                dr["MA_CCLD"] = MaCCLD;
                dr["TEN_CCLD"] = TenCCLD;
                dr["NHOM_CCLD"] = NhomCCLD;
                dr["DON_VI"] = DonVi;
                dr["MA_TINH_TRANG"] = btnMaTinhTrang.Text;
                dr["TINH_TRANG"] = txtTenTinhTrang.Text;
                dr["SL_THEO_KK"] = spinSoLuongKiemKe.Text;
                dr["THANH_TIEN_THEO_KK"] = txtThanhTien.Text;
                dr["SL_THIEU"] = spinSoLuongThieu.Text;
                dr["THANH_TIEN_THIEU"] = txtThanhTienThieu.Text;

                dtChonCCLD.Rows.Add(dr);
            }
            this.Close();
        }

        private void frmChonCCLDChenhLech_Load(object sender, EventArgs e)
        {
            
            if (!string.IsNullOrEmpty(_sEvent))
            {
                btnMaCCLD.Text = MaCCLD;
                txtTenCCLD.Text = TenCCLD;
                DataTable dtb = DAL.RunCMDGetDataSet(new SqlCommand("SELECT * FROM CC_TANG WHERE MA_CCLD = '" + MaCCLD + "'")).Tables[0];
                txtDonGia.Text = txtThanhTien.Text = dtb.Rows[0]["DON_GIA"].ToString();
                txtDonGiaDGL.Text = dtb.Rows[0]["DON_GIA_DGL"].ToString();
                btnMaTinhTrang.Text = MaTinhTrang;
                txtTenTinhTrang.Text = TenTinhTrang;
                txtSoLuongSoSach.Text = "1";
                spinSoLuongKiemKe.Text = SoLuongKK;
                spinSoLuongThieu.Text = SoLuongThieu;
                txtThanhTienThieu.Text = ThanhTienThieu;
                btnMaCCLD.Enabled = _sEvent == "0" ? false : true;
                btnMaTinhTrang.Enabled = _sEvent == "0" ? false : true;
                spinSoLuongKiemKe.Enabled = _sEvent == "0" ? false : true;
                spinSoLuongThieu.Enabled = _sEvent == "0" ? false : true;
                btnGhi.Enabled = _sEvent == "0" ? false : true;
            }
        }

        private void spinSoLuongKiemKe_EditValueChanged(object sender, EventArgs e)
        {
            spinSoLuongThieu.Text = (int.Parse(txtSoLuongSoSach.Text.Replace(".", "")) - int.Parse(spinSoLuongKiemKe.Text.Replace(".", ""))).ToString();
            txtThanhTienThieu.Text = (int.Parse(spinSoLuongThieu.Text.Replace(".", "")) * decimal.Parse(txtDonGia.Text)).ToString();
        }
    }
}