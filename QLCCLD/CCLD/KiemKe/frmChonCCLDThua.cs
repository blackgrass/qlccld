﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EM.UI;
using Business;

namespace QLCCLD.CCLD.KiemKe
{
    public partial class frmChonCCLDThua : DialogBase
    {
        public string TenCCLD = "";
        public string MaNhomCCLD = "";
        public string NhomCCLD = "";
        public string DonViTinh = "";
        public string MaTinhTrang = "";
        public string TenTinhTrang = "";
        public string SoLuongSS = "";
        public string SoLuongKK = "";
        public string DonGia = "";
        public string DonGiaDGL = "";
        public string Don_Gia = "";
        public string MaDonVi = "";
        public string TenDonVi = "";
        public DataTable dtChonCCLD = new DataTable();
        public DataTable dtDonVi = new DataTable();
        public string _sEvent = ""; //0: xem; 1: sửa
        public int _sID = 0;
        clsCCLD cc = new clsCCLD();
        public frmChonCCLDThua()
        {
            InitializeComponent();
        }

        private void btnMaNhom_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.NhomCCLD.frmNhomCCLDTK frm = new QLCCLD.DanhMuc.NhomCCLD.frmNhomCCLDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaNhom.Text = frm._sMa;
            txtTenNhom.Text = frm._sTen;
            txtDonViTinh.Text = frm._sDonViTinh;
        }

        private void btnMaTinhTrang_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            QLCCLD.DanhMuc.TinhTrang.frmTinhTrangTK frm = new QLCCLD.DanhMuc.TinhTrang.frmTinhTrangTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaTinhTrang.Text = frm._sMa;
            txtTenTinhTrang.Text = frm._sTen;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (_sEvent == "1") // sửa
            {
                TenCCLD = txtTenCCLD.Text;
                MaTinhTrang = btnMaTinhTrang.Text;
                TenTinhTrang = txtTenTinhTrang.Text;
                SoLuongSS = spinSoLuongSoSach.Text;
                SoLuongKK = spinSoLuongKiemKe.Text;
                DonGia = spinDonGia.Text;
                DonGiaDGL = spinDonGiaDGL.Text;
                Don_Gia = (spinDonGia.Text != "0") ? spinDonGia.Text : spinDonGiaDGL.Text;
                MaNhomCCLD = btnMaNhom.Text;
                NhomCCLD = txtTenNhom.Text;
                DonViTinh = txtDonViTinh.Text;

                if (lkDonVi.EditValue != null)
                {
                    MaDonVi = lkDonVi.EditValue.ToString();
                    TenDonVi = lkDonVi.Text;
                }
            }
            else
            {
                DataRow dr = dtChonCCLD.NewRow();
                string id = "";
                if (_sID == 0)
                    id = cc.GetIdCCLDThua();
                else
                    id = (_sID + 1).ToString();
                _sID = int.Parse(id);
                dr["ID"] = id;
                dr["TEN_CCLD"] = txtTenCCLD.Text;
                dr["SL_THEO_SS"] = spinSoLuongSoSach.Text;
                dr["SL_THEO_KK"] = spinSoLuongKiemKe.Text;
                dr["DON_GIA"] = spinDonGia.Text;
                dr["DON_GIA_DGL"] = spinDonGiaDGL.Text;
                dr["DONGIA"] = (spinDonGia.Text != "0") ? spinDonGia.Text : spinDonGiaDGL.Text;
                dr["MA_NHOM_CCLD"] = btnMaNhom.Text;
                dr["NHOM_CCLD"] = txtTenNhom.Text;
                dr["DON_VI_TINH"] = txtDonViTinh.Text;
                dr["MA_TINH_TRANG"] = btnMaTinhTrang.Text;
                dr["TINH_TRANG"] = txtTenTinhTrang.Text;
                if (lkDonVi.EditValue != null)
                {
                    dr["DON_VI"] = lkDonVi.EditValue.ToString();
                    dr["TEN_DON_VI"] = lkDonVi.Text;
                }
                dtChonCCLD.Rows.Add(dr);
            }
            this.Close();
        }

        private void frmChonCCLDThua_Load(object sender, EventArgs e)
        {
            lkDonVi.Properties.DataSource = dtDonVi;
            if (!string.IsNullOrEmpty(_sEvent))
            {               
                txtTenCCLD.Text = TenCCLD;
                btnMaNhom.Text = MaNhomCCLD;
                txtTenNhom.Text = NhomCCLD;
                txtDonViTinh.Text = DonViTinh;
                lkDonVi.EditValue = MaDonVi;
                
                spinDonGia.Text = DonGia;
                spinDonGiaDGL.Text = DonGiaDGL;
                btnMaTinhTrang.Text = MaTinhTrang;
                txtTenTinhTrang.Text = TenTinhTrang;
                spinSoLuongSoSach.Text = SoLuongSS;
                spinSoLuongKiemKe.Text = SoLuongKK;

                txtTenCCLD.Enabled = _sEvent == "0" ? false : true;
                btnMaTinhTrang.Enabled = _sEvent == "0" ? false : true;
                spinSoLuongKiemKe.Enabled = _sEvent == "0" ? false : true;
                spinSoLuongSoSach.Enabled = _sEvent == "0" ? false : true;
                btnGhi.Enabled = _sEvent == "0" ? false : true;
            }
        }
    }
}