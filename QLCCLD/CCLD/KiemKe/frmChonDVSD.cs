﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.CCLD.KiemKe
{
    public partial class frmChonDVSD : DevExpress.XtraEditors.XtraForm
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string _sMaDaChon = "";
        public string mode = "";
        
        public DataTable dtChon = new DataTable();

        public frmChonDVSD()
        {
            InitializeComponent();
        }

        private void frmChonDVSD_Load(object sender, EventArgs e)
        {
            if (mode == "ĐVSD")
                Text = "Chọn Đơn vị";
            else
                Text = "Chọn kho";
            LoadDanhSach();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadDanhSach()
        {
            if (mode == "ĐVSD")
                gcDonVi.DataSource = dm.SearchDVSD(txtMaDVSD.Text, txtTenDVSD.Text, _sMaDaChon);
            else
                gcDonVi.DataSource = dm.SearchKho(txtMaDVSD.Text, txtTenDVSD.Text, _sMaDaChon);
            gvDonVi.BestFitColumns();
        }

        private void gvDonVi_Click(object sender, EventArgs e)
        {
            //check all 
            if (gvDonVi.PressedColumn == gvDonVi.Columns["is_check"])
            {
                int count = 0;
                for (int i = 0; i < gvDonVi.RowCount; i++)
                {
                    if ((bool)gvDonVi.GetRowCellValue(i, gvDonVi.Columns["is_check"]) == true)
                        count++;
                }

                if (count == gvDonVi.RowCount)
                {
                    for (int j = 0; j < gvDonVi.RowCount; j++)
                        gvDonVi.SetRowCellValue(j, "is_check", false);
                }
                else
                {
                    for (int i = 0; i < gvDonVi.RowCount; i++)
                        gvDonVi.SetRowCellValue(i, "is_check", true);
                }
            }
        }

        private void gvDonVi_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gvDonVi.RowCount; i++)
            {
                if ((bool)gvDonVi.GetRowCellValue(i, "is_check") == true)
                {
                    DataRow dr = dtChon.NewRow();
                    dr["MA_DON_VI"] = gvDonVi.GetRowCellValue(i, "MA").ToString();
                    dr["TEN_DON_VI"] = gvDonVi.GetRowCellValue(i, "TEN").ToString();
                    
                    dtChon.Rows.Add(dr);
                }
            }
            this.Close();
        }

        
    }
}