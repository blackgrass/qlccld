﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.CCLD.KiemKe
{
    public partial class frmChonKho : DevExpress.XtraEditors.XtraForm
    {
        public frmChonKho()
        {
            InitializeComponent();
        }
        clsCCLD cc = new clsCCLD();
        public string _sMa = "";
        public string _sTen = "";
        public string _sSoBB = "";

        private void LoadForm()
        {
            gcKho.DataSource = cc.SearchKhoKiemKe(txtMa.Text, txtTen.Text, _sSoBB);
        }

        private void frmChonKho_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LinkChon_Click(object sender, EventArgs e)
        {
            _sMa = gvKho.GetFocusedRowCellValue("MA_KHO").ToString();
            _sTen = gvKho.GetFocusedRowCellValue("TEN_KHO").ToString();
            this.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gvKho_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
            if (e.Column.Name == "Chon")
                e.DisplayText = "Chọn";
        }
    }
}