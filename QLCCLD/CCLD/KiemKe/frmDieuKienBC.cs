﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QLCCLD.CCLD.BaoCao;

namespace QLCCLD.CCLD.KiemKe
{
    public partial class frmDieuKienBC : DevExpress.XtraEditors.XtraForm
    {
        public string _sNghiepVu = "";
        public string _sThamSo = "";
        public int _iHienThi = 0;

        public frmDieuKienBC()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            BaoCao.frmReport frm = new frmReport();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sNghiepVu = "8";
            frm._sThamSo = _sThamSo;
            frm._sMaKho = btnMaKho.Text;
            frm._iHienThi = _iHienThi;//0: kiểm kê CCLĐ đang dùng; 1: kiểm kê CCLĐ trong kho
            frm.ShowDialog();
        }

        private void btnMaKho_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmChonKho frm = new frmChonKho();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sSoBB = _sThamSo;
            frm.ShowDialog();
            btnMaKho.Text = frm._sMa;
            txtTenKho.Text = frm._sTen;
        }
    }
}