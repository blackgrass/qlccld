﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using DevExpress.XtraLayout.Utils;
using EM.UI;
using System.Data.SqlClient;
using QLCCLD.CCLD.BaoCao;

namespace QLCCLD.CCLD.KiemKe
{
    public partial class frmKiemKe : DialogBase
    {
        #region Khai báo biến và tham số
        public string _sTrangThai = "";
        public string _sSoBienBan = "";
        clsCCLD cc = new clsCCLD();
        DataTable dtThanhVien = new DataTable();
        DataTable dtDVSD = new DataTable();
        DataTable dtChonCCLDChenhLech = new DataTable();
        DataTable dtChonCCLDThua = new DataTable();
        DataSet ds = new DataSet();
        List<string> lstDelCCLD = new List<string>();
        List<string> lstDelThanhVien = new List<string>();

        public frmKiemKe()
        {
            InitializeComponent();
        }

        #endregion

        #region Các sự kiện
        private void frmKiemKe_Load(object sender, EventArgs e)
        {
            CreateTable();
            if (_sTrangThai == "Search") //Tìm kiếm
            {
                LoadForm();
                VisiableControl(true);
            }
            else
            {
                ResetForm(); //Thêm mới
                VisiableControl(false);
            }
        }

        private void btnThemThanhVien_Click(object sender, EventArgs e)
        {
            frmThanhVien frm = new frmThanhVien();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sEvent = "Insert";
            frm.dtThanhVien = dtThanhVien.Copy();
            frm.ShowDialog();
            dtThanhVien = frm.dtThanhVien.Copy();
            gcThanhVien.DataSource = dtThanhVien;
            gvThanhVien.BestFitColumns();
        }

        private void btnXoaThanhVien_Click(object sender, EventArgs e)
        {
            if (gvThanhVien.RowCount != 0)
            {
                if (XtraMessageBox.Show("Xóa những thành viên này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // hiển thị câu hỏi xác nhận xóa
                {
                    foreach (int i in gvThanhVien.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string sIDThanhVienHD = gvThanhVien.GetRowCellValue(i, "ID_KIEM_KE_HD").ToString();
                        if (!string.IsNullOrEmpty(sIDThanhVienHD))
                            lstDelThanhVien.Add(sIDThanhVienHD); //lấy ID_KIEM_KE_HD của các bản ghi cần xóa trong cơ sở dữ liệu

                    }
                    for (int i = 0; i < lstDelThanhVien.Count; i++)
                    {
                        DataRow[] dr = dtThanhVien.Select("ID_KIEM_KE_HD='" + lstDelThanhVien[i] + "'");
                        dtThanhVien.Rows.Remove(dr[0]);
                    }

                    gcThanhVien.DataSource = dtThanhVien;
                    gvThanhVien.SelectRow(0);
                }
            }
        }

        private void LinkThanhVien_Click(object sender, EventArgs e)
        {
            frmThanhVien frm = new frmThanhVien();
            frm.StartPosition = FormStartPosition.CenterParent;

            frm._sHoTen = gvThanhVien.GetFocusedRowCellValue("HO_TEN").ToString();
            frm._sChucVu = gvThanhVien.GetFocusedRowCellValue("CHUC_VU").ToString();
            frm._sViTri = gvThanhVien.GetFocusedRowCellValue("VI_TRI").ToString();


            if (_sTrangThai == "Search") //xem chi tiết CCLD
            {
                frm._sEvent = "View"; //xem 
            }
            else //sửa 
            {
                frm._sEvent = "Edit"; //sửa 
            }
            frm.ShowDialog();
            int iIndex = gvThanhVien.GetSelectedRows()[0];
            dtThanhVien.Rows[iIndex]["HO_TEN"] = frm._sHoTen;
            dtThanhVien.Rows[iIndex]["CHUC_VU"] = frm._sChucVu;
            dtThanhVien.Rows[iIndex]["VI_TRI"] = frm._sViTri;

            gcThanhVien.DataSource = dtThanhVien;
            gvThanhVien.BestFitColumns();
        }

        private void btnThemDVSD_Click(object sender, EventArgs e)
        {
            frmChonDVSD frm = new frmChonDVSD();
            frm.StartPosition = FormStartPosition.CenterParent;
            if (chkDVSD.Checked)
                frm.mode = "ĐVSD";
            else
                frm.mode = "Kho";
            frm.dtChon = dtDVSD.Copy();
            string sMaDaChon = "";
            for (int i = 0; i < gvDVSD.RowCount; i++)
            {
                if (i > 0)
                    sMaDaChon += ",'" + gvDVSD.GetRowCellValue(i, "MA_DON_VI").ToString() + "'";
                else sMaDaChon += "'" + gvDVSD.GetRowCellValue(i, "MA_DON_VI").ToString() + "'";
            }
            frm._sMaDaChon = sMaDaChon;
            frm.ShowDialog();

            dtDVSD = frm.dtChon.Copy();
            gcDVSD.DataSource = dtDVSD;
            gvDVSD.BestFitColumns();
        }

        private void btnXoaDVSD_Click(object sender, EventArgs e)
        {
            if (gvDVSD.RowCount != 0)
            {
                List<string> lstDelDonVi = new List<string>();
                if (XtraMessageBox.Show("Xóa những bản ghi này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (int i in gvDVSD.GetSelectedRows())
                    {
                        string sMa = gvDVSD.GetRowCellValue(i, "MA_DON_VI").ToString();

                        lstDelDonVi.Add(sMa);
                    }
                    for (int i = 0; i < lstDelDonVi.Count; i++)
                    {
                        DataRow[] dr = dtDVSD.Select("MA_DON_VI='" + lstDelDonVi[i] + "'");
                        dtDVSD.Rows.Remove(dr[0]);
                    }
                    gcDVSD.DataSource = dtDVSD;
                    gvDVSD.BestFitColumns();
                    gvDVSD.SelectRow(0);
                }
            }
        }

        private void chkKho_CheckedChanged(object sender, EventArgs e)
        {
            dtDVSD.Clear();
            gcDVSD.DataSource = null;
            dtChonCCLDChenhLech.Clear();
            gcCCLDChenhLech.DataSource = null;
            dtChonCCLDThua.Clear();
            gcCCLDThua.DataSource = null;
        }

        private void chkDVSD_CheckedChanged(object sender, EventArgs e)
        {
            dtDVSD.Clear();
            gcDVSD.DataSource = null;
            dtChonCCLDChenhLech.Clear();
            gcCCLDChenhLech.DataSource = null;
            dtChonCCLDThua.Clear();
            gcCCLDThua.DataSource = null;
        }

        private void btnThemCCLDChenhLech_Click(object sender, EventArgs e)
        {
            if (HasErrors)
                return;
            string sMaCCLDDaChon = "";
            for (int i = 0; i < gvCCLDChenhLech.RowCount; i++)
            {
                if (i > 0)
                    sMaCCLDDaChon += ",'" + gvCCLDChenhLech.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
                else sMaCCLDDaChon += "'" + gvCCLDChenhLech.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
            }


            string sMaDonViDaChon = "";
            for (int i = 0; i < gvDVSD.RowCount; i++)
            {
                string madonvi = "";
                if (chkDVSD.Checked)
                    madonvi = gvDVSD.GetRowCellValue(i, "MA_DON_VI").ToString();
                else
                    madonvi = cc.LayIdKho(gvDVSD.GetRowCellValue(i, "MA_DON_VI").ToString());
                if (i > 0)
                    sMaDonViDaChon += ",'" + madonvi + "'";
                else sMaDonViDaChon += "'" + madonvi + "'";
            }

            frmChonCCLDChenhLech frm = new frmChonCCLDChenhLech();
            frm.StartPosition = FormStartPosition.CenterParent;
            if (chkDVSD.Checked)
                frm.mode = "ĐVSD";
            else
                frm.mode = "Kho";
            frm._sMaDonViDaChon = sMaDonViDaChon;
            frm.dtChonCCLD = dtChonCCLDChenhLech.Copy();

            frm._sMaCCLDDaChon = sMaCCLDDaChon;
            frm.ShowDialog();

            dtChonCCLDChenhLech = frm.dtChonCCLD.Copy();
            gcCCLDChenhLech.DataSource = dtChonCCLDChenhLech;
            gvCCLDChenhLech.BestFitColumns();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
            {
                DialogResult = DialogResult.None;
                return;
            }

            if (string.IsNullOrEmpty(txtSoBienBan.Text)) //thêm mới
            {
                if (gvDVSD.RowCount < 1)
                {
                    XtraMessageBox.Show("Lỗi: Chưa chọn ĐVSD/Kho kiểm kê");
                }
                else
                {
                    if (SaveData())
                        lblMessage.Text = "Ghi dữ liệu thành công!";
                    else lblMessage.Text = "Không ghi được dữ liệu!";
                    _sTrangThai = "Search";
                    LoadForm();
                    VisiableControl(true);
                }
            }
            else //sửa
            {
                if (SaveData())
                    lblMessage.Text = "Ghi dữ liệu thành công!";
                else lblMessage.Text = "Không ghi được dữ liệu!";
                _sTrangThai = "Search";
                LoadForm();
                VisiableControl(true);
            }
        }

        private void gvCCLD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnXoaCCLDChenhLech_Click(object sender, EventArgs e)
        {
            if (gvCCLDChenhLech.RowCount != 0)
            {
                List<string> lstDelCCLDOnDtChonCCLD = new List<string>();
                if (XtraMessageBox.Show("Xóa những CCLĐ này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // hiển thị câu hỏi xác nhận xóa
                {
                    foreach (int i in gvCCLDChenhLech.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string sMa = gvCCLDChenhLech.GetRowCellValue(i, "MA_CCLD").ToString(); // GetRowCellValue để lấy ra giá trị của cột MA ở dòng thứ i
                        if (!string.IsNullOrEmpty(sMa))
                            lstDelCCLDOnDtChonCCLD.Add(sMa);//lấy MA_CCLD của các bản ghi cần xóa trong datatable dtChonCCLD
                    }
                    for (int i = 0; i < lstDelCCLDOnDtChonCCLD.Count; i++) //xóa CCLĐ tại dtChonCCLD
                    {
                        DataRow[] dr = dtChonCCLDChenhLech.Select("MA_CCLD='" + lstDelCCLDOnDtChonCCLD[i] + "'");
                        dtChonCCLDChenhLech.Rows.Remove(dr[0]);
                    }
                    gcCCLDChenhLech.DataSource = dtChonCCLDChenhLech;
                    gvCCLDChenhLech.BestFitColumns();
                    gvCCLDChenhLech.SelectRow(0);
                }
            }
        }

        private void linkCCLDChenhLech_Click(object sender, EventArgs e)
        {
            frmChonCCLDChenhLech frm = new frmChonCCLDChenhLech();
            frm.StartPosition = FormStartPosition.CenterParent;

            frm.MaCCLD = gvCCLDChenhLech.GetFocusedRowCellValue("MA_CCLD").ToString();
            frm.TenCCLD = gvCCLDChenhLech.GetFocusedRowCellValue("TEN_CCLD").ToString();
            frm.NhomCCLD = gvCCLDChenhLech.GetFocusedRowCellValue("NHOM_CCLD").ToString();
            frm.DonVi = gvCCLDChenhLech.GetFocusedRowCellValue("DON_VI").ToString();
            frm.MaTinhTrang = gvCCLDChenhLech.GetFocusedRowCellValue("MA_TINH_TRANG").ToString();
            frm.TenTinhTrang = gvCCLDChenhLech.GetFocusedRowCellValue("TINH_TRANG").ToString();
            frm.SoLuongKK = gvCCLDChenhLech.GetFocusedRowCellValue("SL_THEO_KK").ToString();
            frm.ThanhTienKK = gvCCLDChenhLech.GetFocusedRowCellValue("THANH_TIEN_THEO_KK").ToString();
            frm.SoLuongThieu = gvCCLDChenhLech.GetFocusedRowCellValue("SL_THIEU").ToString();
            frm.ThanhTienThieu = gvCCLDChenhLech.GetFocusedRowCellValue("THANH_TIEN_THIEU").ToString();

            if (_sTrangThai == "Search") //xem chi tiết CCLD
            {
                frm._sEvent = "0"; //xem 
            }
            else //sửa 
            {
                frm._sEvent = "1"; //sửa 
            }
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                int iIndex = gvCCLDChenhLech.GetSelectedRows()[0];
                dtChonCCLDChenhLech.Rows[iIndex]["MA_TINH_TRANG"] = frm.MaTinhTrang;
                dtChonCCLDChenhLech.Rows[iIndex]["TINH_TRANG"] = frm.TenTinhTrang;
                dtChonCCLDChenhLech.Rows[iIndex]["SL_THEO_KK"] = frm.SoLuongKK;
                dtChonCCLDChenhLech.Rows[iIndex]["SL_THIEU"] = frm.SoLuongThieu;
                dtChonCCLDChenhLech.Rows[iIndex]["THANH_TIEN_THIEU"] = frm.ThanhTienThieu;

                gcCCLDChenhLech.DataSource = dtChonCCLDChenhLech;
                gvCCLDChenhLech.BestFitColumns();
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _sTrangThai = "Update";
            VisiableControl(false);
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            if (_sTrangThai == "Insert")
            {
                LoadForm();
                this.Close();
            }
            else
            {
                _sTrangThai = "Search";
                LoadForm();
                VisiableControl(true);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult Result = XtraMessageBox.Show(this, "Xóa biên bản kiểm kê " + txtSoBienBan.Text + "?", "Xóa biên bản kiểm kê",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Result == DialogResult.Yes)
            {
                // xóa CCLD chênh lệch
                foreach (DataRow dr in dtChonCCLDChenhLech.Rows)
                {
                    cc.DeleteKiemKeCT(txtSoBienBan.Text, dr["MA_CCLD"].ToString());
                }
                // xóa CCLD đủ
                string Loai = (chkDVSD.Checked) ? "0" : "1";
                DataTable dtCCLDDu = LayDanhSachCCLDKiemKeDu(Loai);
                foreach (DataRow dr in dtCCLDDu.Rows)
                {
                    cc.DeleteKiemKeCT(txtSoBienBan.Text, dr["MA_CCLD"].ToString());
                }
                cc.DeleteKiemKe(txtSoBienBan.Text);
                this.Close();
            }
        }

        private void btnDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetKiemKe(txtSoBienBan.Text, "1"))
                lblMessage.Text = "Bạn đã duyệt thành công";
            else
                lblMessage.Text = "Bạn đã duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnTuChoiDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetKiemKe(txtSoBienBan.Text, "2"))
                lblMessage.Text = "Bạn đã từ chối duyệt thành công";
            else
                lblMessage.Text = "Bạn đã từ chối duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnHuyDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetKiemKe(txtSoBienBan.Text, "3"))
                lblMessage.Text = "Bạn đã hủy duyệt thành công";
            else
                lblMessage.Text = "Bạn đã hủy duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnThemCCLDThua_Click(object sender, EventArgs e)
        {
            frmChonCCLDThua frm = new frmChonCCLDThua();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.dtChonCCLD = dtChonCCLDThua.Copy();
            frm.dtDonVi = dtDVSD.Copy();
            frm.ShowDialog();
            dtChonCCLDThua = frm.dtChonCCLD.Copy();
            gcCCLDThua.DataSource = dtChonCCLDThua;
            gvCCLDThua.BestFitColumns();
        }

        private void btnXoaCCLDThua_Click(object sender, EventArgs e)
        {
            if (gvCCLDThua.RowCount != 0)
            {
                List<string> lstDelCCLDOnDtChonCCLD = new List<string>();
                if (XtraMessageBox.Show("Xóa những CCLĐ này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (int i in gvCCLDThua.GetSelectedRows())
                    {
                        string sMa = gvCCLDThua.GetRowCellValue(i, "ID").ToString();
                        if (!string.IsNullOrEmpty(sMa))
                            lstDelCCLDOnDtChonCCLD.Add(sMa);
                    }
                    for (int i = 0; i < lstDelCCLDOnDtChonCCLD.Count; i++)
                    {
                        DataRow[] dr = dtChonCCLDThua.Select("ID='" + lstDelCCLDOnDtChonCCLD[i] + "'");
                        dtChonCCLDThua.Rows.Remove(dr[0]);
                    }
                    gcCCLDThua.DataSource = dtChonCCLDThua;
                    gvCCLDThua.BestFitColumns();
                    gvCCLDThua.SelectRow(0);
                }
            }
        }

        private void linkCCLDThua_Click(object sender, EventArgs e)
        {
            frmChonCCLDThua frm = new frmChonCCLDThua();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.dtDonVi = dtDVSD.Copy();

            frm.TenCCLD = gvCCLDThua.GetFocusedRowCellValue("TEN_CCLD").ToString();
            frm.MaNhomCCLD = gvCCLDThua.GetFocusedRowCellValue("MA_NHOM_CCLD").ToString();
            frm.NhomCCLD = gvCCLDThua.GetFocusedRowCellValue("NHOM_CCLD").ToString();
            frm.DonViTinh = gvCCLDThua.GetFocusedRowCellValue("DON_VI_TINH").ToString();
            frm.MaDonVi = gvCCLDThua.GetFocusedRowCellValue("DON_VI").ToString();
            frm.TenDonVi = gvCCLDThua.GetFocusedRowCellValue("TEN_DON_VI").ToString();
            frm.MaTinhTrang = gvCCLDThua.GetFocusedRowCellValue("MA_TINH_TRANG").ToString();
            frm.TenTinhTrang = gvCCLDThua.GetFocusedRowCellValue("TINH_TRANG").ToString();
            frm.SoLuongSS = gvCCLDThua.GetFocusedRowCellValue("SL_THEO_SS").ToString();
            frm.SoLuongKK = gvCCLDThua.GetFocusedRowCellValue("SL_THEO_KK").ToString();
            frm.DonGia = gvCCLDThua.GetFocusedRowCellValue("DON_GIA").ToString();
            frm.DonGiaDGL = gvCCLDThua.GetFocusedRowCellValue("DON_GIA_DGL").ToString();

            if (_sTrangThai == "Search") //xem chi tiết CCLD
            {
                frm._sEvent = "0"; //xem 
            }
            else //sửa 
            {
                frm._sEvent = "1"; //sửa 
            }
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                int iIndex = gvCCLDThua.GetSelectedRows()[0];
                dtChonCCLDThua.Rows[iIndex]["TEN_CCLD"] = frm.TenCCLD;
                dtChonCCLDThua.Rows[iIndex]["MA_TINH_TRANG"] = frm.MaTinhTrang;
                dtChonCCLDThua.Rows[iIndex]["TINH_TRANG"] = frm.TenTinhTrang;
                dtChonCCLDThua.Rows[iIndex]["SL_THEO_SS"] = frm.SoLuongSS;
                dtChonCCLDThua.Rows[iIndex]["SL_THEO_KK"] = frm.SoLuongKK;
                dtChonCCLDThua.Rows[iIndex]["DON_GIA"] = frm.DonGia;
                dtChonCCLDThua.Rows[iIndex]["DON_GIA_DGL"] = frm.DonGiaDGL;
                dtChonCCLDThua.Rows[iIndex]["DONGIA"] = frm.Don_Gia;
                dtChonCCLDThua.Rows[iIndex]["MA_NHOM_CCLD"] = frm.MaNhomCCLD;
                dtChonCCLDThua.Rows[iIndex]["NHOM_CCLD"] = frm.NhomCCLD;
                dtChonCCLDThua.Rows[iIndex]["DON_VI_TINH"] = frm.DonViTinh;
                dtChonCCLDThua.Rows[iIndex]["DON_VI"] = frm.MaDonVi;
                dtChonCCLDThua.Rows[iIndex]["TEN_DON_VI"] = frm.TenDonVi;
                gcCCLDThua.DataSource = dtChonCCLDThua;
                gvCCLDThua.BestFitColumns();
            }
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            if (chkDVSD.Checked)
            {
                BaoCao.frmReport frm = new frmReport();
                frm.StartPosition = FormStartPosition.CenterParent;
                frm._sNghiepVu = "8";
                frm._sThamSo = txtSoBienBan.Text;
                frm._iHienThi = chkDVSD.Checked ? 0 : 1;//0: kiểm kê CCLĐ đang dùng; 1: kiểm kê CCLĐ trong kho
                frm.ShowDialog();
            }
            else
            {
                frmDieuKienBC frm = new frmDieuKienBC();
                frm.StartPosition = FormStartPosition.CenterParent;
                frm._sNghiepVu = "8";
                frm._sThamSo = txtSoBienBan.Text;
                frm._iHienThi = 1;//0: kiểm kê CCLĐ đang dùng; 1: kiểm kê CCLĐ trong kho
                frm.ShowDialog();
            }
        }

        #endregion

        #region Các hàm

        private void VisiableControl(bool bHide)
        {
            if (_sTrangThai == "Insert") //thêm mới
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemThanhVien.Visibility = LayoutVisibility.Always;
                lcXoaThanhVien.Visibility = LayoutVisibility.Always;
                lcThemDonVi.Visibility = LayoutVisibility.Always;
                lcXoaDonVi.Visibility = LayoutVisibility.Always;
                lcThemCCLDChenhLech.Visibility = LayoutVisibility.Always;
                lcXoaCCLDChenhLech.Visibility = LayoutVisibility.Always;
                lcThemCCLDThua.Visibility = LayoutVisibility.Always;
                lcXoaCCLDThua.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Never;
            }
            else if (_sTrangThai == "Search") //tìm kiếm
            {
                lcGhi.Visibility = LayoutVisibility.Never;
                lcHuyBo.Visibility = LayoutVisibility.Never;
                lcThemThanhVien.Visibility = LayoutVisibility.Never;
                lcXoaThanhVien.Visibility = LayoutVisibility.Never;
                lcThemDonVi.Visibility = LayoutVisibility.Never;
                lcXoaDonVi.Visibility = LayoutVisibility.Never;
                lcThemCCLDChenhLech.Visibility = LayoutVisibility.Never;
                lcXoaCCLDChenhLech.Visibility = LayoutVisibility.Never;
                lcThemCCLDThua.Visibility = LayoutVisibility.Never;
                lcXoaCCLDThua.Visibility = LayoutVisibility.Never;

                lcSua.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcXoa.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcHuyDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Always;
            }
            else //sửa thông tin
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemThanhVien.Visibility = LayoutVisibility.Always;
                lcXoaThanhVien.Visibility = LayoutVisibility.Always;
                lcThemDonVi.Visibility = LayoutVisibility.Always;
                lcXoaDonVi.Visibility = LayoutVisibility.Always;
                lcThemCCLDChenhLech.Visibility = LayoutVisibility.Always;
                lcXoaCCLDChenhLech.Visibility = LayoutVisibility.Always;
                lcThemCCLDThua.Visibility = LayoutVisibility.Always;
                lcXoaCCLDThua.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Never;
            }

            datNgayKiemKe.Properties.ReadOnly = bHide;
            txtThuTruongDonVi.Properties.ReadOnly = bHide;
            txtTruongPhongHanhChinh.Properties.ReadOnly = bHide;
            txtKiemSoatVien.Properties.ReadOnly = bHide;
            txtTruongPhongKeToan.Properties.ReadOnly = bHide;
            txtThuKho.Properties.ReadOnly = bHide;
            txtChuTich.Properties.ReadOnly = bHide;
            chkDVSD.Properties.ReadOnly = bHide;
            chkKho.Properties.ReadOnly = bHide;
        }

        private void LoadForm()
        {
            //load thông tin chung
            ds = cc.GetBienBanKiemKe(_sSoBienBan);
            if (ds.Tables[0].Rows.Count < 1)
                return;
            switch (ds.Tables[0].Rows[0]["TRANG_THAI"].ToString())
            {
                case "0":
                    txtTrangThai.Text = "Chờ duyệt";
                    break;
                case "1":
                    txtTrangThai.Text = "Đã duyệt";
                    break;
                case "2":
                    txtTrangThai.Text = "Từ chối duyệt";
                    break;
                case "3":
                    txtTrangThai.Text = "Hủy duyệt";
                    break;
            }
            txtSoBienBan.Text = _sSoBienBan;
            datNgayKiemKe.Text = ds.Tables[0].Rows[0]["NGAY_KIEM_KE"].ToString();
            txtThuTruongDonVi.Text = ds.Tables[0].Rows[0]["THU_TRUONG_DON_VI"].ToString();
            txtTruongPhongHanhChinh.Text = ds.Tables[0].Rows[0]["TRUONG_PHONG_HANH_CHINH"].ToString();
            txtTruongPhongKeToan.Text = ds.Tables[0].Rows[0]["TRUONG_PHONG_KE_TOAN"].ToString();
            txtKiemSoatVien.Text = ds.Tables[0].Rows[0]["KIEM_SOAT_VIEN"].ToString();
            txtChuTich.Text = ds.Tables[0].Rows[0]["CHU_TICH_HOI_DONG"].ToString();
            txtThuKho.Text = ds.Tables[0].Rows[0]["THU_KHO"].ToString();
            chkDVSD.Checked = ds.Tables[0].Rows[0]["LOAI_KIEM_KE"].ToString() == "0" ? true : false;
            chkKho.Checked = ds.Tables[0].Rows[0]["LOAI_KIEM_KE"].ToString() == "1" ? true : false;
            //load grid hội đồng
            gcThanhVien.DataSource = ds.Tables[1];
            gvThanhVien.BestFitColumns();
            dtThanhVien = ds.Tables[1].Copy();

            //load grid ĐVSD/Kho
            gcDVSD.DataSource = ds.Tables[2];
            gvDVSD.BestFitColumns();
            dtDVSD = ds.Tables[2].Copy();

            //load grid CCLD chênh lệch
            gcCCLDChenhLech.DataSource = ds.Tables[3];
            gvCCLDChenhLech.BestFitColumns();
            dtChonCCLDChenhLech = ds.Tables[3].Copy();

            //load grid CCLD thừa
            gcCCLDThua.DataSource = ds.Tables[4];
            gvCCLDThua.BestFitColumns();
            dtChonCCLDThua = ds.Tables[4].Copy();
        }

        private bool SaveData()
        {
            bool bResult = true;
            string Loai = (chkDVSD.Checked) ? "0" : "1";
            try
            {
                if (_sTrangThai == "Insert") //thêm mới 
                {
                    string sSoBienBan = cc.GetSoBienBanKiemKe();

                    bResult = cc.InsertKiemKe(datNgayKiemKe.Text, sSoBienBan, Loai, txtThuTruongDonVi.Text, txtTruongPhongHanhChinh.Text, txtTruongPhongKeToan.Text, txtKiemSoatVien.Text, txtChuTich.Text, txtThuKho.Text);
                    _sSoBienBan = sSoBienBan;
                }
                else //sửa thông tin
                {
                    bResult = cc.UpdateKiemKe(datNgayKiemKe.Text, txtSoBienBan.Text, Loai, txtThuTruongDonVi.Text, txtTruongPhongHanhChinh.Text, txtTruongPhongKeToan.Text, txtKiemSoatVien.Text, txtChuTich.Text, txtThuKho.Text);
                    _sSoBienBan = txtSoBienBan.Text;
                }

                // thêm hội đồng kiểm kê
                foreach (DataRow dr in dtThanhVien.Rows)
                {
                    bResult = cc.InsertKiemKeHD(_sSoBienBan, dr["HO_TEN"].ToString(), dr["CHUC_VU"].ToString(), dr["VI_TRI"].ToString());
                }

                // thêm ĐVSD/Kho kiểm kê
                foreach (DataRow dr in dtDVSD.Rows)
                {
                    bResult = cc.InsertKiemKePV(_sSoBienBan, dr["MA_DON_VI"].ToString(), Loai);
                }

                //thêm CCLĐ chênh lệch
                foreach (DataRow dr in dtChonCCLDChenhLech.Rows)
                {
                    string thanhtien_ss = DAL.RunCMDGetString(new SqlCommand("SELECT ISNULL(DON_GIA, DON_GIA_DGL) FROM CC_TANG WHERE MA_CCLD = '" + dr["MA_CCLD"].ToString() + "'"));
                    bResult = cc.InsertKiemKeCL(_sSoBienBan, dr["MA_CCLD"].ToString(), dr["MA_TINH_TRANG"].ToString(), "1", dr["SL_THEO_KK"].ToString(), thanhtien_ss, dr["THANH_TIEN_THEO_KK"].ToString());
                }

                //thêm CCLĐ thừa
                foreach (DataRow dr in dtChonCCLDThua.Rows)
                {
                    bResult = cc.InsertKiemKeThua(_sSoBienBan, dr["TEN_CCLD"].ToString(), dr["MA_TINH_TRANG"].ToString(), dr["SL_THEO_SS"].ToString(), dr["SL_THEO_KK"].ToString(), dr["DON_GIA"].ToString(), dr["DON_GIA_DGL"].ToString(), dr["DON_VI"].ToString(), dr["MA_NHOM_CCLD"].ToString(), Loai);
                }

                // lấy danh sách CCLĐ đủ (tất cả CCLĐ trong ĐVSD/Kho trừ những CCLĐ chênh lệch)
                DataTable dtCCLDDu = LayDanhSachCCLDKiemKeDu(Loai);
                // thêm CCLD đủ
                foreach (DataRow dr in dtCCLDDu.Rows)
                {
                    bResult = cc.InsertKiemKeDu(_sSoBienBan, dr["ID_CCLD"].ToString(), dr["MA_TINH_TRANG"].ToString(), dr["MA_DVSDTS"].ToString(), dr["ID_KHO"].ToString());
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return bResult;
        }

        private DataTable LayDanhSachCCLDKiemKeDu(string sLoai)
        {
            string sMaDonVi = "";
            for (int i = 0; i < gvDVSD.RowCount; i++)
            {
                if (sLoai == "0") // ĐVSD
                {
                    if (i > 0)
                        sMaDonVi += ",'" + gvDVSD.GetRowCellValue(i, "MA_DON_VI").ToString() + "'";
                    else sMaDonVi += "'" + gvDVSD.GetRowCellValue(i, "MA_DON_VI").ToString() + "'";
                }
                else
                {
                    if (i > 0)
                        sMaDonVi += "," + cc.LayIdKho(gvDVSD.GetRowCellValue(i, "MA_DON_VI").ToString());
                    else sMaDonVi += cc.LayIdKho(gvDVSD.GetRowCellValue(i, "MA_DON_VI").ToString());
                }
            }

            string sMaCCLDChenhLech = "";
            for (int i = 0; i < gvCCLDChenhLech.RowCount; i++)
            {
                if (i > 0)
                    sMaCCLDChenhLech += ",'" + gvCCLDChenhLech.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
                else sMaCCLDChenhLech += "'" + gvCCLDChenhLech.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
            }

            SqlCommand cmd = new SqlCommand();
            if (sLoai == "0") // ĐVSD
                cmd.CommandText = "SELECT * FROM CC_TANG WHERE MA_DVSDTS IN (" + sMaDonVi + ")";
            else
                cmd.CommandText = "SELECT * FROM CC_TANG WHERE ID_KHO IN (" + sMaDonVi + ")";
            if (sMaCCLDChenhLech != "")
                cmd.CommandText += " AND MA_CCLD NOT IN (" + sMaCCLDChenhLech + ")";
            return DAL.RunCMDGetDataSet(cmd).Tables[0];
        }

        private void ResetForm()
        {
            datNgayKiemKe.ResetText();
            txtThuTruongDonVi.ResetText();
            txtTruongPhongHanhChinh.ResetText();
            txtKiemSoatVien.ResetText();
            txtTruongPhongKeToan.ResetText();
            txtThuKho.ResetText();
            txtChuTich.ResetText();
        }

        private void CreateTable()
        {
            dtThanhVien.Columns.Add("ID_KIEM_KE_HD");
            dtThanhVien.Columns.Add("HO_TEN");
            dtThanhVien.Columns.Add("CHUC_VU");
            dtThanhVien.Columns.Add("VI_TRI");

            dtDVSD.Columns.Add("MA_DON_VI");
            dtDVSD.Columns.Add("TEN_DON_VI");

            dtChonCCLDChenhLech.Columns.Add("MA_CCLD");
            dtChonCCLDChenhLech.Columns.Add("TEN_CCLD");
            dtChonCCLDChenhLech.Columns.Add("NHOM_CCLD");
            dtChonCCLDChenhLech.Columns.Add("DON_VI");
            dtChonCCLDChenhLech.Columns.Add("MA_TINH_TRANG");
            dtChonCCLDChenhLech.Columns.Add("TINH_TRANG");
            dtChonCCLDChenhLech.Columns.Add("SL_THEO_KK");
            dtChonCCLDChenhLech.Columns.Add("THANH_TIEN_THEO_KK");
            dtChonCCLDChenhLech.Columns.Add("SL_THIEU");
            dtChonCCLDChenhLech.Columns.Add("THANH_TIEN_THIEU");

            dtChonCCLDThua.Columns.Add("ID");
            dtChonCCLDThua.Columns.Add("TEN_CCLD");
            dtChonCCLDThua.Columns.Add("DON_VI");
            dtChonCCLDThua.Columns.Add("TEN_DON_VI");
            dtChonCCLDThua.Columns.Add("SL_THEO_SS");
            dtChonCCLDThua.Columns.Add("SL_THEO_KK");
            dtChonCCLDThua.Columns.Add("DONGIA");
            dtChonCCLDThua.Columns.Add("DON_GIA");
            dtChonCCLDThua.Columns.Add("DON_GIA_DGL");
            dtChonCCLDThua.Columns.Add("MA_NHOM_CCLD");
            dtChonCCLDThua.Columns.Add("NHOM_CCLD");
            dtChonCCLDThua.Columns.Add("DON_VI_TINH");
            dtChonCCLDThua.Columns.Add("MA_TINH_TRANG");
            dtChonCCLDThua.Columns.Add("TINH_TRANG");
        }

        private bool DuyetKiemKe(string sSoBienBan, string sTrangThai)
        {
            bool bResult = true;
            try
            {
                bResult = cc.DuyetKiemKe(sSoBienBan, sTrangThai);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bResult;
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            datNgayKiemKe.ValidateEmptyStringRule(); // check bắt buộc nhập
        }

        #endregion

    }
}