﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using EM.UI;
using Business;

namespace QLCCLD.CCLD.KiemKe
{
    public partial class frmThanhVien : DialogBase
    {
        public string _sEvent = "";
        public string _sHoTen = "";
        public string _sChucVu = "";
        public string _sViTri = "";
        public int _sID = 0;
        public DataTable dtThanhVien = new DataTable();
        clsCCLD cc = new clsCCLD();

        public frmThanhVien()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmThanhVien_Load(object sender, EventArgs e)
        {
            if (_sEvent == "View")
            {
                LoadForm();
                HideControl(true);
                btnLuuVaThem.Enabled = false;
            }
            else if (_sEvent == "Insert")
            {
                ResetForm();
                HideControl(false);
                btnLuuVaThem.Enabled = true;
            }
            else //Edit
            {
                LoadForm();
                HideControl(false);
                btnLuuVaThem.Enabled = false;
            }
        }

        private void LoadForm()
        {
            txtHoTen.Text = _sHoTen;
            txtChucVu.Text = _sChucVu;
            chkTruongBan.Checked = _sViTri == "Trưởng ban" ? true : false;
            chkUyVien.Checked = _sViTri == "Trưởng ban" ? false : true;
        }

        private void HideControl(bool bHide)
        {
            txtHoTen.Properties.ReadOnly = bHide;
            txtChucVu.Properties.ReadOnly = bHide;
            chkTruongBan.Properties.ReadOnly = bHide;
            chkUyVien.Properties.ReadOnly = bHide;
            btnLuu.Enabled = !bHide;

        }

        private void ResetForm()
        {
            txtHoTen.ResetText();
            txtChucVu.ResetText();
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (HasErrors)
                return;
            SaveData();
            this.Close();
        }

        private void btnLuuVaThem_Click(object sender, EventArgs e)
        {
            if (HasErrors)
                return;
            SaveData();
            ResetForm();
        }

        private void SaveData()
        {
            if (_sEvent == "Insert")
            {
                string id_kiemke = "";
                if (_sID == 0)
                    id_kiemke = cc.GetIdThanhVienKiemKe();
                else
                    id_kiemke = (_sID + 1).ToString();
                _sID = int.Parse(id_kiemke);
                DataRow dr = dtThanhVien.NewRow();
                dr["ID_KIEM_KE_HD"] = id_kiemke;
                dr["HO_TEN"] = txtHoTen.Text;
                dr["CHUC_VU"] = txtChucVu.Text;
                dr["VI_TRI"] = chkTruongBan.Checked ? "Trưởng ban" : "Ủy viên";
                dtThanhVien.Rows.Add(dr);
            }
            else //Edit
            {
                _sHoTen = txtHoTen.Text;
                _sChucVu = txtChucVu.Text;
                _sViTri = chkTruongBan.Checked ? "Trưởng ban" : "Ủy viên";
            }
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            txtHoTen.ValidateEmptyStringRule(); // check bắt buộc nhập
            txtChucVu.ValidateEmptyStringRule();
        }

    }
}