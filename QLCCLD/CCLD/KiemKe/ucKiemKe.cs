﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.CCLD.KiemKe
{
    public partial class ucKiemKe : DevExpress.XtraEditors.XtraUserControl
    {
        #region "khai báo biến và tham số"
        clsCCLD cc = new clsCCLD();

        public ucKiemKe()
        {
            InitializeComponent();
        }
        #endregion

        #region các sự kiện
        private void ucKiemKe_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void gvBienBan_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void LinkSoBienBan_Click(object sender, EventArgs e)
        {
            frmKiemKe frm = new frmKiemKe();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sTrangThai = "Search";
            frm._sSoBienBan = gvBienBan.GetFocusedRowCellValue("SO_BIEN_BAN").ToString();
            frm.ShowDialog();
            LoadDanhSach();
        }

        private void btnMaKho_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.Kho.frmKhoTK frm = new DanhMuc.Kho.frmKhoTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaKho.Text = frm._sMa;
            txtTenKho.Text = frm._sTen;
        }

        private void btnMaDVSD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.DVSD.frmDVSDTK frm = new DanhMuc.DVSD.frmDVSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDVSD.Text = frm._sMa;
            txtTenDVSD.Text = frm._sTen;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmKiemKe f = new frmKiemKe();
            f.StartPosition = FormStartPosition.CenterParent;
            f._sTrangThai = "Insert";
            f.ShowDialog();
            LoadDanhSach();
        }
        #endregion

        #region các hàm
        private void LoadDanhSach()
        {
            gcBienBan.DataSource = cc.TimKiemKiemKe(txtSoBienBanTu.Text, txtSoBienBanDen.Text, datThoiGianTu.Text, datThoiGianDen.Text,
                                                     btnMaDVSD.Text, btnMaKho.Text, chkChoDuyet.Checked, chkDaDuyet.Checked, chkTuChoiDuyet.Checked,
                                                        chkHuyDuyet.Checked);
            gvBienBan.BestFitColumns(); //tự động điều chỉnh độ rộng của cột
        }

        #endregion
    }
}
