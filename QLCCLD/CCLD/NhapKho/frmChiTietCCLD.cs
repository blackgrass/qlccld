﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QLCCLD.DanhMuc.LyDoNhapKho;
using EM.UI;

namespace QLCCLD.CCLD.NhapKho
{
    public partial class frmChiTietCCLD : DialogBase
    {
        public string _sMaCCLD = "";
        public string _sTenCCLD = "";
        public string _sDonGia = "";
        public string _sDonViTinh = "";
        public string _sMaLyDo = "";
        public string _sTenLyDo = "";
        public string _sEvent = ""; //0: xem; 1: sửa

        public frmChiTietCCLD()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmChiTietCCLD_Load(object sender, EventArgs e)
        {
            txtMaCCLD.Text = _sMaCCLD;
            txtTenCCLD.Text = _sTenCCLD;
            txtDonGia.Text = _sDonGia;
            txtDonViTinh.Text = _sDonViTinh;
            btnMaLyDo.Text = _sMaLyDo;
            txtTenLyDo.Text = _sTenLyDo;
            btnMaLyDo.Enabled = _sEvent == "0" ? false : true;
            btnLuu.Enabled = _sEvent == "0" ? false : true;
        }

        private void btnMaLyDo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.LyDoNhapKho.frmLyDoNhapKhoTK frm = new frmLyDoNhapKhoTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaLyDo.Text = frm._sMa;
            txtTenLyDo.Text = frm._sTen;
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            _sMaLyDo = btnMaLyDo.Text;
            _sTenLyDo = txtTenLyDo.Text;
            this.Close();
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            btnMaLyDo.ValidateEmptyStringRule(); // check bắt buộc nhập
            
        }
    }
}