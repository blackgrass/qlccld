﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using EM.UI;
using QLCCLD.DanhMuc.DVSD;
using QLCCLD.DanhMuc.LyDoNhapKho;
using QLCCLD.DanhMuc.NhomCCLD;

namespace QLCCLD.CCLD.NhapKho
{
    public partial class frmChonCCLD : DialogBase
    {
        #region Khai báo các biến và thuộc tính
        clsCCLD cc = new clsCCLD();
        public string _sMaCCLDDaChon = "";
        public DataTable dtChonCCLD = new DataTable();

        public frmChonCCLD()
        {
            InitializeComponent();
        }
        #endregion

        #region Các sự kiện
        private void frmChonCCLD_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnMaLyDo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmLyDoNhapKhoTK frm = new frmLyDoNhapKhoTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaLyDo.Text = frm._sMa;
            txtTenLyDo.Text = frm._sTen;
        }

        private void btnMaDVSD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.DVSD.frmDVSDTK frm = new frmDVSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDVSD.Text = frm._sMa;
            txtTenDVSD.Text = frm._sTen;
        }

        private void btnMaNhomCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmNhomCCLDTK frm = new frmNhomCCLDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaNhomCCLD.Text = frm._sMa;
            txtTenNhomCCLD.Text = frm._sTen;
        }

        private void btnMaLoCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmLoCCLD frm = new frmLoCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaLoCCLD.Text = frm._sMa;
            txtTenLoCCLD.Text = frm._sTen;
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void gvCCLD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            if (HasErrors)
                return;
            for (int i = 0; i < gvCCLD.RowCount; i++)
            {
                if ((bool)gvCCLD.GetRowCellValue(i, "is_check") == true)
                {
                    DataRow dr = dtChonCCLD.NewRow();
                    dr["MA_CCLD"] = gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString();
                    dr["TEN_CCLD"] = gvCCLD.GetRowCellValue(i, "TEN_CCLD").ToString();
                    dr["DON_VI_TINH"] = gvCCLD.GetRowCellValue(i, "DON_VI_TINH").ToString();
                    dr["DON_GIA"] = gvCCLD.GetRowCellValue(i, "DON_GIA").ToString();
                    dr["TONG_TIEN"] = gvCCLD.GetRowCellValue(i, "TONG_TIEN").ToString();
                    dr["MA_LY_DO_NHAP"] = btnMaLyDo.Text;
                    dr["TEN_LY_DO_NHAP"] = txtTenLyDo.Text;
                    dr["SL_THUC_TE"] = gvCCLD.GetRowCellValue(i, "SL_THUC_TE").ToString();
                    dr["TEN_DVSDTS"] = gvCCLD.GetRowCellValue(i, "TEN_DVSDTS").ToString();
                    dtChonCCLD.Rows.Add(dr);
                }
            }
            this.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gvCCLD_Click(object sender, EventArgs e)
        { //check all 
            if (gvCCLD.PressedColumn == gvCCLD.Columns["is_check"])
            {
                int count = 0;
                for (int i = 0; i < gvCCLD.RowCount; i++)
                {
                    if ((bool)gvCCLD.GetRowCellValue(i, gvCCLD.Columns["is_check"]) == true)
                        count++;
                }

                if (count == gvCCLD.RowCount)
                {
                    for (int j = 0; j < gvCCLD.RowCount; j++)
                        gvCCLD.SetRowCellValue(j, "is_check", false);
                }
                else
                {
                    for (int i = 0; i < gvCCLD.RowCount; i++)
                        gvCCLD.SetRowCellValue(i, "is_check", true);
                }
            }
        }

        #endregion

        #region Các hàm
        private void LoadDanhSach()
        {
            gcCCLD.DataSource = cc.TimKiemCCLD(chkCCLDChuaBanGiao.Checked, chkCCLDDangDung.Checked, btnMaDVSD.Text,
                                               btnMaNhomCCLD.Text, btnMaLoCCLD.Text,_sMaCCLDDaChon);
            gvCCLD.BestFitColumns();//tự động điều chỉnh độ rộng của cột
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            btnMaLyDo.ValidateEmptyStringRule(); // check bắt buộc nhập
        }

        #endregion

        
    }
}