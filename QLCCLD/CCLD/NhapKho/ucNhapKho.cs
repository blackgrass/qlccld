﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using QLCCLD.DanhMuc.Kho;
using QLCCLD.DanhMuc.NhomCCLD;

namespace QLCCLD.CCLD.NhapKho
{
    public partial class ucNhapKho : DevExpress.XtraEditors.XtraUserControl
    {
        #region khai báo biến và tham số
        clsCCLD cc = new clsCCLD();

        public ucNhapKho()
        {
            InitializeComponent();
        }
        #endregion

        #region các sự kiện
        private void ucNhapKho_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void gvNhapKho_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void LinkSoPhieu_Click(object sender, EventArgs e)
        {
            frmNhapKho frm = new frmNhapKho();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sTrangThai = "Search";
            frm._sSoPhieu = gvNhapKho.GetFocusedRowCellValue("SO_PHIEU").ToString();
            frm.ShowDialog();
            LoadDanhSach();
        }

        private void btnMaKho_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.Kho.frmKhoTK frm = new frmKhoTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaKho.Text = frm._sMa;
            txtTenKho.Text = frm._sTen;
        }

        private void btnMaCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmSearchCCLD frm = new frmSearchCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sNghiepVu = "3"; //nhập kho
            frm.ShowDialog();
            btnMaCCLD.Text = frm._sMaCCLD;
            txtTenCCLD.Text = frm._sTenCCLD;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmNhapKho f = new frmNhapKho();
            f.StartPosition = FormStartPosition.CenterParent;
            f._sTrangThai = "Insert";
            f.ShowDialog();
            LoadDanhSach();
        }
        #endregion

        #region các hàm
        private void LoadDanhSach()
        {
            gcNhapKho.DataSource = cc.TimKiemNhapXuat(txtSoPhieuTu.Text, txtSoPhieuDen.Text, datThoiGianNhapTu.Text, datThoiGianNhapDen.Text, 
                                                        btnMaKho.Text, btnMaCCLD.Text, chkChoDuyet.Checked, chkDaDuyet.Checked, chkTuChoiDuyet.Checked, 
                                                        chkHuyDuyet.Checked,"1"); //nhập kho
            gvNhapKho.BestFitColumns(); //tự động điều chỉnh độ rộng của cột
        }

        #endregion

       

        

        

    }
}
