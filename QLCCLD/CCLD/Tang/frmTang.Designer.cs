﻿namespace QLCCLD.CCLD.Tang
{
    partial class frmTang
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.btnDong = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.lblMessage = new DevExpress.XtraEditors.LabelControl();
            this.txtTrangThai = new DevExpress.XtraEditors.TextEdit();
            this.btnHuyDuyet = new DevExpress.XtraEditors.SimpleButton();
            this.btnTuChoiDuyet = new DevExpress.XtraEditors.SimpleButton();
            this.btnDuyet = new DevExpress.XtraEditors.SimpleButton();
            this.btnXoa = new DevExpress.XtraEditors.SimpleButton();
            this.btnSua = new DevExpress.XtraEditors.SimpleButton();
            this.btnHuyBo = new DevExpress.XtraEditors.SimpleButton();
            this.btnGhi = new DevExpress.XtraEditors.SimpleButton();
            this.txtDienGiai = new DevExpress.XtraEditors.TextEdit();
            this.btnXoaLo = new DevExpress.XtraEditors.SimpleButton();
            this.btnThemLo = new DevExpress.XtraEditors.SimpleButton();
            this.gcLoCCLD = new DevExpress.XtraGrid.GridControl();
            this.gvLoCCLD = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LinkTenLo = new DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.datNgayTang = new DevExpress.XtraEditors.DateEdit();
            this.txtSoGiaoDich = new DevExpress.XtraEditors.TextEdit();
            this.datNgayChungTu = new DevExpress.XtraEditors.DateEdit();
            this.txtSoChungTu = new DevExpress.XtraEditors.TextEdit();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcThemLo = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcXoaLo = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcGhi = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcHuyBo = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcSua = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcXoa = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcDuyet = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcTuChoiDuyet = new DevExpress.XtraLayout.LayoutControlItem();
            this.lcHuyDuyet = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTrangThai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienGiai.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLoCCLD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLoCCLD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkTenLo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datNgayTang.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datNgayTang.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGiaoDich.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datNgayChungTu.Properties.VistaTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datNgayChungTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoChungTu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcThemLo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcXoaLo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcGhi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcHuyBo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcSua)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcXoa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcDuyet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcTuChoiDuyet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcHuyDuyet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            this.SuspendLayout();
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.btnDong);
            this.layoutControl1.Controls.Add(this.labelControl2);
            this.layoutControl1.Controls.Add(this.labelControl1);
            this.layoutControl1.Controls.Add(this.lblMessage);
            this.layoutControl1.Controls.Add(this.txtTrangThai);
            this.layoutControl1.Controls.Add(this.btnHuyDuyet);
            this.layoutControl1.Controls.Add(this.btnTuChoiDuyet);
            this.layoutControl1.Controls.Add(this.btnDuyet);
            this.layoutControl1.Controls.Add(this.btnXoa);
            this.layoutControl1.Controls.Add(this.btnSua);
            this.layoutControl1.Controls.Add(this.btnHuyBo);
            this.layoutControl1.Controls.Add(this.btnGhi);
            this.layoutControl1.Controls.Add(this.txtDienGiai);
            this.layoutControl1.Controls.Add(this.btnXoaLo);
            this.layoutControl1.Controls.Add(this.btnThemLo);
            this.layoutControl1.Controls.Add(this.gcLoCCLD);
            this.layoutControl1.Controls.Add(this.datNgayTang);
            this.layoutControl1.Controls.Add(this.txtSoGiaoDich);
            this.layoutControl1.Controls.Add(this.datNgayChungTu);
            this.layoutControl1.Controls.Add(this.txtSoChungTu);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(776, 408);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // btnDong
            // 
            this.btnDong.Image = global::QLCCLD.Properties.Resources.exit2;
            this.btnDong.Location = new System.Drawing.Point(688, 12);
            this.btnDong.Name = "btnDong";
            this.btnDong.Size = new System.Drawing.Size(76, 22);
            this.btnDong.StyleController = this.layoutControl1;
            this.btnDong.TabIndex = 24;
            this.btnDong.Text = "Đóng";
            this.btnDong.Click += new System.EventHandler(this.btnDong_Click);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            this.labelControl2.Location = new System.Drawing.Point(393, 62);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(6, 13);
            this.labelControl2.StyleController = this.layoutControl1;
            this.labelControl2.TabIndex = 23;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Red;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            this.labelControl1.Location = new System.Drawing.Point(393, 86);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(6, 13);
            this.labelControl1.StyleController = this.layoutControl1;
            this.labelControl1.TabIndex = 22;
            this.labelControl1.Text = "*";
            // 
            // lblMessage
            // 
            this.lblMessage.Appearance.ForeColor = System.Drawing.Color.Red;
            this.lblMessage.Appearance.Options.UseForeColor = true;
            this.lblMessage.Location = new System.Drawing.Point(12, 383);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(752, 13);
            this.lblMessage.StyleController = this.layoutControl1;
            this.lblMessage.TabIndex = 21;
            // 
            // txtTrangThai
            // 
            this.txtTrangThai.Enabled = false;
            this.txtTrangThai.Location = new System.Drawing.Point(106, 38);
            this.txtTrangThai.Name = "txtTrangThai";
            this.txtTrangThai.Properties.ReadOnly = true;
            this.txtTrangThai.Size = new System.Drawing.Size(283, 20);
            this.txtTrangThai.StyleController = this.layoutControl1;
            this.txtTrangThai.TabIndex = 20;
            // 
            // btnHuyDuyet
            // 
            this.btnHuyDuyet.Image = global::QLCCLD.Properties.Resources.delete;
            this.btnHuyDuyet.Location = new System.Drawing.Point(438, 12);
            this.btnHuyDuyet.Name = "btnHuyDuyet";
            this.btnHuyDuyet.Size = new System.Drawing.Size(86, 22);
            this.btnHuyDuyet.StyleController = this.layoutControl1;
            this.btnHuyDuyet.TabIndex = 19;
            this.btnHuyDuyet.Text = "Hủy duyệt";
            this.btnHuyDuyet.Click += new System.EventHandler(this.btnHuyDuyet_Click);
            // 
            // btnTuChoiDuyet
            // 
            this.btnTuChoiDuyet.Image = global::QLCCLD.Properties.Resources.delete_16x161;
            this.btnTuChoiDuyet.Location = new System.Drawing.Point(338, 12);
            this.btnTuChoiDuyet.Name = "btnTuChoiDuyet";
            this.btnTuChoiDuyet.Size = new System.Drawing.Size(96, 22);
            this.btnTuChoiDuyet.StyleController = this.layoutControl1;
            this.btnTuChoiDuyet.TabIndex = 18;
            this.btnTuChoiDuyet.Text = "Từ chối duyệt";
            this.btnTuChoiDuyet.Click += new System.EventHandler(this.btnTuChoiDuyet_Click);
            // 
            // btnDuyet
            // 
            this.btnDuyet.Image = global::QLCCLD.Properties.Resources.apply_16x16;
            this.btnDuyet.Location = new System.Drawing.Point(258, 12);
            this.btnDuyet.Name = "btnDuyet";
            this.btnDuyet.Size = new System.Drawing.Size(76, 22);
            this.btnDuyet.StyleController = this.layoutControl1;
            this.btnDuyet.TabIndex = 17;
            this.btnDuyet.Text = "Duyệt";
            this.btnDuyet.Click += new System.EventHandler(this.btnDuyet_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Image = global::QLCCLD.Properties.Resources.delete_16x16;
            this.btnXoa.Location = new System.Drawing.Point(178, 12);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(76, 22);
            this.btnXoa.StyleController = this.layoutControl1;
            this.btnXoa.TabIndex = 16;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnSua
            // 
            this.btnSua.Image = global::QLCCLD.Properties.Resources.edit_16x16___Copy;
            this.btnSua.Location = new System.Drawing.Point(98, 12);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(76, 22);
            this.btnSua.StyleController = this.layoutControl1;
            this.btnSua.TabIndex = 15;
            this.btnSua.Text = "Sửa";
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnHuyBo
            // 
            this.btnHuyBo.Image = global::QLCCLD.Properties.Resources.cancel_16x16;
            this.btnHuyBo.Location = new System.Drawing.Point(608, 12);
            this.btnHuyBo.Name = "btnHuyBo";
            this.btnHuyBo.Size = new System.Drawing.Size(76, 22);
            this.btnHuyBo.StyleController = this.layoutControl1;
            this.btnHuyBo.TabIndex = 14;
            this.btnHuyBo.Text = "Hủy bỏ";
            this.btnHuyBo.Click += new System.EventHandler(this.btnHuyBo_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Image = global::QLCCLD.Properties.Resources.save_16x16;
            this.btnGhi.Location = new System.Drawing.Point(528, 12);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(76, 22);
            this.btnGhi.StyleController = this.layoutControl1;
            this.btnGhi.TabIndex = 13;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // txtDienGiai
            // 
            this.txtDienGiai.Location = new System.Drawing.Point(106, 110);
            this.txtDienGiai.Name = "txtDienGiai";
            this.txtDienGiai.Properties.MaxLength = 255;
            this.txtDienGiai.Size = new System.Drawing.Size(658, 20);
            this.txtDienGiai.StyleController = this.layoutControl1;
            this.txtDienGiai.TabIndex = 12;
            // 
            // btnXoaLo
            // 
            this.btnXoaLo.Image = global::QLCCLD.Properties.Resources.delete_16x16;
            this.btnXoaLo.Location = new System.Drawing.Point(94, 134);
            this.btnXoaLo.Name = "btnXoaLo";
            this.btnXoaLo.Size = new System.Drawing.Size(78, 22);
            this.btnXoaLo.StyleController = this.layoutControl1;
            this.btnXoaLo.TabIndex = 11;
            this.btnXoaLo.Text = "Xóa lô";
            this.btnXoaLo.Click += new System.EventHandler(this.btnXoaLo_Click);
            // 
            // btnThemLo
            // 
            this.btnThemLo.Image = global::QLCCLD.Properties.Resources.add_16x16;
            this.btnThemLo.Location = new System.Drawing.Point(12, 134);
            this.btnThemLo.Name = "btnThemLo";
            this.btnThemLo.Size = new System.Drawing.Size(78, 22);
            this.btnThemLo.StyleController = this.layoutControl1;
            this.btnThemLo.TabIndex = 10;
            this.btnThemLo.Text = "Thêm lô";
            this.btnThemLo.Click += new System.EventHandler(this.btnThemLo_Click);
            // 
            // gcLoCCLD
            // 
            this.gcLoCCLD.Location = new System.Drawing.Point(12, 176);
            this.gcLoCCLD.MainView = this.gvLoCCLD;
            this.gcLoCCLD.Name = "gcLoCCLD";
            this.gcLoCCLD.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.LinkTenLo});
            this.gcLoCCLD.Size = new System.Drawing.Size(752, 203);
            this.gcLoCCLD.TabIndex = 9;
            this.gcLoCCLD.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvLoCCLD});
            // 
            // gvLoCCLD
            // 
            this.gvLoCCLD.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn2,
            this.gridColumn3,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn6});
            this.gvLoCCLD.GridControl = this.gcLoCCLD;
            this.gvLoCCLD.Name = "gvLoCCLD";
            this.gvLoCCLD.OptionsView.ShowGroupPanel = false;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tên lô";
            this.gridColumn2.ColumnEdit = this.LinkTenLo;
            this.gridColumn2.FieldName = "TEN_LO_CCLD";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.ReadOnly = true;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            // 
            // LinkTenLo
            // 
            this.LinkTenLo.AutoHeight = false;
            this.LinkTenLo.Name = "LinkTenLo";
            this.LinkTenLo.Click += new System.EventHandler(this.LinkTenLo_Click);
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "Số lượng";
            this.gridColumn3.FieldName = "SO_LUONG";
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.OptionsColumn.ReadOnly = true;
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 1;
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Đơn giá/Đơn giá ĐGL";
            this.gridColumn4.FieldName = "DON_GIA_CC";
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.OptionsColumn.ReadOnly = true;
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 2;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Thành tiền";
            this.gridColumn5.FieldName = "TONG_TIEN";
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.OptionsColumn.ReadOnly = true;
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 3;
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Mã lô CCLĐ";
            this.gridColumn6.FieldName = "MA_LO_CCLD";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.OptionsColumn.ReadOnly = true;
            // 
            // datNgayTang
            // 
            this.datNgayTang.EditValue = null;
            this.datNgayTang.Location = new System.Drawing.Point(497, 86);
            this.datNgayTang.Name = "datNgayTang";
            this.datNgayTang.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.datNgayTang.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.datNgayTang.Size = new System.Drawing.Size(267, 20);
            this.datNgayTang.StyleController = this.layoutControl1;
            this.datNgayTang.TabIndex = 7;
            // 
            // txtSoGiaoDich
            // 
            this.txtSoGiaoDich.Location = new System.Drawing.Point(106, 86);
            this.txtSoGiaoDich.Name = "txtSoGiaoDich";
            this.txtSoGiaoDich.Properties.ReadOnly = true;
            this.txtSoGiaoDich.Size = new System.Drawing.Size(283, 20);
            this.txtSoGiaoDich.StyleController = this.layoutControl1;
            this.txtSoGiaoDich.TabIndex = 6;
            // 
            // datNgayChungTu
            // 
            this.datNgayChungTu.EditValue = null;
            this.datNgayChungTu.Location = new System.Drawing.Point(497, 62);
            this.datNgayChungTu.Name = "datNgayChungTu";
            this.datNgayChungTu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.datNgayChungTu.Properties.VistaTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.datNgayChungTu.Size = new System.Drawing.Size(267, 20);
            this.datNgayChungTu.StyleController = this.layoutControl1;
            this.datNgayChungTu.TabIndex = 5;
            // 
            // txtSoChungTu
            // 
            this.txtSoChungTu.Location = new System.Drawing.Point(106, 62);
            this.txtSoChungTu.Name = "txtSoChungTu";
            this.txtSoChungTu.Properties.MaxLength = 30;
            this.txtSoChungTu.Size = new System.Drawing.Size(283, 20);
            this.txtSoChungTu.StyleController = this.layoutControl1;
            this.txtSoChungTu.TabIndex = 4;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem3,
            this.layoutControlItem4,
            this.layoutControlItem6,
            this.lcThemLo,
            this.lcXoaLo,
            this.emptySpaceItem1,
            this.layoutControlItem5,
            this.emptySpaceItem2,
            this.lcGhi,
            this.lcHuyBo,
            this.lcSua,
            this.lcXoa,
            this.lcDuyet,
            this.lcTuChoiDuyet,
            this.lcHuyDuyet,
            this.layoutControlItem7,
            this.emptySpaceItem3,
            this.layoutControlItem8,
            this.layoutControlItem9,
            this.layoutControlItem10,
            this.layoutControlItem11});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Size = new System.Drawing.Size(776, 408);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Text = "layoutControlGroup1";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.txtSoChungTu;
            this.layoutControlItem1.CustomizationFormText = "Số chứng từ";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 50);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(381, 24);
            this.layoutControlItem1.Text = "Số chứng từ";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(91, 13);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.datNgayChungTu;
            this.layoutControlItem2.CustomizationFormText = "Ngày chứng từ";
            this.layoutControlItem2.Location = new System.Drawing.Point(391, 50);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem2.Text = "Ngày chứng từ";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(91, 13);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.txtSoGiaoDich;
            this.layoutControlItem3.CustomizationFormText = "Số giao dịch";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 74);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(381, 24);
            this.layoutControlItem3.Text = "Số giao dịch";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(91, 13);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.datNgayTang;
            this.layoutControlItem4.CustomizationFormText = "Ngày tăng";
            this.layoutControlItem4.Location = new System.Drawing.Point(391, 74);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(365, 24);
            this.layoutControlItem4.Text = "Ngày tăng";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(91, 13);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.gcLoCCLD;
            this.layoutControlItem6.CustomizationFormText = "Danh sách lô CCLĐ";
            this.layoutControlItem6.Location = new System.Drawing.Point(0, 148);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(756, 223);
            this.layoutControlItem6.Text = "Danh sách lô CCLĐ";
            this.layoutControlItem6.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem6.TextSize = new System.Drawing.Size(91, 13);
            // 
            // lcThemLo
            // 
            this.lcThemLo.Control = this.btnThemLo;
            this.lcThemLo.CustomizationFormText = "layoutControlItem7";
            this.lcThemLo.Location = new System.Drawing.Point(0, 122);
            this.lcThemLo.MaxSize = new System.Drawing.Size(82, 26);
            this.lcThemLo.MinSize = new System.Drawing.Size(82, 26);
            this.lcThemLo.Name = "lcThemLo";
            this.lcThemLo.Size = new System.Drawing.Size(82, 26);
            this.lcThemLo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcThemLo.Text = "lcThemLo";
            this.lcThemLo.TextSize = new System.Drawing.Size(0, 0);
            this.lcThemLo.TextToControlDistance = 0;
            this.lcThemLo.TextVisible = false;
            // 
            // lcXoaLo
            // 
            this.lcXoaLo.Control = this.btnXoaLo;
            this.lcXoaLo.CustomizationFormText = "layoutControlItem8";
            this.lcXoaLo.Location = new System.Drawing.Point(82, 122);
            this.lcXoaLo.MaxSize = new System.Drawing.Size(100, 26);
            this.lcXoaLo.MinSize = new System.Drawing.Size(82, 26);
            this.lcXoaLo.Name = "lcXoaLo";
            this.lcXoaLo.Size = new System.Drawing.Size(82, 26);
            this.lcXoaLo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcXoaLo.Text = "lcXoaLo";
            this.lcXoaLo.TextSize = new System.Drawing.Size(0, 0);
            this.lcXoaLo.TextToControlDistance = 0;
            this.lcXoaLo.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(164, 122);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(592, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.txtDienGiai;
            this.layoutControlItem5.CustomizationFormText = "Diễn giải";
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 98);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(756, 24);
            this.layoutControlItem5.Text = "Diễn giải";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(91, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 0);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(86, 26);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcGhi
            // 
            this.lcGhi.Control = this.btnGhi;
            this.lcGhi.CustomizationFormText = "layoutControlItem9";
            this.lcGhi.Location = new System.Drawing.Point(516, 0);
            this.lcGhi.MaxSize = new System.Drawing.Size(80, 26);
            this.lcGhi.MinSize = new System.Drawing.Size(80, 26);
            this.lcGhi.Name = "lcGhi";
            this.lcGhi.Size = new System.Drawing.Size(80, 26);
            this.lcGhi.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcGhi.Text = "lcGhi";
            this.lcGhi.TextSize = new System.Drawing.Size(0, 0);
            this.lcGhi.TextToControlDistance = 0;
            this.lcGhi.TextVisible = false;
            // 
            // lcHuyBo
            // 
            this.lcHuyBo.Control = this.btnHuyBo;
            this.lcHuyBo.CustomizationFormText = "lcHuyBo";
            this.lcHuyBo.Location = new System.Drawing.Point(596, 0);
            this.lcHuyBo.MaxSize = new System.Drawing.Size(80, 26);
            this.lcHuyBo.MinSize = new System.Drawing.Size(80, 26);
            this.lcHuyBo.Name = "lcHuyBo";
            this.lcHuyBo.Size = new System.Drawing.Size(80, 26);
            this.lcHuyBo.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcHuyBo.Text = "lcHuyBo";
            this.lcHuyBo.TextSize = new System.Drawing.Size(0, 0);
            this.lcHuyBo.TextToControlDistance = 0;
            this.lcHuyBo.TextVisible = false;
            // 
            // lcSua
            // 
            this.lcSua.Control = this.btnSua;
            this.lcSua.CustomizationFormText = "lcSua";
            this.lcSua.Location = new System.Drawing.Point(86, 0);
            this.lcSua.MaxSize = new System.Drawing.Size(80, 26);
            this.lcSua.MinSize = new System.Drawing.Size(80, 26);
            this.lcSua.Name = "lcSua";
            this.lcSua.Size = new System.Drawing.Size(80, 26);
            this.lcSua.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcSua.Text = "lcSua";
            this.lcSua.TextSize = new System.Drawing.Size(0, 0);
            this.lcSua.TextToControlDistance = 0;
            this.lcSua.TextVisible = false;
            // 
            // lcXoa
            // 
            this.lcXoa.Control = this.btnXoa;
            this.lcXoa.CustomizationFormText = "lcXoa";
            this.lcXoa.Location = new System.Drawing.Point(166, 0);
            this.lcXoa.MaxSize = new System.Drawing.Size(80, 26);
            this.lcXoa.MinSize = new System.Drawing.Size(80, 26);
            this.lcXoa.Name = "lcXoa";
            this.lcXoa.Size = new System.Drawing.Size(80, 26);
            this.lcXoa.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcXoa.Text = "lcXoa";
            this.lcXoa.TextSize = new System.Drawing.Size(0, 0);
            this.lcXoa.TextToControlDistance = 0;
            this.lcXoa.TextVisible = false;
            // 
            // lcDuyet
            // 
            this.lcDuyet.Control = this.btnDuyet;
            this.lcDuyet.CustomizationFormText = "lcDuyet";
            this.lcDuyet.Location = new System.Drawing.Point(246, 0);
            this.lcDuyet.MaxSize = new System.Drawing.Size(80, 26);
            this.lcDuyet.MinSize = new System.Drawing.Size(80, 26);
            this.lcDuyet.Name = "lcDuyet";
            this.lcDuyet.Size = new System.Drawing.Size(80, 26);
            this.lcDuyet.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcDuyet.Text = "lcDuyet";
            this.lcDuyet.TextSize = new System.Drawing.Size(0, 0);
            this.lcDuyet.TextToControlDistance = 0;
            this.lcDuyet.TextVisible = false;
            // 
            // lcTuChoiDuyet
            // 
            this.lcTuChoiDuyet.Control = this.btnTuChoiDuyet;
            this.lcTuChoiDuyet.CustomizationFormText = "lcTuChoiDuyet";
            this.lcTuChoiDuyet.Location = new System.Drawing.Point(326, 0);
            this.lcTuChoiDuyet.MaxSize = new System.Drawing.Size(100, 26);
            this.lcTuChoiDuyet.MinSize = new System.Drawing.Size(100, 26);
            this.lcTuChoiDuyet.Name = "lcTuChoiDuyet";
            this.lcTuChoiDuyet.Size = new System.Drawing.Size(100, 26);
            this.lcTuChoiDuyet.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcTuChoiDuyet.Text = "lcTuChoiDuyet";
            this.lcTuChoiDuyet.TextSize = new System.Drawing.Size(0, 0);
            this.lcTuChoiDuyet.TextToControlDistance = 0;
            this.lcTuChoiDuyet.TextVisible = false;
            // 
            // lcHuyDuyet
            // 
            this.lcHuyDuyet.Control = this.btnHuyDuyet;
            this.lcHuyDuyet.CustomizationFormText = "lcThoaiDuyet";
            this.lcHuyDuyet.Location = new System.Drawing.Point(426, 0);
            this.lcHuyDuyet.MaxSize = new System.Drawing.Size(90, 26);
            this.lcHuyDuyet.MinSize = new System.Drawing.Size(90, 26);
            this.lcHuyDuyet.Name = "lcHuyDuyet";
            this.lcHuyDuyet.Size = new System.Drawing.Size(90, 26);
            this.lcHuyDuyet.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.lcHuyDuyet.Text = "lcHuyDuyet";
            this.lcHuyDuyet.TextSize = new System.Drawing.Size(0, 0);
            this.lcHuyDuyet.TextToControlDistance = 0;
            this.lcHuyDuyet.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.txtTrangThai;
            this.layoutControlItem7.CustomizationFormText = "Trạng thái";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(381, 24);
            this.layoutControlItem7.Text = "Trạng thái";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(91, 13);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(381, 26);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(375, 24);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.lblMessage;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(0, 371);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(756, 17);
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.labelControl1;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(381, 74);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(10, 24);
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.labelControl2;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(381, 50);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(10, 24);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.btnDong;
            this.layoutControlItem11.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItem11.Location = new System.Drawing.Point(676, 0);
            this.layoutControlItem11.MaxSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem11.MinSize = new System.Drawing.Size(80, 26);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(80, 26);
            this.layoutControlItem11.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem11.Text = "layoutControlItem11";
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextToControlDistance = 0;
            this.layoutControlItem11.TextVisible = false;
            // 
            // frmTang
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(776, 408);
            this.Controls.Add(this.layoutControl1);
            this.Name = "frmTang";
            this.Text = "Tăng lô CCLĐ";
            this.Load += new System.EventHandler(this.frmTang_Load);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtTrangThai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDienGiai.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLoCCLD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvLoCCLD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LinkTenLo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datNgayTang.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datNgayTang.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoGiaoDich.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datNgayChungTu.Properties.VistaTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datNgayChungTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSoChungTu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcThemLo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcXoaLo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcGhi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcHuyBo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcSua)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcXoa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcDuyet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcTuChoiDuyet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcHuyDuyet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.TextEdit txtDienGiai;
        private DevExpress.XtraEditors.SimpleButton btnXoaLo;
        private DevExpress.XtraEditors.SimpleButton btnThemLo;
        private DevExpress.XtraGrid.GridControl gcLoCCLD;
        private DevExpress.XtraGrid.Views.Grid.GridView gvLoCCLD;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private DevExpress.XtraEditors.DateEdit datNgayTang;
        private DevExpress.XtraEditors.TextEdit txtSoGiaoDich;
        private DevExpress.XtraEditors.DateEdit datNgayChungTu;
        private DevExpress.XtraEditors.TextEdit txtSoChungTu;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem lcThemLo;
        private DevExpress.XtraLayout.LayoutControlItem lcXoaLo;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.SimpleButton btnHuyBo;
        private DevExpress.XtraEditors.SimpleButton btnGhi;
        private DevExpress.XtraLayout.LayoutControlItem lcGhi;
        private DevExpress.XtraLayout.LayoutControlItem lcHuyBo;
        private DevExpress.XtraEditors.SimpleButton btnHuyDuyet;
        private DevExpress.XtraEditors.SimpleButton btnTuChoiDuyet;
        private DevExpress.XtraEditors.SimpleButton btnDuyet;
        private DevExpress.XtraEditors.SimpleButton btnXoa;
        private DevExpress.XtraEditors.SimpleButton btnSua;
        private DevExpress.XtraLayout.LayoutControlItem lcSua;
        private DevExpress.XtraLayout.LayoutControlItem lcXoa;
        private DevExpress.XtraLayout.LayoutControlItem lcDuyet;
        private DevExpress.XtraLayout.LayoutControlItem lcTuChoiDuyet;
        private DevExpress.XtraLayout.LayoutControlItem lcHuyDuyet;
        private DevExpress.XtraEditors.TextEdit txtTrangThai;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemHyperLinkEdit LinkTenLo;
        private DevExpress.XtraEditors.LabelControl lblMessage;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.SimpleButton btnDong;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
    }
}