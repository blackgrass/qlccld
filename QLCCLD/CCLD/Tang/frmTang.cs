﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using Business;
using EM.UI;

namespace QLCCLD.CCLD.Tang
{
    public partial class frmTang : DialogBase
    {
        #region Khai báo biến và tham số

        public string _sTrangThai = "";
        public string _sSoGiaoDich = "";
        clsCCLD cc = new clsCCLD();
        DataTable dtTang = new DataTable();
        DataSet ds = new DataSet(); //chứa dữ liệu 1 giao dịch đã chọn
        List<string> lstDelLoCCLD = new List<string>();

        public frmTang()
        {
            InitializeComponent();
        }
        #endregion

        #region các sự kiện
        private void frmTang_Load(object sender, EventArgs e)
        {
            CreateTable();
            if (_sTrangThai == "Search") //Tìm kiếm
            {
                LoadForm();
                VisiableControl(true);
            }
            else
            {
                ResetForm(); //Thêm mới
                VisiableControl(false);
            }
        }

        private void btnThemLo_Click(object sender, EventArgs e)
        {
            frmThongTinLo frm = new frmThongTinLo();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sEvent = "1"; //thêm mới lô
            frm.dtChiTietLo = dtTang.Copy();
            frm.ShowDialog();
            dtTang = frm.dtChiTietLo.Copy();

            gcLoCCLD.DataSource = dtTang;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
            {
                DialogResult = DialogResult.None;
                return;
            }

            if (string.IsNullOrEmpty(txtSoGiaoDich.Text)) //thêm mới
            {
                if (gvLoCCLD.RowCount < 1)
                {
                    XtraMessageBox.Show("Lỗi: Chưa nhập thông tin lô");
                }
                else
                {
                    if (SaveData())
                        lblMessage.Text = "Ghi dữ liệu thành công!";
                    else lblMessage.Text = "Không ghi được dữ liệu!";
                    _sTrangThai = "Search";
                    LoadForm();
                    VisiableControl(true);
                }
            }
            else //sửa
            {
                if (SaveData())
                    lblMessage.Text = "Ghi dữ liệu thành công!";
                else lblMessage.Text = "Không ghi được dữ liệu!";
                _sTrangThai = "Search";
                LoadForm();
                VisiableControl(true);
            }
        }       

        private void btnXoaLo_Click(object sender, EventArgs e)
        {
            if (gvLoCCLD.RowCount != 0)
            {
                if (XtraMessageBox.Show("Xóa những lô CCLĐ này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // hiển thị câu hỏi xác nhận xóa
                {
                    foreach (int i in gvLoCCLD.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string sMa = gvLoCCLD.GetRowCellValue(i, "MA_LO_CCLD").ToString(); // GetRowCellValue để lấy ra giá trị của cột MA ở dòng thứ i
                        if (!string.IsNullOrEmpty(sMa))
                            lstDelLoCCLD.Add(sMa); //lấy MA_LO_CCLD của các bản ghi cần xóa trong cơ sở dữ liệu

                        dtTang.Rows.Remove(dtTang.Rows[i]);
                    }
                    gcLoCCLD.DataSource = dtTang;
                }
            }
        }

        private void LinkTenLo_Click(object sender, EventArgs e)
        {
            if (_sTrangThai == "Search") //xem thông tin lô
            {
                frmThongTinLo frm = new frmThongTinLo();
                frm.StartPosition = FormStartPosition.CenterParent;
                frm._sEvent = "0"; //xem thông tin lô
                frm._iRowIndex = gvLoCCLD.GetSelectedRows()[0]; //lấy ra index của row đầu tiên được chọn
                frm.dtChiTietLo = dtTang.Copy();
                frm.ShowDialog();
            }
            else //sửa thông tin lô
            {
                frmThongTinLo frm = new frmThongTinLo();
                frm.StartPosition = FormStartPosition.CenterParent;
                frm._sEvent = "2"; //sửa thông tin lô
                frm._iRowIndex = gvLoCCLD.GetSelectedRows()[0]; //lấy ra index của row đầu tiên được chọn
                frm.dtChiTietLo = dtTang.Copy();
                frm.ShowDialog();
                dtTang = frm.dtChiTietLo.Copy();
                gcLoCCLD.DataSource = dtTang;
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _sTrangThai = "Update";
            VisiableControl(false);
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            if (_sTrangThai == "Insert")
            {
                LoadForm();
                this.Close();
            }
            else
            {
                _sTrangThai = "Search";
                LoadForm();
                VisiableControl(true);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult Result = XtraMessageBox.Show(this, "Xóa giao dịch " + txtSoGiaoDich.Text + "?", "Xóa giao dịch",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Result == DialogResult.Yes)
            {
                cc.DeleteGiaoDich(txtSoGiaoDich.Text);
                this.Close();
            }
        }

        private void btnDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetCCLD(txtSoGiaoDich.Text, "1"))
                lblMessage.Text = "Bạn đã duyệt thành công!";
            else
                lblMessage.Text = "Bạn đã duyệt không thành công!";
            LoadForm();
            VisiableControl(true);
        }

        private void btnTuChoiDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetCCLD(txtSoGiaoDich.Text, "2"))
                lblMessage.Text = "Bạn đã từ chối duyệt thành công!";
            else
                lblMessage.Text = "Bạn đã từ chối duyệt không thành công!";
            LoadForm();
            VisiableControl(true);
        }

        private void btnHuyDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetCCLD(txtSoGiaoDich.Text, "3"))
                lblMessage.Text = "Bạn đã hủy duyệt thành công!";
            else
                lblMessage.Text = "Bạn đã hủy duyệt không thành công!";
            LoadForm();
            VisiableControl(true);
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region các hàm
        private void VisiableControl(bool bHide)
        {
            if (_sTrangThai == "Insert") //thêm mới
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemLo.Visibility = LayoutVisibility.Always;
                lcXoaLo.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
            }
            else if (_sTrangThai == "Search") //tìm kiếm
            {
                lcGhi.Visibility = LayoutVisibility.Never;
                lcHuyBo.Visibility = LayoutVisibility.Never;
                lcThemLo.Visibility = LayoutVisibility.Never;
                lcXoaLo.Visibility = LayoutVisibility.Never;

                lcSua.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcXoa.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcHuyDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Always : LayoutVisibility.Never;
            }
            else //sửa thông tin
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemLo.Visibility = LayoutVisibility.Always;
                lcXoaLo.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
            }
            txtSoChungTu.Properties.ReadOnly = bHide;
            txtDienGiai.Properties.ReadOnly = bHide;
            datNgayChungTu.Properties.ReadOnly = bHide;
            datNgayTang.Properties.ReadOnly = bHide;
        }

        private void LoadForm()
        {
            //load thông tin chung
            ds = cc.GetDataGiaoDich(_sSoGiaoDich);
            if (ds.Tables[0].Rows.Count < 1)
                return;
            switch (ds.Tables[0].Rows[0]["TRANG_THAI"].ToString())
            {
                case "0":
                    txtTrangThai.Text = "Chờ duyệt";
                    break;
                case "1":
                    txtTrangThai.Text = "Đã duyệt";
                    break;
                case "2":
                    txtTrangThai.Text = "Từ chối duyệt";
                    break;
                case "3":
                    txtTrangThai.Text = "Hủy duyệt";
                    break;
            }
            txtSoGiaoDich.Text = _sSoGiaoDich;
            txtSoChungTu.Text = ds.Tables[0].Rows[0]["SO_CHUNG_TU"].ToString();
            datNgayChungTu.Text = ds.Tables[0].Rows[0]["NGAY_CHUNG_TU"].ToString();
            datNgayTang.Text = ds.Tables[0].Rows[0]["NGAY_TANG"].ToString();
            txtDienGiai.Text = ds.Tables[0].Rows[0]["DIEN_GIAI"].ToString();

            //load grid thông tin lô cclđ
            gcLoCCLD.DataSource = ds.Tables[1];
            dtTang = ds.Tables[1].Copy();
        }

        private bool SaveData()
        {
            bool bResult = true;
            try
            {
                string sSoGiaoDich = cc.GetSoGiaoDich(datNgayTang.Text.Substring(6));
                if (lstDelLoCCLD.Count > 0) //xóa lô CCLĐ
                {
                    for (int i = 0; i < lstDelLoCCLD.Count; i++)
                    {
                        bResult = cc.DelLoCCLD(lstDelLoCCLD[i]);
                    }
                }
                if (_sTrangThai == "Insert") //thêm giao dịch
                {
                    bResult = cc.InsertGiaoDich(txtSoChungTu.Text, datNgayChungTu.Text, datNgayTang.Text, txtDienGiai.Text,sSoGiaoDich);
                    _sSoGiaoDich = sSoGiaoDich;
                }
                else //sửa thông tin giao dịch
                {
                    bResult = cc.UpdateGiaoDich(txtSoChungTu.Text, datNgayChungTu.Text, datNgayTang.Text,
                                                txtDienGiai.Text, txtSoGiaoDich.Text);
                }
                //thêm chi tiết lô
                foreach (DataRow dr in dtTang.Rows)
                {
                    if (string.IsNullOrEmpty(dr["MA_LO_CCLD"].ToString())) //thêm mới
                    {
                        if (string.IsNullOrEmpty(txtSoGiaoDich.Text)) //thêm mới giao dịch và lô
                            bResult = cc.InsertLoCCLD(sSoGiaoDich, datNgayTang.Text, dr["TEN_LO_CCLD"].ToString(),
                                                      dr["SO_LUONG"].ToString(), dr["DON_GIA"].ToString(),
                                                      dr["DON_GIA_DGL"].ToString(), dr["TONG_TIEN"].ToString(),
                                                      dr["MA_NGUON_GOC"].ToString(), dr["MA_NHOM_CCLD"].ToString(),
                                                      dr["MA_TINH_TRANG"].ToString(), dr["SO_THANG_BH"].ToString(),
                                                      dr["NGAY_BH"].ToString(), dr["MA_DVSDTS"].ToString());
                        else
                        { //thêm mới lô tại giao dịch đã có
                            bResult = cc.InsertLoCCLD(txtSoGiaoDich.Text,datNgayTang.Text, dr["TEN_LO_CCLD"].ToString(),
                                                      dr["SO_LUONG"].ToString(), dr["DON_GIA"].ToString(),
                                                      dr["DON_GIA_DGL"].ToString(), dr["TONG_TIEN"].ToString(),
                                                      dr["MA_NGUON_GOC"].ToString(), dr["MA_NHOM_CCLD"].ToString(),
                                                      dr["MA_TINH_TRANG"].ToString(), dr["SO_THANG_BH"].ToString(),
                                                      dr["NGAY_BH"].ToString(),dr["MA_DVSDTS"].ToString());
                        } 
                    }
                    else //sửa
                    {
                        bResult = cc.UpdateLoCCLD(dr["TEN_LO_CCLD"].ToString(), dr["SO_LUONG"].ToString(),
                                                  dr["DON_GIA"].ToString(), dr["DON_GIA_DGL"].ToString(),
                                                  dr["TONG_TIEN"].ToString(), dr["MA_NGUON_GOC"].ToString(),
                                                  dr["MA_TINH_TRANG"].ToString(), dr["SO_THANG_BH"].ToString(),
                                                  dr["NGAY_BH"].ToString(),dr["MA_LO_CCLD"].ToString());
                    }
                       
                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return bResult;
        }

        private void ResetForm()
        {
            txtTrangThai.ResetText();
            txtSoGiaoDich.ResetText();
            txtSoChungTu.ResetText();
            txtDienGiai.ResetText();
            datNgayChungTu.ResetText();
            datNgayTang.ResetText();
        }

        private void CreateTable()
        {
            dtTang.Columns.Add("MA_NGUON_GOC");
            dtTang.Columns.Add("TEN_NGUON_GOC");
            dtTang.Columns.Add("MA_DVSDTS");
            dtTang.Columns.Add("TEN_DVSDTS");
            dtTang.Columns.Add("MA_NHOM_CCLD");
            dtTang.Columns.Add("TEN_NHOM_CCLD");
            dtTang.Columns.Add("DON_VI_TINH");
            dtTang.Columns.Add("MA_LO_CCLD");
            dtTang.Columns.Add("TEN_LO_CCLD");
            dtTang.Columns.Add("DON_GIA");
            dtTang.Columns.Add("DON_GIA_DGL");
            dtTang.Columns.Add("DON_GIA_CC");
            dtTang.Columns.Add("SO_LUONG");
            dtTang.Columns.Add("TONG_TIEN");
            dtTang.Columns.Add("MA_TINH_TRANG");
            dtTang.Columns.Add("TEN_TINH_TRANG");
            dtTang.Columns.Add("SO_THANG_BH");
            dtTang.Columns.Add("NGAY_BH");
        }

        private bool DuyetCCLD(string sSoGiaoDich,string sTrangThai)
        {
            bool bResult = true;
            try
            {
                bResult = cc.UpdateTrangThai(sSoGiaoDich, sTrangThai);
                if (sTrangThai == "1") //đã duyệt
                {
                    for (int j = 0; j < gvLoCCLD.DataRowCount; j++)
                    {
                        DataRow dr = gvLoCCLD.GetDataRow(j);
                        string sMaCCLD = "";
                        for (int i = 0; i < double.Parse(dr["SO_LUONG"].ToString()); i++)
                        {
                            sMaCCLD = dr["MA_LO_CCLD"].ToString() + (i + 1).ToString().PadLeft(3, '0');
                            bResult = cc.DuyetCCLD(sSoGiaoDich, sTrangThai, dr["MA_LO_CCLD"].ToString(), sMaCCLD);
                        }
                    }
                }
                else if (sTrangThai == "3") //hủy duyệt
                {
                    for (int j = 0; j < gvLoCCLD.DataRowCount; j++)
                    {
                        DataRow dr = gvLoCCLD.GetDataRow(j);
                        bResult = cc.HuyDuyetCCLD(dr["MA_LO_CCLD"].ToString());
                    }
                }
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bResult;
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            datNgayTang.ValidateEmptyStringRule(); // check bắt buộc nhập
        }

        #endregion

    }
}