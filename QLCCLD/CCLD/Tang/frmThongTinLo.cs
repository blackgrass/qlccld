﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraLayout.Utils;
using Business;
using EM.UI;
using QLCCLD.DanhMuc.NguonGoc;
using QLCCLD.DanhMuc.DVSD;
using QLCCLD.DanhMuc.NhomCCLD;
using QLCCLD.DanhMuc.TinhTrang;

namespace QLCCLD.CCLD.Tang
{
    public partial class frmThongTinLo : DialogBase
    {
        #region "khai báo các biến và tham số"

        public string _sEvent = "0"; // 0:Xem; 1: Thêm mới; 2: Sửa
        public int _iRowIndex = 0;
        clsCCLD cc = new clsCCLD();
        public DataTable dtChiTietLo = new DataTable();

        public frmThongTinLo()
        {
            InitializeComponent();
        }
        #endregion

        #region "các sự kiện"
        private void frmThongTinLo_Load(object sender, EventArgs e)
        {
            if (_sEvent == "0") //xem chi tiết thông tin lô
            {
                LoadForm();
                HideControl(true);
            }
            else if (_sEvent == "1") //thêm mới lô cclđ
            {
                ResetForm();
                HideControl(false);
            }
            else //sửa thông tin lô
            {
                LoadForm();
                HideControl(false);
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
            {
                DialogResult = DialogResult.None;
                return;
            }
            SaveData();
            this.Close();
        }

        private void btnLuuVaThem_Click(object sender, EventArgs e)
        {
            if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
            {
                DialogResult = DialogResult.None;
                return;
            }
            SaveData();
            ResetForm();
        }

        private void btnMaNguonGoc_Click(object sender, EventArgs e)
        {
            frmNguonGocTK frm = new frmNguonGocTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaNguonGoc.Text = frm._sMa;
            txtTenNguonGoc.Text = frm._sTen;
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnMaDVSD_Click(object sender, EventArgs e)
        {
            frmDVSDTK frm = new frmDVSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDVSD.Text = frm._sMa;
            txtTenDVSD.Text = frm._sTen;
        }

        private void btnMaNhomCCLD_Click(object sender, EventArgs e)
        {
            frmNhomCCLDTK frm = new frmNhomCCLDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaNhomCCLD.Text = frm._sMa;
            txtTenNhomCCLD.Text = frm._sTen;
            txtDonViTinh.Text = frm._sDonViTinh;
        }

        private void btnMaTinhTrang_Click(object sender, EventArgs e)
        {
            frmTinhTrangTK frm = new frmTinhTrangTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaTinhTrang.Text = frm._sMa;
            txtTenTinhTrang.Text = frm._sTen;
        }

        private void spnSoLuong_EditValueChanged(object sender, EventArgs e)
        {
            txtTongTien.Text = spnDonGiaDGL.Value == 0 ? (spnSoLuong.Value * spnDonGia.Value).ToString() : (spnSoLuong.Value * spnDonGiaDGL.Value).ToString();
        }

        private void spnDonGia_Leave(object sender, EventArgs e)
        {
            spnDonGia.ValidateCustom(CheckNhapDonGia);
            spnDonGiaDGL.ValidateCustom(CheckNhapDonGiaDGL);
        }

        private void spnSoLuong_Leave(object sender, EventArgs e)
        {
            spnSoLuong.ValidateCustom(CheckNhapSoLuong);
        }

        #endregion

        #region "các hàm"

        private void ResetForm()
        {
            btnMaNguonGoc.ResetText();
            txtTenNguonGoc.ResetText();
            btnMaDVSD.ResetText();
            txtTenDVSD.ResetText();
            btnMaNhomCCLD.ResetText();
            txtTenNhomCCLD.ResetText();
            txtMaLoCCLD.ResetText();
            txtTenLoCCLD.ResetText();
            txtDonViTinh.ResetText();
            spnDonGia.ResetText();
            spnDonGiaDGL.ResetText();
            spnSoLuong.ResetText();
            txtTongTien.ResetText();
            btnMaTinhTrang.ResetText();
            txtTenTinhTrang.ResetText();
            spnSoThangBH.ResetText();
            datNgayBH.ResetText();
        }

        private void HideControl(bool bHide)
        {
            switch (_sEvent)
            {
                case "0": //xem thông tin của lô cclđ
                    lcLuu.Visibility = LayoutVisibility.Never;
                    lcLuuVaThem.Visibility = LayoutVisibility.Never;
                    lcThoat.Visibility = LayoutVisibility.Always;
                    btnMaDVSD.Properties.Enabled = !bHide;
                    btnMaNhomCCLD.Properties.Enabled = !bHide;
                    break;
                case "1": //thêm mới
                    lcLuu.Visibility = LayoutVisibility.Always;
                    lcLuuVaThem.Visibility = LayoutVisibility.Always;
                    lcThoat.Visibility = LayoutVisibility.Always;
                    btnMaDVSD.Properties.Enabled = !bHide;
                    btnMaNhomCCLD.Properties.Enabled = !bHide;
                    ResetForm();
                    break;
                case "2": //sửa
                    lcLuu.Visibility = LayoutVisibility.Always;
                    lcLuuVaThem.Visibility = LayoutVisibility.Always;
                    lcThoat.Visibility = LayoutVisibility.Always;

                    if (string.IsNullOrEmpty(txtMaLoCCLD.Text))
                    {
                        btnMaDVSD.Properties.Enabled = !bHide;
                        btnMaNhomCCLD.Properties.Enabled = !bHide;
                    }
                    else
                    {
                        btnMaDVSD.Properties.Enabled = false;
                        btnMaNhomCCLD.Properties.Enabled = false;
                    }
                    break;
            }

            btnMaNguonGoc.Properties.Enabled = !bHide;
            txtTenLoCCLD.Properties.ReadOnly = bHide;
            spnDonGia.Properties.ReadOnly = bHide;
            spnDonGiaDGL.Properties.ReadOnly = bHide;
            spnSoLuong.Properties.ReadOnly = bHide;
            btnMaTinhTrang.Properties.Enabled = !bHide;
            spnSoThangBH.Properties.ReadOnly = bHide;
            datNgayBH.Properties.ReadOnly = bHide;

        }

        private void LoadForm()
        {
            btnMaNguonGoc.Text = dtChiTietLo.Rows[_iRowIndex]["MA_NGUON_GOC"].ToString();
            txtTenNguonGoc.Text = dtChiTietLo.Rows[_iRowIndex]["TEN_NGUON_GOC"].ToString();
            btnMaDVSD.Text = dtChiTietLo.Rows[_iRowIndex]["MA_DVSDTS"].ToString();
            txtTenDVSD.Text = dtChiTietLo.Rows[_iRowIndex]["TEN_DVSDTS"].ToString();
            btnMaNhomCCLD.Text = dtChiTietLo.Rows[_iRowIndex]["MA_NHOM_CCLD"].ToString();
            txtTenNhomCCLD.Text = dtChiTietLo.Rows[_iRowIndex]["TEN_NHOM_CCLD"].ToString();
            txtMaLoCCLD.Text = dtChiTietLo.Rows[_iRowIndex]["MA_LO_CCLD"].ToString();
            txtTenLoCCLD.Text = dtChiTietLo.Rows[_iRowIndex]["TEN_LO_CCLD"].ToString();
            txtDonViTinh.Text = dtChiTietLo.Rows[_iRowIndex]["DON_VI_TINH"].ToString();
            spnDonGia.Text = dtChiTietLo.Rows[_iRowIndex]["DON_GIA"].ToString();
            spnDonGiaDGL.Text = dtChiTietLo.Rows[_iRowIndex]["DON_GIA_DGL"].ToString();
            spnSoLuong.Text = dtChiTietLo.Rows[_iRowIndex]["SO_LUONG"].ToString();
            txtTongTien.Text = dtChiTietLo.Rows[_iRowIndex]["TONG_TIEN"].ToString();
            btnMaTinhTrang.Text = dtChiTietLo.Rows[_iRowIndex]["MA_TINH_TRANG"].ToString();
            txtTenTinhTrang.Text = dtChiTietLo.Rows[_iRowIndex]["TEN_TINH_TRANG"].ToString();
            spnSoThangBH.Text = dtChiTietLo.Rows[_iRowIndex]["SO_THANG_BH"].ToString();
            datNgayBH.Text = dtChiTietLo.Rows[_iRowIndex]["NGAY_BH"].ToString();
        }

        private void SaveData()
        {
            if (_sEvent == "1") //thêm mới
            {
                DataRow dr = dtChiTietLo.NewRow();
                dr["MA_NGUON_GOC"] = btnMaNguonGoc.Text;
                dr["TEN_NGUON_GOC"] = txtTenNguonGoc.Text;
                dr["MA_DVSDTS"] = btnMaDVSD.Text;
                dr["TEN_DVSDTS"] = txtTenDVSD.Text;
                dr["MA_NHOM_CCLD"] = btnMaNhomCCLD.Text;
                dr["TEN_NHOM_CCLD"] = txtTenNhomCCLD.Text;
                dr["DON_VI_TINH"] = txtDonViTinh.Text;
                dr["TEN_LO_CCLD"] = txtTenLoCCLD.Text;
                dr["DON_GIA"] = spnDonGia.Text;
                dr["DON_GIA_DGL"] = spnDonGiaDGL.Text;
                dr["DON_GIA_CC"] = spnDonGiaDGL.Value == 0 ? spnDonGia.Text : spnDonGiaDGL.Text;
                dr["SO_LUONG"] = spnSoLuong.Text;
                dr["TONG_TIEN"] = txtTongTien.Text;
                dr["MA_TINH_TRANG"] = btnMaTinhTrang.Text;
                dr["TEN_TINH_TRANG"] = txtTenTinhTrang.Text;
                dr["SO_THANG_BH"] = spnSoThangBH.Text;
                dr["NGAY_BH"] = datNgayBH.Text;

                dtChiTietLo.Rows.Add(dr);
            }
            else //sửa
            {
                DataRow[] dr = dtChiTietLo.Select("MA_LO_CCLD='"+txtMaLoCCLD.Text+"'");
                dr[0]["MA_NGUON_GOC"] = btnMaNguonGoc.Text;
                dr[0]["TEN_NGUON_GOC"] = txtTenNguonGoc.Text;
                dr[0]["TEN_LO_CCLD"] = txtTenLoCCLD.Text;
                dr[0]["DON_GIA"] = spnDonGia.Text;
                dr[0]["DON_GIA_DGL"] = spnDonGiaDGL.Text;
                dr[0]["DON_GIA_CC"] = spnDonGiaDGL.Value == 0 ? spnDonGia.Text : spnDonGiaDGL.Text;
                dr[0]["SO_LUONG"] = spnSoLuong.Text;
                dr[0]["TONG_TIEN"] = txtTongTien.Text;
                dr[0]["MA_TINH_TRANG"] = btnMaTinhTrang.Text;
                dr[0]["TEN_TINH_TRANG"] = txtTenTinhTrang.Text;
                dr[0]["SO_THANG_BH"] = spnSoThangBH.Text;
                dr[0]["NGAY_BH"] = datNgayBH.Text;
            }
            
        }

        protected override void SetValidateControl() // hàm Validate của form, cần validate những cái gì thì cho vào đây
        {
            btnMaNguonGoc.ValidateEmptyStringRule(); // ValidateEmptyStringRule là check bắt buộc nhập
            btnMaNhomCCLD.ValidateEmptyStringRule();
            txtTenLoCCLD.ValidateEmptyStringRule();
            spnSoLuong.ValidateEmptyStringRule();
            btnMaTinhTrang.ValidateEmptyStringRule();
            spnDonGia.ValidateCustom(CheckNhapDonGia);
            spnDonGiaDGL.ValidateCustom(CheckNhapDonGiaDGL);
            spnSoLuong.ValidateCustom(CheckNhapSoLuong);
        }

        private string CheckNhapDonGia(Control c) // hàm validate không được nhập đồng thời cả đơn giá và đơn giá đánh giá lại
        {
            SpinEdit DonGia = (SpinEdit)c;
            if (DonGia.Value != 0 && spnDonGiaDGL.Value != 0)
                return "Không được nhập đồng thời cả đơn giá và đơn giá đánh giá lại";
            if (DonGia.Value <= 0 && spnDonGiaDGL.Value <= 0)
                return "Phải nhập đơn giá hoặc đơn giá đánh giá lại > 0";
            return string.Empty;
        }

        private string CheckNhapDonGiaDGL(Control c) // hàm validate không được nhập đồng thời cả đơn giá và đơn giá đánh giá lại
        {
            SpinEdit DonGiaDGL = (SpinEdit)c;
            if (DonGiaDGL.Value != 0 && spnDonGia.Value != 0)
                return "Không được nhập đồng thời cả đơn giá và đơn giá đánh giá lại";
            if (DonGiaDGL.Value <= 0 && spnDonGia.Value <= 0)
                return "Phải nhập đơn giá hoặc đơn giá đánh giá lại > 0";
            return string.Empty;
        }

        private string CheckNhapSoLuong(Control c)
        {
            SpinEdit SoLuong = (SpinEdit)c;
            if (SoLuong.Value <= 0)
                return "Phải nhập số lượng > 0";
            return string.Empty;
        }

        #endregion

    }
}