﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.CCLD.Tang
{
    public partial class ucTang : DevExpress.XtraEditors.XtraUserControl
    {

        clsCCLD cc = new clsCCLD();

        public ucTang()
        {
            InitializeComponent();
        }

        private void ucTang_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void LoadDanhSach()
        {
            gcTang.DataSource = cc.TimKiemLoCCLD(txtSoGDTu.Text, txtSoGDDen.Text, datNgayChungTuTu.Text, datNgayChungTuDen.Text, datNgayTangTu.Text, datNgayTangDen.Text, txtMaLo.Text, chkChoDuyet.Checked, chkDaDuyet.Checked, chkTuChoiDuyet.Checked, chkHuyDuyet.Checked);
            gvTang.BestFitColumns();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmTang f = new frmTang();
            f.StartPosition = FormStartPosition.CenterParent;
            f._sTrangThai = "Insert";
            f.ShowDialog();
            LoadDanhSach();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void gvTang_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT") 
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void LinkSoGiaoDich_Click(object sender, EventArgs e)
        {
            frmTang frm = new frmTang();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sTrangThai = "Search";
            frm._sSoGiaoDich = gvTang.GetFocusedRowCellValue("SO_GIAO_DICH").ToString();
            frm.ShowDialog();
            LoadDanhSach();
        }

       
    }
}
