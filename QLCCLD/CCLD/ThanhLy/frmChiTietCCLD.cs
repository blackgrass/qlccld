﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using QLCCLD.DanhMuc.TinhTrang;

namespace QLCCLD.CCLD.ThanhLy
{
    public partial class frmChiTietCCLD : DevExpress.XtraEditors.XtraForm
    {
        public string _sMaCCLD = "";
        public string _sTenCCLD = "";
        public string _sDonViTinh = "";
        public string _sMaTinhTrang = "";
        public string _sTenTinhTrang = "";
        public string _sGhiChu = "";
        public string _sEvent = "";

        public frmChiTietCCLD()
        {
            InitializeComponent();
        }

        private void frmChiTietCCLD_Load(object sender, EventArgs e)
        {
            txtMaCCLD.Text = _sMaCCLD;
            txtTenCCLD.Text = _sTenCCLD;
            txtSoLuong.Text = "1";
            txtDonViTinh.Text = _sDonViTinh;
            btnMaTinhTrang.Text = _sMaTinhTrang;
            txtTenTinhTrang.Text = _sTenTinhTrang;
            txtGhiChu.Text = _sGhiChu;
            if (_sEvent == "View")
                HideControl(false);
            else HideControl(true); //edit
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            _sMaTinhTrang = btnMaTinhTrang.Text;
            _sTenTinhTrang = txtTenTinhTrang.Text;
            _sGhiChu = txtGhiChu.Text;
            this.Close();
        }

        private void HideControl(bool bHide)
        {
            btnLuu.Enabled = bHide;
            btnMaTinhTrang.Enabled = bHide;
            txtGhiChu.Properties.ReadOnly = !bHide;
        }

        private void btnMaTinhTrang_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.TinhTrang.frmTinhTrangTK frm = new frmTinhTrangTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaTinhTrang.Text = frm._sMa;
            txtTenTinhTrang.Text = frm._sTen;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}