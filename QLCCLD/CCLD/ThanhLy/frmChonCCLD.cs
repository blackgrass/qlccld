﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using QLCCLD.DanhMuc.NhomCCLD;

namespace QLCCLD.CCLD.ThanhLy
{
    public partial class frmChonCCLD : DevExpress.XtraEditors.XtraForm
    {
        clsCCLD cc = new clsCCLD();
        public string _sMaCCLDDaChon = "";
        public string _sMaKho = "";
        public DataTable dtChonCCLD = new DataTable();

        public frmChonCCLD()
        {
            InitializeComponent();
        }

        private void frmChonCCLD_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadDanhSach()
        {
            gcCCLD.DataSource = cc.LayCCLDThanhLy(btnMaNhomCCLD.Text, btnMaLoCCLD.Text, _sMaCCLDDaChon);
            gvCCLD.BestFitColumns();
        }

        private void gvCCLD_Click(object sender, EventArgs e)
        {
            //check all 
            if (gvCCLD.PressedColumn == gvCCLD.Columns["is_check"])
            {
                int count = 0;
                for (int i = 0; i < gvCCLD.RowCount; i++)
                {
                    if ((bool)gvCCLD.GetRowCellValue(i, gvCCLD.Columns["is_check"]) == true)
                        count++;
                }

                if (count == gvCCLD.RowCount)
                {
                    for (int j = 0; j < gvCCLD.RowCount; j++)
                        gvCCLD.SetRowCellValue(j, "is_check", false);
                }
                else
                {
                    for (int i = 0; i < gvCCLD.RowCount; i++)
                        gvCCLD.SetRowCellValue(i, "is_check", true);
                }
            }
        }

        private void gvCCLD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gvCCLD.RowCount; i++)
            {
                if ((bool)gvCCLD.GetRowCellValue(i, "is_check") == true)
                {
                    DataRow dr = dtChonCCLD.NewRow();
                    dr["MA_CCLD"] = gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString();
                    dr["TEN_CCLD"] = gvCCLD.GetRowCellValue(i, "TEN_CCLD").ToString();
                    dr["DON_VI_TINH"] = gvCCLD.GetRowCellValue(i, "DON_VI_TINH").ToString();
                    dr["SO_LUONG"] = "1";
                    dr["MA_TINH_TRANG"] = gvCCLD.GetRowCellValue(i, "MA_TINH_TRANG").ToString();
                    dr["TEN_TINH_TRANG"] = gvCCLD.GetRowCellValue(i, "TEN_TINH_TRANG").ToString();
                    dtChonCCLD.Rows.Add(dr);
                }
            }
            this.Close();
        }

        private void btnMaNhomCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmNhomCCLDTK frm = new frmNhomCCLDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaNhomCCLD.Text = frm._sMa;
            txtTenNhomCCLD.Text = frm._sTen;
        }

        private void btnMaLoCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.NhomCCLD.frmLoCCLD frm = new frmLoCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaLoCCLD.Text = frm._sMa;
            txtTenLoCCLD.Text = frm._sTen;
        }
    }
}