﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using QLCCLD.CCLD.BaoCao;

namespace QLCCLD.CCLD.ThanhLy
{
    public partial class frmDieuKienChayBaoCao : DevExpress.XtraEditors.XtraForm
    {
        public string _sSoBB = "";
        public frmDieuKienChayBaoCao()
        {
            InitializeComponent();
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            BaoCao.frmReport frm = new frmReport();
            frm._sNghiepVu = "7";
            frm._sThamSo = _sSoBB;
            frm._iHienThi = chkTheoCCLD.Checked ? 0 : 1;
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
        }
    }
}