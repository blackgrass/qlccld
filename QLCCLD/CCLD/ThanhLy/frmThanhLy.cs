﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using DevExpress.XtraLayout.Utils;
using EM.UI;
using QLCCLD.CCLD.BanGiao;
using QLCCLD.CCLD.BaoCao;

namespace QLCCLD.CCLD.ThanhLy
{
    public partial class frmThanhLy : DialogBase
    {
        #region Khai báo biến và tham số
        public string _sTrangThai = "";
        public string _sSoBB = "";
        clsCCLD cc = new clsCCLD();
        DataTable dtChonCCLD = new DataTable();
        DataTable dtThanhVien = new DataTable();
        DataSet ds = new DataSet();
        List<string> lstDelCCLD = new List<string>();
        List<string> lstDelThanhVien = new List<string>();

        public frmThanhLy()
        {
            InitializeComponent();
        }

        #endregion

        #region Các sự kiện
        private void frmThanhLy_Load(object sender, EventArgs e)
        {
            CreateTable();
            if (_sTrangThai == "Search") //Tìm kiếm
            {
                LoadForm();
                VisiableControl(true);
            }
            else
            {
                ResetForm(); //Thêm mới
                VisiableControl(false);
            }
        }

        private void btnThemThanhVien_Click(object sender, EventArgs e)
        {
            frmThanhVien frm = new frmThanhVien();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sEvent = "Insert";
            frm.dtThanhVien = dtThanhVien.Copy();
            frm.ShowDialog();
            dtThanhVien = frm.dtThanhVien.Copy();
            gcThanhVien.DataSource = dtThanhVien;
            gvThanhVien.BestFitColumns();

        }

        private void btnThemCCLD_Click(object sender, EventArgs e)
        {
            if (HasErrors)
                return;
            string sMaCCLDDaChon = "";
            for (int i = 0; i < gvCCLD.RowCount; i++)
            {
                if (i > 0)
                    sMaCCLDDaChon += ",'" + gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
                else sMaCCLDDaChon += "'" + gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
            }

            frmChonCCLD frm = new frmChonCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.dtChonCCLD = dtChonCCLD.Copy();
            frm._sMaCCLDDaChon = sMaCCLDDaChon;
            frm.ShowDialog();
            dtChonCCLD = frm.dtChonCCLD.Copy();

            gcCCLD.DataSource = dtChonCCLD;
            gvCCLD.BestFitColumns();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
            {
                DialogResult = DialogResult.None;
                return;
            }

            if (string.IsNullOrEmpty(txtSoBienBan.Text)) //thêm mới
            {
                if (gvCCLD.RowCount < 1 || gvThanhVien.RowCount < 1)
                {
                    XtraMessageBox.Show("Lỗi: Chưa chọn CCLĐ hoặc chưa nhập thông tin thành viên");
                }
                else
                {
                    if (SaveData())
                        lblMessage.Text = "Ghi dữ liệu thành công!";
                    else lblMessage.Text = "Không ghi được dữ liệu!";
                    _sTrangThai = "Search";
                    LoadForm();
                    VisiableControl(true);
                }
            }
            else //sửa
            {
                if (SaveData())
                    lblMessage.Text = "Ghi dữ liệu thành công!";
                else lblMessage.Text = "Không ghi được dữ liệu!";
                _sTrangThai = "Search";
                LoadForm();
                VisiableControl(true);
            }
        }

        private void gvCCLD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnXoaCCLD_Click(object sender, EventArgs e)
        {
            if (gvCCLD.RowCount != 0)
            {
                List<string> lstDelCCLDOnDtChonCCLD = new List<string>();
                if (XtraMessageBox.Show("Xóa những CCLĐ này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // hiển thị câu hỏi xác nhận xóa
                {
                    foreach (int i in gvCCLD.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string sMa = gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString(); // GetRowCellValue để lấy ra giá trị của cột MA ở dòng thứ i
                        if (!string.IsNullOrEmpty(sMa))
                            lstDelCCLD.Add(sMa); //lấy MA_CCLD của các bản ghi cần xóa trong cơ sở dữ liệu
                        lstDelCCLDOnDtChonCCLD.Add(sMa);//lấy MA_CCLD của các bản ghi cần xóa trong datatable dtChonCCLD
                    }
                    for (int i = 0; i < lstDelCCLDOnDtChonCCLD.Count; i++) //xóa CCLĐ tại dtChonCCLD
                    {
                        DataRow[] dr = dtChonCCLD.Select("MA_CCLD='" + lstDelCCLDOnDtChonCCLD[i] + "'");
                        dtChonCCLD.Rows.Remove(dr[0]);
                    }
                    gcCCLD.DataSource = dtChonCCLD;
                    gvCCLD.BestFitColumns();
                    gvCCLD.SelectRow(0);
                }
            }
        }

        private void btnXoaThanhVien_Click(object sender, EventArgs e)
        {
            if (gvThanhVien.RowCount != 0)
            {
               

                if (XtraMessageBox.Show("Xóa những thành viên này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // hiển thị câu hỏi xác nhận xóa
                {
                    foreach (int i in gvThanhVien.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string sIDThanhVienHD = gvThanhVien.GetRowCellValue(i, "ID_THANH_LY_HD").ToString();
                        if (!string.IsNullOrEmpty(sIDThanhVienHD))
                            lstDelThanhVien.Add(sIDThanhVienHD); //lấy ID_THANH_LY_HD của các bản ghi cần xóa trong cơ sở dữ liệu
                        
                    }
                    for (int i = 0; i < lstDelThanhVien.Count; i++)
                    {
                        DataRow[] dr = dtThanhVien.Select("ID_THANH_LY_HD='" + lstDelThanhVien[i] + "'");
                        dtThanhVien.Rows.Remove(dr[0]);
                    }                                  

                    gcThanhVien.DataSource = dtThanhVien;
                    gvThanhVien.SelectRow(0);
                }
                lstDelThanhVien.Clear();
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _sTrangThai = "Update";
            VisiableControl(false);
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            if (_sTrangThai == "Insert")
            {
                LoadForm();
                this.Close();
            }
            else
            {
                _sTrangThai = "Search";
                LoadForm();
                VisiableControl(true);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult Result = XtraMessageBox.Show(this, "Xóa biên bản thanh lý " + txtSoBienBan.Text + "?", "Xóa biên bản thanh lý",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Result == DialogResult.Yes)
            {
                for (int i = 0; i < gvCCLD.RowCount; i++)
                {
                    cc.DelThanhLyCC(txtSoBienBan.Text, gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString());
                }

                cc.DeleteThanhLy(txtSoBienBan.Text);
                this.Close();
            }
        }

        private void btnDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetThanhLy(txtSoBienBan.Text, "1")) //trạng thái = 1: đã duyệt;
                lblMessage.Text = "Bạn đã duyệt thành công!";
            else
                lblMessage.Text = "Bạn đã duyệt không thành công!";
            LoadForm();
            VisiableControl(true);
        }

        private void btnTuChoiDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetThanhLy(txtSoBienBan.Text, "2")) //trang thái = 2: từ chối duyệt
                lblMessage.Text = "Bạn đã từ chối duyệt thành công!";
            else
                lblMessage.Text = "Bạn đã từ chối duyệt không thành công!";
            LoadForm();
            VisiableControl(true);
        }

        private void btnHuyDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetThanhLy(txtSoBienBan.Text, "3")) //trạng thái = 3: hủy duyệt
                lblMessage.Text = "Bạn đã hủy duyệt thành công!";
            else
                lblMessage.Text = "Bạn đã hủy duyệt không thành công!";
            LoadForm();
            VisiableControl(true);
        }

        private void LinkHoTen_Click(object sender, EventArgs e)
        {
            frmThanhVien frm = new frmThanhVien();
            frm.StartPosition = FormStartPosition.CenterParent;

            frm._sHoTen = gvThanhVien.GetFocusedRowCellValue("HO_TEN").ToString();
            frm._sChucVu = gvThanhVien.GetFocusedRowCellValue("CHUC_VU").ToString();
            frm._sViTri = gvThanhVien.GetFocusedRowCellValue("VI_TRI").ToString();


            if (_sTrangThai == "Search") //xem chi tiết CCLD
            {
                frm._sEvent = "View"; //xem 
            }
            else //sửa 
            {
                frm._sEvent = "Edit"; //sửa 
            }
            frm.ShowDialog();
            int iIndex = gvThanhVien.GetSelectedRows()[0];
            dtThanhVien.Rows[iIndex]["HO_TEN"] = frm._sHoTen;
            dtThanhVien.Rows[iIndex]["CHUC_VU"] = frm._sChucVu;
            dtThanhVien.Rows[iIndex]["VI_TRI"] = frm._sViTri;

            gcThanhVien.DataSource = dtThanhVien;
            gvThanhVien.BestFitColumns();
        }

        private void LinkCCLD_Click(object sender, EventArgs e)
        {
            frmChiTietCCLD frm = new frmChiTietCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;

            frm._sMaCCLD = gvCCLD.GetFocusedRowCellValue("MA_CCLD").ToString();
            frm._sTenCCLD = gvCCLD.GetFocusedRowCellValue("TEN_CCLD").ToString();
            frm._sDonViTinh = gvCCLD.GetFocusedRowCellValue("DON_VI_TINH").ToString();
            frm._sMaTinhTrang = gvCCLD.GetFocusedRowCellValue("MA_TINH_TRANG").ToString();
            frm._sTenTinhTrang = gvCCLD.GetFocusedRowCellValue("TEN_TINH_TRANG").ToString();
            frm._sGhiChu = gvCCLD.GetFocusedRowCellValue("GHI_CHU").ToString();

            if (_sTrangThai == "Search") //xem chi tiết CCLD
            {
                frm._sEvent = "View"; //xem 
            }
            else //sửa 
            {
                frm._sEvent = "Edit"; //sửa 
            }
            frm.ShowDialog();

            int iIndex = gvCCLD.GetSelectedRows()[0];
            dtChonCCLD.Rows[iIndex]["MA_TINH_TRANG"] = frm._sMaTinhTrang;
            dtChonCCLD.Rows[iIndex]["TEN_TINH_TRANG"] = frm._sTenTinhTrang;
            dtChonCCLD.Rows[iIndex]["GHI_CHU"] = frm._sGhiChu;

            gcCCLD.DataSource = dtChonCCLD;
            gvCCLD.BestFitColumns();
        }

        private void gvThanhVien_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STTThanhVien")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            frmDieuKienChayBaoCao frm = new frmDieuKienChayBaoCao();
            frm._sSoBB = txtSoBienBan.Text;
            frm.ShowDialog();
        }

        #endregion

        #region Các hàm

        private void VisiableControl(bool bHide)
        {
            if (_sTrangThai == "Insert") //thêm mới
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemLo.Visibility = LayoutVisibility.Always;
                lcXoaLo.Visibility = LayoutVisibility.Always;
                lcThemThanhVien.Visibility = LayoutVisibility.Always;
                s.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Never;
            }
            else if (_sTrangThai == "Search") //tìm kiếm
            {
                lcGhi.Visibility = LayoutVisibility.Never;
                lcHuyBo.Visibility = LayoutVisibility.Never;
                lcThemLo.Visibility = LayoutVisibility.Never;
                lcXoaLo.Visibility = LayoutVisibility.Never;
                lcThemThanhVien.Visibility = LayoutVisibility.Never;
                s.Visibility = LayoutVisibility.Never;

                lcSua.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcXoa.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcHuyDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Always;
            }
            else //sửa thông tin
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemLo.Visibility = LayoutVisibility.Always;
                lcXoaLo.Visibility = LayoutVisibility.Always;
                lcThemThanhVien.Visibility = LayoutVisibility.Always;
                s.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Never;
            }
            txtSoChungTu.Properties.ReadOnly = bHide;
            datNgayThanhLy.Properties.ReadOnly = bHide;
            txtSoQuyetDinh.Properties.ReadOnly = bHide;
            datNgayQuyetDinh.Properties.ReadOnly = bHide;
            txtNguoiQuyetDinh.Properties.Enabled = !bHide;
            datNgayChungTu.Properties.ReadOnly = bHide;
            spChiPhiThanhLy.Properties.ReadOnly = bHide;
            spGiaTriThuHoi.Properties.ReadOnly = bHide;
            txtThuTruongDonVi.Properties.ReadOnly = bHide;
            txtChuTich.Properties.ReadOnly = bHide;
            txtTruongPhongKeToan.Properties.ReadOnly = bHide;
            txtKetLuan.Properties.ReadOnly = bHide;
        }

        private void LoadForm()
        {
            //load thông tin chung
            ds = cc.GetBBThanhLy(_sSoBB);
            if (ds.Tables[0].Rows.Count < 1)
                return;
            switch (ds.Tables[0].Rows[0]["TRANG_THAI"].ToString())
            {
                case "0":
                    txtTrangThai.Text = "Chờ duyệt";
                    break;
                case "1":
                    txtTrangThai.Text = "Đã duyệt";
                    break;
                case "2":
                    txtTrangThai.Text = "Từ chối duyệt";
                    break;
                case "3":
                    txtTrangThai.Text = "Hủy duyệt";
                    break;
            }
            txtSoBienBan.Text = _sSoBB;
            txtSoChungTu.Text = ds.Tables[0].Rows[0]["SO_CHUNG_TU"].ToString();
            datNgayThanhLy.Text = ds.Tables[0].Rows[0]["NGAY_THANH_LY"].ToString();
            txtSoQuyetDinh.Text = ds.Tables[0].Rows[0]["SO_QUYET_DINH"].ToString();
            datNgayQuyetDinh.Text = ds.Tables[0].Rows[0]["NGAY_QUYET_DINH"].ToString();
            txtNguoiQuyetDinh.Text = ds.Tables[0].Rows[0]["NGUOI_QUYET_DINH"].ToString();
            datNgayChungTu.Text = ds.Tables[0].Rows[0]["NGAY_CHUNG_TU"].ToString();
            spChiPhiThanhLy.Text = ds.Tables[0].Rows[0]["SO_TIEN_CHI"].ToString();
            spGiaTriThuHoi.Text = ds.Tables[0].Rows[0]["SO_TIEN_THU"].ToString();
            txtThuTruongDonVi.Text = ds.Tables[0].Rows[0]["THU_TRUONG_DON_VI"].ToString();
            txtTruongPhongKeToan.Text = ds.Tables[0].Rows[0]["TRUONG_PHONG_KE_TOAN"].ToString();
            txtChuTich.Text = ds.Tables[0].Rows[0]["CHU_TICH_HOI_DONG"].ToString();

            //load grid thông tin lô cclđ
            gcCCLD.DataSource = ds.Tables[1];
            gvCCLD.BestFitColumns(); //điều chỉnh độ rộng của cột tự động

            gcThanhVien.DataSource = ds.Tables[2];
            gvThanhVien.BestFitColumns();

            dtChonCCLD = ds.Tables[1].Copy();
            dtThanhVien = ds.Tables[2].Copy();
        }

        private bool SaveData()
        {
            bool bResult = true;
            try
            {
                
                //if (lstDelCCLD.Count > 0) //xóa CCLĐ
                //{
                //    for (int i = 0; i < lstDelCCLD.Count; i++)
                //    {
                //        bResult = cc.DelCCLDThanhLy(lstDelCCLD[i]);
                //    }
                //}

                if (_sTrangThai == "Insert") //thêm mới biên bản thanh lý
                {
                    string sSoBB = cc.GetSoBBThanhLy();
                    bResult = cc.InsertThanhLy(datNgayThanhLy.Text, sSoBB, txtSoQuyetDinh.Text, datNgayQuyetDinh.Text,
                                               txtNguoiQuyetDinh.Text,
                                               txtSoChungTu.Text, datNgayChungTu.Text, spChiPhiThanhLy.Text,
                                               spGiaTriThuHoi.Text, txtThuTruongDonVi.Text,
                                               txtTruongPhongKeToan.Text, txtChuTich.Text,txtKetLuan.Text);
                    _sSoBB = sSoBB;
                }
                else //sửa thông tin thanh lý
                {
                    bResult = cc.UpdateThanhLy(datNgayThanhLy.Text, txtSoBienBan.Text, txtSoQuyetDinh.Text, datNgayQuyetDinh.Text,
                                               txtNguoiQuyetDinh.Text,
                                               txtSoChungTu.Text, datNgayChungTu.Text, spChiPhiThanhLy.Text,
                                               spGiaTriThuHoi.Text, txtThuTruongDonVi.Text,
                                               txtTruongPhongKeToan.Text, txtChuTich.Text, txtKetLuan.Text);
                    _sSoBB = txtSoBienBan.Text;
                }
                //thêm CCLĐ
                foreach (DataRow dr in dtChonCCLD.Rows)
                {
                    bResult = cc.InsertThanhLyCC(_sSoBB, dr["Ma_CCLD"].ToString(), dr["MA_TINH_TRANG"].ToString(), dr["GHI_CHU"].ToString());
                    //if (cc.CheckMaCCLDThanhLy(dr["MA_CCLD"].ToString(),txtSoBienBan.Text).Rows.Count == 0) //thêm mới
                    //{
                    //    if (string.IsNullOrEmpty(txtSoBienBan.Text)) //thêm mới thanh lý và chi tiết thanh lý
                    //        bResult = cc.InsertThanhLyCC(_sSoBB,dr["Ma_CCLD"].ToString(),dr["MA_TINH_TRANG"].ToString(),dr["GHI_CHU"].ToString());
                    //    else
                    //    { //thêm CCLĐ tại số biên bản thanh lý đã có
                    //        bResult = cc.InsertThanhLyCC(txtSoBienBan.Text, dr["Ma_CCLD"].ToString(), dr["MA_TINH_TRANG"].ToString(), dr["GHI_CHU"].ToString());
                    //    }
                    //}
                    //else //sửa
                    //{
                    //    bResult = cc.UpdateThanhLyCC(txtSoBienBan.Text, dr["Ma_CCLD"].ToString(), dr["MA_TINH_TRANG"].ToString(), dr["GHI_CHU"].ToString());
                    //}

                }

                //thêm thành viên
                foreach (DataRow dr in dtThanhVien.Rows)
                {
                    bResult = cc.InsertThanhLyHD(_sSoBB, dr["HO_TEN"].ToString(), dr["CHUC_VU"].ToString(), dr["VI_TRI"].ToString());
                    //if (string.IsNullOrEmpty(dr["ID_THANH_LY_HD"].ToString())) //thêm mới
                    //{
                    //    if (string.IsNullOrEmpty(txtSoBienBan.Text)) //thêm mới thanh lý và đại diện
                    //        bResult = cc.InsertThanhLyHD(sSoBB, dr["HO_TEN"].ToString(), dr["CHUC_VU"].ToString(), dr["VI_TRI"].ToString());
                    //    else
                    //    { //thêm đại diện tại số biên bản thanh lý đã có
                    //        bResult = cc.InsertThanhLyHD(txtSoBienBan.Text, dr["HO_TEN"].ToString(), dr["CHUC_VU"].ToString(), dr["VI_TRI"].ToString());
                    //    }
                    //}
                    //else //sửa
                    //{
                    //    bResult = cc.UpdateThanhLyHD(dr["ID_THANH_LY_HD"].ToString(), dr["HO_TEN"].ToString(), dr["CHUC_VU"].ToString(), dr["VI_TRI"].ToString());
                    //}

                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return bResult;
        }

        private void ResetForm()
        {
            txtTrangThai.ResetText();
            txtSoChungTu.ResetText();
            txtSoBienBan.ResetText();
            txtSoChungTu.ResetText();
            datNgayThanhLy.ResetText();
            txtSoQuyetDinh.ResetText();
            datNgayQuyetDinh.ResetText();
            txtNguoiQuyetDinh.ResetText();
            datNgayChungTu.ResetText();
            spChiPhiThanhLy.ResetText();
            spGiaTriThuHoi.ResetText();
            txtThuTruongDonVi.ResetText();
            txtThuTruongDonVi.ResetText();
            txtChuTich.ResetText();
        }

        private void CreateTable()
        {
            dtChonCCLD.Columns.Add("MA_CCLD");
            dtChonCCLD.Columns.Add("TEN_CCLD");
            dtChonCCLD.Columns.Add("DON_VI_TINH");
            dtChonCCLD.Columns.Add("SO_LUONG");
            dtChonCCLD.Columns.Add("MA_TINH_TRANG");
            dtChonCCLD.Columns.Add("TEN_TINH_TRANG");
            dtChonCCLD.Columns.Add("GHI_CHU");

            //tạo bảng thành viên
            dtThanhVien.Columns.Add("ID_THANH_LY_HD");
            dtThanhVien.Columns.Add("HO_TEN");
            dtThanhVien.Columns.Add("CHUC_VU");
            dtThanhVien.Columns.Add("VI_TRI");
        }

        private bool DuyetThanhLy(string sSoPhieu, string sTrangThai)
        {
            bool bResult = true;
            try
            {
                bResult = cc.DuyetThanhLy(sSoPhieu, sTrangThai);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bResult;
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            datNgayThanhLy.ValidateEmptyStringRule(); // check bắt buộc nhập
        }

        #endregion

        

    }
}