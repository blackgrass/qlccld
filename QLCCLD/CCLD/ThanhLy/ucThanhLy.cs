﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using QLCCLD.DanhMuc.NhomCCLD;

namespace QLCCLD.CCLD.ThanhLy
{
    public partial class ucThanhLy : DevExpress.XtraEditors.XtraUserControl
    {
        #region "khai báo biến và tham số"
        clsCCLD cc = new clsCCLD();

        public ucThanhLy()
        {
            InitializeComponent();
        }
        #endregion

        #region các sự kiện
        private void ucThanhLy_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void gvThanhLy_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void LinkSoBB_Click(object sender, EventArgs e)
        {
            frmThanhLy frm = new frmThanhLy();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sTrangThai = "Search";
            frm._sSoBB = gvThanhLy.GetFocusedRowCellValue("SO_BIEN_BAN").ToString();
            frm.ShowDialog();
            LoadDanhSach();
        }

        private void btnMaCCLD_ButtonClick(object sender, EventArgs e)
        {
            DanhMuc.NhomCCLD.frmSearchCCLD frm = new frmSearchCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaCCLD.Text = frm._sMaCCLD;
            txtTenCCLD.Text = frm._sTenCCLD;
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmThanhLy f = new frmThanhLy();
            f.StartPosition = FormStartPosition.CenterParent;
            f._sTrangThai = "Insert";
            f.ShowDialog();
            LoadDanhSach();
        }
        #endregion

        #region các hàm
        private void LoadDanhSach()
        {
            gcThanhLy.DataSource = cc.TimKiemThanhLy(txtSoBienBanTu.Text, txtSoBienBanDen.Text, datThoiGianTu.Text, datThoiGianDen.Text,
                                                     btnMaCCLD.Text, chkChoDuyet.Checked, chkDaDuyet.Checked, chkTuChoiDuyet.Checked,
                                                        chkHuyDuyet.Checked);
            gvThanhLy.BestFitColumns(); //tự động điều chỉnh độ rộng của cột
        }

        #endregion
    }
}
