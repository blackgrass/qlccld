﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.CCLD.TimKiem
{
    public partial class frmLichSu : DevExpress.XtraEditors.XtraForm
    {
        clsCCLD cc = new clsCCLD();

        public string MaCCLD = "";
        public string TenCCLD = "";
        public string DonGia = "";
        public string DVSD = "";
        public string DTSD = "";
        public string DuAn = "";
        
        public frmLichSu()
        {
            InitializeComponent();
        }

        private void frmLichSu_Load(object sender, EventArgs e)
        {
            txtMaCCLD.Text = MaCCLD;
            txtTenCCLD.Text = TenCCLD;
            txtDonGia.Text = DonGia;
            txtDVSD.Text = DVSD;
            txtDTSD.Text = DTSD;
            txtDuAn.Text = DuAn;

            LoadDanhSachBienDong();
        }

        private void LoadDanhSachBienDong()
        {
            gcLichSu.DataSource = cc.TimKiemLichSuCCLD(MaCCLD);
            gvLichSu.BestFitColumns();
        }

        private void gvLichSu_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }
    }
}