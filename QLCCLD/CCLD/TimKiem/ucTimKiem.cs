﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using QLCCLD.DanhMuc.DVSD;
using QLCCLD.DanhMuc.NhomCCLD;
using QLCCLD.DanhMuc.TinhTrang;
using QLCCLD.DanhMuc.Kho;
using QLCCLD.DanhMuc.DTSD;
using QLCCLD.DanhMuc.DuAn;

namespace QLCCLD.CCLD.TimKiem
{
    public partial class ucTimKiem : DevExpress.XtraEditors.XtraUserControl
    {
        clsCCLD cc = new clsCCLD();

        public ucTimKiem()
        {
            InitializeComponent();
        }

        private void ucTimKiem_Load(object sender, EventArgs e)
        {
            //LoadDanhSach();
        }

        private void LoadDanhSach()
        {
            gcCCLD.DataSource = cc.TimKiemCCLD(btnMaDVSD.Text, btnMaKho.Text, btnMaDTSD.Text, btnMaDuAn.Text, btnMaNhomCCLD.Text, btnMaTinhTrang.Text, txtMaLo.Text, txtTenLo.Text,spinDonGiaTu.Text, spinDonGiaDen.Text, chkChoDuyet.Checked, chkDaDuyet.Checked, chkTuChoiDuyet.Checked, chkHuyDuyet.Checked);
            gvCCLD.BestFitColumns();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void gvTang_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnMaDVSD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmDVSDTK frm = new frmDVSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDVSD.Text = frm._sMa;
            txtTenDVSD.Text = frm._sTen;
        }

        private void btnMaKho_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmKhoTK frm = new frmKhoTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaKho.Text = frm._sMa;
            txtTenKho.Text = frm._sTen;
        }

        private void btnMaDTSD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmDTSDTK frm = new QLCCLD.DanhMuc.DTSD.frmDTSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDTSD.Text = frm._sMa;
            txtTenDTSD.Text = frm._sTen;
        }

        private void btnMaDuAn_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmDuAnTK frm = new frmDuAnTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDuAn.Text = frm._sMa;
            txtTenDuAn.Text = frm._sTen;
        }

        private void btnMaNhomCCLD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmNhomCCLDTK frm = new frmNhomCCLDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaNhomCCLD.Text = frm._sMa;
            txtTenNhomCCLD.Text = frm._sTen;
        }

        private void btnMaTinhTrang_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmTinhTrangTK frm = new frmTinhTrangTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaTinhTrang.Text = frm._sMa;
            txtTenTinhTrang.Text = frm._sTen;
        }

        private void linkMaCCLD_Click(object sender, EventArgs e)
        {
            frmLichSu frm = new frmLichSu();
            frm.MaCCLD = gvCCLD.GetFocusedRowCellValue("MA_CCLD").ToString();
            frm.TenCCLD = gvCCLD.GetFocusedRowCellValue("TEN_CCLD").ToString();
            frm.DonGia = gvCCLD.GetFocusedRowCellValue("DON_GIA").ToString();
            frm.DVSD = gvCCLD.GetFocusedRowCellValue("DON_VI").ToString();
            frm.DTSD = gvCCLD.GetFocusedRowCellValue("TEN_DTSDTS").ToString();
            frm.DuAn = gvCCLD.GetFocusedRowCellValue("TEN_DU_AN").ToString();

            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
        }
    }
}
