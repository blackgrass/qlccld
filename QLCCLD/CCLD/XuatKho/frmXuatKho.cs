﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using DevExpress.XtraLayout.Utils;
using EM.UI;
using QLCCLD.CCLD.BaoCao;
using QLCCLD.DanhMuc.Kho;

namespace QLCCLD.CCLD.XuatKho
{
    public partial class frmXuatKho : DialogBase
    {
        #region Khai báo biến và tham số
        public string _sTrangThai = "";
        public string _sSoPhieu = "";
        clsCCLD cc = new clsCCLD();
        DataTable dtChonCCLD = new DataTable();
        DataSet ds = new DataSet(); //chứa dữ liệu 1 phieu nhap kho đã chọn
        List<string> lstDelCCLD = new List<string>();

        public frmXuatKho()
        {
            InitializeComponent();
        }

        #endregion

        #region Các sự kiện
        private void frmXuatKho_Load(object sender, EventArgs e)
        {
            CreateTable();
            if (_sTrangThai == "Search") //Tìm kiếm
            {
                Text = "Sửa phiếu xuất kho";
                LoadForm();
                VisiableControl(true);
            }
            else
            {
                Text = "Thêm phiếu xuất kho";
                ResetForm(); //Thêm mới
                VisiableControl(false);
            }
        }

        private void btnThemCCLD_Click(object sender, EventArgs e)
        {
            if (HasErrors)
                return;
            string sMaCCLDDaChon = "";
            for (int i = 0; i < gvCCLD.RowCount; i++)
            {
                if (i > 0)
                    sMaCCLDDaChon += ",'" + gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
                else sMaCCLDDaChon += "'" + gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString() + "'";
            }

            frmChonCCLD frm = new frmChonCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            //frm._sEvent = "1"; //thêm mới lô
            frm.dtChonCCLD = dtChonCCLD.Copy();
            frm._sMaCCLDDaChon = sMaCCLDDaChon;
            frm._sMaKho = btnMaKho.Text;
            frm.ShowDialog();
            dtChonCCLD = frm.dtChonCCLD.Copy();

            gcCCLD.DataSource = dtChonCCLD;
            gvCCLD.BestFitColumns();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
            {
                DialogResult = DialogResult.None;
                return;
            }

            if (string.IsNullOrEmpty(txtSoPhieu.Text)) //thêm mới
            {
                if (gvCCLD.RowCount < 1)
                {
                    XtraMessageBox.Show("Lỗi: Chưa chọn CCLĐ");
                }
                else
                {
                    if (SaveData())
                        lblMessage.Text = "Ghi dữ liệu thành công!";
                    else lblMessage.Text = "Không ghi được dữ liệu!";
                    _sTrangThai = "Search";
                    LoadForm();
                    VisiableControl(true);
                }
            }
            else //sửa
            {
                if (SaveData())
                    lblMessage.Text = "Ghi dữ liệu thành công!";
                else lblMessage.Text = "Không ghi được dữ liệu!";
                _sTrangThai = "Search";
                LoadForm();
                VisiableControl(true);
            }
        }       

        private void btnXoaCCLD_Click(object sender, EventArgs e)
        {
            if (gvCCLD.RowCount != 0)
            {
                List<string> lstDelCCLDOnDtChonCCLD = new List<string>();

                if (XtraMessageBox.Show("Xóa những CCLĐ này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (int i in gvCCLD.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string sMa = gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString(); // GetRowCellValue để lấy ra giá trị của cột MA ở dòng thứ i
                        if (!string.IsNullOrEmpty(sMa))
                            lstDelCCLD.Add(sMa); //lấy MA_CCLD của các bản ghi cần xóa trong cơ sở dữ liệu
                        lstDelCCLDOnDtChonCCLD.Add(sMa);//lấy MA_CCLD của các bản ghi cần xóa trong datatable dtChonCCLD
                    }
                    for (int i = 0; i < lstDelCCLDOnDtChonCCLD.Count; i++) //xóa CCLĐ tại dtChonCCLD
                    {
                        DataRow[] dr = dtChonCCLD.Select("MA_CCLD='" + lstDelCCLDOnDtChonCCLD[i] + "'");
                        dtChonCCLD.Rows.Remove(dr[0]);
                    }
                    gcCCLD.DataSource = dtChonCCLD;
                    gvCCLD.BestFitColumns();
                }
            }
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            _sTrangThai = "Update";
            VisiableControl(false);
        }

        private void btnHuyBo_Click(object sender, EventArgs e)
        {
            if (_sTrangThai == "Insert")
            {
                LoadForm();
                this.Close();
            }
            else
            {
                _sTrangThai = "Search";
                LoadForm();
                VisiableControl(true);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            DialogResult Result = XtraMessageBox.Show(this, "Xóa phiếu xuất kho " + txtSoPhieu.Text + "?", "Xóa phiếu xuất kho",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Result == DialogResult.Yes)
            {
                for (int i = 0; i < gvCCLD.RowCount; i++)
                {
                    cc.DelNhapXuatCT(txtSoPhieu.Text, "0", gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString());
                }

                cc.DeleteNhapXuat(txtSoPhieu.Text, "0"); //0: xuất kho
                this.Close();
            }
        }

        private void btnDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetXuatKho(txtSoPhieu.Text, "1", "0")) //trạng thái = 1: đã duyệt; loại = 0: xuất kho
                lblMessage.Text = "Bạn đã duyệt thành công";
            else
                lblMessage.Text = "Bạn đã duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnTuChoiDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetXuatKho(txtSoPhieu.Text, "2", "0")) //trang thái = 2: từ chối duyệt; loại = 0: xuất kho
                lblMessage.Text = "Bạn đã từ chối duyệt thành công";
            else
                lblMessage.Text = "Bạn đã từ chối duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnHuyDuyet_Click(object sender, EventArgs e)
        {
            if (DuyetXuatKho(txtSoPhieu.Text, "3", "0")) //trạng thái = 3: hủy duyệt; loại = 0: xuất kho
                lblMessage.Text = "Bạn đã hủy duyệt thành công";
            else
                lblMessage.Text = "Bạn đã hủy duyệt không thành công";
            LoadForm();
            VisiableControl(true);
        }

        private void btnMaKho_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.Kho.frmKhoTK frm = new frmKhoTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaKho.Text = frm._sMa;
            txtTenKho.Text = frm._sTen;
            txtDiaDiem.Text = frm._sDiaChi;
        }

        private void btnIn_Click(object sender, EventArgs e)
        {
            BaoCao.frmReport frm = new frmReport();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm._sNghiepVu = "4";
            frm._sThamSo = txtSoPhieu.Text;
            frm.ShowDialog();
        }

        #endregion

        #region Các hàm

        private void VisiableControl(bool bHide)
        {
            if (_sTrangThai == "Insert") //thêm mới
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemLo.Visibility = LayoutVisibility.Always;
                lcXoaLo.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Never;
            }
            else if (_sTrangThai == "Search") //tìm kiếm
            {
                lcGhi.Visibility = LayoutVisibility.Never;
                lcHuyBo.Visibility = LayoutVisibility.Never;
                lcThemLo.Visibility = LayoutVisibility.Never;
                lcXoaLo.Visibility = LayoutVisibility.Never;

                lcSua.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcXoa.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Never : LayoutVisibility.Always;
                lcDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "0" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcHuyDuyet.Visibility = ds.Tables[0].Rows[0]["TRANG_THAI"].ToString() == "1" ? LayoutVisibility.Always : LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Always;
            }
            else //sửa thông tin
            {
                lcGhi.Visibility = LayoutVisibility.Always;
                lcHuyBo.Visibility = LayoutVisibility.Always;
                lcThemLo.Visibility = LayoutVisibility.Always;
                lcXoaLo.Visibility = LayoutVisibility.Always;

                lcSua.Visibility = LayoutVisibility.Never;
                lcXoa.Visibility = LayoutVisibility.Never;
                lcDuyet.Visibility = LayoutVisibility.Never;
                lcTuChoiDuyet.Visibility = LayoutVisibility.Never;
                lcHuyDuyet.Visibility = LayoutVisibility.Never;
                lcIn.Visibility = LayoutVisibility.Never;
            }
            txtSoChungTu.Properties.ReadOnly = bHide;
            txtTenNguoiGiao.Properties.ReadOnly = bHide;
            txtSoQD.Properties.ReadOnly = bHide;
            txtNguoiQD.Properties.ReadOnly = bHide;
            btnMaKho.Properties.Enabled = !bHide;
            txtGhiChu.Properties.ReadOnly = bHide;
            txtThuTruongDV.Properties.ReadOnly = bHide;
            txtKeToan.Properties.ReadOnly = bHide;
            txtNguoiGiao.Properties.ReadOnly = bHide;
            datNgayNhapKho.Properties.ReadOnly = bHide;
            txtDiaChiNguoiGiao.Properties.ReadOnly = bHide;
            txtCanCuVao.Properties.ReadOnly = bHide;
            datNgayQD.Properties.ReadOnly = bHide;
            txtTruongPhongKeToan.Properties.ReadOnly = bHide;
            txtThuKho.Properties.ReadOnly = bHide;
            txtLyDo.Properties.ReadOnly = bHide;
        }

        private void LoadForm()
        {
            //load thông tin chung
            ds = cc.GetPhieuNhapXuat(_sSoPhieu,"0"); //xuất kho
            if (ds.Tables[0].Rows.Count < 1)
                return;
            switch (ds.Tables[0].Rows[0]["TRANG_THAI"].ToString())
            {
                case "0":
                    txtTrangThai.Text = "Chờ duyệt";
                    break;
                case "1":
                    txtTrangThai.Text = "Đã duyệt";
                    break;
                case "2":
                    txtTrangThai.Text = "Từ chối duyệt";
                    break;
                case "3":
                    txtTrangThai.Text = "Hủy duyệt";
                    break;
            }
            txtSoPhieu.Text = _sSoPhieu;
            txtSoChungTu.Text = ds.Tables[0].Rows[0]["SO_CHUNG_TU"].ToString();
            datNgayNhapKho.Text = ds.Tables[0].Rows[0]["NGAY_NHAP_XUAT"].ToString();
            txtTenNguoiGiao.Text = ds.Tables[0].Rows[0]["NGUOI_GIAO_NHAN"].ToString();
            txtDiaChiNguoiGiao.Text = ds.Tables[0].Rows[0]["DIA_CHI_GIAO_NHAN"].ToString();
            txtSoQD.Text = ds.Tables[0].Rows[0]["SO_QUYET_DINH"].ToString();
            txtCanCuVao.Text = ds.Tables[0].Rows[0]["CAN_CU"].ToString();
            txtNguoiQD.Text = ds.Tables[0].Rows[0]["NGUOI_QUYET_DINH"].ToString();
            datNgayQD.Text = ds.Tables[0].Rows[0]["NGAY_QUYET_DINH"].ToString();
            btnMaKho.Text = ds.Tables[0].Rows[0]["MA_KHO"].ToString();
            txtDiaDiem.Text = ds.Tables[0].Rows[0]["DIA_CHI"].ToString();
            txtTenKho.Text = ds.Tables[0].Rows[0]["TEN_KHO"].ToString();
            txtGhiChu.Text = ds.Tables[0].Rows[0]["GHI_CHU"].ToString();
            txtThuTruongDV.Text = ds.Tables[0].Rows[0]["THU_TRUONG_DON_VI"].ToString();
            txtKeToan.Text = ds.Tables[0].Rows[0]["KE_TOAN"].ToString();
            txtNguoiGiao.Text = ds.Tables[0].Rows[0]["NGUOI_GIAO"].ToString();
            txtTruongPhongKeToan.Text = ds.Tables[0].Rows[0]["TRUONG_PHONG_KE_TOAN"].ToString();
            txtThuKho.Text = ds.Tables[0].Rows[0]["THU_KHO"].ToString();

            //load grid thông tin lô cclđ
            gcCCLD.DataSource = ds.Tables[1];
            gvCCLD.BestFitColumns(); //điều chỉnh độ rộng của cột tự động
            dtChonCCLD = ds.Tables[1].Copy();
        }

        private bool SaveData()
        {
            bool bResult = true;
            try
            {
                
                //if (lstDelCCLD.Count > 0) //xóa CCLĐ
                //{
                //    for (int i = 0; i < lstDelCCLD.Count; i++)
                //    {
                //        bResult = cc.DelCCLD(lstDelCCLD[i]);
                //    }
                //}
                decimal dTongTien = 0;

                if (_sTrangThai == "Insert") //thêm mới xuất kho
                {
                    string sSoPhieu = cc.GetSoPhieu("0"); //xuất kho
                    bResult = cc.InsertNhapXuat(datNgayNhapKho.Text, sSoPhieu, txtSoChungTu.Text, null, txtTenNguoiGiao.Text, txtDiaChiNguoiGiao.Text,
                                                txtCanCuVao.Text, txtSoQD.Text, txtNguoiQD.Text, datNgayQD.Text, btnMaKho.Text, null, txtGhiChu.Text,
                                                txtThuTruongDV.Text, txtKeToan.Text, txtNguoiGiao.Text, txtThuKho.Text, txtTruongPhongKeToan.Text, "0", dTongTien); //Loại = 0-xuất kho
                    _sSoPhieu = sSoPhieu;
                }
                else //sửa thông tin nhập kho
                {
                    bResult = cc.UpdateNhapXuat(datNgayNhapKho.Text, txtSoPhieu.Text, txtSoChungTu.Text, null, txtTenNguoiGiao.Text, txtDiaChiNguoiGiao.Text,
                                                txtCanCuVao.Text, txtSoQD.Text, txtNguoiQD.Text, datNgayQD.Text, btnMaKho.Text,null, txtGhiChu.Text,
                                                txtThuTruongDV.Text, txtKeToan.Text, txtNguoiGiao.Text, txtThuKho.Text, txtTruongPhongKeToan.Text, "0", dTongTien);
                    _sSoPhieu = txtSoPhieu.Text;
                }
                //thêm CCLĐ
                foreach (DataRow dr in dtChonCCLD.Rows)
                {
                    dTongTien += decimal.Parse(dr["TONG_TIEN"].ToString());
                    bResult = cc.InsertNhapXuatCT(_sSoPhieu, dr["MA_CCLD"].ToString(), "1", btnMaKho.Text, txtLyDo.Text, dTongTien, "0");
                    //if (cc.CheckMaCCLD(dr["MA_CCLD"].ToString(),"0").Rows.Count == 0) //thêm mới
                    //{
                    //    if (string.IsNullOrEmpty(txtSoPhieu.Text)) //thêm mới xuất kho và chi tiết xuất kho
                    //        bResult = cc.InsertNhapXuatCT(sSoPhieu, dr["MA_CCLD"].ToString(), datNgayNhapKho.Text, "1",
                    //                                      txtLyDo.Text, dTongTien,"0");
                    //    else
                    //    { //thêm CCLĐ tại số phiếu xuất kho đã có
                    //        bResult = cc.InsertNhapXuatCT(txtSoPhieu.Text, dr["MA_CCLD"].ToString(), datNgayNhapKho.Text, "1",
                    //                                     txtLyDo.Text, dTongTien,"0");
                    //    }
                    //}
                    //else //sửa
                    //{
                    //    bResult = cc.UpdateNhapXuatCT(datNgayNhapKho.Text, txtLyDo.Text, txtSoPhieu.Text, dr["MA_CCLD"].ToString(), dTongTien,"0");
                    //}

                }

            }
            catch (Exception ex)
            {
                throw (ex);
            }
            return bResult;
        }

        private void ResetForm()
        {
            txtTrangThai.ResetText();
            txtSoChungTu.ResetText();
            txtSoPhieu.ResetText();
            txtTenNguoiGiao.ResetText();
            txtSoQD.ResetText();
            txtNguoiQD.ResetText();
            btnMaKho.ResetText();
            txtTenKho.ResetText();
            txtGhiChu.ResetText();
            txtThuTruongDV.ResetText();
            txtKeToan.ResetText();
            txtNguoiGiao.ResetText();
            datNgayNhapKho.ResetText();
            txtDiaChiNguoiGiao.ResetText();
            txtCanCuVao.ResetText();
            datNgayQD.ResetText();
            txtDiaDiem.ResetText();
            txtTruongPhongKeToan.ResetText();
            txtThuKho.ResetText();
        }

        private void CreateTable()
        {
            dtChonCCLD.Columns.Add("MA_CCLD");
            dtChonCCLD.Columns.Add("TEN_CCLD");
            dtChonCCLD.Columns.Add("DON_VI_TINH");
            dtChonCCLD.Columns.Add("DON_GIA");
            dtChonCCLD.Columns.Add("TONG_TIEN");
            dtChonCCLD.Columns.Add("SL_THUC_TE");
        }

        private bool DuyetXuatKho(string sSoPhieu, string sTrangThai, string sLoai)
        {
            bool bResult = true;
            try
            {
                bResult = cc.DuyetNhapXuat(sSoPhieu, sTrangThai, sLoai);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return bResult;
        }

        protected override void SetValidateControl() // hàm Validate của form
        {
            datNgayNhapKho.ValidateEmptyStringRule(); // check bắt buộc nhập
            btnMaKho.ValidateEmptyStringRule();
        }

        #endregion

    }
}