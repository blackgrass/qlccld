﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using EM.UI;
using QLCCLD.DanhMuc.DVSD;

namespace QLCCLD.DanhMuc.DTSD
{
    public partial class frmDTSD : DialogBase
    {
        public string _sTrangThai = "";
        public string _sMaDTSD = "";
        clsDanhMuc dm = new clsDanhMuc();

        public frmDTSD()
        {
            InitializeComponent();
        }

        private void frmDTSD_Load(object sender, EventArgs e)
        {
            if (_sTrangThai == "Update")
                LoadThongTin();
        }

        private void LoadThongTin()
        {
            DataTable dtb = dm.LayThongTinTuMa(_sMaDTSD, "DM_DTSDTS", "MA_DTSDTS");

            txtMa.Text = dtb.Rows[0]["MA_DTSDTS"].ToString();
            txtTen.Text = dtb.Rows[0]["TEN_DTSDTS"].ToString();
            txtDiaChi.Text = dtb.Rows[0]["DIA_CHI"].ToString();
            txtDienThoai.Text = dtb.Rows[0]["DIEN_THOAI"].ToString();
            txtFax.Text = dtb.Rows[0]["FAX"].ToString();
            btnMaDVSD.Text = dtb.Rows[0]["MA_DVSDTS"].ToString();
            txtTenDVSD.Text = dm.LayThongTinTuMa(dtb.Rows[0]["MA_DVSDTS"].ToString(), "DM_DVSDTS", "MA_DVSDTS").Rows[0]["TEN_DVSDTS"].ToString();
            txtMa.Enabled = false;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
                {
                    DialogResult = DialogResult.None;
                    return;
                }

                if (_sTrangThai == "Update")
                    dm.SuaDTSD(txtMa.Text, txtTen.Text, txtDiaChi.Text, txtDienThoai.Text, txtFax.Text, btnMaDVSD.Text);
                else
                    dm.ThemDTSD(txtMa.Text, txtTen.Text, txtDiaChi.Text, txtDienThoai.Text, txtFax.Text, btnMaDVSD.Text);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
            }
        }

       

        protected override void SetValidateControl() // hàm Validate của form, cần validate những cái gì thì cho vào đây
        {
            txtTen.ValidateEmptyStringRule(); // ValidateEmptyStringRule là check bắt buộc nhập
            btnMaDVSD.ValidateEmptyStringRule();
            txtMa.ValidateCustom(CheckTrungMa); // cần validate một trường hợp đặc biệt thì dùng ValidateCustom, bên trong là tên hàm mình định nghĩa          
        }

        private string CheckTrungMa(Control c) // hàm validate mã ko được để trống hoặc đã tồn tại
        {
            TextEdit ma = (TextEdit)c;
            if (string.IsNullOrEmpty(ma.Text))
                return "Dữ liệu không được để trống.";

            DataTable dtb = dm.LayThongTinTuMa(ma.Text, "DM_DTSDTS", "MA_DTSDTS");
            if (dtb.Rows.Count != 0)
                return "Đã tồn tại mã ĐTSD này.";
            return string.Empty;
        }

        private void btnMaDVSD_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            DanhMuc.DVSD.frmDVSDTK frm = new frmDVSDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaDVSD.Text = frm._sMa;
            txtTenDVSD.Text = frm._sTen;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        
    }
}