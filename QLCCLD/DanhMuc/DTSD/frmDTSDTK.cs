﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.DTSD
{
    public partial class frmDTSDTK : DevExpress.XtraEditors.XtraForm
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string _sMa = "";
        public string _sTen = "";
        public string _sMaDVSD = "";

        public frmDTSDTK()
        {
            InitializeComponent();
        }

        private void LoadForm()
        {
            gcDTSD.DataSource = dm.SearchDTSD(txtMa.Text, txtTen.Text, _sMaDVSD);
            gvDTSD.BestFitColumns();
        }

        private void frmDTSDTK_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void gvDTSD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
            if (e.Column.Name == "Chon")
                e.DisplayText = "Chọn";
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LinkChon_Click(object sender, EventArgs e)
        {
            _sMa = gvDTSD.GetFocusedRowCellValue("MA").ToString();
            _sTen = gvDTSD.GetFocusedRowCellValue("TEN").ToString();
            this.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}