﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.DTSD
{
    public partial class ucDTSD : DevExpress.XtraEditors.XtraUserControl
    {
        clsDanhMuc dm = new clsDanhMuc();

        public ucDTSD()
        {
            InitializeComponent();
        }

        private void LoadDanhSach()
        {
            gcDTSD.DataSource = dm.LayDanhSachDTSD(txtTimKiem.Text);
            gvDTSD.BestFitColumns();//tự động điều chỉnh độ rộng của cột
        }

        private void ucDTSD_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmDTSD f = new frmDTSD();
            f.StartPosition = FormStartPosition.CenterParent;
            f.Text = "Thêm đối tượng sử dụng";
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
                LoadDanhSach();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (gvDTSD.RowCount != 0)
            {
                frmDTSD f = new frmDTSD();
                f.StartPosition = FormStartPosition.CenterParent;
                f.Text = "Sửa đối tượng sử dụng";

                f._sTrangThai = "Update"; // biến để xác định gọi form là thêm hay sửa

                f._sMaDTSD = gvDTSD.GetFocusedRowCellValue("MA").ToString(); // GetFocusedRowCellValue để lấy giá trị một cột của dòng đang chọn, ở đây là cột có FieldName là MA
                f.ShowDialog();
                if (f.DialogResult == DialogResult.OK)
                    LoadDanhSach();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (gvDTSD.RowCount != 0)
            {
                if (XtraMessageBox.Show("Xóa những ĐTSD này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // hiển thị câu hỏi xác nhận xóa
                {
                    foreach (int i in gvDTSD.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string sMa = gvDTSD.GetRowCellValue(i, "MA").ToString(); // GetRowCellValue để lấy ra giá trị của cột MA ở dòng thứ i
                        dm.XoaDTSD(sMa);
                    }
                    LoadDanhSach();
                }
            }
        }

        private void gvDTSD_DoubleClick(object sender, EventArgs e)
        {
            btnSua_Click(null, null);
        }

        private void gvDTSD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "") // sự kiện này để hiển thị cột STT cho cột không được gán FieldName nào trên grid
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

        private void btnTimKiem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) // nếu người dùng gõ từ khóa rồi bấm Enter thì tìm kiếm
                LoadDanhSach();
        }


    }
}
