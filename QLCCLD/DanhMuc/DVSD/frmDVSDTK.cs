﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.DVSD
{
    public partial class frmDVSDTK : DevExpress.XtraEditors.XtraForm
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string _sMa = "";
        public string _sTen = "";
        public frmDVSDTK()
        {
            InitializeComponent();
        }

        private void frmDVSDTK_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LoadForm()
        {
            gcDVSD.DataSource = dm.SearchDVSD(txtMa.Text, txtTen.Text);
            gvDVSD.BestFitColumns();
        }

        private void gvDVSD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
            if (e.Column.Name == "Chon")
                e.DisplayText = "Chọn";
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LinkChon_Click(object sender, EventArgs e)
        {
            _sMa = gvDVSD.GetFocusedRowCellValue("MA").ToString();
            _sTen = gvDVSD.GetFocusedRowCellValue("TEN").ToString();            
            this.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}