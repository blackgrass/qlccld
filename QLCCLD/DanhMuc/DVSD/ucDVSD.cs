﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.DVSD
{
    public partial class ucDVSD : DevExpress.XtraEditors.XtraUserControl
    {
        clsDanhMuc dm = new clsDanhMuc();

        public ucDVSD()
        {
            InitializeComponent();
        }

        private void ucDVSD_Load(object sender, EventArgs e)
        {           
            LoadDanhSach();            
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void LoadDanhSach()
        {
            gcDVSD.DataSource = dm.LayDanhSachDVSD(txtTimKiem.Text);
            gvDVSD.BestFitColumns();//tự động điều chỉnh độ rộng của cột
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmDVSD f = new frmDVSD();
            f.StartPosition = FormStartPosition.CenterParent;
            f.Text = "Thêm ĐVSD";
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
                LoadDanhSach();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (gvDVSD.RowCount != 0)
            {
                frmDVSD f = new frmDVSD();
                f.StartPosition = FormStartPosition.CenterParent;
                f.Text = "Sửa ĐVSD";

                f._sTrangThai = "Update"; // biến để xác định gọi form là thêm hay sửa

                f._sMaDVSD = gvDVSD.GetFocusedRowCellValue("MA").ToString(); // GetFocusedRowCellValue để lấy giá trị một cột của dòng đang chọn, ở đây là cột có FieldName là MA
                f.ShowDialog();
                if (f.DialogResult == DialogResult.OK)
                    LoadDanhSach();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (gvDVSD.RowCount != 0)
            {
                if (XtraMessageBox.Show("Xóa những ĐVSD này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // hiển thị câu hỏi xác nhận xóa
                {
                    foreach (int i in gvDVSD.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string sMa = gvDVSD.GetRowCellValue(i, "MA").ToString(); // GetRowCellValue để lấy ra giá trị của cột MA ở dòng thứ i
                        dm.XoaDVSD(sMa);
                    }
                    LoadDanhSach();
                }
            }
        }

        private void gvDVSD_DoubleClick(object sender, EventArgs e)
        {
            btnSua_Click(null, null);
        }

        private void txtTimKiem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) // nếu người dùng gõ từ khóa rồi bấm Enter thì tìm kiếm
                LoadDanhSach();
        }

        private void gvDVSD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.FieldName == "") // sự kiện này để hiển thị cột STT cho cột không được gán FieldName nào trên grid
                e.DisplayText = (e.RowHandle + 1).ToString();
        }

      
        
    }
}
