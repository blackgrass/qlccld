﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using EM.UI;

namespace QLCCLD.DanhMuc.DuAn
{
    public partial class frmDuAn : DialogBase
    {
        public string _sTrangThai = "";
        public string _sMaDuAn = "";
        clsDanhMuc dm = new clsDanhMuc();

        public frmDuAn()
        {
            InitializeComponent();
        }

        private void frmDuAn_Load(object sender, EventArgs e)
        {
            if (_sTrangThai == "Update")
                LoadThongTin();
        }

        private void LoadThongTin()
        {
            DataTable dtb = dm.LayThongTinTuMa(_sMaDuAn, "DM_DU_AN", "MA_DU_AN");

            txtMa.Text = dtb.Rows[0]["MA_DU_AN"].ToString();
            txtTen.Text = dtb.Rows[0]["TEN_DU_AN"].ToString();
            txtSoQD.Text = dtb.Rows[0]["SO_QUYET_DINH"].ToString();
            if (!string.IsNullOrEmpty(dtb.Rows[0]["NGAY_QUYET_DINH"].ToString()))
                datNgayQD.Text = DateTime.ParseExact(dtb.Rows[0]["NGAY_QUYET_DINH"].ToString(), "yyyyMMdd", null).ToShortDateString();

            txtMa.Enabled = false;
        }

        protected override void SetValidateControl() // hàm Validate của form, cần validate những cái gì thì cho vào đây
        {
            txtTen.ValidateEmptyStringRule(); // ValidateEmptyStringRule là check bắt buộc nhập
            txtMa.ValidateCustom(CheckTrungMa); // cần validate một trường hợp đặc biệt thì dùng ValidateCustom, bên trong là tên hàm mình định nghĩa          
        }

        private string CheckTrungMa(Control c) // hàm validate mã ko được để trống hoặc đã tồn tại
        {
            TextEdit ma = (TextEdit)c;
            if (string.IsNullOrEmpty(ma.Text))
                return "Dữ liệu không được để trống.";

            DataTable dtb = dm.LayThongTinTuMa(ma.Text, "DM_DU_AN", "MA_DU_AN");
            if (dtb.Rows.Count != 0)
                return "Đã tồn tại mã dự án này.";
            return string.Empty;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
                {
                    DialogResult = DialogResult.None;
                    return;
                }

                if (_sTrangThai == "Update")
                    dm.SuaDuAn(txtMa.Text, txtTen.Text, txtSoQD.Text, DateTime.Parse(datNgayQD.Text).ToString("yyyyMMdd"));
                else
                    dm.ThemDuAn(txtMa.Text, txtTen.Text, txtSoQD.Text, DateTime.Parse(datNgayQD.Text).ToString("yyyyMMdd"));
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}