﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.DuAn
{
    public partial class frmDuAnTK : DevExpress.XtraEditors.XtraForm
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string _sMa = "";
        public string _sTen = "";

        public frmDuAnTK()
        {
            InitializeComponent();
        }

        private void frmDuAnTK_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LoadForm()
        {
            gcDuAn.DataSource = dm.SearchDuAn(txtMa.Text, txtTen.Text);
        }

        private void LinkChon_Click(object sender, EventArgs e)
        {
            _sMa = gvDuAn.GetFocusedRowCellValue("MA").ToString();
            _sTen = gvDuAn.GetFocusedRowCellValue("TEN").ToString();
            this.Close();
        }

        private void gvLyDo_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
            if (e.Column.Name == "Chon")
                e.DisplayText = "Chọn";
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadForm();
        }
    }
}