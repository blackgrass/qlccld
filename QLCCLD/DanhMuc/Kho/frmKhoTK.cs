﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.Kho
{
    public partial class frmKhoTK : DevExpress.XtraEditors.XtraForm
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string _sMa = "";
        public string _sTen = "";
        public string _sDiaChi = "";

        public frmKhoTK()
        {
            InitializeComponent();
        }

        private void frmKhoTK_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LoadForm()
        {
            gcKho.DataSource = dm.SearchKho(txtMa.Text, txtTen.Text);
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LinkChon_Click(object sender, EventArgs e)
        {
            _sMa = gvKho.GetFocusedRowCellValue("MA").ToString();
            _sTen = gvKho.GetFocusedRowCellValue("TEN").ToString();
            _sDiaChi = gvKho.GetFocusedRowCellValue("DIA_CHI").ToString();
            this.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void gvKho_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
            if (e.Column.Name == "Chon")
                e.DisplayText = "Chọn";
        }
    }
}