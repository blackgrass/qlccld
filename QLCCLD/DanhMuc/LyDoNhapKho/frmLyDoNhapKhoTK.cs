﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.LyDoNhapKho
{
    public partial class frmLyDoNhapKhoTK : DevExpress.XtraEditors.XtraForm
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string _sMa = "";
        public string _sTen = "";

        public frmLyDoNhapKhoTK()
        {
            InitializeComponent();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LoadForm()
        {
            gcLyDoNhapKho.DataSource = dm.SearchLyDoNhapKho(txtMa.Text, txtTen.Text);
        }

        private void LinkChon_Click(object sender, EventArgs e)
        {
            _sMa = gvLyDoNhapKho.GetFocusedRowCellValue("MA").ToString();
            _sTen = gvLyDoNhapKho.GetFocusedRowCellValue("TEN").ToString();
            this.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmLyDoNhapKhoTK_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void gvLyDoNhapKho_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
            if (e.Column.Name == "Chon")
                e.DisplayText = "Chọn";
        }
    }
}