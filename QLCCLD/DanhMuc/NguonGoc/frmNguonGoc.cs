﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using EM.UI;

namespace QLCCLD.DanhMuc.NguonGoc
{
    public partial class frmNguonGoc : DialogBase // DialogBase là class của dll EM.UI, có nhiều chức năng, ở đây mình chỉ tạm dùng chức năng Validate control của nó
    {
        #region "khai báo biến và tham số"

        public string _sTrangThai = "";
        public string _sMaNguonGoc = "";
        clsDanhMuc dm = new clsDanhMuc();

        public frmNguonGoc()
        {
            InitializeComponent();
        }

        #endregion

        #region "Các sự kiện"

        private void frmNguonGoc_Load(object sender, EventArgs e)
        {
            if (_sTrangThai == "Update")
                LoadThongTin();
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
                {
                    DialogResult = DialogResult.None;
                    return;
                }

                if (_sTrangThai == "Update")
                    dm.SuaNguonGoc(txtMa.Text, txtTen.Text);
                else
                    dm.ThemNguonGoc(txtMa.Text, txtTen.Text);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
            }
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion

        #region "các hàm"
        private void LoadThongTin()
        {
            DataTable dtb = dm.LayThongTinTuMa(_sMaNguonGoc, "DM_NGUON_GOC", "MA_NGUON_GOC");

            txtMa.Text = dtb.Rows[0]["MA_NGUON_GOC"].ToString();
            txtTen.Text = dtb.Rows[0]["TEN_NGUON_GOC"].ToString();

            txtMa.Enabled = false;
        }

        protected override void SetValidateControl() // hàm Validate của form, cần validate những cái gì thì cho vào đây
        {
            txtTen.ValidateEmptyStringRule(); // ValidateEmptyStringRule là check bắt buộc nhập
            txtMa.ValidateCustom(CheckTrungMa); // cần validate một trường hợp đặc biệt thì dùng ValidateCustom, bên trong là tên hàm mình định nghĩa          
        }

        private string CheckTrungMa(Control c) // hàm validate mã ko được để trống hoặc đã tồn tại
        {
            TextEdit ma = (TextEdit)c;
            if (string.IsNullOrEmpty(ma.Text))
                return "Dữ liệu không được để trống.";

            DataTable dtb = dm.LayThongTinTuMa(ma.Text, "DM_NGUON_GOC", "MA_NGUON_GOC");
            if (dtb.Rows.Count != 0)
                return "Đã tồn tại mã nguồn gốc này.";
            return string.Empty;
        }
        #endregion

       
        

        

        
    }
}