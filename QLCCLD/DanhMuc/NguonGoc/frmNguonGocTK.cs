﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.NguonGoc
{
    public partial class frmNguonGocTK : DevExpress.XtraEditors.XtraForm
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string _sMa = "";
        public string _sTen = "";
        public frmNguonGocTK()
        {
            InitializeComponent();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LinkChon_Click(object sender, EventArgs e)
        {
            _sMa = gvNguonGoc.GetFocusedRowCellValue("MA").ToString();
            _sTen = gvNguonGoc.GetFocusedRowCellValue("TEN").ToString();
            this.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadForm()
        {
            gcNguonGoc.DataSource = dm.SearchNguonGoc(txtMa.Text, txtTen.Text);
            gvNguonGoc.BestFitColumns();
        }

        private void frmNguonGocTK_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void gvNguonGoc_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
            if (e.Column.Name == "Chon")
                e.DisplayText = "Chọn";
        }
    }
}