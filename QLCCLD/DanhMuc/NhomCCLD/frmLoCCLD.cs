﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.NhomCCLD
{
    public partial class frmLoCCLD : DevExpress.XtraEditors.XtraForm
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string _sMa = "";
        public string _sTen = "";

        public frmLoCCLD()
        {
            InitializeComponent();
        }

        private void frmLoCCLD_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LoadForm()
        {
            gcLoCCLD.DataSource = dm.SearchLoCCLD(txtMa.Text, txtTen.Text);
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LinkChon_Click(object sender, EventArgs e)
        {
            _sMa = gvLoCCLD.GetFocusedRowCellValue("MA").ToString();
            _sTen = gvLoCCLD.GetFocusedRowCellValue("TEN").ToString();
            this.Close();
        }

        private void gvLoCCLD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
            if (e.Column.Name == "Chon")
                e.DisplayText = "Chọn";
        }
    }
}