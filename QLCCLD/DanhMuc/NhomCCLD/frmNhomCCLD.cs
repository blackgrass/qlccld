﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using EM.UI;

namespace QLCCLD.DanhMuc.NhomCCLD
{
    public partial class frmNhomCCLD : DialogBase
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string MaNhom = "";

        public frmNhomCCLD()
        {
            InitializeComponent();
        }

        private void frmNhomCCLD_Load(object sender, EventArgs e)
        {
            LoadNhomCapTren();
            if (Mode == Modes.Update)
                LoadThongTin();
        }

        private void LoadThongTin()
        {
            DataTable dtb = dm.LayThongTinTuMa(MaNhom, "DM_NHOM_CCLD", "MA_NHOM_CCLD");

            txtMaNhom.Text = dtb.Rows[0]["MA_NHOM_CCLD"].ToString();
            txtTenNhom.Text = dtb.Rows[0]["TEN_NHOM_CCLD"].ToString();
            txtDonViTinh.Text = dtb.Rows[0]["DON_VI_TINH"].ToString();

            if (dtb.Rows[0]["MA_NHOM_CHA"].ToString() != "")
            {
                lkNhomCapTren.EditValue = dtb.Rows[0]["MA_NHOM_CHA"].ToString(); // EditValue tương tự như SelectedValue của Combobox
                ckNhom2.Checked = true;
            }

            txtMaNhom.Enabled = false;
        }

        private void LoadNhomCapTren()
        {
            lkNhomCapTren.Properties.DataSource = dm.LayDanhSachNhomCCLDCapTren();
            lkNhomCapTren.ItemIndex = 0; // sau khi load thì mặc định chọn dòng đầu tiên
        }

        private void ckNhom1_CheckedChanged(object sender, EventArgs e)
        {
            lkNhomCapTren.Enabled = !ckNhom1.Checked;
        }

        private void ckNhom2_CheckedChanged(object sender, EventArgs e)
        {
            lkNhomCapTren.Enabled = !ckNhom1.Checked;
        }

        protected override void SetValidateControl()
        {
            txtTenNhom.ValidateEmptyStringRule();
            txtMaNhom.ValidateCustom(CheckTrungMaNhom);
        }

        private string CheckTrungMaNhom(Control c)
        {
            TextEdit ma = (TextEdit)c;
            if (string.IsNullOrEmpty(ma.Text))
                return "Dữ liệu không được để trống.";
            DataTable dtb = dm.LayThongTinTuMa(ma.Text, "DM_NHOM_CCLD", "MA_NHOM_CCLD");
            if (dtb.Rows.Count != 0)
                return "Đã tồn tại mã nhóm này.";
            return string.Empty;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                if (HasErrors)
                {
                    DialogResult = DialogResult.None;
                    return;
                }
                string MaNhomCha = (ckNhom2.Checked) ? lkNhomCapTren.EditValue.ToString() : "";
                if (Mode == Modes.Update)
                    dm.SuaNhomCCLD(txtMaNhom.Text, txtTenNhom.Text, txtDonViTinh.Text, MaNhomCha);
                else
                    dm.ThemNhomCCLD(txtMaNhom.Text, txtTenNhom.Text, txtDonViTinh.Text, MaNhomCha);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
            }
        }
    }
}