﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.NhomCCLD
{
    public partial class frmNhomCCLDTK : DevExpress.XtraEditors.XtraForm
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string _sMa = "";
        public string _sTen = "";
        public string _sDonViTinh = "";

        public frmNhomCCLDTK()
        {
            InitializeComponent();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LoadForm()
        {
            gcNhomCCLD.DataSource = dm.SearchNhomCCLD(txtMa.Text, txtTen.Text);
        }

        private void frmNhomCCLDTK_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LinkChon_Click(object sender, EventArgs e)
        {
            _sMa = gvNhomCCLD.GetFocusedRowCellValue("MA").ToString();
            _sTen = gvNhomCCLD.GetFocusedRowCellValue("TEN").ToString();
            _sDonViTinh = gvNhomCCLD.GetFocusedRowCellValue("DON_VI_TINH").ToString();
            this.Close();
        }

        private void gvNhomCCLD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
            if (e.Column.Name == "Chon")
                e.DisplayText = "Chọn";
        }
    }
}