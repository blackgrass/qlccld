﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using DevExpress.XtraLayout.Utils;

namespace QLCCLD.DanhMuc.NhomCCLD
{
    public partial class frmSearchCCLD : DevExpress.XtraEditors.XtraForm
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string _sMaCCLD = "";
        public string _sTenCCLD = "";
        public string _sNghiepVu = "";
        public string _sMaCCLDDaChon = "";
        public DataTable dtChonCCLD = new DataTable();

        public frmSearchCCLD()
        {
            InitializeComponent();
        }

        private void frmSearchCCLD_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void LoadDanhSach()
        {
            gcCCLD.DataSource = dm.SearchCCLD(btnMaNhom.Text, btnMaLo.Text,_sNghiepVu,_sMaCCLDDaChon);
            gvCCLD.BestFitColumns();//tự động điều chỉnh độ rộng của cột

            if (_sNghiepVu == "2") //bàn giao
            {
                gvCCLD.Columns["Chon"].Visible = false;
                gvCCLD.Columns["ChkChon"].Visible = true;
                lcChon.Visibility = LayoutVisibility.Always;
            }
            else //3: nhập kho
            {
                gvCCLD.Columns["Chon"].Visible = true;
                gvCCLD.Columns["ChkChon"].Visible = false;
                lcChon.Visibility = LayoutVisibility.Never;
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void LinkChon_Click(object sender, EventArgs e)
        {
            _sMaCCLD = gvCCLD.GetFocusedRowCellValue("MA_CCLD").ToString();
            _sTenCCLD = gvCCLD.GetFocusedRowCellValue("TEN_CCLD").ToString();
            this.Close();
        }

        private void gvCCLD_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
            if (e.Column.Name == "Chon")
                e.DisplayText = "Chọn";
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < gvCCLD.RowCount; i++)
            {
                if ((bool)gvCCLD.GetRowCellValue(i, "ChkChon") == true)
                {
                    DataRow dr = dtChonCCLD.NewRow();
                    dr["MA_CCLD"] = gvCCLD.GetRowCellValue(i, "MA_CCLD").ToString();
                    dr["TEN_CCLD"] = gvCCLD.GetRowCellValue(i, "TEN_CCLD").ToString();
                    dr["DON_GIA"] = gvCCLD.GetRowCellValue(i, "DON_GIA").ToString();
                    dr["TONG_TIEN"] = gvCCLD.GetRowCellValue(i, "DON_GIA").ToString();
                    dr["SO_LUONG"] = "1";//gvCCLD.GetRowCellValue(i, "SO_LUONG").ToString();
                    dr["MA_DU_AN"] = ""; //gvCCLD.GetRowCellValue(i, "MA_DU_AN").ToString();
                    dr["TEN_DU_AN"] = "";//gvCCLD.GetRowCellValue(i, "TEN_DU_AN").ToString();
                    dr["MA_TINH_TRANG"] = gvCCLD.GetRowCellValue(i, "MA_TINH_TRANG").ToString();
                    dr["TEN_TINH_TRANG"] = gvCCLD.GetRowCellValue(i, "TEN_TINH_TRANG").ToString();
                    dtChonCCLD.Rows.Add(dr);
                }
            }
            this.Close();
        }

        private void btnMaNhom_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmNhomCCLDTK frm = new frmNhomCCLDTK();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaNhom.Text = frm._sMa;
            txtTenNhom.Text = frm._sTen;
        }

        private void btnMaLo_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            frmLoCCLD frm = new frmLoCCLD();
            frm.StartPosition = FormStartPosition.CenterParent;
            frm.ShowDialog();
            btnMaLo.Text = frm._sMa;
            txtTenLo.Text = frm._sTen;
        }
    }
}