﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using EM.UI;

namespace QLCCLD.DanhMuc.NhomCCLD
{
    public partial class ucNhomCCLD : DevExpress.XtraEditors.XtraUserControl
    {
        clsDanhMuc dm = new clsDanhMuc();
        public ucNhomCCLD()
        {
            InitializeComponent();
        }

        private void ucNhomCCLD_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void LoadDanhSach()
        {
            gcNhomCCLD.DataSource = dm.LayDanhSachNhomCCLD(txtTimKiem.Text);
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmNhomCCLD f = new frmNhomCCLD();
            f.StartPosition = FormStartPosition.CenterParent;
            f.Text = "Thêm nhóm CCLĐ";
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
                LoadDanhSach();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (gvNhomCCLD.RowCount != 0)
            {
                frmNhomCCLD f = new frmNhomCCLD();
                f.StartPosition = FormStartPosition.CenterParent;
                f.Text = "Sửa nhóm CCLĐ";
                f.Mode = Modes.Update;
                f.MaNhom = gvNhomCCLD.GetFocusedRowCellValue("MA").ToString();
                f.ShowDialog();
                if (f.DialogResult == DialogResult.OK)
                    LoadDanhSach();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (gvNhomCCLD.RowCount != 0)
            {
                if (XtraMessageBox.Show("Xóa những nhóm CCLĐ này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    foreach (int i in gvNhomCCLD.GetSelectedRows())
                    {
                        string MaNhom = gvNhomCCLD.GetRowCellValue(i, "MA").ToString();
                        if (!dm.XoaNhomCCLD(MaNhom))
                        {
                            XtraMessageBox.Show("Không thể xóa do nhóm CCLĐ đang được sử dụng!", "Lỗi xóa nhóm CCLĐ: " + MaNhom, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    LoadDanhSach();
                }
            }
        }

        private void gvNhomCCLD_DoubleClick(object sender, EventArgs e)
        {
            btnSua_Click(null, null);
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void txtTimKiem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                LoadDanhSach();
        }       
    }
}
