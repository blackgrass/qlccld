﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.QuocGia
{
    public partial class ucQuocGia : DevExpress.XtraEditors.XtraUserControl
    {
        #region "khai báo các biến và tham số"
        public ucQuocGia()
        {
            InitializeComponent();
        }

        clsDanhMuc dm = new clsDanhMuc();
        #endregion

        #region "các sự kiện"
        private void ucQuocGia_Load(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            frmQuocGia frm = new frmQuocGia();
            frm.Text = "Thêm quốc gia";
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
                LoadDanhSach();
        }

        private void gvQuocGia_DoubleClick(object sender, EventArgs e)
        {
            btnSua_Click(null, null);
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            frmQuocGia frm = new frmQuocGia();
            frm.Text = "Sửa quốc gia";
            frm._sTrangThai = "Update";
            frm._sMaQuocGia = gvQuocGia.GetFocusedRowCellValue("MA").ToString(); // GetFocusedRowCellValue để lấy giá trị một cột của dòng đang chọn, ở đây là cột có FieldName là MA
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                LoadDanhSach();
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (gvQuocGia.RowCount != 0)
            {
                if (XtraMessageBox.Show("Xóa những quốc gia này?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) // hiển thị câu hỏi xác nhận xóa
                {
                    foreach (int i in gvQuocGia.GetSelectedRows()) // GetSelectedRow dùng để lấy ra index của các dòng đang chọn cần xóa
                    {
                        string MaQuocGia = gvQuocGia.GetRowCellValue(i, "MA").ToString(); // GetRowCellValue để lấy ra giá trị của cột MA ở dòng thứ i
                        dm.XoaQuocGia(MaQuocGia);
                    }
                    LoadDanhSach();
                }
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadDanhSach();
        }

        #endregion

        #region "các hàm"
        private void LoadDanhSach()
        {
            gcQuocGia.DataSource = dm.LayDanhSachQuocGia(txtTimKiem.Text);
        }
        #endregion

    }
}
