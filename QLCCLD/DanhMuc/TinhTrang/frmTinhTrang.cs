﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;
using EM.UI;

namespace QLCCLD.DanhMuc.TinhTrang
{
    public partial class frmTinhTrang :DialogBase
    {
        public string _sTrangThai = "";
        public string _sMaTinhTrang = "";
        clsDanhMuc dm = new clsDanhMuc();

        public frmTinhTrang()
        {
            InitializeComponent();
        }

        private void frmTinhTrang_Load(object sender, EventArgs e)
        {
            if (_sTrangThai == "Update")
                LoadThongTin();
        }

        private void LoadThongTin()
        {
            DataTable dtb = dm.LayThongTinTuMa(_sMaTinhTrang, "DM_TINH_TRANG", "MA_TINH_TRANG");

            txtMa.Text = dtb.Rows[0]["MA_TINH_TRANG"].ToString();
            txtTen.Text = dtb.Rows[0]["TEN_TINH_TRANG"].ToString();

            txtMa.Enabled = false;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                if (HasErrors) // nếu validate có lỗi thì biến HasErrors = true, khi đó thì báo lỗi và không làm gì cả
                {
                    DialogResult = DialogResult.None;
                    return;
                }

                if (_sTrangThai == "Update")
                    dm.SuaTinhTrang(txtMa.Text, txtTen.Text);
                else
                    dm.ThemTinhTrang(txtMa.Text, txtTen.Text);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.ToString());
            }
        }

        protected override void SetValidateControl() // hàm Validate của form, cần validate những cái gì thì cho vào đây
        {
            txtTen.ValidateEmptyStringRule(); // ValidateEmptyStringRule là check bắt buộc nhập
            txtMa.ValidateCustom(CheckTrungMa); // cần validate một trường hợp đặc biệt thì dùng ValidateCustom, bên trong là tên hàm mình định nghĩa          
        }

        private string CheckTrungMa(Control c) // hàm validate mã ko được để trống hoặc đã tồn tại
        {
            TextEdit ma = (TextEdit)c;
            if (string.IsNullOrEmpty(ma.Text))
                return "Dữ liệu không được để trống.";

            DataTable dtb = dm.LayThongTinTuMa(ma.Text, "DM_TINH_TRANG", "MA_TINH_TRANG");
            if (dtb.Rows.Count != 0)
                return "Đã tồn tại mã nguồn gốc này.";
            return string.Empty;
        }

        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}