﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Business;

namespace QLCCLD.DanhMuc.TinhTrang
{
    public partial class frmTinhTrangTK : DevExpress.XtraEditors.XtraForm
    {
        clsDanhMuc dm = new clsDanhMuc();
        public string _sMa = "";
        public string _sTen = "";

        public frmTinhTrangTK()
        {
            InitializeComponent();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void LoadForm()
        {
            gcTinhTrang.DataSource = dm.SearchTinhTrang(txtMa.Text, txtTen.Text);
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmTinhTrangTK_Load(object sender, EventArgs e)
        {
            LoadForm();
        }

        private void gvTinhTrang_CustomColumnDisplayText(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs e)
        {
            if (e.Column.Name == "STT")
                e.DisplayText = (e.RowHandle + 1).ToString();
            if (e.Column.Name == "Chon")
                e.DisplayText = "Chọn";
        }

        private void LinkChon_Click(object sender, EventArgs e)
        {
            _sMa = gvTinhTrang.GetFocusedRowCellValue("MA").ToString();
            _sTen = gvTinhTrang.GetFocusedRowCellValue("TEN").ToString();
            this.Close();
        }
    }
}