﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace QLCCLD.Helper
{
    public class Configuration
    {
        #region Application folders

        private static readonly string _baseDirectory = (string)GetAppSetting(typeof(string), "BaseDirectory", false);
        public static string BaseDirectory
        {
            get { return string.IsNullOrEmpty(_baseDirectory) ? AppDomain.CurrentDomain.BaseDirectory : _baseDirectory; }
        }

        #endregion

        #region private funtcion
        private static object GetAppSetting(Type expectedType, string key, bool require)
        {
            return GetAppSetting(expectedType, key, null, require);
        }

        private static object GetAppSetting(Type expectedType, string key, object defaultvalue, bool require)
        {
            var value = ConfigurationManager.AppSettings.Get(key);
            if (string.IsNullOrEmpty(value))
            {
                if (require) throw new Exception(string.Format("AppSetting: {0} not config yet.", key));
                else
                {
                    return defaultvalue ?? (expectedType.IsValueType ? Activator.CreateInstance(expectedType) : null);
                }
            }
            else
            {

                try
                {
                    if (expectedType.Equals(typeof(int)))
                    {
                        return int.Parse(value);
                    }

                    if (expectedType.Equals(typeof(string)))
                    {
                        return value;
                    }

                    if (expectedType.Equals(typeof(bool)))
                    {
                        return bool.Parse(value);
                    }

                    throw new Exception("Type not supported.");
                }
                catch (Exception ex)
                {
                    throw new Exception(
                        string.Format("AppSetting: Invalid config. {0} must be {1}.", key,
                                      expectedType),
                        ex);
                }
            }
        }
        #endregion

        #region Application settings

        #region MaNghiepVu
        public static string MaNghiepVuBanGiao = (string)GetAppSetting(typeof(string), "MaNghiepVuBanGiao", false);        
        #endregion

        #endregion


    }
}
