﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QLCCLD.Helper
{
    public static class ControlExtenstions
    {
        //public static void ClearFormControls(Form form)
        //{
        //    foreach (Control control in form.Controls)
        //    {
        //        if (control is TextBox)
        //        {
        //            TextBox txtbox = (TextBox) control;
        //            txtbox.Text = string.Empty;
        //        }
        //        else if (control is CheckBox)
        //        {
        //            CheckBox chkbox = (CheckBox) control;
        //            chkbox.Checked = false;
        //        }
        //        else if (control is RadioButton)
        //        {
        //            RadioButton rdbtn = (RadioButton) control;
        //            rdbtn.Checked = false;
        //        }
        //        else if (control is DateTimePicker)
        //        {
        //            DateTimePicker dtp = (DateTimePicker) control;
        //            dtp.Value = DateTime.Now;
        //        }
        //    }
        //}

        private static readonly Dictionary<Type, Action<Control>> Controldefaults = new Dictionary<Type, Action<Control>>()
        {
            {typeof (TextBox), c => ((TextBox) c).Clear()},
            {typeof (CheckBox), c => ((CheckBox) c).Checked = false},
            {typeof (ListBox), c => ((ListBox) c).Items.Clear()},
            {typeof (RadioButton), c => ((RadioButton) c).Checked = false},
            {typeof (GroupBox), c => ((GroupBox) c).Controls.ClearControls()},
            {typeof (Panel), c => ((Panel) c).Controls.ClearControls()},
            //dev controls
            {typeof (DevExpress.XtraEditors.TextEdit), c => ((DevExpress.XtraEditors.TextEdit) c).Controls.ClearControls()}
        };

        private static void FindAndInvoke(Type type, Control control)
        {
            if (Controldefaults.ContainsKey(type))
            {
                Controldefaults[type].Invoke(control);
            }
        }

        public static void ClearControls(this Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                FindAndInvoke(control.GetType(), control);
            }
        }

        public static void ClearControls<T>(this Control.ControlCollection controls) where T : class
        {
            if (!Controldefaults.ContainsKey(typeof (T))) return;

            foreach (Control control in controls)
            {
                if (control.GetType() == typeof (T))
                {
                    FindAndInvoke(typeof (T), control);
                }
                else if (control.Controls.Count>0)
                {
                    foreach (Control c in control.Controls)
                    {
                        
                    }
                }
            }

        }

    }

    public static class DevExpressControlsExtensions
    {
        public static void DateEditDisplayFormat(DevExpress.XtraEditors.DateEdit dateEdit, string format, System.Globalization.CultureInfo cultureInfo)
        {
            dateEdit.Properties.Mask.Culture = cultureInfo;
            dateEdit.Properties.Mask.EditMask = format;
            dateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
            dateEdit.Properties.CharacterCasing = CharacterCasing.Upper;
        }

        public static void DateEditDisplayFormat(ICollection<DevExpress.XtraEditors.DateEdit> dateEdits, string format, System.Globalization.CultureInfo cultureInfo)
        {
            foreach (var dateEdit in dateEdits)
            {
                dateEdit.Properties.Mask.Culture = cultureInfo;
                dateEdit.Properties.Mask.EditMask = format;
                dateEdit.Properties.Mask.UseMaskAsDisplayFormat = true;
                dateEdit.Properties.CharacterCasing = CharacterCasing.Upper;
            }
            
        }

        public static void XtraGridColumnDisplayFormat(DevExpress.XtraGrid.Columns.GridColumn gridColumn,string format)
        {
            gridColumn.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            gridColumn.DisplayFormat.FormatString = format;
        }
        public static void XtraGridColumnDisplayFormat(ICollection<DevExpress.XtraGrid.Columns.GridColumn> gridColumns, string format)
        {
            foreach (var gridColumn in gridColumns)
            {
                gridColumn.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
                gridColumn.DisplayFormat.FormatString = format;
            }
            
        }
              

    }
}
