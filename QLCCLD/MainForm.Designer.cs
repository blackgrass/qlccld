﻿namespace QLCCLD
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.dockManager1 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockPanel2 = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel2_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.barManager1 = new DevExpress.XtraBars.BarManager(this.components);
            this.bar2 = new DevExpress.XtraBars.Bar();
            this.barSubItem1 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem2 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem3 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem9 = new DevExpress.XtraBars.BarButtonItem();
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.dockManager2 = new DevExpress.XtraBars.Docking.DockManager(this.components);
            this.dockQLCCLD = new DevExpress.XtraBars.Docking.DockPanel();
            this.dockPanel1_Container = new DevExpress.XtraBars.Docking.ControlContainer();
            this.nbMain = new DevExpress.XtraNavBar.NavBarControl();
            this.menuQTHT = new DevExpress.XtraNavBar.NavBarGroup();
            this.navQLTK = new DevExpress.XtraNavBar.NavBarItem();
            this.navQuyen = new DevExpress.XtraNavBar.NavBarItem();
            this.navPhanQuyen = new DevExpress.XtraNavBar.NavBarItem();
            this.menuDanhMuc = new DevExpress.XtraNavBar.NavBarGroup();
            this.navDuAn = new DevExpress.XtraNavBar.NavBarItem();
            this.navDTSD = new DevExpress.XtraNavBar.NavBarItem();
            this.navDVSD = new DevExpress.XtraNavBar.NavBarItem();
            this.navKho = new DevExpress.XtraNavBar.NavBarItem();
            this.navLyDoNhapKho = new DevExpress.XtraNavBar.NavBarItem();
            this.navNguonGoc = new DevExpress.XtraNavBar.NavBarItem();
            this.navNhomCCLD = new DevExpress.XtraNavBar.NavBarItem();
            this.navTinhTrang = new DevExpress.XtraNavBar.NavBarItem();
            this.menuCCLD = new DevExpress.XtraNavBar.NavBarGroup();
            this.navTangCCLD = new DevExpress.XtraNavBar.NavBarItem();
            this.navNhapKho = new DevExpress.XtraNavBar.NavBarItem();
            this.navXuatKho = new DevExpress.XtraNavBar.NavBarItem();
            this.navBanGiao = new DevExpress.XtraNavBar.NavBarItem();
            this.navDieuChuyenCungDVSD = new DevExpress.XtraNavBar.NavBarItem();
            this.navDieuChuyenKhacDVSD = new DevExpress.XtraNavBar.NavBarItem();
            this.navThanhLy = new DevExpress.XtraNavBar.NavBarItem();
            this.navKiemKe = new DevExpress.XtraNavBar.NavBarItem();
            this.navTimKiem = new DevExpress.XtraNavBar.NavBarItem();
            this.menuBaoCao = new DevExpress.XtraNavBar.NavBarGroup();
            this.navDanhSach = new DevExpress.XtraNavBar.NavBarItem();
            this.imgButton = new DevExpress.Utils.ImageCollection(this.components);
            this.barSubItem4 = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItem5 = new DevExpress.XtraBars.BarSubItem();
            this.menuItemLiquidSky = new DevExpress.XtraBars.BarButtonItem();
            this.barBttDangXuat = new DevExpress.XtraBars.BarButtonItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.TenThanhPho = new DevExpress.XtraGrid.Columns.GridColumn();
            this.idDatNuoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.idThanhPho = new DevExpress.XtraGrid.Columns.GridColumn();
            this.TenDatNuoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabControl2 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEdit6 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit5 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.comboBoxEdit4 = new DevExpress.XtraEditors.ComboBoxEdit();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.emptySpaceItem13 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem16 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.tabChild = new DevExpress.XtraTab.XtraTabControl();
            this.xtratabChildMain = new DevExpress.XtraTab.XtraTabPage();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).BeginInit();
            this.dockPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager2)).BeginInit();
            this.dockQLCCLD.SuspendLayout();
            this.dockPanel1_Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).BeginInit();
            this.xtraTabControl2.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit6.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit4.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabChild)).BeginInit();
            this.tabChild.SuspendLayout();
            this.SuspendLayout();
            // 
            // dockManager1
            // 
            this.dockManager1.Form = this;
            this.dockManager1.HiddenPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockPanel2});
            this.dockManager1.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockPanel2
            // 
            this.dockPanel2.Controls.Add(this.dockPanel2_Container);
            this.dockPanel2.Dock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanel2.ID = new System.Guid("bae8d686-e049-44d5-8193-08e3a4c12758");
            this.dockPanel2.Location = new System.Drawing.Point(3, 25);
            this.dockPanel2.Name = "dockPanel2";
            this.dockPanel2.OriginalSize = new System.Drawing.Size(0, 0);
            this.dockPanel2.SavedDock = DevExpress.XtraBars.Docking.DockingStyle.Fill;
            this.dockPanel2.SavedIndex = 0;
            this.dockPanel2.SavedTabbed = true;
            this.dockPanel2.Size = new System.Drawing.Size(194, 415);
            this.dockPanel2.Text = "menu2";
            this.dockPanel2.Visibility = DevExpress.XtraBars.Docking.DockVisibility.Hidden;
            // 
            // dockPanel2_Container
            // 
            this.dockPanel2_Container.Location = new System.Drawing.Point(0, 0);
            this.dockPanel2_Container.Name = "dockPanel2_Container";
            this.dockPanel2_Container.Size = new System.Drawing.Size(194, 415);
            this.dockPanel2_Container.TabIndex = 0;
            // 
            // barManager1
            // 
            this.barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar2,
            this.bar3});
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.DockManager = this.dockManager2;
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barSubItem1,
            this.barSubItem2,
            this.barSubItem3,
            this.barButtonItem1,
            this.barSubItem4,
            this.barButtonItem2,
            this.barButtonItem3,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem8,
            this.barButtonItem9,
            this.barSubItem5,
            this.menuItemLiquidSky,
            this.barBttDangXuat});
            this.barManager1.MainMenu = this.bar2;
            this.barManager1.MaxItemId = 23;
            this.barManager1.StatusBar = this.bar3;
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockRow = 0;
            this.bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barSubItem3)});
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Hệ thống";
            this.barSubItem1.Id = 0;
            this.barSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barButtonItem1, DevExpress.XtraBars.BarItemPaintStyle.Standard),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem3)});
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Đăng xuất";
            this.barButtonItem1.Glyph = global::QLCCLD.Properties.Resources.power1;
            this.barButtonItem1.Id = 4;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBttDangXuat_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Đổi mật khẩu";
            this.barButtonItem2.Glyph = global::QLCCLD.Properties.Resources.key;
            this.barButtonItem2.Id = 6;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBttDoiMatKhau_ItemClick);
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Thoát";
            this.barButtonItem3.Glyph = global::QLCCLD.Properties.Resources.exit2;
            this.barButtonItem3.Id = 7;
            this.barButtonItem3.Name = "barButtonItem3";
            this.barButtonItem3.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBttThoat_ItemClick);
            // 
            // barSubItem2
            // 
            this.barSubItem2.Caption = "Trợ giúp";
            this.barSubItem2.Id = 2;
            this.barSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem7),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem8)});
            this.barSubItem2.Name = "barSubItem2";
            // 
            // barButtonItem7
            // 
            //this.barButtonItem7.Caption = "Thông tin";
            //this.barButtonItem7.Glyph = global::QLCCLD.Properties.Resources.info_16x16;
            //this.barButtonItem7.Id = 11;
            //this.barButtonItem7.Name = "barButtonItem7";
            //this.barButtonItem7.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem7_ItemClick);
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Caption = "Trợ giúp";
            this.barButtonItem8.Glyph = global::QLCCLD.Properties.Resources.index_16x16;
            this.barButtonItem8.Id = 12;
            this.barButtonItem8.Name = "barButtonItem8";
            // 
            // barSubItem3
            // 
            this.barSubItem3.Caption = "Thiết lập";
            this.barSubItem3.Id = 3;
            this.barSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem9)});
            this.barSubItem3.Name = "barSubItem3";
            // 
            // barButtonItem9
            // 
            this.barButtonItem9.Caption = "Tùy chỉnh";
            this.barButtonItem9.Glyph = global::QLCCLD.Properties.Resources.setting_tools;
            this.barButtonItem9.Id = 13;
            this.barButtonItem9.Name = "barButtonItem9";
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(710, 24);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 556);
            this.barDockControlBottom.Size = new System.Drawing.Size(710, 22);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 24);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 532);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(710, 24);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 532);
            // 
            // dockManager2
            // 
            this.dockManager2.Form = this;
            this.dockManager2.RootPanels.AddRange(new DevExpress.XtraBars.Docking.DockPanel[] {
            this.dockQLCCLD});
            this.dockManager2.TopZIndexControls.AddRange(new string[] {
            "DevExpress.XtraBars.BarDockControl",
            "System.Windows.Forms.StatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl"});
            // 
            // dockQLCCLD
            // 
            this.dockQLCCLD.Controls.Add(this.dockPanel1_Container);
            this.dockQLCCLD.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left;
            this.dockQLCCLD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.dockQLCCLD.ID = new System.Guid("ea3e0ecf-e0d4-46ec-a8c1-187a7aef63e7");
            this.dockQLCCLD.Location = new System.Drawing.Point(0, 24);
            this.dockQLCCLD.Name = "dockQLCCLD";
            this.dockQLCCLD.Options.ShowAutoHideButton = false;
            this.dockQLCCLD.Options.ShowCloseButton = false;
            this.dockQLCCLD.OriginalSize = new System.Drawing.Size(200, 200);
            this.dockQLCCLD.Size = new System.Drawing.Size(200, 532);
            // 
            // dockPanel1_Container
            // 
            this.dockPanel1_Container.Controls.Add(this.nbMain);
            this.dockPanel1_Container.Location = new System.Drawing.Point(3, 25);
            this.dockPanel1_Container.Name = "dockPanel1_Container";
            this.dockPanel1_Container.Size = new System.Drawing.Size(194, 504);
            this.dockPanel1_Container.TabIndex = 0;
            // 
            // nbMain
            // 
            this.nbMain.ActiveGroup = this.menuQTHT;
            this.nbMain.ContentButtonHint = null;
            this.nbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nbMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nbMain.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.menuQTHT,
            this.menuDanhMuc,
            this.menuCCLD,
            this.menuBaoCao});
            this.nbMain.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navTangCCLD,
            this.navQLTK,
            this.navPhanQuyen,
            this.navQuyen,
            this.navNhapKho,
            this.navXuatKho,
            this.navDanhSach,
            this.navBanGiao,
            this.navDieuChuyenCungDVSD,
            this.navThanhLy,
            this.navKiemKe,
            this.navNhomCCLD,
            this.navNguonGoc,
            this.navDVSD,
            this.navTinhTrang,
            this.navDuAn,
            this.navLyDoNhapKho,
            this.navKho,
            this.navDTSD,
            this.navDieuChuyenKhacDVSD,
            this.navTimKiem});
            this.nbMain.Location = new System.Drawing.Point(0, 0);
            this.nbMain.Name = "nbMain";
            this.nbMain.OptionsNavPane.ExpandedWidth = 194;
            this.nbMain.OptionsNavPane.ShowOverflowPanel = false;
            this.nbMain.Size = new System.Drawing.Size(194, 504);
            this.nbMain.SmallImages = this.imgButton;
            this.nbMain.TabIndex = 0;
            this.nbMain.Text = "menuMain";
            this.nbMain.View = new DevExpress.XtraNavBar.ViewInfo.SkinExplorerBarViewInfoRegistrator();
            // 
            // menuQTHT
            // 
            this.menuQTHT.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.menuQTHT.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.menuQTHT.Appearance.Options.UseFont = true;
            this.menuQTHT.Appearance.Options.UseForeColor = true;
            this.menuQTHT.AppearanceBackground.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.menuQTHT.AppearanceBackground.Options.UseFont = true;
            this.menuQTHT.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.menuQTHT.AppearancePressed.Options.UseFont = true;
            this.menuQTHT.Caption = "Quản trị hệ thống";
            this.menuQTHT.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navQLTK),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navQuyen),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navPhanQuyen)});
            this.menuQTHT.Name = "menuQTHT";
            // 
            // navQLTK
            // 
            this.navQLTK.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navQLTK.Appearance.Options.UseForeColor = true;
            this.navQLTK.Caption = "Quản lý tài khoản";
            this.navQLTK.LargeImageIndex = 0;
            this.navQLTK.Name = "navQLTK";
            this.navQLTK.SmallImageIndex = 0;
            this.navQLTK.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navQLTK_LinkClicked);
            // 
            // navQuyen
            // 
            this.navQuyen.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navQuyen.Appearance.Options.UseForeColor = true;
            this.navQuyen.Caption = "Quản lý quyền";
            this.navQuyen.Name = "navQuyen";
            this.navQuyen.SmallImageIndex = 1;
            // 
            // navPhanQuyen
            // 
            this.navPhanQuyen.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navPhanQuyen.Appearance.Options.UseForeColor = true;
            this.navPhanQuyen.Caption = "Phân quyền";
            this.navPhanQuyen.Name = "navPhanQuyen";
            this.navPhanQuyen.SmallImageIndex = 2;
            // 
            // menuDanhMuc
            // 
            this.menuDanhMuc.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.menuDanhMuc.Appearance.Options.UseForeColor = true;
            this.menuDanhMuc.Caption = "Danh mục";
            this.menuDanhMuc.Expanded = true;
            this.menuDanhMuc.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navDuAn),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navDTSD),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navDVSD),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navKho),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navLyDoNhapKho),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navNguonGoc),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navNhomCCLD),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navTinhTrang)});
            this.menuDanhMuc.Name = "menuDanhMuc";
            // 
            // navDuAn
            // 
            this.navDuAn.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navDuAn.Appearance.Options.UseForeColor = true;
            this.navDuAn.Caption = "Dự án";
            this.navDuAn.Name = "navDuAn";
            this.navDuAn.SmallImageIndex = 14;
            this.navDuAn.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navDuAn_LinkClicked);
            // 
            // navDTSD
            // 
            this.navDTSD.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navDTSD.Appearance.Options.UseForeColor = true;
            this.navDTSD.Caption = "Đối tượng sử dụng";
            this.navDTSD.Name = "navDTSD";
            this.navDTSD.SmallImageIndex = 15;
            this.navDTSD.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navDTSD_LinkClicked);
            // 
            // navDVSD
            // 
            this.navDVSD.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navDVSD.Appearance.Options.UseForeColor = true;
            this.navDVSD.Caption = "Đơn vị sử dụng";
            this.navDVSD.Name = "navDVSD";
            this.navDVSD.SmallImageIndex = 16;
            this.navDVSD.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navDVSD_LinkClicked);
            // 
            // navKho
            // 
            this.navKho.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navKho.Appearance.Options.UseForeColor = true;
            this.navKho.Caption = "Kho";
            this.navKho.Name = "navKho";
            this.navKho.SmallImageIndex = 17;
            this.navKho.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navKho_LinkClicked);
            // 
            // navLyDoNhapKho
            // 
            this.navLyDoNhapKho.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navLyDoNhapKho.Appearance.Options.UseForeColor = true;
            this.navLyDoNhapKho.Caption = "Lý do nhập kho";
            this.navLyDoNhapKho.Name = "navLyDoNhapKho";
            this.navLyDoNhapKho.SmallImageIndex = 18;
            this.navLyDoNhapKho.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navLyDoNhapKho_LinkClicked);
            // 
            // navNguonGoc
            // 
            this.navNguonGoc.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navNguonGoc.Appearance.Options.UseForeColor = true;
            this.navNguonGoc.Caption = "Nguồn gốc";
            this.navNguonGoc.Name = "navNguonGoc";
            this.navNguonGoc.SmallImageIndex = 13;
            this.navNguonGoc.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navNguonGoc_LinkClicked);
            // 
            // navNhomCCLD
            // 
            this.navNhomCCLD.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navNhomCCLD.Appearance.Options.UseForeColor = true;
            this.navNhomCCLD.Caption = "Nhóm CCLĐ";
            this.navNhomCCLD.Name = "navNhomCCLD";
            this.navNhomCCLD.SmallImageIndex = 3;
            this.navNhomCCLD.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navNhomCCLD_LinkClicked);
            // 
            // navTinhTrang
            // 
            this.navTinhTrang.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navTinhTrang.Appearance.Options.UseForeColor = true;
            this.navTinhTrang.Caption = "Tình trạng";
            this.navTinhTrang.Name = "navTinhTrang";
            this.navTinhTrang.SmallImageIndex = 19;
            this.navTinhTrang.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navTinhTrang_LinkClicked);
            // 
            // menuCCLD
            // 
            this.menuCCLD.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.menuCCLD.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.menuCCLD.Appearance.Options.UseFont = true;
            this.menuCCLD.Appearance.Options.UseForeColor = true;
            this.menuCCLD.AppearanceBackground.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.menuCCLD.AppearanceBackground.Options.UseFont = true;
            this.menuCCLD.AppearancePressed.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.menuCCLD.AppearancePressed.Options.UseFont = true;
            this.menuCCLD.Caption = "Công cụ lao động";
            this.menuCCLD.Expanded = true;
            this.menuCCLD.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navTangCCLD),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navNhapKho),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navXuatKho),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navBanGiao),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navDieuChuyenCungDVSD),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navDieuChuyenKhacDVSD),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navThanhLy),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navKiemKe),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navTimKiem)});
            this.menuCCLD.Name = "menuCCLD";
            // 
            // navTangCCLD
            // 
            this.navTangCCLD.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navTangCCLD.Appearance.Options.UseForeColor = true;
            this.navTangCCLD.Caption = "Tăng CCLĐ";
            this.navTangCCLD.Name = "navTangCCLD";
            this.navTangCCLD.SmallImageIndex = 4;
            this.navTangCCLD.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navTangCCLD_LinkClicked);
            // 
            // navNhapKho
            // 
            this.navNhapKho.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navNhapKho.Appearance.Options.UseForeColor = true;
            this.navNhapKho.Caption = "Nhập kho";
            this.navNhapKho.Name = "navNhapKho";
            this.navNhapKho.SmallImageIndex = 6;
            this.navNhapKho.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navNhapKho_LinkClicked);
            // 
            // navXuatKho
            // 
            this.navXuatKho.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navXuatKho.Appearance.Options.UseForeColor = true;
            this.navXuatKho.Caption = "Xuất kho";
            this.navXuatKho.Name = "navXuatKho";
            this.navXuatKho.SmallImageIndex = 5;
            this.navXuatKho.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navXuatKho_LinkClicked);
            // 
            // navBanGiao
            // 
            this.navBanGiao.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navBanGiao.Appearance.Options.UseForeColor = true;
            this.navBanGiao.Caption = "Bàn giao CCLĐ";
            this.navBanGiao.Name = "navBanGiao";
            this.navBanGiao.SmallImageIndex = 7;
            this.navBanGiao.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navBanGiao_LinkClicked);
            // 
            // navDieuChuyenCungDVSD
            // 
            this.navDieuChuyenCungDVSD.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navDieuChuyenCungDVSD.Appearance.Options.UseForeColor = true;
            this.navDieuChuyenCungDVSD.Caption = "Điều chuyển cùng ĐVSD";
            this.navDieuChuyenCungDVSD.Name = "navDieuChuyenCungDVSD";
            this.navDieuChuyenCungDVSD.SmallImageIndex = 8;
            this.navDieuChuyenCungDVSD.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navDieuChuyenCungDVSD_LinkClicked);
            // 
            // navDieuChuyenKhacDVSD
            // 
            this.navDieuChuyenKhacDVSD.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navDieuChuyenKhacDVSD.Appearance.Options.UseForeColor = true;
            this.navDieuChuyenKhacDVSD.Caption = "Điều chuyển khác ĐVSD";
            this.navDieuChuyenKhacDVSD.Name = "navDieuChuyenKhacDVSD";
            this.navDieuChuyenKhacDVSD.SmallImageIndex = 8;
            this.navDieuChuyenKhacDVSD.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navDieuChuyenKhacDVSD_LinkClicked);
            // 
            // navThanhLy
            // 
            this.navThanhLy.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navThanhLy.Appearance.Options.UseForeColor = true;
            this.navThanhLy.Caption = "Thanh lý";
            this.navThanhLy.Name = "navThanhLy";
            this.navThanhLy.SmallImageIndex = 10;
            this.navThanhLy.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navThanhLy_LinkClicked);
            // 
            // navKiemKe
            // 
            this.navKiemKe.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navKiemKe.Appearance.Options.UseForeColor = true;
            this.navKiemKe.Caption = "Kiểm kê";
            this.navKiemKe.Name = "navKiemKe";
            this.navKiemKe.SmallImageIndex = 11;
            this.navKiemKe.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navKiemKe_LinkClicked);
            // 
            // navTimKiem
            // 
            this.navTimKiem.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navTimKiem.Appearance.Options.UseForeColor = true;
            this.navTimKiem.Caption = "Tra cứu CCLĐ";
            this.navTimKiem.Name = "navTimKiem";
            this.navTimKiem.SmallImageIndex = 20;
            this.navTimKiem.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navTimKiem_LinkClicked);
            // 
            // menuBaoCao
            // 
            this.menuBaoCao.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.menuBaoCao.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.menuBaoCao.Appearance.Options.UseFont = true;
            this.menuBaoCao.Appearance.Options.UseForeColor = true;
            this.menuBaoCao.Caption = "Báo cáo";
            this.menuBaoCao.Expanded = true;
            this.menuBaoCao.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navDanhSach)});
            this.menuBaoCao.Name = "menuBaoCao";
            // 
            // navDanhSach
            // 
            this.navDanhSach.Appearance.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.navDanhSach.Appearance.Options.UseForeColor = true;
            this.navDanhSach.Caption = "Danh sách báo cáo";
            this.navDanhSach.Name = "navDanhSach";
            this.navDanhSach.SmallImageIndex = 12;
            this.navDanhSach.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navDanhSach_LinkClicked);
            // 
            // imgButton
            // 
            this.imgButton.ImageSize = new System.Drawing.Size(24, 24);
            this.imgButton.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imgButton.ImageStream")));
            this.imgButton.Images.SetKeyName(0, "user-32x32.png");
            this.imgButton.Images.SetKeyName(1, "task-32x32.png");
            this.imgButton.Images.SetKeyName(2, "position-32x32.png");
            this.imgButton.Images.SetKeyName(3, "toolbox.png");
            this.imgButton.Images.SetKeyName(4, "basket_add.png");
            this.imgButton.Images.SetKeyName(5, "document_export.png");
            this.imgButton.Images.SetKeyName(6, "document_import.png");
            this.imgButton.Images.SetKeyName(7, "user_go.png");
            this.imgButton.Images.SetKeyName(8, "refresh-32x32.png");
            this.imgButton.Images.SetKeyName(9, "calculator_edit.png");
            this.imgButton.Images.SetKeyName(10, "document_move.png");
            this.imgButton.Images.SetKeyName(11, "document_prepare.png");
            this.imgButton.Images.SetKeyName(12, "report.png");
            this.imgButton.Images.SetKeyName(13, "shopping_cart_reset.png");
            this.imgButton.Images.SetKeyName(14, "folder.png");
            this.imgButton.Images.SetKeyName(15, "user.png");
            this.imgButton.Images.SetKeyName(16, "office_apps.png");
            this.imgButton.Images.SetKeyName(17, "storage.png");
            this.imgButton.Images.SetKeyName(18, "document_prepare.png");
            this.imgButton.Images.SetKeyName(19, "statistics.png");
            this.imgButton.Images.SetKeyName(20, "search.png");
            // 
            // barSubItem4
            // 
            this.barSubItem4.Caption = "Thêm";
            this.barSubItem4.Id = 5;
            this.barSubItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem6)});
            this.barSubItem4.Name = "barSubItem4";
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "Nhân viên";
            this.barButtonItem4.Id = 8;
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Quyết định";
            this.barButtonItem5.Id = 9;
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Sự kiện";
            this.barButtonItem6.Id = 10;
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barSubItem5
            // 
            this.barSubItem5.Caption = "Giao diện";
            this.barSubItem5.Id = 14;
            this.barSubItem5.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.menuItemLiquidSky)});
            this.barSubItem5.Name = "barSubItem5";
            // 
            // menuItemLiquidSky
            // 
            this.menuItemLiquidSky.Caption = "Liquid Sky";
            this.menuItemLiquidSky.Id = 15;
            this.menuItemLiquidSky.Name = "menuItemLiquidSky";
            this.menuItemLiquidSky.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.SkinMenuClickEvent);
            // 
            // barBttDangXuat
            // 
            this.barBttDangXuat.Caption = "Đăng xuất";
            this.barBttDangXuat.Id = 17;
            this.barBttDangXuat.Name = "barBttDangXuat";
            this.barBttDangXuat.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barBttDangXuat_ItemClick);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 324);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(494, 22);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // TenThanhPho
            // 
            this.TenThanhPho.Caption = "Thành phố";
            this.TenThanhPho.FieldName = "TenThanhPho";
            this.TenThanhPho.Name = "TenThanhPho";
            this.TenThanhPho.Visible = true;
            this.TenThanhPho.VisibleIndex = 0;
            // 
            // idDatNuoc
            // 
            this.idDatNuoc.Caption = "idDatNuoc";
            this.idDatNuoc.FieldName = "idDatNuoc";
            this.idDatNuoc.Name = "idDatNuoc";
            // 
            // idThanhPho
            // 
            this.idThanhPho.Caption = "idThanhPho";
            this.idThanhPho.FieldName = "idThanhPho";
            this.idThanhPho.Name = "idThanhPho";
            // 
            // TenDatNuoc
            // 
            this.TenDatNuoc.Caption = "Đất nước";
            this.TenDatNuoc.FieldName = "TenDatNuoc";
            this.TenDatNuoc.Name = "TenDatNuoc";
            this.TenDatNuoc.Visible = true;
            this.TenDatNuoc.VisibleIndex = 1;
            // 
            // gridView1
            // 
            this.gridView1.Name = "gridView1";
            // 
            // xtraTabControl2
            // 
            this.xtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl2.Location = new System.Drawing.Point(3, 3);
            this.xtraTabControl2.Name = "xtraTabControl2";
            this.xtraTabControl2.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl2.Size = new System.Drawing.Size(498, 380);
            this.xtraTabControl2.TabIndex = 0;
            this.xtraTabControl2.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.gridControl1);
            this.xtraTabPage2.Controls.Add(this.labelControl3);
            this.xtraTabPage2.Controls.Add(this.labelControl2);
            this.xtraTabPage2.Controls.Add(this.labelControl1);
            this.xtraTabPage2.Controls.Add(this.comboBoxEdit6);
            this.xtraTabPage2.Controls.Add(this.comboBoxEdit5);
            this.xtraTabPage2.Controls.Add(this.comboBoxEdit4);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(489, 349);
            this.xtraTabPage2.Text = "xtraTabPage2";
            // 
            // gridControl1
            // 
            this.gridControl1.EmbeddedNavigator.UseWaitCursor = true;
            this.gridControl1.Location = new System.Drawing.Point(22, 105);
            this.gridControl1.MainView = this.gridView2;
            this.gridControl1.MenuManager = this.barManager1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(400, 198);
            this.gridControl1.TabIndex = 13;
            this.gridControl1.UseWaitCursor = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn1});
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "stt";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(367, 44);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(79, 13);
            this.labelControl3.TabIndex = 12;
            this.labelControl3.Text = "Khu vực bầu cử:";
            this.labelControl3.UseWaitCursor = true;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(188, 44);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(71, 13);
            this.labelControl2.TabIndex = 11;
            this.labelControl2.Text = "Đơn vị bầu cử:";
            this.labelControl2.UseWaitCursor = true;
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(22, 44);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(43, 13);
            this.labelControl1.TabIndex = 10;
            this.labelControl1.Text = "Tỉnh, TP:";
            this.labelControl1.UseWaitCursor = true;
            // 
            // comboBoxEdit6
            // 
            this.comboBoxEdit6.Enabled = false;
            this.comboBoxEdit6.Location = new System.Drawing.Point(367, 63);
            this.comboBoxEdit6.MenuManager = this.barManager1;
            this.comboBoxEdit6.Name = "comboBoxEdit6";
            this.comboBoxEdit6.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit6.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit6.TabIndex = 9;
            this.comboBoxEdit6.UseWaitCursor = true;
            this.comboBoxEdit6.Visible = false;
            // 
            // comboBoxEdit5
            // 
            this.comboBoxEdit5.Location = new System.Drawing.Point(188, 63);
            this.comboBoxEdit5.MenuManager = this.barManager1;
            this.comboBoxEdit5.Name = "comboBoxEdit5";
            this.comboBoxEdit5.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit5.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit5.TabIndex = 8;
            this.comboBoxEdit5.UseWaitCursor = true;
            // 
            // comboBoxEdit4
            // 
            this.comboBoxEdit4.EditValue = "Hà Nội";
            this.comboBoxEdit4.Location = new System.Drawing.Point(22, 63);
            this.comboBoxEdit4.MenuManager = this.barManager1;
            this.comboBoxEdit4.Name = "comboBoxEdit4";
            this.comboBoxEdit4.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEdit4.Size = new System.Drawing.Size(100, 20);
            this.comboBoxEdit4.TabIndex = 7;
            this.comboBoxEdit4.UseWaitCursor = true;
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(498, 358);
            // 
            // emptySpaceItem13
            // 
            this.emptySpaceItem13.CustomizationFormText = "emptySpaceItem13";
            this.emptySpaceItem13.Location = new System.Drawing.Point(0, 62);
            this.emptySpaceItem13.Name = "emptySpaceItem13";
            this.emptySpaceItem13.Size = new System.Drawing.Size(121, 343);
            this.emptySpaceItem13.Text = "emptySpaceItem13";
            this.emptySpaceItem13.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem16
            // 
            this.emptySpaceItem16.CustomizationFormText = "emptySpaceItem16";
            this.emptySpaceItem16.Location = new System.Drawing.Point(408, 62);
            this.emptySpaceItem16.Name = "emptySpaceItem16";
            this.emptySpaceItem16.Size = new System.Drawing.Size(121, 343);
            this.emptySpaceItem16.Text = "emptySpaceItem16";
            this.emptySpaceItem16.TextSize = new System.Drawing.Size(0, 0);
            // 
            // panelControl1
            // 
            this.panelControl1.Controls.Add(this.tabChild);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelControl1.Location = new System.Drawing.Point(200, 24);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(510, 532);
            this.panelControl1.TabIndex = 18;
            // 
            // tabChild
            // 
            this.tabChild.AppearancePage.Header.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.tabChild.AppearancePage.Header.Options.UseFont = true;
            this.tabChild.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InAllTabPageHeaders;
            this.tabChild.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabChild.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.tabChild.Location = new System.Drawing.Point(2, 2);
            this.tabChild.Name = "tabChild";
            this.tabChild.SelectedTabPage = this.xtratabChildMain;
            this.tabChild.Size = new System.Drawing.Size(506, 528);
            this.tabChild.TabIndex = 1;
            this.tabChild.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtratabChildMain});
            this.tabChild.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.tabChild_SelectedPageChanged);
            this.tabChild.CloseButtonClick += new System.EventHandler(this.tabChild_CloseButtonClick);
            // 
            // xtratabChildMain
            // 
            this.xtratabChildMain.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.xtratabChildMain.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.xtratabChildMain.Name = "xtratabChildMain";
            this.xtratabChildMain.ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            this.xtratabChildMain.Size = new System.Drawing.Size(499, 499);
            this.xtratabChildMain.Text = "QUẢN LÝ CCLĐ";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 578);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.dockQLCCLD);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.Text = "Quản lý Công cụ lao động";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dockManager1)).EndInit();
            this.dockPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dockManager2)).EndInit();
            this.dockQLCCLD.ResumeLayout(false);
            this.dockPanel1_Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl2)).EndInit();
            this.xtraTabControl2.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit6.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit5.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEdit4.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabChild)).EndInit();
            this.tabChild.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void MyInit()
        {
            //this.tabDSungcuvien=new BCQH.UngCuVien.frmDanhSachUCV();
            //this.tabDSungcuvien_cb = new BCQH.UngCuVien.frmDanhSachUCV_CB();
            //this.tabThongtincoban = new BCQH.UngCuVien.frmThongTinNK();
            //this.TabKetquasolieu = new BCQH.KetQuaSoLieu.KetQuaHiepThuong.frmKetQuaHiepThuong();
            //this.tabTKCoCauTinhThanh = new BCQH.ThongKeSoLieu.From.frmThongKeCoCauTinhThanh();

            //tabTKCoCauTinhThanh.Dock = System.Windows.Forms.DockStyle.Fill;
            //TabKetquasolieu.Dock = System.Windows.Forms.DockStyle.Fill;
            //tabThongtincoban.Dock=System.Windows.Forms.DockStyle.Fill;
            //tabDSungcuvien.Dock=System.Windows.Forms.DockStyle.Fill;
            //tabDSungcuvien_cb.Dock = System.Windows.Forms.DockStyle.Fill;
        }
        #endregion


        //private BCQH.UngCuVien.frmDanhSachUCV tabDSungcuvien;
        //private BCQH.UngCuVien.frmDanhSachUCV_CB tabDSungcuvien_cb;
        //private BCQH.UngCuVien.frmThongTinNK tabThongtincoban;
        //public BCQH.KetQuaSoLieu.KetQuaHiepThuong.frmKetQuaHiepThuong TabKetquasolieu;
        //private BCQH.ThongKeSoLieu.From.frmThongKeCoCauTinhThanh tabTKCoCauTinhThanh;


        private DevExpress.XtraBars.Docking.DockManager dockManager1;
        private DevExpress.XtraBars.Docking.DockPanel dockPanel2;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel2_Container;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraBars.Bar bar2;
        private DevExpress.XtraBars.BarSubItem barSubItem1;
        private DevExpress.XtraBars.BarSubItem barSubItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.BarSubItem barSubItem2;
        private DevExpress.XtraBars.BarSubItem barSubItem3;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraBars.BarButtonItem barButtonItem9;
        private DevExpress.XtraBars.BarSubItem barSubItem5;
        private DevExpress.XtraBars.BarButtonItem menuItemLiquidSky;
        private DevExpress.XtraBars.BarButtonItem barBttDangXuat;
        private DevExpress.XtraBars.Docking.DockManager dockManager2;
        private DevExpress.XtraBars.Docking.DockPanel dockQLCCLD;
        private DevExpress.XtraBars.Docking.ControlContainer dockPanel1_Container;
        private DevExpress.XtraNavBar.NavBarControl nbMain;
        private DevExpress.XtraNavBar.NavBarGroup menuQTHT;
        private DevExpress.XtraNavBar.NavBarGroup menuCCLD;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraGrid.Columns.GridColumn TenThanhPho;
        private DevExpress.XtraGrid.Columns.GridColumn idDatNuoc;
        private DevExpress.XtraGrid.Columns.GridColumn idThanhPho;
        private DevExpress.XtraGrid.Columns.GridColumn TenDatNuoc;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.Utils.ImageCollection imgButton;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit6;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit5;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEdit4;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraNavBar.NavBarItem navTangCCLD;
        private DevExpress.XtraNavBar.NavBarItem navQuyen;
        private DevExpress.XtraNavBar.NavBarItem navPhanQuyen;
        private DevExpress.XtraNavBar.NavBarItem navQLTK;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem13;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem16;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraTab.XtraTabControl tabChild;
        private DevExpress.XtraTab.XtraTabPage xtratabChildMain;
        private DevExpress.XtraNavBar.NavBarItem navNhapKho;
        private DevExpress.XtraNavBar.NavBarItem navXuatKho;
        private DevExpress.XtraNavBar.NavBarGroup menuBaoCao;
        private DevExpress.XtraNavBar.NavBarItem navDanhSach;
        private DevExpress.XtraNavBar.NavBarItem navBanGiao;
        private DevExpress.XtraNavBar.NavBarItem navDieuChuyenCungDVSD;
        private DevExpress.XtraNavBar.NavBarItem navThanhLy;
        private DevExpress.XtraNavBar.NavBarItem navKiemKe;
        private DevExpress.XtraNavBar.NavBarGroup menuDanhMuc;
        private DevExpress.XtraNavBar.NavBarItem navNhomCCLD;
        private DevExpress.XtraNavBar.NavBarItem navNguonGoc;
        private DevExpress.XtraNavBar.NavBarItem navDVSD;
        private DevExpress.XtraNavBar.NavBarItem navTinhTrang;
        private DevExpress.XtraNavBar.NavBarItem navDuAn;
        private DevExpress.XtraNavBar.NavBarItem navLyDoNhapKho;
        private DevExpress.XtraNavBar.NavBarItem navKho;
        private DevExpress.XtraNavBar.NavBarItem navDTSD;
        private DevExpress.XtraNavBar.NavBarItem navDieuChuyenKhacDVSD;
        private DevExpress.XtraNavBar.NavBarItem navTimKiem;
    }
}