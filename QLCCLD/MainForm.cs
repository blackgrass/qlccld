﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraNavBar;
using QLCCLD.CCLD.Tang;
using QLCCLD.CCLD.NhapKho;
using QLCCLD.DanhMuc.NhomCCLD;
using QLCCLD.DanhMuc.NguonGoc;
using QLCCLD.DanhMuc.QuocGia;
using QLCCLD.DanhMuc.DVSD;
using QLCCLD.DanhMuc.TinhTrang;
using QLCCLD.DanhMuc.DuAn;
using QLCCLD.DanhMuc.LyDoNhapKho;
using QLCCLD.DanhMuc.Kho;
using QLCCLD.CCLD.XuatKho;
using QLCCLD.CCLD.BanGiao;
using QLCCLD.DanhMuc.DTSD;
using QLCCLD.CCLD.DieuChuyenCungDVSD;
using QLCCLD.CCLD.DieuChuyenKhacDVSD;
using QLCCLD.CCLD.ThanhLy;
using QLCCLD.CCLD.KiemKe;
using QLCCLD.CCLD.TimKiem;
using QLCCLD.CCLD.BaoCao;

namespace QLCCLD
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            frmLogin frm = new frmLogin();
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK)
            {
                this.WindowState = FormWindowState.Maximized;
                ucGioiThieu gt = new ucGioiThieu();
                gt.Dock = DockStyle.Fill;
                xtratabChildMain.Text = "Giới thiệu";
                tabChild.SelectedTabPage.Controls.Add(gt);
            }
        }

        private void navQLTK_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            //ucQLTK qltk = new ucQLTK();
            //qltk.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();
            
            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "QLTK")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    //currentPage.Controls.Add(qltk);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "QLTK";
            tabChild.SelectedTabPage = t;
            t.Text = "Quản lý tài khoản";
            //t.Controls.Add(qltk);

        }            

        private void barBttDoiMatKhau_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //DoiMatKhau frm = new DoiMatKhau();
            //frm.ShowDialog();
        }

        private void barBttDangXuat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (DialogResult.Yes == XtraMessageBox.Show("Bạn có muốn Đăng Xuất khỏi chương trình ?", "Xác Nhận", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                
                Application.Restart();
            }
        }

        private void barBttThoat_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Application.Exit();
        }

        private void SkinMenuClickEvent(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DevExpress.LookAndFeel.UserLookAndFeel.Default.SkinName = e.Item.Caption;
        }

        private void tabChild_CloseButtonClick(object sender, EventArgs e)
        {
            XtraTabControl t = sender as XtraTabControl;
            ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;
            t.TabPages.Remove(arg.Page as XtraTabPage);
        }

        private void tabChild_SelectedPageChanged(object sender, TabPageChangedEventArgs e)
        {

        }

        private void navTangCCLD_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucTang tang = new ucTang();
            tang.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "TANG")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(tang);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "TANG";
            tabChild.SelectedTabPage = t;
            t.Text = "Tăng CCLĐ";
            t.Controls.Add(tang);
        }

        private void navNhomCCLD_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucNhomCCLD nhom = new ucNhomCCLD();
            nhom.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "NHOMCCLD")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(nhom);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "NHOMCCLD";
            tabChild.SelectedTabPage = t;
            t.Text = "Nhóm CCLĐ";
            t.Controls.Add(nhom);
        }

        private void navNguonGoc_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucNguonGoc nguongoc = new ucNguonGoc();
            nguongoc.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "NGUONGOC")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(nguongoc);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "NGUONGOC";
            tabChild.SelectedTabPage = t;
            t.Text = "Nguồn gốc";
            t.Controls.Add(nguongoc);
        }

        private void navQuocGia_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucQuocGia quocgia = new ucQuocGia();
            quocgia.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "QUOCGIA")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(quocgia);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "QUOCGIA";
            tabChild.SelectedTabPage = t;
            t.Text = "Quốc gia";
            t.Controls.Add(quocgia);
        }

        private void navDVSD_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucDVSD DVSD = new ucDVSD();
            DVSD.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "DVSD")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(DVSD);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "DVSD";
            tabChild.SelectedTabPage = t;
            t.Text = "Đơn vị sử dụng";
            t.Controls.Add(DVSD);
        }

        private void navTinhTrang_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucTinhTrang TinhTrang = new ucTinhTrang();
            TinhTrang.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "TinhTrang")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(TinhTrang);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "TinhTrang";
            tabChild.SelectedTabPage = t;
            t.Text = "Tình trạng";
            t.Controls.Add(TinhTrang);
        }

        private void navDuAn_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucDuAn DuAn = new ucDuAn();
            DuAn.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "DuAn")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(DuAn);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "DuAn";
            tabChild.SelectedTabPage = t;
            t.Text = "Dự án";
            t.Controls.Add(DuAn);
        }

        private void navLyDoNhapKho_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucLyDoNhapKho LyDo = new ucLyDoNhapKho();
            LyDo.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "LyDo")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(LyDo);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "LyDo";
            tabChild.SelectedTabPage = t;
            t.Text = "Lý do nhập kho";
            t.Controls.Add(LyDo);
        }

        private void navKho_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucKho Kho = new ucKho();
            Kho.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "Kho")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(Kho);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "Kho";
            tabChild.SelectedTabPage = t;
            t.Text = "Kho";
            t.Controls.Add(Kho);
        }

        private void navNhapKho_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucNhapKho NhapKho = new ucNhapKho();
            NhapKho.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "NhapKho")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(NhapKho);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "NhapKho";
            tabChild.SelectedTabPage = t;
            t.Text = "Nhập kho";
            t.Controls.Add(NhapKho);
        }

        private void navXuatKho_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucXuatKho XuatKho = new ucXuatKho();
            XuatKho.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "XuatKho")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(XuatKho);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "XuatKho";
            tabChild.SelectedTabPage = t;
            t.Text = "Xuất kho";
            t.Controls.Add(XuatKho);
        }

        private void navBanGiao_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucBanGiao BanGiao = new ucBanGiao();
            BanGiao.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "BanGiao")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(BanGiao);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "BanGiao";
            tabChild.SelectedTabPage = t;
            t.Text = "Bàn giao";
            t.Controls.Add(BanGiao);
        }

        private void navDTSD_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucDTSD DTSD = new ucDTSD();
            DTSD.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "DTSD")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(DTSD);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "DTSD";
            tabChild.SelectedTabPage = t;
            t.Text = "Đối tượng sử dụng";
            t.Controls.Add(DTSD);
        }

        private void navDieuChuyenCungDVSD_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucDieuChuyenCungDVSD DC = new ucDieuChuyenCungDVSD();
            DC.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "DC")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(DC);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "DC";
            tabChild.SelectedTabPage = t;
            t.Text = "Điều chuyển cùng ĐVSD";
            t.Controls.Add(DC);
        }

        private void navDieuChuyenKhacDVSD_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucDieuChuyenKhacDVSD DC = new ucDieuChuyenKhacDVSD();
            DC.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "DC2")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(DC);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "DC2";
            tabChild.SelectedTabPage = t;
            t.Text = "Điều chuyển khác ĐVSD";
            t.Controls.Add(DC);
        }

        private void navThanhLy_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucThanhLy TL = new ucThanhLy();
            TL.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "ThanhLy")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(TL);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "ThanhLy";
            tabChild.SelectedTabPage = t;
            t.Text = "Thanh lý";
            t.Controls.Add(TL);
        }

        private void navKiemKe_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucKiemKe KK = new ucKiemKe();
            KK.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "KiemKe")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(KK);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "KiemKe";
            tabChild.SelectedTabPage = t;
            t.Text = "Kiểm kê";
            t.Controls.Add(KK);
        }

        private void navTimKiem_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucTimKiem TK = new ucTimKiem();
            TK.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "TimKiem")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(TK);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "TimKiem";
            tabChild.SelectedTabPage = t;
            t.Text = "Tra cứu CCLĐ";
            t.Controls.Add(TK);
        }

        private void navDanhSach_LinkClicked(object sender, NavBarLinkEventArgs e)
        {
            ucBaoCao BC = new ucBaoCao();
            BC.Dock = DockStyle.Fill;
            XtraTabPage t = new XtraTabPage();

            foreach (XtraTabPage currentPage in tabChild.TabPages)
            {
                if (currentPage.Name == "BaoCao")
                {
                    tabChild.SelectedTabPage = currentPage;
                    currentPage.Controls.Clear();
                    currentPage.Controls.Add(BC);
                    return;
                }

            }
            tabChild.TabPages.Add(t);
            t.Name = "BaoCao";
            tabChild.SelectedTabPage = t;
            t.Text = "Báo cáo";
            t.Controls.Add(BC);
        }

        //private void barButtonItem7_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        //{
        //    frmThongTin f = new frmThongTin();
        //    f.StartPosition = FormStartPosition.CenterParent;
        //    f.ShowDialog();
        //}
    }
}